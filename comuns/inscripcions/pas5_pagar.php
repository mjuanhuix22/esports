
<?php

// Se incluye la librería
include 'apiRedsys.php';

// Se crea Objeto
$miObj = new RedsysAPI;

//$amount 		="001";
$amount=$import*100;
	
// Valores de entrada
$merchantCode 	="010675262";
$terminal 		="1";

$currency 		="978"; //moneda
$transactionType="0";

$merchantURL 	="http://www.selvaesports.cat";
$urlOK			="http://www.selvaesports.cat/comuns/inscripcions/pas6_confirmar.php?id_ins=".$id_ins."&id_reserva=".$id_reserva."&confirmat=1&order=".$order;
$urlKO 			="http://www.selvaesports.cat/comuns/inscripcions/pas6_confirmar.php?id_ins=".$id_ins."&id_reserva=".$id_reserva."&confirmat=0&order=".$order;;
$order 			=time();

//Entorno
$urlPago = "https://sis.redsys.es/sis/realizarPago"; //ENTORNO REAL
//$urlPago = "https://sis-t.redsys.es:25443/sis/realizarPago"; //ENTORNO DE PRUEBAS

// Se Rellenan los campos
$miObj->setParameter("DS_MERCHANT_AMOUNT",$amount);
$miObj->setParameter("DS_MERCHANT_ORDER",strval($order));
$miObj->setParameter("DS_MERCHANT_MERCHANTCODE",$merchantCode);
$miObj->setParameter("DS_MERCHANT_CURRENCY",$currency);
$miObj->setParameter("DS_MERCHANT_TRANSACTIONTYPE",$transactionType);
$miObj->setParameter("DS_MERCHANT_TERMINAL",$terminal);
$miObj->setParameter("DS_MERCHANT_MERCHANTURL",$merchantURL);
$miObj->setParameter("DS_MERCHANT_URLOK",$urlOK);	
$miObj->setParameter("DS_MERCHANT_URLKO",$urlKO);

//Datos de configuración
$version="HMAC_SHA256_V1";

$key="2ilgA7m5BpaeHaKLJaAw99/GF2pVx9B0";

// Se generan los parámetros de la petición
$request = "";
$params = $miObj->createMerchantParameters();
$signature = $miObj->createMerchantSignature($key);



if($fI["trans"]==1){
?>




<p class="lletra_verd">Hem rebut la vosta pre-inscripció. Per validar-la cal que feu l'ingrés de <strong><?php echo $filaR["import"];?>&#8364;.<br>
    Feu clic a una de les dos opcions:</strong></p>

<fieldset class="fiel_paga">
    <legend class="legend_paga">Opció 1</legend>
    <form name="frm" action=" <?php echo  $urlPago ?>" method="POST" target="_blank">
        <input type="submit" name="pagar" value="Pagament amb targeta"  class="boto_pagament">
        <input type="hidden" name="Ds_SignatureVersion" value="<?php echo  $version; ?>"/></br>
        <input type="hidden" name="Ds_MerchantParameters" value="<?php echo  $params; ?>"/></br>
        <input type="hidden" name="Ds_Signature" value="<?php echo  $signature; ?>" /></br>
    </form>

</fieldset>

<fieldset class="fiel_paga">
    <legend class="legend_paga">Opció 2</legend>
    Fer una transferència al compte corrent <strong>ES35 0081 0162 19 0001243525</strong> del Banc Sabadell i fer arribar un comprovant a <strong>info@selvaesports.cat</strong> indicant la referència <strong><?php echo  $filaR["refer"];?></strong>

    <form method="post">
       <p><input type="submit" name="transfer" value="Transferència" class="boto_pagament"></p>
        <?php if(isset($_SESSION["id_entidad"])){?>
            <?php include("comuns.php");?>
            <input type="hidden" name="id_ins" value="<?php echo  $id_ins;?>" />

        <?php }?>
        <input type="hidden" name="id_reserva" value="<?php echo  $id_reserva;?>" />

    </form>

</fieldset>

<?php }else{?>


    <p class="lletra_verd">Ara cal que feu l'ingrés de <strong><?php echo $filaR["import"];?>&#8364; :<br>
         </strong></p>

    <form name="frm" action=" <?php echo  $urlPago ?>" method="POST" target="_blank">
        <input type="submit" name="pagar" value="Pagament amb targeta"  class="boto_pagament">
        <input type="hidden" name="Ds_SignatureVersion" value="<?php echo  $version; ?>"/></br>
        <input type="hidden" name="Ds_MerchantParameters" value="<?php echo  $params; ?>"/></br>
        <input type="hidden" name="Ds_Signature" value="<?php echo  $signature; ?>" /></br>
    </form>

<?php } ?>