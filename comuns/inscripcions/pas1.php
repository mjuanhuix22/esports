<link rel="stylesheet" href="comuns/inscripcions/css/jquery-ui.css" />
<link rel="stylesheet" href="comuns/inscripcions/css/estilo.css" />
<script src="comuns/inscripcions/js/jquery-1.8.2.js"></script>
<script src="comuns/inscripcions/js/jquery-ui.js"></script>
<script>
    $(function() {

        $( "#Buscar" ).autocomplete({
            source: "comuns/inscripcions/buscar.php",
            select: function(event, ui) {
                $('#id_entidad').val(ui.item.id_entidad);
            }
        });
    });

</script>




<form method="post" name="form_inscripcio" action="#inscripcio">


<table border="0">
	<?php if(!isset($_SESSION["id_entidad"])){?>
  <tr>
    <td class="lletraGrisa">Entitat</td>
    <td>
    <?php 
	if($fI["sol_entitat"]==1){/*?>
    <select name="id_entidad">
      <option value="328" <?php 
	  if((isset($_POST["id_entidad"]))&&($_POST["id_entidad"]==328)){?> selected="selected"<?php }?>>INDEPENDENTS</option>
      <?php $con=mysqli_query($cnx_cesportiu,"select nom_entidad,entidades.id_entidad 
		from entidades,entidades_deportes,i1_inscripcions,a1_activitats where 
		entidades.id_entidad!=1 and 
		entidades.estat=1 and 
		entidades.id_entidad=entidades_deportes.id_entidad and 
		entidades_deportes.id_deporte=a1_activitats.id_deporte and 
		a1_activitats.id_act=i1_inscripcions.id_act and 
		i1_inscripcions.id_ins=".$id_ins." and entidades.id_entidad!=328 
		order by nom_entidad");
		while($fila=mysqli_fetch_array($con)){?>
      <option value="<?php echo  $fila["id_entidad"]?>" 
            <?php if((isset($_POST["id_entidad"]))&&($_POST["id_entidad"]==$fila["id_entidad"])){?> selected="selected"<?php }?>><?php echo   stripslashes($fila["nom_entidad"]);?></option>
      <?php }?>
    </select>
    <?php */?>
        <input type="text" class="caja"  name="Buscar" id="Buscar"  >
        <input type="hidden" name="id_entidad" id="id_entidad">

	<?php }else{?>
    	<input type="text" name="nom_entitat" value="" size="40" />(Opcional)
        <input type="hidden" name="id_entidad" value="500" />
    <?php }?>
    </td>
  </tr>
  <?php }else{?><input type="hidden" name="id_entidad" value="<?php echo  $_SESSION["id_entidad"];?>"><?php }

 ?>
  <tr>
    <td class="lletraGrisa">Mail de contacte</td>
    <td><input type="text" name="mail_contacte" value="" size="40"/></td>
  </tr>
   <?php if($fI["sol_entitat"]==1){?>
  <tr>
    <td class="lletraGrisa">Nom de contacte </td>
    <td><input type="text" name="nom_contacte" value="<?php if(isset($_POST["nom_contacte"])){echo $_POST["nom_contacte"];}?>"  size="30" /></td>
  </tr>
  <?php }?>
</table>


<style type="text/css">
.vora_esportista{
	border:#333 1px dotted;
	padding:5px;
	margin-top:10px;
	width:600px;
	
	clear:both;
}
</style>

<div id="llistat"></div>

<div class="vora_esportista" style="height:50px;">
Afegir nous esportistes:<br />

<?php if($fI["sol_DNI"]==1){?>
    DNI/NIE
    <input type="text" name="DNI" size="9" maxlength="9" id="DNI" />
<?php }else{?>
    Cat Salut
    <input type="text" name="NSS1" id="NSS1" size="4" maxlength="4" />
    <input type="text" name="NSS2" id="NSS2" size="1" maxlength="1" />
    
    <input type="text" name="NSS3" id="NSS3" size="6" maxlength="6" />
    <input type="text" name="NSS4" id="NSS4" size="2" maxlength="2" />
    <input type="text" name="NSS5" id="NSS5" size="1" maxlength="1" />
<?php }?>
<input type="hidden" name="total_llistat" id="total_llistat" value="0" size="1">

<input type="button" value="+" id="boto_mes"
       onClick="validar_pas2('<?php echo  $fI["sol_DNI"];?>','<?php echo  $id_ins;?>',<?php if(isset($_SESSION["id_entidad"])){ echo "1";}else{ echo "0";}?>);"
       class="visual_no">

    <input type="button" value="Continuar" id="boto_continuar"
           onClick="validar_pas2('<?php echo  $fI["sol_DNI"];?>','<?php echo  $id_ins;?>',<?php if(isset($_SESSION["id_entidad"])){ echo "1";}else{ echo "0";}?>);"
           class="boto_verd">
</div>


    <div  id="capa_proteccio" class="visual_no">
<br /><br />
    <textarea name="escrit" rows="10" cols="100" style="font-size:10px;" readonly="yes">El Consell Esportiu de la Selva l’informa que disposa d’un fitxer informatitzat dels participants de les nostres competicions (esportistes, entrenadors i delegats), així com un arxiu documental que conté dades personals que formen part de la BASE DE DADES GENERALS D’ADMINISTRACIÓ.
La finalitat de la seva creació és el tractament de les dades per tal de cobrir les necessitats de gestió de les nostres activitats i serveis.
El destinatari de la informació és el mateix Consell Esportiu, així com els estaments oficials que per llei demanin la cessió de les dades.La negativa a facilitar les dades demanades tindrà com a conseqüència la impossibilitat de participar en qualsevol activitat que s’organitzi en aquest Consell.
En tot cas, vostè pot exercir el dret d’oposició, accés, rectificació i cancel·lació en l’àmbit  reconegut per la Llei Orgànica 15/1999 de 13 desembre, mitjançant el que disposa el R.D. 1332/1994 de 20 de juny.
El responsable de la base de dades és el Consell Esportiu de la Selva. Per exercir els drets esmentats i per a qualsevol aclariment, es pot adreçar per escrit al mateix Consell Esportiu a info@selvaesports.cat o bé al Passeig de Sant Salvador, 25-27 | 17430 Santa Coloma de Farners.
     </textarea>
      <br />
     <input name="accepto"  type="checkbox" id="accepto"  value="1"  
     <?php if((isset($_POST["accepto"]))&&($_POST["accepto"]==1)){?>checked="checked"<?php }?>/>
    Accepto la pol&iacute;tica de privacitat del Consell Esportiu de la Selva
<p>
    <input name="guardar" type="submit" class="boto_pagament" id="guardar"
     onclick="borrar_anterior();return validar_inscrip('form_inscripcio','<?php echo  $fI["sol_federacio"];?>','<?php echo  $fI["sol_federacio_any"];?>');" value="<?php if($fI["preu"]==0){ echo "Enviar";}else{ echo "Validar";}?>" />

    </div>


     
     <?php if(isset($_SESSION["id_entidad"])){?>
     	<?php include("comuns.php");?>
         <input type="hidden" name="id_ins" value="<?php echo  $id_ins?>" />
     <?php }?>
    
    </p>
</form>