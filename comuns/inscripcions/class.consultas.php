<?php
/*
Autor: Manuel Cortes Crisanto
Nota: El codigo se puede realizar la modificacion.
Web: www.desarrollosphp.com
email:info@desarrollosphp.com
*/
require("clases/config.php");

class conectorDB extends config
{
	private $conexion;
	public function __construct(){
		$this->conexion = parent::conectar(); //creo una variable con la conexión
		return $this->conexion;										
	}
	
	public function EjecutarSentencia($consulta, $valores = array()){  //funcion principal, ejecuta todas las consultas
		$resultado = false;
		
		if($statement = $this->conexion->prepare($consulta)){  //prepara la consulta
			if(preg_match_all("/(:\w+)/", $consulta, $campo, PREG_PATTERN_ORDER)){ //tomo los nombres de los campos iniciados con :xxxxx
				$campo = array_pop($campo); //inserto en un arreglo
				foreach($campo as $parametro){
					$statement->bindValue($parametro, $valores[substr($parametro,1)]);
				}
			}
			try {
				if (!$statement->execute()) { //si no se ejecuta la consulta...
					print_r($statement->errorInfo()); //imprimir errores
					return false;
				}
				$resultado = $statement->fetchAll(PDO::FETCH_ASSOC); //si es una consulta que devuelve valores los guarda en un arreglo.
				$statement->closeCursor();
			}
			catch(PDOException $e){
				echo "Error de ejecución: \n";
				print_r($e->getMessage());
			}	
		}
		return $resultado;
		$this->conexion = null; //cerramos la conexión
	} /// Termina funcion consultarBD
}/// Termina clase conectorDB

class Json
{
	private $json;
	public function BuscaClientes($filtro,$id_ins){

/*
	    $consulta="select nom_entidad as label,entidades.id_entidad 
		from entidades,entidades_deportes,i1_inscripcions,a1_activitats where 
		entidades.id_entidad!=1 and 
		entidades.estat=1 and 
		entidades.id_entidad=entidades_deportes.id_entidad and 
		entidades_deportes.id_deporte=a1_activitats.id_deporte and 
		a1_activitats.id_act=i1_inscripcions.id_act and 
		i1_inscripcions.id_ins=".$id_ins." and 
		entidades.id_entidad!=328 and  
		nom_entidad LIKE '%".$filtro."%' 
		order by nom_entidad";
*/
	    //echo $consulta;

		$consulta = "SELECT nom_entidad as label,id_entidad FROM entidades WHERE
        entidades.id_entidad!=1 and  
		entidades.estat=1 and 
        nom_entidad LIKE '%".$filtro."%'";

		$conexion = new conectorDB;
		$this->json = $conexion->EjecutarSentencia($consulta);
		return $this->json;
	}
	
}/// TERMINA CLASE  ///
