<script language="javascript">

function borrar_anterior(){
	var valor_total=document.getElementById("total_llistat").value;
	if(valor_total!=0){
		for(var n=1;n<=valor_total;n++){
			
			if (eval('document.getElementById("text_error_'+n+'").className=="visual_si lletra_vermella"')){
				
				document.getElementById("total_llistat").value=document.getElementById("total_llistat").value-1;
				eval('document.getElementById("text_error_'+n+'").className="visual_no"');
			}
		}
	}else{
		document.getElementById("guardar").className="boto_verd visual_no";	
	}
}

function validar_pas2(sol_DNI,id_ins,intranet){

	//borrar_anterior();

    if((eval('window.document.form_inscripcio.id_entidad.value==""'))||
        (eval('window.document.form_inscripcio.mail_contacte.value==""'))){
        alert("Es obligatori omplir l'entitat");
        return (false);
    }
	
	var error=0;
	if(sol_DNI==1){
		var identifica=window.document.getElementById("DNI").value;
		var temp = identifica.toUpperCase();
		var cadenadni = "TRWAGMYFPDXBNJZSQVHLCKE";
		
		if( ( !/^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$/.test( temp ) && !/^[T]{1}[A-Z0-9]{8}$/.test( temp ) ) && !/^[0-9]{8}[A-Z]{1}$/.test( temp ) )
		{
			error=1;alert(" "+identifica+" incorrecte"); return false;
		}
	}else{
		var NSS1=window.document.getElementById("NSS1").value;
		var NSS2=window.document.getElementById("NSS2").value;
		var NSS3=window.document.getElementById("NSS3").value;
		var NSS4=window.document.getElementById("NSS4").value;
		var NSS5=window.document.getElementById("NSS5").value;

		var any=NSS3.substr(0,2);var mes=NSS3.substr(2,2);var dia=NSS3.substr(4,2);
		
		if( ((NSS1.toString().length)!=4)||((NSS2.toString().length)!=1)||((NSS3.toString().length)!=6)
		     ||((NSS4.toString().length)!=2)||((NSS5.toString().length)!=1) ||(isNaN(NSS1)==false)
			 ||(isNaN(NSS2)==true)||(isNaN(NSS3)==true)||(isNaN(NSS4)==true)||(isNaN(NSS5)==true)||
			 (NSS2>1)||(mes>12)||(dia>31)
			 ){
				error=1; alert(" "+NSS1+NSS2+NSS3+NSS4+NSS5+" incorrecte"); return false;
		}
	}

	if(error==0){ afegir_lineas('comuns/inscripcions/pas2',sol_DNI,id_ins,intranet);}
	
}

function CrearObjecte() {
	if(window.XMLHttpRequest){return new XMLHttpRequest();}
	else if(window.ActiveXObject){
		try{return new ActiveXObject('Msxml2.XMLHTTP');}
		catch(e){
			try{return new ActiveXObject('Microsoft.XMLHTTP');}
			catch(E){}
		}
	}
	alert("No s'ha pogut crear una instància d'XMLHttpRequest");
}
function afegir_lineas(arxiu,sol_DNI,id_ins,intranet){

    var valor_total=document.getElementById("total_llistat").value;
	var valor_total= parseInt(valor_total)+1;
	
	var forma = document.getElementById("llistat"); 
	var nou = forma.appendChild(document.createElement("div"));
	
	
	var nomm="llistat"+valor_total;
	nou.setAttribute('id', nomm);
	
	var objecte=CrearObjecte();	
	
	var id_entidad=document.form_inscripcio.id_entidad.value;
	
	var url=arxiu+".php?num="+valor_total+"&id_ins="+id_ins+"&id_entidad="+id_entidad+"&intranet="+intranet;

	if(intranet==1){
	    url="../"+url;
    }
    //alert(url);
	
	if(sol_DNI==1){
		var url=url+"&DNI="+document.getElementById("DNI").value;	
	}else{
		var url=url+"&NSS1="+document.getElementById("NSS1").value+"&NSS2="+document.getElementById("NSS2").value+"&NSS3="+document.getElementById("NSS3").value+"&NSS4="+document.getElementById("NSS4").value+"&NSS5="+document.getElementById("NSS5").value;	
	}
	
	objecte.open('GET',url);
	objecte.onreadystatechange = function(){	
	if(objecte.readyState==4){
			if(objecte.status==200)	{
				nou.innerHTML=objecte.responseText;		
			}
		}
	}

	objecte.send(null);
	document.getElementById("total_llistat").value=valor_total;
	if(sol_DNI==1){
		document.getElementById("DNI").value="";
		
	}else{
		document.getElementById("NSS1").value="";
		document.getElementById("NSS2").value="";
		document.getElementById("NSS3").value="";
		document.getElementById("NSS4").value="";
		document.getElementById("NSS5").value="";
	}

	for( var n=1;n<valor_total;n++){
		eval('document.getElementById("boto_elimina_'+n+'").className="visual_no"');	
	}
	
	if(valor_total>0){
		document.getElementById("capa_proteccio").className="visual_si";
        document.getElementById("boto_continuar").className="visual_no";
        document.getElementById("boto_mes").className="";
	}
}


function eliminar_lineas(arxiu){	
	var valor_total=document.getElementById("total_llistat").value;
	
	imagen=document.getElementById("llistat"+valor_total);
	
	
	if (!imagen){
		alert("El elemento selecionado no existe");
	} else {
		padre = imagen.parentNode;
		padre.removeChild(imagen);
	}
	var valor_total=(parseInt)(valor_total)-1;
	document.getElementById("total_llistat").value=valor_total;
	
	if(valor_total>1){
	eval('document.getElementById("boto_elimina_'+valor_total+'").className="visual_si"');
	}
}


function validar_inscrip(formulari,sol_federacio,sol_federacio_any){


	if((eval('window.document.'+formulari+'.id_entidad.value==""'))||
	(eval('window.document.'+formulari+'.mail_contacte.value==""'))||
	(eval('window.document.'+formulari+'.total_llistat.value==0')) ){
		alert("Es obligatori omplir els camps del formulari");
		return (false);
	}
	var email=eval('window.document.'+formulari+'.mail_contacte.value');
	var expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if ( !expr.test(email) ){
		alert("Email incorrecte");
		return (false);
	}
	
	var cognom1;var nom;var any; var id_genere;var llicencia;
	var num=eval('window.document.'+formulari+'.total_llistat.value');
	
	
	for(var n=1;n<=num;n++){
		cognom1="cognom1_"+n;		
		nom="nom_"+n;
		any="any_"+n;	
		mes="mes_"+n;	
		dia="dia_"+n;	
		id_genere="id_genere_"+n;	
		tramit="tramit_"+n;	
		
		if((eval('window.document.'+formulari+'.'+id_genere+'.value==""'))||
		(eval('window.document.'+formulari+'.'+cognom1+'.value==""'))||
		(eval('window.document.'+formulari+'.'+nom+'.value==""'))||
		(eval('window.document.'+formulari+'.'+any+'.value==""'))||
		(eval('window.document.'+formulari+'.'+mes+'.value==""'))||
		(eval('window.document.'+formulari+'.'+dia+'.value==""')) ){
			alert("Es obligatori omplir els camps del formulari");
			return (false);
		}
		valor_any=(eval('window.document.'+formulari+'.'+any+'.value'));
	
		
		tramit=document.getElementById(tramit).checked;
		if((sol_federacio==1)&&(valor_any<=sol_federacio_any) &&(tramit==false)){ 
					
			llicencia="llicencia_federativa_"+n;
			
			llicencia=eval('window.document.'+formulari+'.'+llicencia+'.value');
			
			if(llicencia==''){ 
				alert("Es obligatori omplir la llicencia");
				return (false);
			}
			var lletres=llicencia.substr(0,2);
			lletres=lletres.toLowerCase();
			var digits=llicencia.substr(2);
			
			if(
			((lletres !="cy")&&(lletres !="cl")&&(lletres!="ct")&&(lletres!="ib")&&(lletres!="ma")&&(lletres!="bi"))
			|| (isNaN(digits))
			|| (digits.length>5)
			|| (digits.length<2)
			){
				alert ("Llicencia "+llicencia+" incorrecte");				
				return(false);
			}
		
		}

	}
	if(eval('window.document.'+formulari+'.accepto.checked==false')){
		alert("Es obligatori acceptar la politica de privacitat del Consell");
		return (false);
	}
	
	return (true);
}

function veure_cat(casella,valor){
	eval('window.document.getElementById("'+casella+'").value="'+valor+'"');
}
function veure_fede(num,sol_federacio,sol_federacio_any){

	var valor_any=eval('window.document.getElementById("any_'+num+'").value');
	
	if((sol_federacio==1)&&(valor_any<=sol_federacio_any)){
		eval('window.document.getElementById("federacio_'+num+'").className="visual_si"');
	}else{
		eval('window.document.getElementById("federacio_'+num+'").className="visual_no"');	
	}

	/*Samarretes i samarretes infantils incloses o no dins de la carrera*/
	if((new Date().getFullYear()-valor_any)>=18){
		if(document.getElementById('samarreta_'+num) != null){
			eval('window.document.getElementById("samarreta_'+num+'").className="visual_si"');
			window.document.getElementById("input_samarreta_"+num).value = '1';
		}
		eval('window.document.getElementById("samarreta_infantil_'+num+'").className="visual_no"');
		window.document.getElementById("input_samarreta_infantil_"+num).value = '0';
	}else if((new Date().getFullYear()-valor_any)<18 && document.getElementById('samarreta_infantil_'+num) != null){
		if(document.getElementById('samarreta_'+num) != null){
			eval('window.document.getElementById("samarreta_'+num+'").className="visual_no"');
			window.document.getElementById("input_samarreta_"+num).value = '0';
		}
		eval('window.document.getElementById("samarreta_infantil_'+num+'").className="visual_si"');
		window.document.getElementById("input_samarreta_infantil_"+num).value = '1';
	}else{
		eval('window.document.getElementById("samarreta_infantil_'+num+'").className="visual_no"');
		eval('window.document.getElementById("samarreta_'+num+'").className="visual_no"');
	}
	
}

function mostrarTallesSamarretaInfantil(num){
	if(document.getElementById('check_samarreta_infantil_'+num).checked){
		eval('window.document.getElementById("taula_talla_samarreta_infantil_'+num+'").className="visual_si"');
	}else{
		eval('window.document.getElementById("taula_talla_samarreta_infantil_'+num+'").className="visual_no"');
	}
}
function no_veure_fede(num){
	eval('window.document.getElementById("federacio_'+num+'").className="visual_no"');
}
</script>