<script language="javascript" type="text/javascript">

    function validar_entitat_web(){
        //alert(window.document.form_dades.accepto.checked);

        if ( (window.document.form_dades.nom_entidad.value=="")||
            (window.document.form_dades.email.value=="")||
            (window.document.form_dades.login.value=="")||
            (window.document.form_dades.psw.value=="")||
            (window.document.form_dades.accepto.checked==false)
        ){
            alert("És obligatori omplir els camps nom entitat, municipi, adreça, correu electrònic, login i acceptar la política de privacitat");
            return (false);
        }
        else{
            if( window.document.form_dades.psw.value!= window.document.form_dades.psw_repe.value){
                alert("Les contrasenya no coincideix");
                return (false);
            }


            return(true);
        }
    }


    function validar_entitat(){
	if ( (window.document.form_dades.nom_entidad.value=="")||(window.document.form_dades.mail.value=="")){
		alert("És obligatori omplir els camps nom, adreça i mail");
		return (false);	
	}
	else{
		  return(true);
	}
}

</script>
<form method="post" enctype="multipart/form-data" name="form_dades">
  <?php 

if(isset($id_entidad))
{
    $con = mysqli_query($cnx_cesportiu, "select * from entidades where id_entidad=" . $id_entidad);
    $fila = mysqli_fetch_array($con);
}

?>
  <table  border="0" cellspacing="3" cellpadding="3">

 <tr>
      <td class="lletraGrisa">Nom:</td>
      <td><input name="nom_entidad" type="text"  value="<?php

          if(isset($_POST["nom_entidad"])){ echo $_POST["nom_entidad"];}
          elseif(isset($id_entidad)){echo  StripSlashes($fila["nom_entidad"]);}
          ?>" size="60" />      </td>
    </tr>

    <tr>
      <td class="lletraGrisa">Municipi:</td>
      <td >
          <select name="id_muni" onchange="submit()">


              <option value=""></option>
              <?php

              $con1=mysqli_query($cnx_cesportiu,"select * from Zmunicipis order by nom_muni");
              while($fila1=mysqli_fetch_array($con1)){?>

                  <option value="<?php echo $fila1["id_muni"]; ?>"
                  <?php
                  if((isset($_POST["id_muni"])) &&($_POST["id_muni"]!="fora")){
                      if($_POST["id_muni"]==$fila1["id_muni"]){ echo "selected";}
                  }

                  elseif(isset($id_entidad)){
                      if($fila["id_muni"]==$fila1["id_muni"]){ echo "selected";}

                  } ?>><?php echo $fila1["nom_muni"]; ?></option>
              <?php }?>

              <option value="fora" >Fora de la Comarca de la Selva</option>

          </select>


          <?php
          if((isset($_POST["id_muni"])) &&($_POST["id_muni"]!="")&&($_POST["id_muni"]!="fora")){
              $id_muni=$_POST["id_muni"];
          }

          if(isset($id_entidad)){
              if($fila["id_muni"]!=0){
                  $id_muni=$fila["id_muni"];
              }
          }

          if(!isset($id_muni)){?>

            <input type="text" name="municipi" value="<?php if(isset($id_entidad)){echo  $fila["municipi"];}?> ">
          <?php }?>
      </td>
    </tr>
    <tr>
      <td class="lletraGrisa">Adre&ccedil;a:</td>
      <td>




        <?php
        if(isset($id_muni)){?>

          <select name="id_adreca"  >
              <option value=""></option>
              <?php $con1=mysqli_query($cnx_adreces,"select * from adreca,tipus where
        adreca.id_muni=".$id_muni." and adreca.id_tipus=tipus.id_tipus order by adreca.nom_adreca;");
              while($fila1=mysqli_fetch_array($con1)){?>
                  <option value="<?php echo  $fila1["id_adreca"];?>"
                      <?php  if(isset($id_entidad)){if($fila1["id_adreca"]==$fila["id_adreca"]){ echo "selected";}}?>><?php echo  $fila1["nom_adreca"].", ".$fila1["nom_tipus"];
                      if($fila1["id_urba"]!=0){
                          $con2=mysqli_query($cnx_adreces,"select * from urba where id_urba=".$fila1["id_urba"]);
                          $fila2=mysqli_fetch_array($con2);
                          echo " (".$fila2["nom_urba"].")";
                      }?></option>
              <?php }?>
          </select>


        <?php }else{?>

            <input type="text" name="adreca" size="40" value="<?php if(isset($id_entidad)){echo StripSlashes($fila["adreca"]);}?>">



        <?php } ?>


<br>
        <span >Número</span> 
        <input name="numero" type="text"  value="<?php if(isset($id_entidad)){if($fila["numero"]!=0){ echo $fila["numero"]; }}?>" size="3" />

        <span >Escala</span>
         <select name="id_escala"  >
            <option value=""></option>
        <?php $con1=mysqli_query($cnx_adreces,"select * from escala order by nom_escala;");
        while($fila1=mysqli_fetch_array($con1)){?>
            <option value="<?php echo  $fila1["id_escala"];?>"
            <?php if(isset($id_entidad)){if($fila1["id_escala"]==$fila["id_escala"]){ echo "selected";}}?>><?php echo  $fila1["nom_escala"];?></option>
        <?php }?>
        </select>
        <span >Pis</span> 
         <select name="id_pis"  >
            <option value=""></option>
        <?php $con1=mysqli_query($cnx_adreces,"select * from pis order by nom_pis;");
        while($fila1=mysqli_fetch_array($con1)){?>
            <option value="<?php echo  $fila1["id_pis"];?>"
            <?php if(isset($id_entidad)){if($fila1["id_pis"]==$fila["id_pis"]){ echo "selected";}}?>><?php echo  $fila1["nom_pis"];?></option>
        <?php }?>
        </select>
            <span >Porta</span> 
         <select name="id_porta"  >
            <option value=""></option>
        <?php $con1=mysqli_query($cnx_adreces,"select * from porta order by nom_porta;");
        while($fila1=mysqli_fetch_array($con1)){?>
            <option value="<?php echo  $fila1["id_porta"];?>"
            <?php if(isset($id_entidad)){if($fila1["id_porta"]==$fila["id_porta"]){ echo "selected";}}?>><?php echo  $fila1["nom_porta"];?></option>
        <?php }?>
        </select>
    
        	</td>
    </tr>

    <tr>
      <td class="lletraGrisa">Tel&egrave;fon:</td>
      <td><input name="telefon" type="text"  value="<?php if(isset($id_entidad))
          {if($fila["telefon"]!=0){echo $fila["telefon"];}}?>" size="9" maxlength="9" />      </td>
    </tr>
    <tr>
      <td class="lletraGrisa">Fax:</td>
      <td><input name="fax" type="text"  
			value="<?php if(isset($id_entidad))
            {if($fila["fax"]!=0){echo $fila["fax"];}}?>" size="9" maxlength="9" />      </td>
    </tr>
    <tr>
      <td class="lletraGrisa">Altres tel i/o fax:</td>
      <td><input name="altres_tel_fax" type="text"  id="altre_tel_fax" value="<?php
          if(isset($id_entidad))
          {echo  StripSlashes($fila["altres_tel_fax"]);}?>" /></td>
    </tr>
    <tr>
      <td class="lletraGrisa">email:</td>
      <td><input name="email" type="text"  value="<?php
          if(isset($id_entidad)){echo  $fila["email"];}?>" size="30" />

          <input name="email2" type="text"  value="<?php if(isset($id_entidad)){echo  $fila["email2"];}?>" size="30" />      </td>
    </tr>
    <tr>
      <td class="lletraGrisa">Web:</td>
      <td><input name="web" type="text"  value="<?php if(isset($id_entidad))
          {echo  $fila["web"];}?>" size="30" /></td>
    </tr>
    <tr>
      <td class="lletraGrisa">Horari:</td>
      <td><textarea name="horari" cols="80" rows="4" 
	  ><?php if(isset($id_entidad))
              {echo  Stripslashes(str_replace("<br />",'',$fila["horari"]));}?></textarea>      </td>
    </tr>
    <tr>
      <td class="lletraGrisa">A qui va adre&ccedil;at:</td>
      <td>
          <textarea name="ambit" cols="80"  rows="3"
		  ><?php if(isset($id_entidad))
              {echo  Stripslashes(str_replace("<br />",'',$fila["ambit"]));}?></textarea>      </td>
    </tr>
    <tr>
      <td class="lletraGrisa">Descripci&oacute;:</td>
      <td>
          <textarea name="descripcio" cols="80" rows="10" ><?php if(isset($id_entidad))
              { echo  Stripslashes(str_replace("<br />",'',$fila["descripcio"]));}?></textarea>      </td>
    </tr>
    <tr>
      <td class="lletraGrisa">Logotip de l'entitat:</td>
      <td><input name="logo" type="file" id="logo" />
          <br />
          <?php if(isset($id_entidad))
          {
              if ($fila["logo_extensio"] != '')
              {
                  ?>
                  <img src="../carregues/entitats_logo/<?php echo $id_entidad . "." . $fila["logo_extensio"]; ?>"
                       alt="logotip de l'entitat"/><br/>
                  <input name="eliminar_logo" type="submit" class="boto_eliminar_peke" value="Eliminar el logotip"/>
              <?php }
          }?>
          	
	  <input type="hidden" name="logo_extensio" value="<?php  if(isset($id_entidad)){ echo $fila["logo_extensio"];}?>" />      </td>
    </tr>
    <tr>
      <td class="lletraGrisa">Foto de la seu:</td>
      <td><input name="foto" type="file" id="foto" />
          <br />
          <?php if(isset($id_entidad))
          {
              if ($fila["foto_extensio"] != '')
              {
                  ?>
                  <img src="../carregues/entitats_foto/<?php echo $id_entidad . "." . $fila["foto_extensio"]; ?>"
                       alt="foto de la seu" align="left"/><br/>
                  <input name="eliminar_foto" type="submit" class="boto_eliminar_peke" value="Eliminar la foto"/>
              <?php }
          }?>
      <input type="hidden" name="foto_extensio" value="<?php  if(isset($id_entidad)){ echo  $fila["foto_extensio"];}?>" />      </td>
    </tr>




    <?php if(!isset($id_entidad)){?>
        <tr>
            <td class="lletraGrisa">Login</td>
            <td><input type="text" name="login" value=""></td>

        </tr>

        <tr>
            <td class="lletraGrisa">Contrasenya</td>
            <td><input  type="password" name="psw" value=""></td>

        </tr>


        <tr>
            <td class="lletraGrisa" style="text-align: left">Repeteix la contrasenya</td>
            <td><input type="password" name="psw_repe" value=""></td>

        </tr>


    <?php }?>
    </table>


    <textarea name="politica"></textarea>
    <br>
    <input type="checkbox" name="accepto" value="1">Accepto la política de privacitat del Consell Esportiu de la Selva<br>




<?php if(isset($id_entidad)){include("comuns.php");}?>

    <?php if(isset($id_entidad)){ ?>
        <input type="hidden" name="id_entidad" value="<?php echo  $id_entidad;?>" />

        <input name="guardar_entitat" type="submit" class="boto_guardar" value="Guardar" onClick="return validar_entitat();"  />


    <?php }else{?>
 <input name="guardar_entitat" type="submit" class="boto_guardar" value="Següent" onClick="return validar_entitat_web();"  />

    <?php } ?>

 <?php if(isset($_SESSION["id_usuesport"])) {?>
  <input name="eliminar" type="submit" class="boto_eliminar" value="Eliminar" onClick="return validar_elim('aquesta entitat');"  />
 <?php }?>
 
</form>

