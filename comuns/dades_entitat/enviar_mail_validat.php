<?php

$assumpte="Intranet per coordinadors";


$contingut.="
<span class=\"lletraNorm\">Benvolguts companys i companyes de '<b>".StripSlashes($nom_entidad)."</b>',<br /><br />
Us enviem aquest correu per anunciar-vos que pel present curs trobareu moltes novetats al web del Consell Esportiu de la Selva.
.<br />
La majoria d'aquestes novetats tenen a veure amb l'intranet per coordinadors i la vostra interacció amb el web, per accedir a aquest apartat us fem arribar el vostre usuari i contrasenya:</span>
<br />";



$contingut.="
&#8226;<a href=\"http://www.selva.cat/esports/entitats\">www.selva.cat/esports/entitats</a><br />
&#8226;Usuari: ".$login."<br />
&#8226;Contrasenya: ".$psw."<br />";



$contingut.="<span class=\"lletraNorm\"><br />Més facilitat per renovar llicències, nous formats més pràctics pels carnets i absolut control de les dades de la vostra entitat i els vostres equips s�n en resum les novetats que el web us ofereix pel present curs.<br />

Esperem que aquestes novetats siguin del vostre agrat, per resoldre qualsevol dubte ens podeu trobar al 972 843 522 o bé a <a href=\"mailto:info@selvaesports.cat\">info@selvaesports.cat</a><br /><br />

Rebeu una cordial salutació, <br /><br />
Consell Esportiu de la Selva<br />
<a href=\"http://www.selvaesports.cat\">www.selvaesports.cat</a><br />
</span>";

@mail( $to, StripSlashes($assumpte), $contingut, $headers );
?>