<?php

if(isset($_POST["guardar_entitat"])){
	include("guardar.php");
}
if(isset($_POST["eliminar"])){
	$con=mysqli_query($cnx_cesportiu,"select * from entidades WHERE id_entidad=".$id_entidad.";");
	$fila=mysqli_fetch_array($con);
	if($fila["foto_extensio"]!=""){
		$origen="../carregues/entitats_foto/".$id_entidad.".".$_POST["foto_extensio"];
		unlink($origen);
	}
	if($fila["logo_extensio"]!=""){
		$origen="../carregues/entitats_logo/".$id_entidad.".".$_POST["logo_extensio"];
		unlink($origen);
	}
	mysqli_query($cnx_cesportiu,"delete from entitats where id_entidad=".$id_entidad);

	mysqli_query($cnx_cesportiu,"update entidades set estat=2 where id_entidad=".$id_entidad);
}

elseif(isset($_POST["eliminar_foto"])){
	$ins_entitat=mysqli_query($cnx_cesportiu,"update entidades SET 
	foto_extensio=''  
	WHERE id_entidad=".$id_entidad.";");
    $origen="../carregues/entitats_foto/".$id_entidad.".".$_POST["foto_extensio"];
	unlink($origen);
	msn("Foto eliminada",1);
}
elseif(isset($_POST["eliminar_logo"])){
	$ins_entitat=mysqli_query($cnx_cesportiu,"update entidades SET 
	logo_extensio='' 
	WHERE id_entidad=".$id_entidad.";");
    $origen="../carregues/entitats_logo/".$id_entidad.".".$_POST["logo_extensio"];
	unlink($origen);
	msn("Logo eliminat",1);
}
if(!isset($_POST["eliminar"])){
	include("form_entitat.php");
}

?>
