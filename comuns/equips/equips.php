<script language="javascript">
function elim_equip(valor){
	window.document.getElementById("id_equip").value=valor;
}
</script>
<fieldset>
<legend>Equips</legend><?php 
//Mostrem els equips que existeixen en la lliga actual
if(isset($id_comu)){



	$con1=mysqli_query($cnx_cesportiu,"select * from ass_3equips,Zcategories,Zgeneres where 
	ass_3equips.id_comu=".$id_comu." and 
	ass_3equips.id_cat=Zcategories.id_cat and 
	ass_3equips.id_genere=Zgeneres.id_genere 
	order by Zcategories.inici,Zgeneres.id_genere, lletra" );
	while($fila1=mysqli_fetch_array($con1)){?>
		<fieldset class="fiel" >
		<legend class="leg"><?php echo  $fila1["nom_cat"]." ".$fila1["nom_genere"]." ".nom_lletra($fila1["lletra"]);?></legend>
		<label>Dia</label>
		<select name="dia_<?php echo  $fila1["id_equip"];?>">
			<?php for($i=1;$i<=count($dia_joc);$i++){?>
				<option value="<?php echo  $i;?>" <?php if($fila1["dia"]==$i){?> selected="selected"<?php }?>><?php echo  $dia_joc[$i];?></option>
			<?php }?>
		</select>
		
	   <label>Hora</label>
	   <input type="text" name="hora_<?php echo  $fila1["id_equip"];?>" id="hora_<?php echo  $fila1["id_equip"];?>" 
	   size="5" value="<?php echo  $fila1["hora"];?>"
	   onchange="validar_horari('hora_<?php echo  $fila1["id_equip"];?>','<?php echo  $fila1["hora"];?>');"  maxlength="5" />
	
		<label>Foto de l'equip</label>
		<input type="file" name="foto_<?php echo  $fila1["id_equip"];?>" />
		<?php if($fila1["foto"]!=""){?><p class="neteja"><img src="../carregues/equips/<?php echo  $fila1["foto"]?>" class="neteja"/></p><?php }?>
		
		<label>Jugadors (indicar num de camiseta)</label>
		<?php $con2=mysqli_query($cnx_cesportiu,"select esportistes.id_esportista, esportistes.nom, esportistes.cognom1,
		esportistes.cognom2,esportistes.dia,esportistes.mes,esportistes.any 
		from ass_1config,llice_sol,llice_sol_esportista,esportistes  where 
		ass_1config.id_config=".$id_config." and 
		ass_1config.id_curs=llice_sol.id_curs and 
		llice_sol.id_entidad=".$id_entidad." and 
		llice_sol.data_valida is not null and 
		llice_sol.id_sol=llice_sol_esportista.id_sol and 
		llice_sol_esportista.id_deporte=ass_1config.id_deporte and 
		llice_sol_esportista.id_esportista=esportistes.id_esportista 
		order by nom,cognom1");
		while($fila2=mysqli_fetch_array($con2)){?>
        	<p class="neteja">
            <?php $con3=mysqli_query($cnx_cesportiu,"select * from ass_3esportistes where 
			id_equip=".$fila1["id_equip"]." and 
			id_esportista=".$fila2["id_esportista"]);?>
            
            <input type="checkbox" name="esportista_<?php echo  $fila1["id_equip"]."_".$fila2["id_esportista"];?>" value="1" 
            <?php if(mysqli_num_rows($con3)==1){?> checked="checked"<?php }?> />
            
			<input type="text" size="2" name="num_<?php echo  $fila1["id_equip"]."_".$fila2["id_esportista"];?>" 
			<?php 
			if(mysqli_num_rows($con3)==1){
				$fila3=mysqli_fetch_array($con3);	
				echo "value=\"".$fila3["num"]."\"";
			}?>/>
			<?php echo  $fila2["nom"]." ".$fila2["cognom1"]." ".$fila2["cognom2"];
			echo "(".edat($fila2["dia"],$fila2["mes"],$fila2["any"]).")";
			echo "<br>";
			?></p>
		
		<?php }?>
		
		
        <?php //L'equip es pot eliminar si no esta dins de cap grup
		
		if($fila1["id_grup2"]==0){?>
        
		 <input type="submit" name="eliminar_equip" value="Eliminar equip" 
		  class="boto_eliminar" onclick="elim_equip('<?php echo  $fila1["id_equip"];?>');validar_elim('aquest equip')" />
		<?php }?>
        
		</fieldset>
		
	<?php }
}?>


<fieldset class="fiel">
	<legend class="leg">Nou</legend>
    
    <label>Categoria</label>
    <select name="id_cat_nou">
        <option value=""></option>
    <?php $con1=mysqli_query($cnx_cesportiu,"SELECT * FROM Zcategories order by inici");
    while($fila1=mysqli_fetch_array($con1)){?>
    	<?php $con2=mysqli_query($cnx_cesportiu,"select count(*) from ass_1config where 
		id_config=".$id_config." and find_in_set(".$fila1["id_cat"].",categories)");
		$fila2=mysqli_fetch_array($con2);
		if($fila2["count(*)"]==1){?>
    
        <option value="<?php echo  $fila1["id_cat"];?>"><?php echo  StripSlashes($fila1["nom_cat"]);?></option>
        <?php }?>
        
    <?php }?>
  </select>
  
  <label>Genere</label>
    <select name="id_genere_nou">
        <option value=""></option>
    <?php $con1=mysqli_query($cnx_cesportiu,"SELECT * FROM Zgeneres");
    while($fila1=mysqli_fetch_array($con1)){
		
		$con2=mysqli_query($cnx_cesportiu,"select count(*) from ass_1config where 
		id_config=".$id_config." and find_in_set(".$fila1["id_genere"].",generes)");
		$fila2=mysqli_fetch_array($con2);
		if($fila2["count(*)"]==1){?>
        
	        <option value="<?php echo  $fila1["id_genere"];?>"><?php echo  StripSlashes($fila1["nom_genere"]);?></option>
        <?php 
		}?>
    <?php }?>
  </select>
   
   <label>Dia</label>
    <select name="dia_nou">
        <option value=""></option>
         <?php for($j=1;$j<=count($dia_joc);$j++){?>
	            <option value="<?php echo  $j;?>" ><?php echo  $dia_joc[$j];?></option>
         <?php }?>
    </select>
    
    <label>Hora</label>
    <input type="text" name="hora_nou" id="hora_nou"
     size="5" onchange="validar_horari('hora_nou','');" value="" maxlength="5" />
     
    <label>Foto de l'equip</label>
    <input type="file" name="foto_nou" />
    

</fieldset>

</fieldset>