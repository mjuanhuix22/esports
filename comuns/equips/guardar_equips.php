<?php 
function nom_equip($id_equip){
	require("../conexio.php");
	$con=mysqli_query($cnx_cesportiu,"select * from 
	ass_1config,ass_3comu,ass_3equips,Zcategories,Zgeneres,Zdeportes,Zcurs,entidades
	where 
	ass_3equips.id_equip=".$id_equip." and 
	ass_3equips.id_cat=Zcategories.id_cat and 
	ass_3equips.id_genere=Zgeneres.id_genere and 
	ass_3equips.id_comu=ass_3comu.id_comu and 
	ass_3comu.id_entidad=entidades.id_entidad and 
	ass_3comu.id_config=ass_1config.id_config and 
	ass_1config.id_deporte=Zdeportes.id_deporte and
	ass_1config.id_curs=Zcurs.id_curs");
	$fila=mysqli_fetch_array($con);
	
	$foto=normalitza($fila["nom_entidad"])."_".$fila["any1"]."-".$fila["any2"]."_".normalitza($fila["nom_deporte"])."_".normalitza($fila["nom_cat"])."_".normalitza($fila["nom_genere"]).nom_lletra($fila["lletra"]);
	
	return ($foto);
		
}

if(isset($id_comu)){
	$con=mysqli_query($cnx_cesportiu,"select * from ass_3equips where id_comu=".$id_comu);
	while($fila=mysqli_fetch_array($con)){
		$dia="dia_".$fila["id_equip"];$hora="hora_".$fila["id_equip"]; $foto="foto_".$fila["id_equip"];
		
		
		$SQL="";
		if($_FILES["$foto"]["name"]!=""){
			//Primer eliminem la foto existent
			$con1=mysqli_query($cnx_cesportiu,"select foto from ass_3equips where id_equip=".$fila["id_equip"]);
			$fila1=mysqli_fetch_array($con1);
			if($fila1["foto"]!=""){
				unlink("../carregues/equips/".$fila1["foto"]);
			}
			
			$nom_foto=nom_equip($fila["id_equip"]);
			$arxiu="foto_".$fila["id_equip"];
			$ruta="../carregues/equips/".$nom_foto;
			$obli_imatge=1;
			include("../carregar.php");
			if($penjat==1){
				$SQL=",foto='".$nom_foto.".".$extensio."'";
			}else{
				$SQL=",foto=''";
			}
		}
		
		if($_POST["$hora"]!=""){
			mysqli_query($cnx_cesportiu,"update ass_3equips set dia=".$_POST["$dia"].", hora='".$_POST["$hora"]."' ".$SQL."  
			where id_equip=".$fila["id_equip"]);
		}
		
		
		
		//-------Jugadors
		
		
		
		$con2=mysqli_query($cnx_cesportiu,"select esportistes.id_esportista  
		from ass_1config,llice_sol,llice_sol_esportista,esportistes  where 
		ass_1config.id_config=".$id_config." and 
		ass_1config.id_curs=llice_sol.id_curs and 
		llice_sol.id_entidad=".$id_entidad." and 
		llice_sol.data_valida is not null and 
		llice_sol.id_sol=llice_sol_esportista.id_sol and 
		llice_sol_esportista.id_deporte=ass_1config.id_deporte and 
		llice_sol_esportista.id_esportista=esportistes.id_esportista 
		order by nom,cognom1");
		while($fila2=mysqli_fetch_array($con2)){
			$esportista="esportista_".$fila["id_equip"]."_".$fila2["id_esportista"];
			$num="num_".$fila["id_equip"]."_".$fila2["id_esportista"];
			if( ((isset($_POST["$num"]))&&($_POST["$num"]!=""))  ||
				((isset($_POST["$esportista"])) && ($_POST["$esportista"]==1)) ){
				
				
				$con3=mysqli_query($cnx_cesportiu,"select * from ass_3esportistes where 
				id_equip=".$fila["id_equip"]." and 
				id_esportista=".$fila2["id_esportista"]);
				if(mysqli_num_rows($con3)==0){
					mysqli_query($cnx_cesportiu,"insert into ass_3esportistes set 
					id_equip=".$fila["id_equip"].",
					id_esportista=".$fila2["id_esportista"].",
					num='".$_POST["$num"]."'");
				}else{
					mysqli_query($cnx_cesportiu,"update ass_3esportistes set 
					num=".$_POST["$num"]." where 
					id_equip=".$fila["id_equip"]." and 
					id_esportista=".$fila2["id_esportista"]);
				}
			}else{
				mysqli_query($cnx_cesportiu,"delete from ass_3esportistes where 
				id_equip=".$fila["id_equip"]." and 
				id_esportista=".$fila2["id_esportista"]);
			}
		}
		
		
		
	}
}



//---------------Nous equips

$id_cat="id_cat_nou";$id_genere="id_genere_nou";$dia="dia_nou";$hora="hora_nou";

if(($_POST["$id_cat"]!="")&&($_POST["$id_genere"]!="")&&($_POST["$dia"]!="")&&($_POST["$hora"]!="")){
	
	//Si ja existeix un altre equip amb aquesta categoria i genere 
	if(isset($id_comu)){
		$con=mysqli_query($cnx_cesportiu,"select count(*) from ass_3equips where 
		id_comu=".$id_comu." and 
		id_cat=".$_POST["$id_cat"]." and 
		id_genere=".$_POST["$id_genere"]."");
		
		$fila=mysqli_fetch_array($con);
		$lletra=0;
		if($fila["count(*)"]!=0){
			
			//El primer equip amb aquesta categoria i genere tindra el lletra 1=A
			mysqli_query($cnx_cesportiu,"update ass_3equips set lletra=1 where 
			id_comu=".$id_comu." and 
			id_cat=".$_POST["$id_cat"]." and 
			id_genere=".$_POST["$id_genere"]." and 
			lletra=0 ");
			
			//El nou equip tindrà el lletra
			$lletra=$fila["count(*)"]+1;
		}
	}
	
	$SQL="insert into ass_3equips set 
	id_comu=".$id_comu.",
	id_cat=".$_POST["$id_cat"].",
	id_genere=".$_POST["$id_genere"].",
	dia=".$_POST["$dia"].",
	hora='".$_POST["$hora"]."',
	lletra=".$lletra.";";
	echo $SQL;
	mysqli_query($cnx_cesportiu,$SQL);


	if($_FILES["foto_nou"]["name"]!=""){

		$arxiu="foto_nou";;
		$nom_foto=nom_equip(mysqli_insert_id($cnx_cesportiu));
		$ruta="../carregues/equips/".$nom_foto;
		include("../carregar.php");
		if($penjat==1){
			mysqli_query($cnx_cesportiu,"update ass_3equips set foto='".$nom_foto.".".$extensio."' where id_equip=".mysqli_insert_id($cnx_cesportiu));
		}
	
	}
}




?>