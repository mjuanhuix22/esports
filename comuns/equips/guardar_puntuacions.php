<?php 

//Entra amb $id_equip
$p_jugats=0;$p_guanyats=0;$p_perduts=0;$p_empatats=0;$punts_favor=0;$punts_contra=0;$punts_totals=0;$targetes_verdes=0;


//Buscar com es puntua la lliga
$conE=mysqli_query($cnx_cesportiu,"select * from ass_2lliga,ass_3comu,ass_3equips where 
ass_3equips.id_equip=".$id_equip." and 
ass_3equips.id_comu=ass_3comu.id_comu and 
ass_3comu.id_lliga=ass_2lliga.id_lliga");
$filaE=mysqli_fetch_array($conE);
$pp=$filaE["pp"];$pg=$filaE["pg"];$pe=$filaE["pe"];


//Partits que ha jugat a casa
$conE=mysqli_query($cnx_cesportiu,"select * from ass_5partits where 
id_equip1=".$id_equip." 
and data<=".date('Ymd')." and p_equip1!=''");
while($filaE=mysqli_fetch_array($conE)){
	$p_jugats++;
	if($filaE["p_equip1"]>$filaE["p_equip2"]){ $p_guanyats++;}
	elseif($filaE["p_equip1"]<$filaE["p_equip2"]){ $p_perduts++;}
	elseif($filaE["p_equip1"]=$filaE["p_equip2"]){ $p_empatats++;}
	$punts_favor+=$filaE["p_equip1"];
	$punts_contra+=$filaE["p_equip2"];
	$targetes_verdes+=$filaE["v_equip1"];
}


//Partits que ha jugat a fora
$conE=mysqli_query($cnx_cesportiu,"select * from ass_5partits where 
id_equip2=".$id_equip." and data<=".date('Ymd')." 
and p_equip2!=''");

while($filaE=mysqli_fetch_array($conE)){
	$p_jugats++;
	if($filaE["p_equip2"]>$filaE["p_equip1"]){ $p_guanyats++;}
	elseif($filaE["p_equip2"]<$filaE["p_equip1"]){ $p_perduts++;}
	elseif($filaE["p_equip2"]=$filaE["p_equip1"]){ $p_empatats++;}
	$punts_favor+=$filaE["p_equip2"];
	$punts_contra+=$filaE["p_equip1"];
    $targetes_verdes+=$filaE["v_equip2"];
}

//Calcula puntuació de l'esquip

$punts_totals=($p_perduts*$pp)+($p_guanyats*$pg)+($p_empatats*$pe);
$average=$punts_favor-$punts_contra;

$SQL="update ass_3equips set 
p_jugats='".$p_jugats."',
p_guanyats='".$p_guanyats."',
p_perduts='".$p_perduts."',
p_empatats='".$p_empatats."',
punts_favor='".$punts_favor."',
punts_contra='".$punts_contra."',
punts_totals='".$punts_totals."',
targetes_verdes='".$targetes_verdes."',
average='".$average."' where id_equip=".$id_equip;

mysqli_query($cnx_cesportiu,$SQL);
?>