<?php 
//-------------Entra amb $id_entidad i $id_config

//----Buscar la lliga per defecte
if(isset($_POST["id_lliga"])){
	$id_lliga=$_POST["id_lliga"];
}else{	
	//Si en aquest curs encara no s'ha creat cap lliga els grups es guardaran amd id_lliga=0
	$con=mysqli_query($cnx_cesportiu,"SELECT * FROM ass_2lliga WHERE id_config=".$id_config." order by id_lliga DESC");
	if(mysqli_num_rows($con)==0){
		$id_lliga=0;	
	}elseif(mysqli_num_rows($con)!=0){ //Si hi ha una o més lligues per defecte es veu l'ultima
		
		$fila=mysqli_fetch_array($con);
		$id_lliga=$fila["id_lliga"];
	}
}


//Si hi ha més d'una lliga es poden seleccionar
$con=mysqli_query($cnx_cesportiu,"SELECT * FROM ass_2lliga WHERE id_config=".$id_config." order by id_lliga DESC");
if(mysqli_num_rows($con)>1){?>
    <form method="post" >
          <select name="id_lliga" class="menu_selec"  onchange="submit();">
            <?php $con=mysqli_query($cnx_cesportiu,"SELECT * FROM ass_2lliga,Zvariables WHERE 
            id_config=".$id_config." and 
            ass_2lliga.id_nom=Zvariables.id_var 
			order by id_lliga DESC");
            while($fila=mysqli_fetch_array($con)){?>
                <option value="<?php echo  $fila["id_lliga"];?>"
                <?php if($fila["id_lliga"]==$id_lliga){?> selected="selected"<?php }?>><?php echo  StripSlashes($fila["nom_var"]);?></option>
            <?php }?>
          </select>
      
        <input type="hidden" name="id_config" value="<?php echo  $id_config; ?>" />
        <input type="hidden" name="id_entidad" value="<?php echo  $id_entidad; ?>" />
        <?php include("comuns.php");?>
    </form>

<?php }?>



<?php 
echo "<h3>";
if($id_lliga==0){
	echo "Preinscripcions";
}else{
 	$con=mysqli_query($cnx_cesportiu,"SELECT * FROM ass_2lliga,Zvariables WHERE 
	id_lliga=".$id_lliga." and 
    ass_2lliga.id_nom=Zvariables.id_var");
	$fila=mysqli_fetch_array($con);
	echo $fila["nom_var"];
}echo "</h3>";


if(isset($_POST["eliminar_equip"])){ include("eliminar_equip.php"); }
if(isset($_POST["eliminar_arbitre"])){ msn(mysqli_query($cnx_cesportiu,"delete from arbitres where id_arbitre=".$_POST["id_arbitre"])); }

if(isset($_POST["guardar"])){

	include("guardar_comu.php");
	if($ins){ 
		if($id_lliga==0){include("guardar_equips_pre.php");}
		else{ include("guardar_equips.php");}
		include("guardar_rtats.php");
	}
	include("guardar_arbitre.php");
}
if(isset($_POST["guardar_rtats"])){
	include("guardar_rtats.php");
}

?>


<?php include("form_print.php");?>

<form method="post" enctype="multipart/form-data" id="formulari" >
<?php 
if($id_lliga!=0){ include("partits.php");}

if(!isset($_SESSION["id_arbitre"])){ //Si és un arbitre nomes pot entrar partits
	include("comu.php");
	if($id_lliga==0){
		include("equips_pre.php");		
	}else{
		include("equips.php");
	}
	include("arbitres.php");
}
?>

<input type="hidden" name="id_equip" id="id_equip" />
<input type="hidden" name="id_config" value="<?php echo  $id_config; ?>" />
<input type="hidden" name="id_entidad" value="<?php echo  $id_entidad; ?>" />
<input type="hidden" name="id_lliga" value="<?php echo  $id_lliga;?>" />

<?php if(!isset($_SESSION["id_arbitre"])){ ?>
	<input type="submit" name="guardar" value="Guardar" class="boto_guardar" />
<?php }?>

<?php include("comuns.php");?>
</form>