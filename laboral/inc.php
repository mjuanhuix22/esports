<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php 
function nom_data8_breu($data){
	$any=substr($data,0,4);$mes=substr($data,4,2);$dia=substr($data,6,2);	
	echo $dia."/".$mes."/".$any;
}

function nomm_data8($dia,$mes,$any){
	
	$id_dia= date("w", mktime(0, 0, 0, $mes, $dia, $any));
	$dia_set=array(1=>'dilluns',2=>'dimarts',3=>'dimecres',4=>'dijous',5=>'divendres',6=>'dissabte',7=>'diumenge',0=>'diumenge');
	$text= $dia_set[$id_dia]." ".$dia." ";
	if(($mes==4)||($mes==8)||($mes==10)){ $text.= "d'";}else{ $text.= "de ";}
	$nom_mes=array(1=>'gener',2=>'febrer',3=>'març',4=>'abril',5=>'maig',6=>'juny',
7=>'juliol',8=>'agost',9=>'setembre',10=>'octubre',11=>'novembre','12'=>"desembre");
	$text.= $nom_mes[(int)$mes];
	$text.= " de ".$any;
	
	return ($text);
}

function nom_data8($data){
	$any=substr($data,0,4);$mes=substr($data,4,2);$dia=substr($data,6,2);
	
	$id_dia= date("w", mktime(0, 0, 0, $mes, $dia, $any));
	$dia_set=array(1=>'dilluns',2=>'dimarts',3=>'dimecres',4=>'dijous',5=>'divendres',6=>'dissabte',7=>'diumenge',0=>'diumenge');
	echo $dia_set[$id_dia]." ".$dia." ";
	if(($mes==4)||($mes==8)||($mes==10)){ echo "d'";}else{ echo "de ";}
	$nom_mes=array(1=>'gener',2=>'febrer',3=>'març',4=>'abril',5=>'maig',6=>'juny',
7=>'juliol',8=>'agost',9=>'setembre',10=>'octubre',11=>'novembre','12'=>"desembre");
	echo $nom_mes[(int)$mes];
	echo " de ".$any;
}

function nom_data($dia,$mes,$any){
	
	$id_dia= date("w", mktime(0, 0, 0, $mes, $dia, $any));
	$dia_set=array(1=>'dilluns',2=>'dimarts',3=>'dimecres',4=>'dijous',5=>'divendres',6=>'dissabte',7=>'diumenge',0=>'diumenge');
	echo $dia_set[$id_dia]." ".$dia." ";
	if(($mes==4)||($mes==8)||($mes==10)){ echo "d'";}else{ echo "de ";}
	$nom_mes=array(1=>'gener',2=>'febrer',3=>'març',4=>'abril',5=>'maig',6=>'juny',
7=>'juliol',8=>'agost',9=>'setembre',10=>'octubre',11=>'novembre','12'=>"desembre");
	echo $nom_mes[(int)$mes];
	echo " de ".$any;
}


$minuts=array(0=>"00",1=>"30");
function det_mes($mes){
	if(($mes==4)||($mes==8)||($mes==10)||($mes=='04')||($mes=='08')){ return "d'";}
	else{ return "de ";}

}

function article($nom,$sexe){
	$lletra=substr($nom,0,1);
	if(($lletra=='A')||($lletra=='E')||($lletra=='I')||($lletra=='O')||($lletra=='U')){
		$art="l'";
	}else{
		if($sexe==1){
			$art="la ";
		}else{
			$art="en ";
		}
	}
	return($art);
}
function dia7($any,$mes,$dia){
	$data=mktime(0,0,0,$mes,$dia,$any);
	$temps=getdate($data);
	$diasetmana=$temps['wday'];
	return ($diasetmana);
}


function num_nom_dia($mes,$dia,$any){
	$nom_dia_angles= date("l", mktime(0, 0, 0, $mes, $dia, $any));
	$dies_setmana=array('Monday'=>0,'Tuesday'=>1,'Wednesday'=>2,'Thursday'=>3,'Friday'=>4,'Saturday'=>5,'Sunday'=>6);
	$num_dia=$dies_setmana[$nom_dia_angles];
	return ($num_dia);
}

function data_actual8(){
	$dia=date('d');$mes=date('m');
	if(strlen($dia)==1){ $dia="0".$dia;}
	if(strlen($mes)==1){ $mes="0".$mes;}
	$data=date('Y').$mes.$dia;
	return ($data);
}

function convert_num2($num){
	if(strlen($num)==1){ $num="0".$num;}
	if(strlen($num)==0){ $num="00";}
	return($num);
}

function convert_data8($any,$mes,$dia){

	if(strlen($dia)==1){ $dia="0".$dia;}
	if(strlen($mes)==1){ $mes="0".$mes;}
	$data=$any.$mes.$dia;
	return($data);

}

$dies_setmana=array('Monday'=>'dilluns','Tuesday'=>'dimarts','Wednesday'=>'dimecres',
'Thursday'=>'dijous','Friday'=>'divendres','Saturday'=>'dissabte','Sunday'=>'diumenge');

$dia_laboral=array(1=>'dilluns',2=>'dimarts',3=>'dimecres',4=>'dijous',5=>'divendres',6=>'dissabte',7=>'diumenge',0=>'diumenge');

function total_dies($mes,$any){
	$mes=(int)$mes;
	if($mes==2)	{
		if($any%4==0){ return(29);}else{ return(28);}
	}
	elseif(($mes==4)||($mes==6)||($mes==9)||($mes==11)){ return(30);}
	else{ return(31);}
}

$total_dies=array(1=>31,2=>28,3=>31,4=>30,5=>31,6=>30,7=>31,8=>31,9=>30,10=>31,11=>30,12=>31,'01'=>31,'02'=>28,'03'=>31,'04'=>30,'05'=>31,'06'=>30,'07'=>31,'08'=>31,'09'=>30);

$nom_mes=array(1=>'gener',2=>'febrer',3=>'març',4=>'abril',5=>'maig',6=>'juny',
7=>'juliol',8=>'agost',9=>'setembre',10=>'octubre',11=>'novembre','12'=>"desembre");

function det($nom){
	$lletra=substr($nom,0,1);
	if(($lletra=='A')||($lletra=='E')||($lletra=='I')||($lletra=='O')||($lletra=='U')){
		return "d'";
	}else{
		return "de ";
	}
}
function msn($text,$ok){?>
	<div class="msn <?php if($ok==1){ echo "msn_correcte";}{ echo "msn_incorrecte";}?>"><?php echo   $text;?></div>

<?php }


$nom_dir=array("laboral"=>"La meva gestió laboral","dades"=>"Les meves dades laborals","solicitud_usuari"=>"Les meves sol·licituds pendents","formacio"=>"Formació","nomines"=>"Les meves nòmines");



$any_seg=date('Y')+1;
$nom_carpeta3=array("absencies_mod"=>"Modificar permísos retribuïts","contracte"=>"Nou contracte","contracte_perso"=>"Contractes del personal","fitxatge_mod"=>"Modificar fitxatge","festius"=>"Festius $any_seg","informatic"=>"Dades electróniques","responsables"=>"Responsables d'area","usu_eliminar"=>"Eliminar treballador","usu_modificar"=>"Modificar treballador","usu_nou"=>"Nou treballador","validar"=>"Validar sol·licituds","categories"=>"Categories","despeses"=>"Despeses","entrega"=>"Propera entrega","exist"=>"Material existent","inventari"=>"Inventari","mod_fotos"=>"Modificar fotos","eliminar_material"=>"Eliminar material","nou_mat"=>"Material nou","proveidors"=>"Proveidors","solicituds"=>"Sol·licituds","aprovades"=>"Sol·licituds aprovades","pendents"=>"Sol·licituds pendents","veure_absencies"=>"Veure permísos retribuïts","vacances"=>"Vacances","contracte_mod"=>"Modificar contractes","absencies_visual"=>"Visualitzar permísos retribuïts","absencies_validar"=>"Validar permísos retribuïts","absencies_solicitud"=>"Sol·licitar permísos retribuïts","on_son"=>"On son els treballadors?","fitxatge_visual"=>"Visualitzar fitxatge","contracte"=>"Contractes","absencies_normativa"=>"Normativa dels permísos retribuïts","contracte_finalitzar"=>"Finalitzar un contracte vigent","frases"=>"Frases fetes","santoral"=>"Santoral","missatges"=>"Missatges","horari_mod_nou"=>"Modificar Permisos no retribuïts i hores extres","horari_visual"=>"Visualitzar Permisos no retribuïts i hores extres","horari_normativa"=>"Normativa de Permisos no retribuïts i hores extres","info_psw"=>"Contrassenyes","info_telf"=>"Extensions i telèfons","info_mails"=>"Correus electrònics","fitxatge_desquadra"=>"Desquadraments de fitxatge","compensa_elimina"=>"Eliminar compensació d'hores","compensa_nou"=>"Nova compensació d'hores","compensa_visual"=>"Visualitzar compensació d'hores","CAAS"=>"Jornada laboral al CAAS","abs_totes"=>"Absències","mails_gerent"=>"Mails enviats per fitxatge erroni","veure_formacio"=>"Veure formació","entregues"=>"Historial d'entregues","nova_entrega"=>"Nova entrega","compensa_calcul"=>"Càlcul de compensacions","portada"=>"Portada","historial"=>"Historial","llistat"=>"Llistat","llistat_sol"=>"Llistat de sol·licituds","nova_sol"=>"Nova sol·licitud","justificant"=>"Justificants pendents d'entregar","totes"=>"Totes les reserves","futures"=>"Reserves futures","contracte_assu_vac"=>"Total de dies de vacances i assumptes","festa_starita"=>"Festa de Santa Rita","priv_valida"=>"Validadors","priv_visual"=>"Visualitzadors","priv_valida_visual"=>"Validadors i visualitzadors","llistats"=>"Llistats de treballadors","tasques"=>"Tasques","formacio"=>"Formació","1validar_presupost"=>"Validar per Gestió Pressupostària","2validar_gerent"=>"Validar per Gerència","3entregar"=>"Entregar l'efectiu","4justificant"=>"Entrega de justificants","treballadors"=>"Qui pot fer una sol·licitud","fora_horari"=>"Treball fora l'horari laboral","fitxatge_rrhh"=>"Fitxatge arreglat per RRHH","fitxatge_automatic"=>"Fitxatge massiu","vist_caps"=>"Vist-i-plau dels caps de departament","lopd_usuaris"=>"Relació entre personal i fitxers","lopd_registre"=>"Registre incidències pendents","lopd_historial"=>"Registre incidències finalitzades","Totes"=>"Totes les reserves","nova"=>"Nova reserva","actuals"=>"Reserves actuals","festes_consell"=>"Festes del Consell","sol_eliminades"=>"Sol·licituds de permisos eliminades","fitxatge_arregla"=>"Fitxatge modificat pel treballador","fora_horari_validadors"=>"Validadors del treball fora l'horari laboral","config"=>"Configuració","ordre"=>"Ordre de tasques","tasques"=>"Tasques","validadors"=>"Validadors","festes_estatals"=>"Festes estatals","festes_locals"=>"Festes locals","perfils"=>"Perfils","maquinari"=>"Maquinari","config"=>"Configuració","llistatss"=>"Llistats","etiquetes"=>"Etiquetes","material"=>"Material",""=>"","cursos"=>"Pla de formació integral","permisos"=>"Qui veu aquest apartat","nomin"=>"Nòmines","compensa_config"=>"Configurar paràmetres de compensació");
function restar_temps_actual($hora_menys,$min_menys) {
	
	list($hora, $min, $seg, $dia, $mes, $anno) =  explode( " ", date( "H i s d m Y"));
	$h = $hora - $hora_menys;
	$m=$min-$min_menys;
	$hora = date("H", mktime($h, $m, $seg, $mes, $dia, $anno));
	$min = date("i", mktime($h, $m, $seg, $mes, $dia, $anno));
	return array($hora,$min);
} 

//pasar els minuts a hores
function passar_minuts_hores($hores,$minuts) {

	if($minuts>=60){
		$data=$minuts/60;
		$data=explode('.',$data);
		$hores=$hores+$data[0];
		(int)$min="0.".$data[1];
		$min=$min*60;
		$min=explode('.',$min);
		$minuts=$min[0];
		if($min[1]>=5){
			$minuts++;
		}
	}
	return array($hores,$minuts);
} 

function sumar_temps($hora_1,$min_1,$hora_2,$min_2) {
	
	$dia=1;
	$mes=1;
	$anno=2009;
	
	$h = $hora_1 +$hora_2;
	$m=$min_1+$min_2;
	
	$hora = date("H", mktime($h, $m, 0, $mes, $dia, $anno));
	$min = date("i", mktime($h, $m, 0  , $mes, $dia, $anno));
	if($min==60){ $hora=$hora+1; $min=0;}
	return array($hora,$min);
}

function restar_temps($hora_1,$min_1,$hora_2,$min_2) {
	$positiu=1;
	if(($hora_1<$hora_2)||( ($hora_1==$hora_2)&&($min_1<$min_2) )){//  ?No se quan es dona aquest cas
		$hora22=$hora_1;$min22=$min_1;
		$hora_1=$hora_2;$min_1=$min_2;
		$hora_2=$hora22;$min_2=$min22;
		$positiu=0;
	}

	$data1 = mktime($hora_1,$min_1,0,1,1,2000);
	$data2 = mktime($hora_2,$min_2,0,1,1,2000);
	$dif=$data1-$data2;
	
	$min=floor($dif/(60));
	$hora=0;
	if($min>60){
		$hora=floor($min/60);
		$min=$min%60;
	}
	if($min==60){ $hora=$hora+1; $min=0;}
	if(strlen($hora)==1){ $hora="0".$hora;}
	if(strlen($min)==1){ $min="0".$min;}
	
	return array($hora,$min,$positiu);
}

function sum_rest_dies_actual($dies){

	$dia=date('d')+$dies;
	$mes = date("m", mktime(0, 0, 0,date('m'), $dia, date('Y')));
	$any = date("Y", mktime(0, 0, 0,date('m'), $dia, date('Y')));
	$dia = date("d", mktime(0, 0, 0,date('m'), $dia, date('Y')));

	return array($any,$mes,$dia);
	
}

function sum_rest_dies($dies,$dia_min,$mes_min,$any_min){
	
	$dia=$dia_min+$dies;
	$mes = date("m", mktime(0, 0, 0,$mes_min, $dia, $any_min));
	$any = date("Y", mktime(0, 0, 0,$mes_min, $dia, $any_min));
	$dia = date("d", mktime(0, 0, 0,$mes_min, $dia, $any_min));
	return array($any,$mes,$dia);
}

function sumar_hores_min($hores1,$minuts1,$hores2,$minuts2){
	$hores=$hores1+$hores2;
	$minuts=$minuts1+$minuts2;
	if($minuts>59){
		(int)$h=$minuts/60;
		(int)$minuts=$minuts % 60;
		(int)$hores=$hores+(int)$h;
	}
	return array($hores,$minuts);
}
function nom_area($id_persona){
	
	require("conexio.php");

	$con=mysqli_query($cnx_intranet,"SELECT * FROM personal WHERE id_persona=".$id_persona);
	$fila=mysqli_fetch_array($con);
	
	$con1=mysqli_query($cnx_intranet,"SELECT * FROM orga1 WHERE id_orga1=".$fila["id_orga1"]);
	$fila1=mysqli_fetch_array($con1);
	echo $fila1["nom_orga1"]."<br />";
	
	if($fila["id_orga2"]!=0){
		$con1=mysqli_query($cnx_intranet,"SELECT * FROM orga2 WHERE id_orga2=".$fila["id_orga2"]);
		$fila1=mysqli_fetch_array($con1);
		echo $fila1["nom_orga2"]."<br>";
	}
	if($fila["id_orga3"]!=0){
		$con1=mysqli_query($cnx_intranet,"SELECT * FROM orga3 WHERE id_orga3=".$fila["id_orga3"]);
		$fila1=mysqli_fetch_array($con1);
		echo $fila1["nom_orga3"]."<br>";
	}
	if($fila["id_orga4"]!=0){
		$con1=mysqli_query($cnx_intranet,"SELECT * FROM orga4 WHERE id_orga4=".$fila["id_orga4"]);
		$fila1=mysqli_fetch_array($con1);
		echo $fila1["nom_orga4"]."<br>";
	}
	
	
}
function convert_temps($temps){
	$temps=str_replace(',','.',$temps);
	$temps=str_replace("\'",'.',$temps);
	$temps=str_replace(":",'.',$temps);
	
	$t=explode('.',$temps);
	
	return array($t[0],$t[1]);
}
function diferencia($v1,$v2,$signe){

	//inici
	$valor1=str_replace('-','',$v1);
	$valor1=explode('.',$valor1);
	$v1m=$valor1[1]; 
	if(strlen($v1m)==1){
		$v1m=$v1m."0";
	}elseif(strlen($v1m)>2){
		$v1m=substr($v1m,0,2);
	}
	if($valor1[0]!=0){
		$inici=($valor1[0]*60)+$v1m;
	}else{
		$inici=$v1m;	
	}

	if(substr($v1,0,1)=='-'){ $inici="-".$inici;}
	
	//fi
	$valor2=str_replace('-','',$v2);
	$valor2=explode('.',$valor2);
	$v2m=$valor2[1];
	if(strlen($v2m)==1){
		$v2m=$v2m."0";
	}elseif(strlen($v2m)>2){
		$v2m=substr($v2m,0,2);
	}
	if($valor2[0]!=0){
		$fi=($valor2[0]*60)+($v2m);
	}else{
		$fi=$v2m;	
	}

	if(substr($v2,0,1)=='-'){ $fi="-".$fi;}

	//signe=1
	if($signe==1){
		//echo $inici." ".$fi." ";
		$minuts=($inici)+($fi);

		$hores=(int)($minuts/60);
		$min=$minuts%60;
		$min=str_replace('-','',$min);
		if(strlen($min)==1){ $min="0".$min;}
		$temps=$hores.".".$min;
		return ($temps);
	}
	
	//signe=0
	elseif($signe==0){
		if($fi<$inici){
			$minuts=$inici-$fi;

			$hores=(int)($minuts/60);
			$min=$minuts%60;
			$min=str_replace('-','',$min);
			if(strlen($min)==1){ $min="0".$min;}
			$temps=$hores.".".$min;
			return ($temps);
		}else{
			$minuts=$fi-$inici;

			$hores=(int)($minuts/60);
			$min=$minuts%60;
			$min=str_replace('-','',$min);
			if(strlen($min)==1){ $min="0".$min;}
			$temps=$hores.".".$min;
			return (-$temps);
		
		}
	}

}

function neteja($netejar){
  $netejar = preg_replace("/<+\s*\/*\s*([A-Z][A-Z0-9]*)\b[^>]*\/*\s*>+/i","",$netejar);
 
  $netejar = str_replace("'","",$netejar);  
  $netejar = str_replace("\"","",$netejar);
 
  $netejar = str_replace(";","",$netejar);
  $netejar = str_replace("drop","",$netejar);
  $netejar = str_replace("--","",$netejar);
  $netejar = str_replace("=","",$netejar);
  $netejar = str_replace("*","",$netejar);
  $netejar = str_replace("%","",$netejar);
  $netejar = str_replace("[","",$netejar);
  $netejar = str_replace("]","",$netejar);
  $netejar = str_replace("..","",$netejar);
  $netejar = str_replace("/","",$netejar);
  $netejar = str_replace("(","",$netejar);
  $netejar = str_replace(")","",$netejar);
  $netejar = str_replace(">","",$netejar);
  $netejar = str_replace("<","",$netejar);
  $netejar = str_replace("&&","",$netejar);
  $netejar = str_replace("!","",$netejar);
  $netejar = str_replace("||","",$netejar);
  $netejar = str_replace(" and ","",$netejar);
  $netejar = str_replace(" or ","",$netejar);
  $netejar = str_replace(" xor ","",$netejar);
  $netejar = str_replace(" union ","",$netejar);
  $netejar = str_replace(" null ","",$netejar);
  $netejar = str_replace(" select ","",$netejar);
  $netejar = str_replace(" from ","",$netejar);
  $netejar = str_replace(" personal ","",$netejar);
  return ($netejar);
 
}

function veure_codi($id_inventari){

	$codi="";
	$con=mysqli_query($cnx_intranet,"select * from inventari where camp=1 and id_inventari=".$id_inventari);
	$fila=mysqli_fetch_array($con);
	if($fila["valor"]==1){ $codi="O";}
	elseif($fila["valor"]==2){ $codi="I";}
	elseif($fila["valor"]==3){ $codi="P";}
	elseif($fila["valor"]==4){ $codi="M";}
	elseif($fila["valor"]==5){ $codi="E";}
	elseif($fila["valor"]==6){ $codi="S";}
	elseif($fila["valor"]==7){ $codi="A";}
	$codi.="-";
	for($i=strlen($fila["id_inventari"]);$i<=4;$i++){
		$codi.="0";	
	}
	$codi.=$id_inventari;
	return ($codi);
}



function calendari_visual($data){
	if(($data!="")&&($data!=0)){
		return (substr($data,6,2)."/".substr($data,4,2)."/".substr($data,0,4));
	}
	return ("");
}

function calendari_guardar($data){
	
	$sortida="";
	
	if((strlen($data)==10)&&($data!="")){ 
		$any=substr($data,6,4);
		$mes=substr($data,3,2);
		$dia=substr($data,0,2);
		if(($any>1900)&&($mes>=1)&&($mes<=12)&&($dia>=1)&&($dia<=31)){
			$sortida=$any.$mes.$dia;
		}
	}
	return ($sortida);
}

function chunk_split_unicode($str, $l = 76, $e = "\r\n") {
    $tmp = array_chunk(
        preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY), $l);
    $str = "";
    foreach ($tmp as $t) {
        $str .= join("", $t) . $e;
    }
    return $str;
}


$tipus_sol_mobil=array(1=>"Alta",2=>"Reposició",3=>"Baixa");

$array_tipusnomina = array(0=>"Nómina", 1=>"Nómina extra", 2=>"Nómina especial",3=>"Certificat de retencions");

$tipus_tasca=array(1=>"Urgència",2=>"Data programada",3=>"Sense data programada");


$total_carpes=5;
?>
