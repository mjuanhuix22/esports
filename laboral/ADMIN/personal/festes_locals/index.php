<div class="quadre_desplegables">
<form name="form_select" method="post" action="">
<span class="lletraNegreta">Treballador:</span>
	  <select name="any" class="lletraNegreta" onChange="submit()">
        <?php for($any=(date('Y')-1);$any<=(date('Y')+1);$any++){?>
        <option value="<?php echo   $any;?>" 
		<?php if( (isset($_POST["any"]))&&($_POST["any"]==$any)){ echo "selected";}?>><?php echo   $any;?></option>
        <?php }?>
      </select>
  <?php include("comuns3.php");?>
</form>
</div>

<?php
if( (isset($_POST["any"]))&&($_POST["any"]!="")){
	 if(isset($_POST["guardar_locals"])){
		mysqli_query($cnx_intranet,"delete from festes where any=".$_POST["any"]." and id_muni!=0");
		mysqli_query($cnx_intranet,"delete from t_absencies_jornada where any=".$_POST["any"]." and (id_concepte=121 or id_concepte=16)");
		
		//Guardar festes municipals
		$con=mysqli_query($cnx_intranet,"select * from municipis order by id_muni");
		while($fila=mysqli_fetch_array($con)){
			
			$dia1="dia_1_".$fila["id_muni"];
			$mes1="mes_1_".$fila["id_muni"];
			if(($_POST["$dia1"]!='')&&($_POST["$mes1"]!='')){
				$ins=mysqli_query($cnx_intranet,"insert into festes  SET 
				dia=".$_POST["$dia1"].",
				mes=".$_POST["$mes1"].",
				any=".$_POST["any"].", 
				id_muni=".$fila["id_muni"]);
			}
			$dia2="dia_2_".$fila["id_muni"];
			$mes2="mes_2_".$fila["id_muni"];	
			if(($_POST["$dia2"]!='')&&($_POST["$mes2"]!='')){
				$ins=mysqli_query($cnx_intranet,"insert into festes  SET 
				dia=".$_POST["$dia2"].",
				mes=".$_POST["$mes2"].",
				any=".$_POST["any"].", 
				id_muni=".$fila["id_muni"]);
			}
			
		}
		
		//Assigno locals
		$con=mysqli_query($cnx_intranet,"select * from festes where id_muni!=0 and any=".$_POST["any"]);
		while($fila=mysqli_fetch_array($con)){
			
			$data_festa=convert_data8($fila["any"],$fila["mes"],$fila["dia"]);
			$con1=mysqli_query($cnx_intranet,"select * from t_contracte where inici<=".$data_festa." and (fi>=".$data_festa." or fi='')");
			while($fila1=mysqli_fetch_array($con1)){
				
				//nomes se li assignen les festes dels dies que li toca treballar segons contracte 
				$con2=mysqli_query($cnx_intranet,"SELECT * FROM t_contracte_hores WHERE 
				id_contracte=".$fila1["id_contracte"]." and diaset=".dia7($fila["any"],$fila["mes"],$fila["dia"]));
				if(mysqli_num_rows($con2)!=0){
					
					//Assigno les festes municipals que dependran del municipi que està el seu servei
					$con2=mysqli_query($cnx_intranet,"select zedificis.id_muni 
					from personal,zedificis where id_persona=".$fila1["id_persona"]." and 
					personal.id_edifici=zedificis.id_edifici");
					$fila2=mysqli_fetch_array($con2);
					$id_muni=$fila2["id_muni"];
					
					
					if($id_muni==$fila["id_muni"]){			
						$ins=mysqli_query($cnx_intranet,"insert into t_absencies_jornada set 
						id_persona=".$fila1["id_persona"].",
						any=".$fila["any"].",
						mes=".$fila["mes"].",
						dia=".$fila["dia"].",
						id_concepte=16");
					}
				}
			}
		}
		

		msn("Les festes han estat guardades correctament",1);
	}
	include("form_festes.php");
}
?>