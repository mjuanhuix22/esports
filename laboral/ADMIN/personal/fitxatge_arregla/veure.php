<TABLE border="1" cellpadding="5" cellspacing="0">
    <TR>
        <TD>Data</TD>
        <td>Treballador</td>
        <td>Fitxatge arreglat</td>
        <td>Qui</td>
    </TR>

<?php
$cons="";
if(($_POST["mes_inici"]!="")&&($_POST["any_inici"]!="")){
	$cons=" and (
	(any>".$_POST["any_inici"].") or 
	(any=".$_POST["any_inici"]." and mes>=".$_POST["mes_inici"].")
	)";
}
if(($_POST["mes_fi"]!="")&&($_POST["any_fi"]!="")){
	$cons.=" and (
	(any<".$_POST["any_fi"].") or 
	(any=".$_POST["any_fi"]." and mes<=".$_POST["mes_fi"].")
	)";
}
if($_POST["id_persona"]!=""){
	$cons.=" and personal.id_persona=".$_POST["id_persona"];
}

$con=mysqli_query($cnx_intranet,"SELECT * FROM t_f_arregla,personal,t_visual where 
personal.id_persona=t_f_arregla.id_persona and 
personal.id_persona=t_visual.treballador and 
t_visual.visualitzador=".$_SESSION["id_usuari"]." 
".$cons." order by any DESC, mes DESC, dia DESC, cognom1");
while($fila=mysqli_fetch_array($con)){?>
	<tr>	
		<td><?php echo   $fila["dia"]."/".$fila["mes"]."/".$fila["any"];?></td>
    	<td><?php echo   $fila["cognom1"]." ".$fila["cognom2"]." ,".$fila["nom"];?></td>
        <td>
		<?php if($fila["id_fitxatge"]!=0){
		
			$con1=mysqli_query($cnx_intranet,"select * from t_fitxatge 
			where id_fitxatge=".$fila["id_fitxatge"]);
			$fila1=mysqli_fetch_array($con1);
			if(($fila["entra"]==1)&&($fila["surt"]==1)){
				echo "Treball de les ".$fila1["entra_h"].":".$fila1["entra_m"];
				echo " a ".$fila1["surt_h"].":".$fila1["surt_m"]."h.";
			}else{
				if($fila["entra"]==1){
					echo "Entra a treballar a ".$fila1["entra_h"].":".$fila1["entra_m"];
				}
				if($fila["surt"]==1){
					echo "Surt del treball a ".$fila1["surt_h"].":".$fila1["surt_m"]."h.";
				}
			}
		}
		if($fila["id_descans"]!=0){
		
			$con1=mysqli_query($cnx_intranet,"select * from t_f_descans  
			where id_descans=".$fila["id_descans"]);
			$fila1=mysqli_fetch_array($con1);
			if(($fila["entra"]==1)&&($fila["surt"]==1)){
				echo "Descans de les ".$fila1["surt_h"].":".$fila1["surt_m"];
				echo " a ".$fila1["torna_h"].":".$fila1["torna_m"]."h.";				
			}else{
				if($fila["entra"]==1){
					echo "Surt a descansar a ".$fila1["surt_h"].":".$fila1["surt_m"];
				}
				if($fila["surt"]==1){
					echo "Torna del config a ".$fila1["torna_h"].":".$fila1["torna_m"]."h.";
				}
			}
		}
		if($fila["id_exterior"]!=0){
			$con1=mysqli_query($cnx_intranet,"select * from t_f_exterior  
			where id_exterior=".$fila["id_exterior"]);
			$fila1=mysqli_fetch_array($con1);

			if(($fila["entra"]==1)&&($fila["surt"]==1)){
				echo "Treball exterior de les ".$fila1["surt_h"].":".$fila1["surt_m"];
				echo " a ".$fila1["torna_h"].":".$fila1["torna_m"]."h.";				
			}else{		
				if($fila["entra"]==1){
					echo "Surt per treball exterior a ".$fila1["surt_h"].":".$fila1["surt_m"];
				}if($fila["surt"]==1){
					echo "Torna del treball exterior ".$fila1["torna_h"].":".$fila1["torna_m"]."h.";
				}
			}
		}?>
         </td> 
         <td><?php if($fila["rrhh"]==1){ echo "RRHH";}else{ echo "el treballador";}?></td>    
    </tr>
<?php }?>
</TABLE>