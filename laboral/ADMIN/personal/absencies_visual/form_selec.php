<script language="javascript">
function validar_llistat(){
	if((window.document.form_llistat.mes.value=="")||
	(window.document.form_llistat.any.value=="")){
		alert("Es obligatori omplir el camp mes i any");
		return (false);
	}else{
		return (true);
	}	
}

function validar_calendari(){
	if((window.document.form_calendari.id_persona.value=="")||
	(window.document.form_calendari.any.value=="")){
		alert("Es obligatori omplir el camp treballador i any");
		return (false);
	}else{
		return (true);
	}	
}
function validar_conceptes(){
	if(window.document.form_permisos.id_concepte.value==""){
		alert("Es obligatori omplir el camp permis");
		return (false);
	}else{
		return (true);
	}	
}
</script>

<div class="quadre_desplegables">
<form  method="post" name="form_llistat" >

<?php if(isset($_POST["mes_inici"])){
	$mes_inici=$_POST["mes_inici"];	
	$any_inici=$_POST["any_inici"];
	$mes_fi=$_POST["mes_fi"];
	$any_fi=$_POST["any_fi"];
}else{
	$mes_inici=date('m');	
	$any_inici=date('Y');
	$mes_fi=date('m');
	$any_fi=date('Y');
}?>


      <span class="lletraNegreta">Inici</span>      
      <select name="mes_inici"  class="lletraNorm">
        <?php for($i=1;$i<=12;$i++){?>
        <option value="<?php echo   $i;?>" <?php if($i==$mes_inici){ echo "selected";}?>><?php echo   $nom_mes[$i];?></option>
        <?php }?>
      </select>
	    <select name="any_inici"  class="lletraNorm">
         <?php 
		for($i=(date('Y')-1);$i<=(date('Y')+1);$i++){?>
        <option value="<?php echo   $i;?>" <?php if($i==$any_fi){ echo "selected";}?>><?php echo   $i;?></option>
        <?php }?>
        </select>
        
        
       <span class="lletraNegreta">Fi</span>      
      <select name="mes_fi"  class="lletraNorm">
        <?php for($i=1;$i<=12;$i++){?>
        <option value="<?php echo   $i;?>" <?php if($i==$mes_fi){ echo "selected";}?>><?php echo   $nom_mes[$i];?></option>
        <?php }?>
      </select>
	    <select name="any_fi"  class="lletraNorm">
         <?php 

		for($i=(date('Y')-1);$i<=(date('Y')+1);$i++){?>
        <option value="<?php echo   $i;?>" <?php if($i==$any_fi){ echo "selected";}?>><?php echo   $i;?></option>
        <?php }?>  
      </select>
      
      <?php if($_SESSION["perfil"]!="CBSS"){?>
	    <span class="lletraNegreta">Area</span>
	    <select name="organigrama"  onchange="submit();">
	  <option value=""></option>
	  <?php  
	  $con1=mysqli_query($cnx_intranet,"SELECT * FROM orga1 ");
	  while($fila1=mysqli_fetch_array($con1)){?>
      	<option value="<?php echo   $fila1["id_orga1"];?>" 
            class="orga1_<?php echo   $fila1["id_orga1"];?>"
            <?php if((isset($_POST["organigrama"]))&&($_POST["organigrama"]==$fila1["id_orga1"])){ echo "selected";}?>
			><?php echo   $fila1["nom_orga1"]?></option>

	  <?php }?>
	</select>
	<?php }?>

<input type="submit" value="" name="veure_llistat" class="boto_visual" onclick="return validar_llistat();">
<?php include("comuns3.php");?>
</form>
</div>



<div class="quadre_desplegables">
<form  method="post" name="form_calendari" >
       
       <span class="lletraNegreta">Treballador</span>      
		<select name="id_persona"class="lletraNorm">
      <option value=""></option>
      <?php 
	  //Surt tot el personal que treballa o treballava i que hagi tingut un contracte
	  $con=mysqli_query($cnx_intranet,"select * from personal,t_visual where 
	  personal.id_persona=t_visual.treballador and 
	  personal.exist=1 and 
	  t_visual.visualitzador=".$_SESSION["id_usuari"]." 
	  order by personal.cognom1");
	  while($fila=mysqli_fetch_array($con)){?>
      <option value="<?php echo   $fila["id_persona"];?>" <?php if((isset($_POST["id_persona"]))&&($fila["id_persona"]==$_POST["id_persona"])){ echo "selected";}?>><?php echo   $fila["cognom1"]." ".$fila["cognom2"]." ,".$fila["nom"];?></option>
      <?php }?>
    </select>
    
      <span class="lletraNegreta">Any</span> 
	    <select name="any"  class="lletraNorm">
        <option value=""></option>
        <?php 
		for($i=(date('Y')-1);$i<=(date('Y')+1);$i++){?>
        <option value="<?php echo   $i;?>" <?php if((isset($_POST["any"]))&&($i==$_POST["any"])){ echo "selected";}?>><?php echo   $i;?></option>
        <?php }?>
      </select>
    
<input type="submit" value="" name="veure_calendari" class="boto_visual" onclick="return validar_calendari();" >
<?php include("comuns3.php");?>
</form>
</div>


<div class="quadre_desplegables">
<form  method="post" name="form_permisos" >
       
       <span class="lletraNegreta">Permés retribuït</span>      
		<select name="id_concepte"class="lletraNorm">
      <option value=""></option>
      <?php 
	  //Surt tot el personal que treballa o treballava i que hagi tingut un contracte
	  $con=mysqli_query($cnx_intranet,"select * from t_conceptes where exist=1 order by nom_concepte");
	  while($fila=mysqli_fetch_array($con)){?>
      <option value="<?php echo   $fila["id_concepte"];?>" <?php if((isset($_POST["id_concepte"]))&&($fila["id_concepte"]==$_POST["id_concepte"])){ echo "selected";}?>><?php echo   $fila["nom_concepte"];?></option>
      <?php }?>
    </select>
    
      <span class="lletraNegreta">Any</span> 
	    <select name="any"  class="lletraNorm">
        <option value=""></option>
        <?php 
		for($i=(date('Y')-1);$i<=(date('Y')+1);$i++){?>
        <option value="<?php echo   $i;?>" <?php if((isset($_POST["any"]))&&($i==$_POST["any"])){ echo "selected";}?>><?php echo   $i;?></option>
        <?php }?>
      </select>
<input type="submit" value="" name="veure_conceptes" class="boto_visual" onclick="return validar_conceptes();" ><?php include("comuns3.php");?>
</form>
</div>