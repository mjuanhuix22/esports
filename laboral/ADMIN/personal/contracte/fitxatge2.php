<?php 
//recorro tots els dies del nou periode de vigencia

$inici=convert_data8($_POST["any_inici"],$_POST["mes_inici"],$_POST["dia_inici"]);
if($inici<data_actual8()){
	$dia=$_POST["dia_inici"];
	$mes=$_POST["mes_inici"];
	$any=$_POST["any_inici"];
}else{
	$dia=(int)date('d');
	$mes=(int)date('m');
	$any=date('Y');
}

$fi=convert_data8($_POST["any_fi"],$_POST["mes_fi"],$_POST["dia_fi"]);
if(($fi!='')&&($fi<data_actual8())){
	$dia_fi=$_POST["dia_fi"];
	$mes_fi=$_POST["mes_fi"];
	$any_fi=$_POST["any_fi"];
}else{
	$dia_fi=(int)date('d');
	$mes_fi=(int)date('m');
	$any_fi=date('Y');
}

if($dia_fi>total_dies($mes_fi,$any_fi)){ 
	$dia_fi=total_dies($mes_fi,$any_fi);
}

while(($dia!=$dia_fi)||($mes!=$mes_fi)||($any!=$any_fi)){
	descuadra_fit($any,$mes,$dia,$_POST["id_persona"]);
	if($dia==total_dies($mes,$any)){
		$dia=1;
		if($mes==12){
			$mes=1;
			$any=$any+1;
		}else{
			$mes=$mes+1;
		}
		
	}else{
		$dia++;
	}
}
?>
