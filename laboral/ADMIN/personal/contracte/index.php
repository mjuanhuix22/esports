<?php
include("form_select.php");
if(isset($_POST["contracte_nou"])){
	include("nou/validar_periode.php");
	if($validat==1){
		
		include("nou/guardar.php");
		include("guardar_assum_vac.php");
		include("festes_guardar.php");
		include("fitxatge2.php");
	}
	
}
elseif((isset($_POST["funcio"]))&&($_POST["funcio"]=="eliminar")){
	include("eliminar.php");
}
elseif((isset($_POST["funcio"]))&&($_POST["funcio"]=="guardar")){
	include("validar_periode.php");//Comprovo que no hi hagi un altre contracte que inclogui algun dia d'aquest periode de vigencia
	if($validat==1){
		include("festes_eliminar.php");
		include("fitxatge1.php");
		include("guardar.php");
		//include("guardar_assum_vac.php");
		include("festes_guardar.php");
		include("fitxatge2.php");
	}
}
elseif((isset($_POST["funcio"]))&&($_POST["funcio"]=="guardar_tanca")){
	
	
	//Si l'unic que ha canviat és la data de fi
	$con=mysqli_query($cnx_intranet,"select id_contracte from t_contracte where
	inici='".convert_data8($_POST["any_inici"],$_POST["mes_inici"],$_POST["dia_inici"])."'  
	and id_contracte=".$_POST["id_contracte"]);
	if(mysqli_num_rows($con)==1){ 
		
		$fi=convert_data8($_POST["any_fi"],$_POST["mes_fi"],$_POST["dia_fi"]);

			if($fi!=''){
				include("canvi_fi_contracte/festes_eliminar.php");
				include("canvi_fi_contracte/fitxatge1.php");
			}
			include("canvi_fi_contracte/guardar_contracte.php");
			//include("guardar_assum_vac.php");
			include("canvi_fi_contracte/festes_guardar.php");
			
			msn("Contracte guardat correctament",1);
		
	}

}

if((isset($_POST["id_persona"]))&&($_POST["id_persona"]!='')){?>
	<?php include("script.jsp");?>
	<table width="100%">
	<tr><td class="punt_hori"></td></tr>
	<tr bgcolor="#E5E5E5"><td class="lletraNegreta" >Contractes Futurs</td>
	</tr>
	</table>
	<?php $con0=mysqli_query($cnx_intranet,"SELECT * FROM t_contracte where id_persona=".$_POST["id_persona"]."
	and inici>".data_actual8()." order by inici DESC");
	if(mysqli_num_rows($con0)==0){?>
		<p class="incorrecte">No te cap contracte futur</p> 
	<?php }else{
		while($fila0=mysqli_fetch_array($con0)){
			include("contracte.php");
		}
	}?>
	<table width="100%">
	<tr><td class="punt_hori"></td></tr>
	<tr bgcolor="#E5E5E5"><td class="lletraNegreta" >Contracte Actual</td>
	</tr>
	</table>
	<?php $con0=mysqli_query($cnx_intranet,"SELECT * FROM t_contracte where id_persona=".$_POST["id_persona"]."
	and inici<=".data_actual8()." and (fi>=".data_actual8()." or fi=0) order by inici DESC;");
	if(mysqli_num_rows($con0)==0){?>
		<p class="incorrecte">No te cap contracte actual</p> 
	<?php }else{
		$fila0=mysqli_fetch_array($con0);
		include("contracte.php");
	}?>
		<table width="100%">
	<tr><td class="punt_hori"></td></tr>
	<tr bgcolor="#E5E5E5"><td class="lletraNegreta" >Contractes Anteriors</td>
	</tr>
	</table>
	<?php $con0=mysqli_query($cnx_intranet,"SELECT * FROM t_contracte where id_persona=".$_POST["id_persona"]."
	and fi<".data_actual8()." and fi!='' order by inici DESC");
	if(mysqli_num_rows($con0)==0){?>
		<p class="incorrecte">No te cap contracte anterior</p> 
	<?php }else{
		while($fila0=mysqli_fetch_array($con0)){
			include("contracte.php");
		}
	}
	
	include("nou/index.php");
}
?>