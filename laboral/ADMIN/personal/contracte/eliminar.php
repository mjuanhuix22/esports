<?php
//Valido que el treballador NO hagi fitxat cap dia d'aquest periode de contracte
$exist=0;
$dia=$_POST["dia_inici"];
$mes=$_POST["mes_inici"];
$any=$_POST["any_inici"];

if($_POST["dia_fi"]==""){
	//Si la data d'inici es superior a avui
	$data_inici=convert_data8($_POST["any_inici"],$_POST["mes_inici"],$_POST["dia_inici"]);
	if($data_inici>data_actual8){
		$dia_fi=date('d');
		$mes_fi=date('m');
		$any_fi=date('Y');
	}else{
		$dia_fi=$_POST["dia_inici"];
		$mes_fi=$_POST["mes_inici"];
		$any_fi=$_POST["any_inici"];
	}
}else{
	$dia_fi=$_POST["dia_fi"];
	$mes_fi=$_POST["mes_fi"];
	$any_fi=$_POST["any_fi"];
}

while(($dia!=$dia_fi)||($mes!=$mes_fi)||($any!=$any_fi)){
	$con=mysqli_query($cnx_intranet,"select * from t_fitxatge where dia=".$dia." and mes=".$mes." and 
	any=".$any." and id_persona=".$_POST["id_persona"]);
	if(mysqli_num_rows($con)!=0){
		$exist=1;
	}
	if($dia==total_dies($mes,$any)){
		$dia=1;
		if($mes==12){
			$mes=1;
			$any=$any+1;
		}else{
			$mes=$mes+1;
		}
		
	}else{
		$dia++;
	}
}
if($exist==1){
	msn("Aquest contracte no es pot eliminar perque el treballador hi va fitxar",0);
}else{
	include("festes_eliminar.php");
	include("fitxatge1.php");
	mysqli_query($cnx_intranet,"delete from t_contracte where id_contracte=".$_POST["id_contracte"]);
	mysqli_query($cnx_intranet,"delete from t_contracte_hores where id_contracte=".$_POST["id_contracte"]);
	msn("Contracte eliminat correctament",1);
}
?>