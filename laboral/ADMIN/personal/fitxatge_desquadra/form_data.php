<script language="javascript">
function validar_data(){
	var any_inici=window.document.form_seleccio.any_inici.value;
	var mes_inici=window.document.form_seleccio.mes_inici.value;
	var dia_inici=window.document.form_seleccio.dia_inici.value;
	var any_fi=window.document.form_seleccio.any_fi.value;
	var mes_fi=window.document.form_seleccio.mes_fi.value;
	var dia_fi=window.document.form_seleccio.dia_fi.value;
	var incorrecte=false;
	if(any_inici>any_fi) {
		incorrecte=true;
	}else if(any_inici==any_fi){
		if(mes_inici>mes_fi){
			incorrecte=true;
		}
		else if(mes_inici==mes_fi){
			if(dia_inici>dia_fi){
				incorrecte=true;
			}
		}
	}
	if(incorrecte==true){
		alert("ERROR. La data d'inici ha de ser inferior a la data de fi");
		return (false);
	}else{
		return (true);
	}
	
}
</script>

<div class="quadre_desplegables">
  <form  name="form_seleccio" method="post" >


<p><span class="lletraNegreta">De :</span>      
        <?php if(isset($_POST["any_inici"])){ 
	  		$any_inici=$_POST["any_inici"];
			$mes_inici=$_POST["mes_inici"];
			$dia_inici=$_POST["dia_inici"];
			$any_fi=$_POST["any_fi"];
			$mes_fi=$_POST["mes_fi"];
			$dia_fi=$_POST["dia_fi"];
			$id_persona=$_POST["id_persona"];
		}else{ 
			$any_inici=date('Y');
			$mes_inici=1;
			$dia_inici=7;
			list($any_fi,$mes_fi,$dia_fi)=sum_rest_dies_actual(-1);
			$id_persona="";
		}?>  
        <select name="dia_inici"  class="lletraNorm" id="dia_inici">
          <?php 
	  for($i=1;$i<=31;$i++){?>
          <option value="<?php echo   $i;?>" <?php if( $dia_inici==$i){ echo "selected";}?>><?php echo   $i;?></option>
           <?php }?>
        </select>
         <select name="mes_inici" class="lletraNorm" id="mes_inici">
           
           <?php for($i=1;$i<=12;$i++){?>
           <option value="<?php echo   $i;?>" <?php if($mes_inici==$i){ echo "selected";}?>><?php echo   $nom_mes[$i];?></option>
           <?php }?>
         </select>
         <select name="any_inici"  class="lletraNorm" id="any_inici">
           <?php  
		   
		   for($i=(date('Y')-1);$i<=date('Y');$i++){?>
           <option value="<?php echo   $i;?>" <?php if($any_inici==$i){ echo "selected";}?>><?php echo   $i;?></option>
           <?php }?>
         </select>
        
         <span class="lletraBlanca65">al          </span>        
           <select name="dia_fi"  class="lletraNorm" id="dia_fi">
             <?php 
	  for($i=1;$i<=31;$i++){?>
             <option value="<?php echo   $i;?>" <?php if( $dia_fi==$i){ echo "selected";}?>><?php echo   $i;?></option>
             <?php }?>
           </select> 
        <select name="mes_fi" class="lletraNorm" id="mes_fi">
          <?php for($i=1;$i<=12;$i++){?>
          <option value="<?php echo   $i;?>" <?php if($mes_fi==$i){ echo "selected";}?>><?php echo   $nom_mes[$i];?></option>
           <?php }?>
        </select>
        <select name="any_fi"  class="lletraNorm" id="any_fi">
          <?php for($i=(date('Y')-1);$i<=date('Y');$i++){?>
          <option value="<?php echo   $i;?>" <?php if($any_fi==$i){ echo "selected";}?>><?php echo   $i;?></option>
           <?php }?>
        </select>
         <br />
        <span class="lletraNegreta">Treballador:</span>	   
        <select name="id_persona" class="lletraNorm">
          <option value=""></option>
          <?php
	  $con=mysqli_query($cnx_intranet,"select * from personal,t_visual where 
	  t_visual.treballador=personal.id_persona and 
	  t_visual.visualitzador=".$_SESSION["id_usuari"]." and 
	  personal.obli_fitxar=1 and 
	  personal.exist=1 
	  order by personal.cognom1");
	  while($fila=mysqli_fetch_array($con)){?>
          <option value="<?php echo   $fila["id_persona"];?>" <?php if($fila["id_persona"]==$id_persona){ echo "selected";}?>><?php echo   $fila["cognom1"]." ".$fila["cognom2"]." ,".$fila["nom"];?></option>
          <?php }?>
        </select>
        <?php if($_SESSION["perfil"]!="CBSS"){?>
            <span class="lletraNegreta">Area:</span>	
	    <select name="organigrama" >
	  <option value=""></option>
	  <?php 
	  $con1=mysqli_query($cnx_intranet,"SELECT * FROM orga1");
	  while($fila1=mysqli_fetch_array($con1)){?>
      	<option value="<?php echo   $fila1["id_orga1"];?>" 
            class="orga1_<?php echo   $fila1["id_orga1"];?>"
            <?php if((isset($_POST["organigrama"]))&&($_POST["organigrama"]==$fila1["id_orga1"])){ echo "selected";}?>
			><?php echo   $fila1["nom_orga1"]?></option>
      
      <?php 
		
		  $con2=mysqli_query($cnx_intranet,"SELECT * FROM orga2 where id_orga1=".$fila1["id_orga1"]);
		  while($fila2=mysqli_fetch_array($con2)){?>
			<option value="<?php echo   $fila1["id_orga1"]."_".$fila2["id_orga2"];?>" 
            class="orga1_<?php echo   $fila1["id_orga1"];?>"
            <?php if((isset($_POST["organigrama"]))&&($_POST["organigrama"]==($fila1["id_orga1"]."_".$fila2["id_orga2"]))){ echo "selected";}?>
			
			><?php echo   "------>".$fila2["nom_orga2"];?></option>
            
          
            <?php }?>
	  <?php }?>
</select>
            <?php }?>
        
          </p>
        <p class="lletraNorm">VEURE TEMPS:<br />
		
		<input name="hores" type="radio" value="0" <?php 
		if( (!isset($_POST["hores"]))||($_POST["hores"]==0)){ echo "checked=\"checked\"";}?> />
Real
          <input name="hores" type="radio" value="1" <?php 
		if( (isset($_POST["hores"]))&&($_POST["hores"]==1)){ echo "checked=\"checked\"";}?> />
Nomes hores enteres
<br />
        
		<input name="negatius" type="radio" value="0" <?php 
		if( (!isset($_POST["negatius"]))||($_POST["negatius"]==0) ){ echo "checked=\"checked\"";}?> />
		Negatiu i positiu
		
		<input name="negatius" type="radio" value="1" <?php 
		if( (isset($_POST["negatius"]))&&($_POST["negatius"]==1) ){ echo "checked=\"checked\"";}?> />
		Nomes negatiu
		<input name="negatius" type="radio" value="2" <?php 
		if( (isset($_POST["negatius"]))&&($_POST["negatius"]==2) ){ echo "checked=\"checked\"";}?> />
		Nomes positiu
		<br />
		<input name="ordenat" type="radio" value="0" <?php 
		if( (!isset($_POST["ordenat"]))||($_POST["ordenat"]==0) ){ echo "checked=\"checked\"";}?> />
		Ordenat per cognom
		
		<input name="ordenat" type="radio" value="1" <?php 
		if( (isset($_POST["ordenat"]))&&($_POST["ordenat"]==1) ){ echo "checked=\"checked\"";}?> />
		Ordenat per temps 
		</p>
		<p>
          <input type="submit" name="veure" value="" class="boto_visual">
          </p>
<?php include("comuns3.php");?>

  </form>
</div>