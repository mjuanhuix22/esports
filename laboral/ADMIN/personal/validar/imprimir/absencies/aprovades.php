<?php 
/*Visualitzem les absencies aprovades (d'hores o de jornada) que substituran a les sol·licitades: Data de sol·licitud +/- minim 
*/
if($fila2["minim"]!=0){
	//Data solicitud +/- minim
	 list($any_min,$mes_min,$dia_min)=sum_rest_dies($fila2["minim"],substr($data_sol[$a],6,2),substr($data_sol[$a],4,2),substr($data_sol[$a],0,4));
}else{
	$dia_min=substr($data_sol[$a],6,2);$mes_min=substr($data_sol[$a],4,2);$any_min=substr($data_sol[$a],0,4);
}

//Hores
$con0=mysqli_query($cnx_intranet,"select * from t_absencies_hores where 
id_concepte=".$id_concepte[$a]." and
id_persona=".$id_persona[$a]." and ( (any>".$any_min.") or (any=".$any_min." and mes>".$mes_min.") or (any=".$any_min." and mes=".$mes_min." and dia>".$dia_min.") ) 
and validador=0 order by any,mes");

//Jornada

$conk=mysqli_query($cnx_intranet,"select * from t_absencies_jornada where 
id_concepte=".$id_concepte[$a]." and
id_persona=".$id_persona[$a]." and 
((any>".$any_min.") or
(any=".$any_min." and mes>".$mes_min.") or
(any =".$any_min." and mes=".$mes_min." and dia>=".$dia_min.")) and 
validador=0 group by any,mes order by any,mes");

if( (mysqli_num_rows($con0)!=0)||(mysqli_num_rows($conk)!=0) ){?>
	<span class="lletraNorm">Les abs&egrave;ncies sol·licitades substituiran les següents abs&egrave;ncies aprovades:</span><br />
<?php }
if(mysqli_num_rows($con0)!=0){
	include("aprovades_hores.php");
}
if(mysqli_num_rows($conk)!=0){
	include("aprovades_jornada_futur.php");
}

//si es vacances o assumptes veig fets en el passat
if(($carpeta[$a]==8)||($carpeta[$a]==9)){ 

	$conk=mysqli_query($cnx_intranet,"select * from t_absencies_jornada where 
	id_concepte=".$id_concepte[$a]." and
	id_persona=".$id_persona[$a]." and 
	(
	(any<".$any_min.") or 
	(any=".$any_min." and mes<".$mes_min.") or
	(any=".$any_min." and mes=".$mes_min." and dia<".$dia_min.") 
	) and 
	 validador=0 group by mes order by any,mes");
	if(mysqli_num_rows($conk)!=0){?>
		<span class="lletraNorm">Durant l'any <?php echo   $any_min;?> ja ha fet:</span><br />
		<?php include("aprovades_jornada_passat.php");
	}
}
?>
			