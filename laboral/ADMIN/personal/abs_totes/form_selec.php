<script language="javascript">
function validar(){
	if((window.document.form_data.mes.value=="")||
	(window.document.form_data.any.value=="")){
		alert("Es obligatori omplir el camp mes i any");
		return (false);
	}else{
		return (true);
	}	
}
</script>
<table width="100%"  border="0" cellpadding="3" cellspacing="0" bgcolor="#CCCCCC">
	<form action="<?php echo   $PHP_SELF;?>" method="post" name="form_data" onSubmit="return validar()">
  <tr>
    <td>
      <span class="lletraNegreta">Abs&egrave;ncies laborals de tots els treballadors durant el</span>      
      <select name="mes"  class="lletraNorm">
        <option value=""></option>
        <?php for($i=1;$i<=12;$i++){?>
        <option value="<?php echo   $i;?>" <?php if((isset($_POST["mes"]))&&($i==$_POST["mes"])){ echo "selected";}?>><?php echo   $nom_mes[$i];?></option>
        <?php }?>
      </select>
	  <span class="lletraNegreta">del 
      </span> 
	    <select name="any"  class="lletraNorm">
        <option value=""></option>
        <?php 
		
			$seguent=date('Y')+1;
		
		for($i=2009;$i<=$seguent;$i++){?>
        <option value="<?php echo   $i;?>" <?php if((isset($_POST["any"]))&&($i==$_POST["any"])){ echo "selected";}?>><?php echo   $i;?></option>
        <?php }?>
      </select>
       
	  <input type="submit" value="Ok" class="lletraNorm" >
      <?php include("comuns3.php");?>
   </td>
  </tr>
   </form>
</table>  
