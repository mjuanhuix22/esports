
<table width="100%"  cellspacing="0" cellpadding="0" border="1">
  <?php 

$id_area=0;
$id_servei=0;
$diatreball=array();

  
//veig el personal que en l'epoca seleccionada tenia contracte i veig el personal agrupat per arees

$data_seleccionada=convert_data8($_POST["any"],$_POST["mes"],"01");
$con=mysqli_query($cnx_intranet,"select * from personal,t_contracte,t_visual where 
personal.id_persona=t_contracte.id_persona and 
personal.id_persona=t_visual.treballador and 
t_visual.visualitzador=".$id_usuari." and 
t_contracte.inici<=".$data_seleccionada." and
(t_contracte.fi>=".$data_seleccionada." or t_contracte.fi='') 
group by personal.id_persona 
order by personal.id_area,personal.id_servei,personal.cognom1");
while($fila=mysqli_fetch_array($con)){
	  
	   if($fila["id_area"]!=$id_area){?>
				<tr><td colspan="<?php echo   $total_dies[$_POST["mes"]]+1;?>"  class="lletraBlanca85" height="20" bgcolor="#000000" >&#8226;
				<?php $con1=mysqli_query($cnx_intranet,"select * from arees where id_area=".$fila["id_area"]);
				$fila1=mysqli_fetch_array($con1);
				echo $fila1["nom_area"];
				$id_area=$fila["id_area"];
				$id_servei=0;
				?></td>
				</tr>
	  <?php }?>
	  <?php if($fila["id_servei"]!=$id_servei){?>
				<tr><td colspan="<?php echo   $total_dies[$_POST["mes"]]+1;?>"  class="lletraBlanca75" height="15" bgcolor="#494949">
				<?php $con1=mysqli_query($cnx_intranet,"select * from arees_serveis where id_servei=".$fila["id_servei"]);
				$fila1=mysqli_fetch_array($con1);
				echo $fila1["nom_servei"];
				$id_servei=$fila["id_servei"];
				?></td>
				</tr>
	  <?php }?>
	  <tr><td class="lletraNorm"><?php echo   $fila["cognom1"]." ".$fila["cognom2"]." ,".$fila["nom"];?></td>
	  
		  <?php for($dia=1;$dia<=$total_dies[$_POST["mes"]];$dia++){?>
				<td  
				<?php $id_concepte="";
					$class="";
					
					//miro les hores que havia d'haver fet en la taula t_contracte
					
					$data_a=convert_data8($_POST["any"],$_POST["mes"],$dia);
					$hores_obli=0;$minuts_obli=0;
					$con1=mysqli_query($cnx_intranet,"SELECT * FROM t_contracte WHERE id_persona=".$fila["id_persona"]." 
					and inici<=".$data_a." and (fi>=".$data_a." or fi='')");
					if(mysqli_num_rows($con1)==1){
						$fila1=mysqli_fetch_array($con1);
						$con1=mysqli_query($cnx_intranet,"SELECT * FROM t_contracte_hores WHERE 
						id_contracte=".$fila1["id_contracte"]." and diaset=".dia7($_POST["any"],$_POST["mes"],$dia));
						while($fila1=mysqli_fetch_array($con1)){
							list($hores_cont,$minuts_cont)=restar_temps($fila1["a_h"],$fila1["a_m"],$fila1["de_h"],$fila1["de_m"]);
							list($hores_obli,$minuts_obli)=sumar_temps($hores_obli,$minuts_obli,$hores_cont,$minuts_cont);
						}
					}
		
					if(($hores_obli==0)&&($minuts_obli==0)){ 
							$class=" dia_festiu";
					}
					
					
					
					$con1=mysqli_query($cnx_intranet,"SELECT * FROM t_horari,t_horari_sol WHERE 
					t_horari.menys=1 and t_horari_sol.id_motiu!=19 and 
					t_horari.id_sol=t_horari_sol.id_sol and 
					t_horari_sol.id_persona=".$fila["id_persona"]." and 
					t_horari.dia=".$dia." and t_horari.mes=".$_POST["mes"]."
					 and t_horari.any=".$_POST["any"]);
					if(mysqli_num_rows($con1)!=0){
						$fila1=mysqli_fetch_array($con1);
						$con1=mysqli_query($cnx_intranet,"SELECT * FROM t_horari_sol,t_horari_motiu WHERE 
						t_horari_sol.id_sol=".$fila1["id_sol"]." and 
						t_horari_motiu.id_motiu=t_horari_sol.id_motiu");
						$fila1=mysqli_fetch_array($con1);
						if($fila1["menys"]==1){
							$class=" demenys lletraPeke";
						}else{
							$class=" compensa lletraPeke";
						}
						
					}
					
					//Faig que es vegi les hores dels treballadors del CAAS que estan a 't_horari_sol' com a jornada normal
					$con1=mysqli_query($cnx_intranet,"SELECT * FROM t_horari,t_horari_sol WHERE 
					t_horari_sol.id_motiu=19 and 
					t_horari.id_sol=t_horari_sol.id_sol and 
					t_horari_sol.id_persona=".$fila["id_persona"]." and 
					t_horari.dia=".$dia." and t_horari.mes=".$_POST["mes"]."
					 and t_horari.any=".$_POST["any"]);
					if(mysqli_num_rows($con1)!=0){
						$class="";
					}
					
					
					
					//Si té una absencia de jornada
					if($class==""){
						$con1=mysqli_query($cnx_intranet,"select * from t_absencies_jornada where
						id_persona=".$fila["id_persona"]." and dia=".$dia." and 
						mes=".$_POST["mes"]." and any=".$_POST["any"]);
						while($fila1=mysqli_fetch_array($con1)){
							$id_concepte=$fila1["id_concepte"];
							if($fila1["validador"]==0){
								$class="concepte_".$fila1["id_concepte"];
							}else{
								$class="concepte_".$fila1["id_concepte"]." temporal";
							}
						}
					}
					

					//Si té una absencia de jornada
					if($class==""){
						$con1=mysqli_query($cnx_intranet,"select * from t_absencies_hores where
						id_persona=".$fila["id_persona"]." and dia=".$dia." and 
						mes=".$_POST["mes"]." and any=".$_POST["any"]);
						while($fila1=mysqli_fetch_array($con1)){
							$id_concepte=$fila1["id_concepte"];
							if($fila1["validador"]==0){						
								$class="concepte_".$fila1["id_concepte"];
							}else{
								$class="concepte_".$fila1["id_concepte"]." temporal";
							}
						}
					}
				
				?> class="lletraPeke <?php echo   $class;?>"><?php 
				if($id_concepte!=""){
					$con3=mysqli_query($cnx_intranet,"select * from t_conceptes where id_concepte=".$id_concepte);
					$fila3=mysqli_fetch_array($con3);
					echo "<a  title=\"".$fila3["nom_concepte"]."\">".$dia."</a>";
				}elseif($class!=""){
					echo "<a  title=\"Dia NO laboral\">".$dia."</a>";
				}else{
					echo $dia;
				}?></td>
		<?php }?>
	  </tr>
  <?php }?>
  
</table>
