<style type="text/css">
.veure_si{display:inline;}
.veure_no{ display:none;}
select,div{ margin-bottom:10px; display:block;}
</style>
<script language="javascript">
	function canvi(){
		document.getElementById("ambdies").className="veure_no";
		document.getElementById("noveu_treballador").className="veure_no";
		document.getElementById("minim").className="veure_no";
		document.getElementById("justificant").className="veure_no";
		document.getElementById("resta_desc").className="veure_no";
		
		if( ((document.getElementById("carpeta").value==1)||(document.getElementById("carpeta").value==2))
		&&(document.getElementById("solicitud").value==1)){	
			document.getElementById("ambdies").className="veure_si";
			document.getElementById("noveu_treballador").className="veure_si";
		}
		
		if(document.getElementById("solicitud").value==1){
			document.getElementById("minim").className="veure_si";
			document.getElementById("justificant").className="veure_si";
		}
		if(document.getElementById("carpeta").value==2){
			document.getElementById("resta_desc").className="veure_si";
		}
	}
</script>

<?php 
if($_POST["id_concepte"]!="nou"){
	$con=mysqli_query($cnx_intranet,"SELECT * FROM t_conceptes where id_concepte=".$_POST["id_concepte"]);
	$fila=mysqli_fetch_array($con);
	$carpeta=$fila["carpeta"];
	$nom_concepte=$fila["nom_concepte"];
	$dies_retribuits=$fila["dies_retribuits"];
	$minim=$fila["minim"];
	$resta_descans=$fila["resta_descans"];
	$normes=$fila["normes"];
	$solicitud=$fila["solicitud"];
	$just=$fila["just"];
	$color=$fila["color"];
	$noveu_treballador=$fila["noveu_treballador"];
	$dates=explode(',',$fila["dates"]);
}else{
	$carpeta=2;
	$nom_concepte="";
	$dies_retribuits="";
	
	$minim="";
	$resta_descans=1;
	$normes="";
	$solicitud=1;
	$just="";
	$color="";
	$noveu_treballador="";
}?>
<form name="form_normes" method="post" action="">


<p><input type="text" name="nom_concepte" value="<?php echo   $nom_concepte?>" size="50"/></p>

<?php if(($carpeta==1)||($carpeta==2)){ ?>
	<select name="carpeta" id="carpeta" onchange="canvi();">
    <option value="1" <?php if($carpeta==1){?> selected="selected"<?php }?>>Permis que afecta tota la jornada</option>
    <option value="2" <?php if($carpeta==2){?> selected="selected"<?php }?>>Permis que afecta algunes hores o tota la jornada</option>
	</select>
<?php }else{?>
	<input type="hidden" name="carpeta" value="<?php echo   $carpeta;?>" />
   
<?php }?>

<select name="resta_descans" id="resta_desc" class="<?php if($carpeta!=2){ echo "veure_no";}?>">
	<option value="1" <?php if($resta_descans==1){?> selected="selected"<?php }?>>Resta minuts de descans</option>
    <option value="0" <?php if($resta_descans==0){?> selected="selected"<?php }?>>No resta minuts de descans</option>
</select>


<select name="solicitud" id="solicitud" onchange="canvi();">
<option value="1" <?php if($solicitud==1){?> selected="selected"<?php }?>>El treballador pot fer la sol·licitud</option>
<option value="0" <?php if($solicitud==0){?> selected="selected"<?php }?>>El treballador NO pot fer la sol·licitud</option>
</select>



<select name="dies_retribuits" id="ambdies" class="<?php if( (($carpeta!=1)&&($carpeta!=2)&&($carpeta!=11))||($solicitud==0)){ echo "veure_no";}?>">
  <option value="0">No hi ha màxim de dies que es poden sol·licitar</option>
  <?php for($i=1;$i<=120;$i++){?>
      <option value="<?php echo   $i;?>" <?php if($dies_retribuits==$i){ echo "selected";}?>
      ><?php echo   "Es poden sol·licitar un màxim de ".$i." ";
	  if($i==1){ echo "dia";}else{ echo "dies";}
	  ?></option>
 <?php }?>
</select>
<div class="<?php if( (($carpeta!=1)&&($carpeta!=2))||($solicitud==0)){ echo "veure_no";}?>" id="noveu_treballador">
<input type="checkbox" name="noveu_treballador"  value="1" <?php if($noveu_treballador==1){?> checked="checked"<?php }?>
/>El treballador NO veu el text anterior.
</div>

<select name="minim" id="minim" class="<?php if($solicitud==0){ echo "veure_no";}?>">
<?php for($i=-10;$i<=15;$i++){?>
  <option value="<?php echo   $i;?>" <?php if($minim==$i){ echo "selected";}?>
  ><?php if($i>0){
        echo "S'ha de sol·licitar amb ".$i." ";
		if($i==1){ echo "dia";}else{ echo "dies";}
		echo " d'antel·lacio";
    }elseif($i==0){
        echo "Es pot sol·licitar el mateix dia";
    }else{
      echo "Es pot dir fins a ". $i." dies despres.";
    }?></option>
<?php }?>
</select> 


<?php if($carpeta==11){ ?>
    Dies a escollir
    <?php for($d=0;$d<count($dates);$d++){?>
        <br>
        <input type="text" name="data_<?php echo $d;?>" value="<?php echo calendari_visual($dates[$d]);?>" size="10">
    <?php }?>
    <br>
    <input type="text" name="data_<?php echo $d;?>" value="" size="10">
<?php } ?>

<div>
Color<span style="background-color:<?php echo   $color?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><input name="color" type="text" value="<?php echo   $color?>" size="9" maxlength="7" /><a href="https://html-color-codes.info/codigos-de-colores-hexadecimales/" target="_blank">https://html-color-codes.info/codigos-de-colores-hexadecimales/</a>
</div>

<div id="justificant" class="<?php if($solicitud==0){ echo "veure_no";}?>">
Si s'ha d'entregar justificant especificar el nom:
<input name="just" type="text" value="<?php echo   StripSlashes($just);?>" size="40" />
</div>

<div>

    <textarea name="normes" vocab="<?php echo $normes ?>" cols="100" rows="5"></textarea>

</div>




<input name="modificar" type="submit" class="lletra_concepte_<?php echo   $_POST["id_concepte"];?>" id="modificar" value="Guardar">

<input name="eliminar" type="submit" class="botovermell_senseamplada" id="eliminar" value="Eliminar">          
         
<input type="hidden" name="id_concepte" value="<?php echo   $_POST["id_concepte"];?>" />
<?php include("comuns3.php");?>
</form>