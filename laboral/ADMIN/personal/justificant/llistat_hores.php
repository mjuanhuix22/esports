<?php 

$SQL="SELECT personal.id_persona,personal.nom,personal.cognom1,personal.cognom2, 
hora_surt,min_surt,hora_torna,min_torna,hora_surt,min_surt,hora_surt_p,min_surt_p,hora_torna_p,min_torna_p,
t_absencies_hores.dia, t_absencies_hores.mes, t_absencies_hores.any, 
t_conceptes.id_concepte,t_conceptes.nom_concepte, justificant  
FROM t_absencies_hores,personal,t_conceptes where 
t_conceptes.just!='' and 
t_absencies_hores.id_persona=personal.id_persona and 
t_absencies_hores.id_concepte=t_conceptes.id_concepte and 
t_absencies_hores.validador=0 and 
(
(t_absencies_hores.any<".date('Y').") or 
(t_absencies_hores.any=".date('Y')." and t_absencies_hores.mes<".date('m').") or 
(t_absencies_hores.any=".date('Y')." and t_absencies_hores.mes=".date('m')." and t_absencies_hores.dia<=".date('d').")
) and entrega_just is null";

if((isset($_POST["b_id_persona"]))&&($_POST["b_id_persona"]!="")){
	$SQL.=" and personal.id_persona=".$_POST["b_id_persona"];
}
$SQL.=" order by t_absencies_hores.any,t_absencies_hores.mes,t_absencies_hores.dia";

$con=mysqli_query($cnx_intranet,$SQL);
if(mysqli_num_rows($con)!=0){

	$print.="<table border='1' cellpadding='5' cellspacing='0'>
	<caption>Abséncies que afecten part de la jornada</caption>
	<tr>
		<th>Data</th>
		<th>Treballador</th>
		<th >Concepte</th>
		<th>Horari sol·licitat</th>
		<th>Horari fet</th>";
		if(!isset($_POST["imprimir"])){ $print.="<td></td>";}

	$print.="</tr>";
	while($fila=mysqli_fetch_array($con)){

			$mal++;
			$print.="<tr>
			<td>".$fila["dia"]." ".$nom_mes[$fila["mes"]]."/".$fila["any"]."</td>
			<td style=\"width:100px\">".$fila["cognom1"]." ".$fila["cognom2"]." ".$fila["nom"]."</td>
			<td style=\"width:100px\">".$fila["nom_concepte"]."</td>
			<td>de ".$fila["hora_surt_p"].":".$fila["min_surt_p"]." a ".$fila["hora_torna_p"].":".$fila["min_torna_p"]."</td>
			<td>";
			if(($fila["hora_surt"]==$fila["hora_torna"])&&($fila["min_surt"]==$fila["min_torna"])){
				$print.="No fitxat correctament";
			}else{
				$print.="de ".$fila["hora_surt"].":".$fila["min_surt"]." a ".$fila["hora_torna"].":".$fila["min_torna"];
			}
			$print.="</td>";
			if(!isset($_POST["imprimir"])){
				
				$print.="<td>";	
				
				if($fila["justificant"]!=''){
					$print.="<a href='carregues/justificants/".$fila["justificant"]."' target=\"_blank\">Veure justificant</a><br>";
			}
			
				$print.="
				<form method='post'>
				<input type='hidden' name='id_concepte' value='".$fila["id_concepte"]."'>
				<input type='hidden' name='id_persona' value='".$fila["id_persona"]."'>
				<input type='hidden' name='taula' value='t_absencies_hores'>
				<input type='submit' value='Entregat'  name='entregat'>
				<input type='hidden' name='dia' value='".$fila["dia"]."'>
				<input type='hidden' name='mes' value='".$fila["mes"]."'>
				<input type='hidden' name='any' value='".$fila["any"]."'>
				<input type='hidden' name='carpeta1' value='".$_POST["carpeta1"]."' />
				<input type='hidden' name='carpeta2' value='".$_POST["carpeta2"]."' />
				<input type='hidden' name='carpeta3' value='".$_POST["carpeta3"]."' />
				<input type='hidden' name='id_prog' value='".$_POST["id_prog"]."' />
				</form>
				</td>";
			}
			$print.="</tr>";
	}
	$print.="</table>";
}
?>