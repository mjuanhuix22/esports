<?php 

$SQL="SELECT t_absencies_jornada.dia,t_absencies_jornada.mes,t_absencies_jornada.any, 
personal.cognom1,personal.cognom2,personal.nom,personal.id_persona,
t_conceptes.id_concepte,t_conceptes.nom_concepte,justificant   
FROM t_absencies_jornada,personal,t_conceptes where 
t_conceptes.id_concepte!=7 and dies_retribuits<=1  and 
t_conceptes.just!='' and 
t_absencies_jornada.id_persona=personal.id_persona and 
t_absencies_jornada.id_concepte=t_conceptes.id_concepte and 
t_absencies_jornada.validador=0 and 
(
(t_absencies_jornada.any<".date('Y').") or 
(t_absencies_jornada.any=".date('Y')." and t_absencies_jornada.mes<".date('m').") or 
(t_absencies_jornada.any=".date('Y')." and t_absencies_jornada.mes=".date('m')." and t_absencies_jornada.dia<=".date('d').")
) and entrega_just is null ";

if((isset($_POST["b_id_persona"]))&&($_POST["b_id_persona"]!="")){
	$SQL.=" and personal.id_persona=".$_POST["b_id_persona"];
}

//Tota la jornada
$SQL.=" order by t_absencies_jornada.any,t_absencies_jornada.mes,t_absencies_jornada.dia";
//echo $SQL;

$con=mysqli_query($cnx_intranet,$SQL);
if(mysqli_num_rows($con)!=0){
	$print="<table border='1' cellpadding='5' cellspacing='0'>
	<caption>Abséncies que afecten a tota la jornada</caption>
	<tr>
		<th>Data</th>
		<th>Treballador</th>
		<th>Concepte</th>";
		if(!isset($_POST["imprimir"])){ $print.="<td></td>";}
	$print.="</tr>";
	
	
	while($fila=mysqli_fetch_array($con)){
		
			$mal++;
			$print.="<tr>
			<td >".$fila["dia"]." ".$nom_mes[$fila["mes"]]."/".$fila["any"]."</td>
			<td>".$fila["cognom1"]." ".$fila["cognom2"]." ".$fila["nom"]."</td>
			<td>".$fila["nom_concepte"]."</td>";
			
			
			if(!isset($_POST["imprimir"])){
			
				$print.="<td>";
				
				if($fila["justificant"]!=''){
				$print.="<a href='carregues/justificants/".$fila["justificant"]."' target=\"_blank\">Veure justificant</a><br>";
			}
				
				$print.="<form method='post'>
				<input type='hidden' name='id_concepte' value='".$fila["id_concepte"]."'>
				<input type='hidden' name='id_persona' value='".$fila["id_persona"]."'>
				<input type='hidden' name='taula' value='t_absencies_jornada'>
				<input type='submit' value='Entregat'  name='entregat'>
				<input type='hidden' name='dia' value='".$fila["dia"]."'>
				<input type='hidden' name='mes' value='".$fila["mes"]."'>
				<input type='hidden' name='any' value='".$fila["any"]."'>
				<input type='hidden' name='carpeta1' value='".$_POST["carpeta1"]."' />
				<input type='hidden' name='carpeta2' value='".$_POST["carpeta2"]."' />
				<input type='hidden' name='carpeta3' value='".$_POST["carpeta3"]."' />
				
				</form>
				</td>";
			}
			$print.="</tr>";
		
	}
	$print.="</table><br>";
}
