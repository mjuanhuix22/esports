

<form name="form_select" method="post" action="" class="quadre_desplegables">

      <select name="perfil" onchange="submit();" >
        <option value=""></option>
        <?php $con1=mysqli_query($cnx_intranet,"select * from perfils where perfil!='webmaster' group by perfil");
        while($fila1=mysqli_fetch_array($con1)){?>
        <option value="<?php echo $fila1["perfil"];?>" 
        <?php if($_POST["perfil"]==$fila1["perfil"]){?> selected="selected"<?php }?>><?php echo $fila1["perfil"];?></option>
        <?php }?>
      </select>    
	<?php include("comuns3.php");?>
           
</form>


<?php if(isset($_POST["perfil"])){
	
	if($_POST["perfil"]=="gerent"){
		echo  "
		<li>
		Quan es dona d'alta un nou treballador:<br>
		Per defecte visualitzador de la gestió laboral<br>
		Per defecte últim validador de permisos retribuits, fitxatge exterior o compensació d'hores <br>
		Per defecte últim validador de formació <br>
		</li>
		<li>Firma en mails de fitxatge erroni</li>
		<li>Valida efectiu de caixa despres de gestió pressupostària</li>
		<li>En Gestió de personal visualitza el boté de 'Vist dels caps'</li>
		";
	}
	if($_POST["perfil"]=="personal"){
		echo  "
		<li>Accés a tots els apartats de gestió laboral</li>
		<li>Quan es dona d'alta un nou treballador:<br>
		Per defecte visualitzador de la gestió laboral<br>
		Per defecte penúltim validador de permisos retribuits, fitxatge exterior o compensació d'hores</li>
		";
	}
	if($_POST["perfil"]=="personal_aux"){
		echo  "
		<li>Accés a tots els apartats de gestió laboral</li>";
	}
	if($_POST["perfil"]=="CAAS"){
		echo  "
		<li>En l'apartat de Gestió laboral accedeix als apartats modificar permisos retribuits, modificar fitxatge, jornada laboral per els treballador_firma del CAAS</li>";
	}
	if($_POST["perfil"]=="CBSS"){
		echo  "
		<li>En l'apartat de Gestió laboral accedeix a tots els apartats per gestionar els treballador_firma del CBSS.
		(No accedeix als apartats 'Festes del Consell', 'Festes estatals', 'Festes locals' i 'Normativa de permisos retribuïts', 'Jornada laboral del CAAS', 'Dades electròniques' i 'perfils')</li>";
	}
	
	
	if($_POST["perfil"]=="tresorer"){
		echo  "<li>Visualitza els apartats entrega de efectiu de caixa i justificants</li>
		";
	}
	if($_POST["perfil"]=="pressupost"){
		echo  "<li>Valida efectiu de caixa en primer lloc</li>
		<li>
		Quan es dona d'alta un nou treballador:<br>
		Per defecte penúltim validador de formació <br>
		</li>
		";
	}
	
	
	
	if(isset($_POST["guardar"])){
		
		if($_POST["id_persona"]!=$_POST["persona_ant"]){
			$data_fi=date('Y-m-d');
			$data_fi=strtotime('-1 day',strtotime($data_fi));
			$data_fi=date('Ymd',$data_fi);
			
			mysqli_query($cnx_intranet,"update perfils set fi=".$data_fi." where 
			perfil='".$_POST["perfil"]."' and fi=0");	
			
			mysqli_query($cnx_intranet,"insert into perfils set 
			inici=".date('Ymd').",
			id_persona=".$_POST["id_persona"].",
			perfil='".$_POST["perfil"]."',
			fi=0");	
			
			msn("Modificat",1);
		}
	}

	echo "<h2>Assignacio actual</h2>";
	$con=mysqli_query($cnx_intranet,"SELECT * FROM perfils
    where perfil='".$_POST["perfil"]."' and fi=0 order by inici");
    $fila=mysqli_fetch_array($con);?>
    <form name="form_select" method="post">
     <input type="hidden" name="persona_ant" value="<?php echo $fila["id_persona"]?>" />
     <select name="id_persona" >
     <option value=""></option>
        <?php $con1=mysqli_query($cnx_intranet,"select * from personal where exist=1 order by cognom1");
        while($fila1=mysqli_fetch_array($con1)){?>
            <option value="<?php echo $fila1["id_persona"];?>" 
			<?php if($fila["id_persona"]==$fila1["id_persona"]){?> selected="selected"<?php }?>
            ><?php echo $fila1["cognom1"]." ".$fila1["cognom2"].", ".$fila1["nom"];?></option>
        <?php }?>
      </select>
      
      <?php include("comuns3.php");?>
      <input name="guardar" type="submit" class="boto_save" value="">
      <input type="hidden" name="perfil" value="<?php echo $_POST["perfil"];?>" />
      </form>

    
    <?php $con=mysqli_query($cnx_intranet,"SELECT * FROM perfils,personal 
    where perfils.id_persona=personal.id_persona and 
    perfil='".$_POST["perfil"]."' and fi!=0 order by inici");
	if(mysqli_num_rows($con)!=0){
		echo "<h2>Assignacions anteriors</h2>";
   		while($fila=mysqli_fetch_array($con)){?>
    	
        <table border="1" cellpadding="5" cellspacing="0">
        <tr>
            <th width="100px">Persona</th>
            <td><?php echo $fila["nom"]." ".$fila["cognom1"]." ".$fila["cognom2"];?></td>
        </tr><tr>    
            <th>Data inici</th>
            <td><?php echo nom_data8_breu($fila["inici"]);?></td>
        </tr><tr>
            <th>Data fi</th>
            <td><?php echo nom_data8_breu($fila["fi"]);?></td>
        </tr>
        </table>
        <br />
       <?php }
	}?>

<?php }?>