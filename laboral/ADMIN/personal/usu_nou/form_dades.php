<script language="javascript">

function validar_form(){
	if((window.document.form_p.nom.value=="")||
	(window.document.form_p.cognom1.value=="")||(window.document.form_p.sexe.checked==false)||
	(window.document.form_p.dni.value=="")){
		alert("Cal omplir el formulari");
		return false;
	}else{
		
		return true;
	}

}
</script>

<form action="" method="post" enctype="multipart/form-data" name="form_p" id="form_p">
  <table width="100%"  border="0" cellspacing="10" cellpadding="0">

    <tr>
      <td colspan="2" class="lletraNegreta">Dades personals </td>
    </tr>
    <tr>
      <td class="lletraGrisa">DNI</td>
      <td><input name="dni" type="text" id="dni"  size="8" maxlength="8" ></td>
    </tr>
    <tr>
      <td class="lletraGrisa">Nom</td>
      <td><input name="nom" type="text" id="nom" ></td>
    </tr>
    <tr>
      <td class="lletraGrisa">1er cognom</td>
      <td><input name="cognom1" type="text" id="cognom1"  ></td>
    </tr>
    <tr>
      <td class="lletraGrisa">2on cognom </td>
      <td><input name="cognom2" type="text" id="cognom2" ></td>
    </tr>
	    <tr>
      <td class="lletraGrisa">Sexe </td>
      <td><input name="sexe" type="radio" value="1" />
      Dona 
        <input name="sexe" type="radio" value="0" />
        Home</td>
    </tr>


    <tr>
          <td class="lletraNegreta">Dades laborals</td>
      <td>&nbsp;</td>
    </tr>

<tr>
  <td class="lletraGrisa">Obligat fitxar:
  <td>
    <select name="obli_fitxar" >
        <option></option>
        <option value="0" >no</option>
        <option value="1" selected="selected">si</option>
    </select>
 </td>
</tr>

<tr>
  <td class="lletraGrisa">
  Conexio exclusiva des de edificis del consell</td>
  <td>
  <input name="conexio" type="radio" value="1" checked="checked">
  Si 
    <input name="conexio" type="radio" value="2" >
  No</td>
</tr>
            

  <tr>
<td class="lletraGrisa">
Sol·licita permisos retribuits</td>
<td>
<input name="sol_permisos" type="radio" value="1"  checked="checked">
Si 
<input name="sol_permisos" type="radio" value="0" >
No</td>
</tr>


 <tr>
      <td width="20%" class="lletraGrisa">Organigrama orgànic</td>
      <td>  

	  <select name="organigrama"  class="lletraNorm">
	 
	  <?php 
	  $con1=mysqli_query($cnx_intranet,"SELECT * FROM orga1 ");
	  while($fila1=mysqli_fetch_array($con1)){?>
      
      	<option value="<?php echo   $fila1["id_orga1"];?>" 
            class="orga1_<?php echo   $fila1["id_orga1"];?>"
			><?php echo   $fila1["nom_orga1"]?></option>
         <?php    
		   /*
		  $con2=mysqli_query($cnx_intranet,"SELECT * FROM orga2 where id_orga1=".$fila1["id_orga1"]."  order by nom_orga2");
		  while($fila2=mysqli_fetch_array($con2)){?>
			<option value="<?php echo   $fila1["id_orga1"]."_".$fila2["id_orga2"];?>" 
              class="orga1_<?php echo   $fila1["id_orga1"];?>"><?php echo   "------>".$fila2["nom_orga2"];?></option>
            
            
              <?php $con3=mysqli_query($cnx_intranet,"SELECT * FROM orga3 where id_orga2=".$fila2["id_orga2"]." order by nom_orga3");
		  while($fila3=mysqli_fetch_array($con3)){?>
			  <option value="<?php echo   $fila1["id_orga1"]."_".$fila2["id_orga2"]."_".$fila3["id_orga3"];?>" 
              class="orga1_<?php echo   $fila1["id_orga1"];?>"><?php echo   "------------------>".$fila3["nom_orga3"];?></option>
            
            
             <?php $con4=mysqli_query($cnx_intranet,"SELECT * FROM orga4 where id_orga3=".$fila3["id_orga3"]."  order by nom_orga4");
		  while($fila4=mysqli_fetch_array($con4)){?>
			  <option value="<?php echo   $fila1["id_orga1"]."_".$fila2["id_orga2"]."_".$fila3["id_orga3"]."_".$fila4["id_orga4"];?>" 
              class="orga1_<?php echo   $fila1["id_orga1"];?>"><?php echo   "------------------------>".$fila4["nom_orga4"];?></option>
            <?php }?>
            
            
            
            <?php }?>
            
            
            <?php }*/?>
	  <?php }?>
	  </select>
     </td>
    </tr>
    
    
    <tr>
      <td width="20%" class="lletraGrisa">Organigrama funcional</td>
      <td>  

	  <select name="organigrama_funcional"  class="lletraNorm">
	 
	  <?php 
	  $con1=mysqli_query($cnx_intranet,"SELECT * FROM orga1 ");
	  while($fila1=mysqli_fetch_array($con1)){?>
      
      	<option value="<?php echo   $fila1["id_orga1"];?>" 
            class="orga1_<?php echo   $fila1["id_orga1"];?>"
			><?php echo   $fila1["nom_orga1"]?></option>
         <?php    
		   
		  $con2=mysqli_query($cnx_intranet,"SELECT * FROM orga2 where id_orga1=".$fila1["id_orga1"]."  order by nom_orga2");
		  while($fila2=mysqli_fetch_array($con2)){?>
			<option value="<?php echo   $fila1["id_orga1"]."_".$fila2["id_orga2"];?>" 
              class="orga1_<?php echo   $fila1["id_orga1"];?>"><?php echo   "------>".$fila2["nom_orga2"];?></option>
            
            
              <?php $con3=mysqli_query($cnx_intranet,"SELECT * FROM orga3 where id_orga2=".$fila2["id_orga2"]." order by nom_orga3");
		  while($fila3=mysqli_fetch_array($con3)){?>
			  <option value="<?php echo   $fila1["id_orga1"]."_".$fila2["id_orga2"]."_".$fila3["id_orga3"];?>" 
              class="orga1_<?php echo   $fila1["id_orga1"];?>"><?php echo   "------------------>".$fila3["nom_orga3"];?></option>
            
            
             <?php $con4=mysqli_query($cnx_intranet,"SELECT * FROM orga4 where id_orga3=".$fila3["id_orga3"]."  order by nom_orga4");
		  while($fila4=mysqli_fetch_array($con4)){?>
			  <option value="<?php echo   $fila1["id_orga1"]."_".$fila2["id_orga2"]."_".$fila3["id_orga3"]."_".$fila4["id_orga4"];?>" 
              class="orga1_<?php echo   $fila1["id_orga1"];?>"><?php echo   "------------------------>".$fila4["nom_orga4"];?></option>
            <?php }?>
            
            
            
            <?php }?>
            
            
            <?php }?>
	  <?php }?>
	  </select>
     </td>
    </tr>


    <tr>
      <td class="lletraGrisa">Edifici</td>
      <td><select name="id_edifici">
 <?php $con=mysqli_query($cnx_intranet,"select * from zedificis order by nom_edifici");
while($fila=mysqli_fetch_array($con)){?>
	<option value="<?php echo   $fila["id_edifici"]?>" 
    <?php if($fila["id_edifici"]==1){?> selected="selected"<?php }?>><?php echo   StripSlashes($fila["nom_edifici"]);?></option>
<?php }?>
</select>&nbsp;</td>
    </tr>
    <tr>
      <td class="lletraGrisa">C&agrave;rrec</td>
      <td><input name="descr" type="text" id="descr" size="50"></td>
    </tr>
    <tr>
      <td class="lletraGrisa">Adre&ccedil;a electr&ograve;nica </td>
      <td><input name="mail" type="text" id="mail" >
          </td>
    </tr>
    <tr>
      <td class="lletraGrisa">Extensi&oacute; o tel&egrave;fon</td>
      <td><input name="extensio" type="text" id="extensio"  size="9">
      </td>
    </tr>

	<tr>
	  <td valign="top" class="lletraGrisa">Visualitzadors</td>
	  <td>
<?php 
$conP=mysqli_query($cnx_intranet,"select id_persona from perfils where perfil='gerent' and  fi=0");
$filaP=mysqli_fetch_array($conP);
$id_persona_gerent=$filaP["id_persona"];


$conP=mysqli_query($cnx_intranet,"select id_persona from perfils where perfil='personal' and  fi=0");
$filaP=mysqli_fetch_array($conP);
$id_persona_personal=$filaP["id_persona"];

$conP=mysqli_query($cnx_intranet,"select id_persona from perfils where perfil='pressupost' and  fi=0");
$filaP=mysqli_fetch_array($conP);
$id_persona_pressupost=$filaP["id_persona"];

for($i=1;$i<=4;$i++){?>      
      
    <select name="id_visual_<?php echo   $i;?>">
    <option value=""></option>
     <?php $con=mysqli_query($cnx_intranet,"select nom,cognom1,cognom2,id_persona 
    from personal where valida_visual=1 
    order by cognom1");
    while($fila=mysqli_fetch_array($con)){?>
            <option value="<?php echo   $fila["id_persona"]?>" 
            <?php if(($i==1)&&(($fila["id_persona"]==$id_persona_personal))){ echo "selected";}?>
            <?php if(($i==2)&&($fila["id_persona"]==$id_persona_gerent)){ echo "selected";}?>
            ><?php echo   $fila["cognom1"]." ".$fila["cognom2"]." ,".$fila["nom"];?></option>
    <?php }?>
    </select><br />
<?php }?>
      <br />
      </td>
    </tr>
	<tr>
	  <td valign="top" class="lletraGrisa">Validadors</td>
	  <td>
<?php for($i=1;$i<=4;$i++){?>
<?php echo   $i;?><select name="id_valida_<?php echo   $i;?>">
<option value=""></option>
 <?php $con=mysqli_query($cnx_intranet,"select nom,cognom1,cognom2,id_persona 
from personal where valida_visual=1 
order by cognom1");
while($fila=mysqli_fetch_array($con)){?>
        <option value="<?php echo   $fila["id_persona"]?>" 
        <?php if(($i==3)&&($fila["id_persona"]==$id_persona_personal)){ echo "selected";}?>
        <?php if(($i==4)&&($fila["id_persona"]==$id_persona_gerent)){ echo "selected";}?>
        ><?php echo   $fila["cognom1"]." ".$fila["cognom2"]." ,".$fila["nom"];?></option>
    
<?php }?>
</select><br />
 <?php }?><br />
      </td>
    </tr>
    
    
    	<tr>
	  <td valign="top" class="lletraGrisa">Validadors de formació </td>
	  <td>
<?php for($i=1;$i<=4;$i++){?>
<?php echo   $i;?><select name="id_valida_formacio_<?php echo   $i;?>">
<option value=""></option>
 <?php $con=mysqli_query($cnx_intranet,"select nom,cognom1,cognom2,id_persona 
from personal where valida_visual=1 
order by cognom1");
while($fila=mysqli_fetch_array($con)){?>
        <option value="<?php echo   $fila["id_persona"]?>" 
        <?php if(($i==3)&&($fila["id_persona"]==$id_persona_pressupost)){ echo "selected";}?>
        <?php if(($i==4)&&($fila["id_persona"]==$id_persona_gerent)){ echo "selected";}?>
        ><?php echo   $fila["cognom1"]." ".$fila["cognom2"]." ,".$fila["nom"];?></option>
    
<?php }?>
</select><br />
 <?php }?>
      </td>
    </tr>


    	<tr>
	  <td valign="top" class="lletraGrisa">Validador de treball per la tarda al Consell</td>
	  <td>
      <select name="validador_tarda">
      <option value=""></option>
 <?php $con=mysqli_query($cnx_intranet,"select nom,cognom1,cognom2,id_persona 
from personal where valida_visual=1 
order by cognom1");
while($fila=mysqli_fetch_array($con)){?>
	<option value="<?php echo   $fila["id_persona"]?>"  />
      <?php echo   $fila["cognom1"]." ".$fila["cognom2"]." ,".$fila["nom"];?>
	</option>
<?php }?>
</select>
      </td>
    </tr>    


	<tr><td colspan="2">
		<input name="guardar" type="submit" class="boto_save" onClick="return validar_form();" value="">
  		<?php include("comuns3.php"); ?>
  </td></tr>
  </table>


</form>

