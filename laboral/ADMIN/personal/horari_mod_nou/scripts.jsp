<script language="javascript">
function validar_caselles(id_sol,nom,num,guarda){
	var n;
	for(n=1;n<=num;n++){
		
		var dia=eval('window.document.form_horari_'+id_sol+'.dia_'+nom+'_'+n+'.value');
		var mes=eval('window.document.form_horari_'+id_sol+'.mes_'+nom+'_'+n+'.value');
		var any=eval('window.document.form_horari_'+id_sol+'.any_'+nom+'_'+n+'.value');
		var de_h=eval('window.document.form_horari_'+id_sol+'.de_h_'+nom+'_'+n+'.value');
		var de_m=eval('window.document.form_horari_'+id_sol+'.de_m_'+nom+'_'+n+'.value');
		var a_h=eval('window.document.form_horari_'+id_sol+'.a_h_'+nom+'_'+n+'.value');
		var a_m=eval('window.document.form_horari_'+id_sol+'.a_m_'+nom+'_'+n+'.value');
		
		
		var entra=1;
		if(guarda==1){
			if((dia!='')||(mes!='')||(any!='')||(de_h!='')||(de_m!='')||(a_h!='')||(a_m!='')){
				entra=1;
			}else{
				entra=0;
				menys_caselles(id_sol,nom,num);
				
			}
		}
		
		if(entra==1){
			
			if( (dia=="")||(mes=="")||(any=="")||(de_h=="")||(de_m=="")||(a_h=="")||(a_m=="")){
				alert("Error.�s obligatori omplir totes les caselles");
				return (false);
			}else if( (isNaN(de_h))||(isNaN(de_m))||(isNaN(a_h))||(isNaN(a_m))  ){
				alert("Error.�s obligatori omplir l'horari amb dos xifres");
				return (false);
			}
			if((de_h>24)||(de_m>59)||(a_h>24)||(a_m>59)){
				alert("Error.L'horari que heu escrit no existeix");
				return (false);
			}
			
			if(de_h.length==1){
				de_h="0"+de_h;
				eval('window.document.form_horari_'+id_sol+'.de_h_'+nom+'_'+n+'.value="'+de_h+'"');
			}
			if(de_m.length==1){
				de_m="0"+de_m;
				eval('window.document.form_horari_'+id_sol+'.de_m_'+nom+'_'+n+'.value="'+de_m+'"');
			}
			if(a_h.length==1){
				a_h="0"+a_h;
				eval('window.document.form_horari_'+id_sol+'.a_h_'+nom+'_'+n+'.value="'+a_h+'"');
			}
			if(a_m.length==1){
				a_m="0"+a_m;
				eval('window.document.form_horari_'+id_sol+'.a_m_'+nom+'_'+n+'.value="'+a_m+'"');
			}
			
			if( (de_h>a_h) || 
			((de_h==a_h)&&(de_m>a_m)) || 
			((de_h==a_h)&&(de_m==a_m))
			){
				alert("Error.L'horari escrit �s erroni.");
				return (false);
			}
		}
	}
	if(guarda=='0'){
		num=num+1;
		eval('window.document.form_horari_'+id_sol+'.num_'+nom+'.value='+num);
	}
	return (true);
	
}
function menys_caselles(id_sol,nom,num){
	num=num-1;
	eval('window.document.form_horari_'+id_sol+'.num_'+nom+'.value='+num);
	
}

function guardaN(id_sol,num_unlessN,num_moreN){

	var resp=true;
	resp=validar_caselles(id_sol,'unlessN',num_unlessN,1);
	if(resp==true){
		resp=validar_caselles(id_sol,'moreN',num_moreN,1);
	}

	return (resp);
}

function guardaa(id_sol,num_unless,num_more){

	var resp=true;
	resp=validar_caselles(id_sol,'unless',num_unless,1);
	resp=validar_caselles(id_sol,'more',num_more,1);
	

	if(resp==true){
		
		eval('window.document.form_horari_'+id_sol+'.funcio.value="guarda"');
	}
	
	return (resp);
}

</script>