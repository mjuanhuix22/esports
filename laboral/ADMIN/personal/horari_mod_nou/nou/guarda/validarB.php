<?php
//Valido que els dies que ha posat que NO moreN havia de moreNr
for($n=1;$n<=$_POST["num_unlessN"];$n++){

	$dia="dia_unlessN_".$n;
	$dia=$_POST["$dia"];
	$mes="mes_unlessN_".$n;
	$mes=$_POST["$mes"];
	$any="any_unlessN_".$n;
	$any=$_POST["$any"];
	
	if($validat==1){
		//Comprovo que en aquesta data no té una absencia de jornada
		$con=mysqli_query($cnx_intranet,"select * from t_absencies_jornada where id_persona=".$_POST["id_persona"]." and 
		dia=".$dia." and mes=".$mes." and any=".$any);
		if(mysqli_num_rows($con)==1){
			msn("Error. El dia ".$dia."/".$mes."/".$any." NO heu de treballar perquè te  un permés retribuït ",0);
			$validat=0;
		}
	}
	
	if($validat==1){
		//Comprovo que en aquesta data té que treballar en t_contracte

		$con=mysqli_query($cnx_intranet,"select * from t_contracte where id_persona=".$_POST["id_persona"]." 
		and inici<=".convert_data8($any,$mes,$dia)." and (fi>=".convert_data8($any,$mes,$dia)." or fi='')");
		if(mysqli_num_rows($con)==0){
			msn("Error. El dia ".$dia."/".$mes."/".$any." NO té cap contracte.",0);
			$validat=0;
		}else{
			$fila=mysqli_fetch_array($con);
			$con=mysqli_query($cnx_intranet,"select * from t_contracte_hores where  
			diaset=".dia7($any,$mes,$dia)." and id_contracte=".$fila["id_contracte"]);
			if(mysqli_num_rows($con)==0){
				msn("Error. El dia ".$dia."/".$mes."/".$any." és un dia NO laboral.",0);
				$validat=0;
			}
			else{
				$de_h_unlessN="de_h_unlessN_".$n;
				$de_h_unlessN=$_POST["$de_h_unlessN"];
				$de_m_unlessN="de_m_unlessN_".$n;
				$de_m_unlessN=$_POST["$de_m_unlessN"];
				$a_h_unlessN="a_h_unlessN_".$n;
				$a_h_unlessN=$_POST["$a_h_unlessN"];
				$a_m_unlessN="a_m_unlessN_".$n;
				$a_m_unlessN=$_POST["$a_m_unlessN"];
				$algunok=0;
				while($fila=mysqli_fetch_array($con)){
					//Que no hagi sol·licitat no treballar un horari diferent del que té en t_contracte
					if( (($fila["de_h"]<$de_h_unlessN) || (($fila["de_h"]==$de_h_unlessN)&&($fila["de_m"]<=$de_m_unlessN)) ) 
					   &&
					   (($fila["a_h"]>$a_h_unlessN) || (($fila["a_h"]==$a_h_unlessN)&&($fila["a_m"]>=$a_m_unlessN)) )
					  ){
						$algunok=1;
					}
				}
				if($algunok==0){
					msn("Error. El dia ".$dia."/".$mes."/".$any." no ha de treballar entre ".$de_h_unlessN.":".$de_m_unlessN." i ".$a_h_unlessN.":".$a_m_unlessN,0);
					$validat=0;
				}
			}//else
		}//else
	}//if($validat==1){
}//for
	

?>