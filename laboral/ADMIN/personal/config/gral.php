<?php
if(isset($_POST["guardar_config"])){


    $SQL="update t_config set data_fi=".calendari_guardar($_POST["data_inici"])." where data_fi is null and id_persona is null";

    mysqli_query($cnx_intranet,$SQL);

    $SQL="insert into t_config set 
    data_inici=".calendari_guardar($_POST["data_inici"]).",
    minuts_resta_inici=".$_POST["minuts_resta_inici"].",
    minuts_descans=".$_POST["minuts_descans"].",
    dret_descans='".$_POST["dret_descans"]."',
    boto_inici_descans='".$_POST["boto_inici_descans"]."',
    boto_fi_descans='".$_POST["boto_fi_descans"]."'";
    //echo $SQL;

    $ins=mysqli_query($cnx_intranet,$SQL);
    msn("Ok", $ins);

}
?>
<h2>Configuracions generals del fitxatge</h2>

    <?php
    $SQL="select * from t_config where id_persona is null order by data_inici";
    $con=mysqli_query($cnx_intranet,$SQL);
    while($fila=mysqli_fetch_array($con)){?>
        <table>
        <tr>
            <th align="left">Data inici</th>
            <td><?php echo calendari_visual($fila["data_inici"]); ?></td>
        </tr>
        <tr>
            <th align="left">Data de fi</th>
            <td><?php echo calendari_visual($fila["data_fi"]); ?></td>
        </tr>
        <tr>
            <th align="left">Minuts resta inici fitxatge</th>
            <td><?php echo $fila["minuts_resta_inici"] ?> min.</td>
        </tr>

        <tr>
            <th align="left">Temps mínim per tenir dret a descans</th>
            <td><?php echo $fila["dret_descans"]?>h.</td>
        </tr>

        <tr>
            <th align="left">Minuts descans</th>
            <td><?php echo $fila["minuts_descans"]?> min.</td>
        </tr>
        <tr>
            <th align="left">Hora apareix botó descans</th>
            <td><?php echo $fila["boto_inici_descans"]?>h.</td>
        </tr>
        <tr>
            <th align="left">Hora desapareix botó descans</th>
            <td><?php echo $fila["boto_fi_descans"]?>h.</td>
        </tr>
    </table>
    <br><br>

 <?php }?>

<form method="post">
<table>
    <tr>
        <th align="left">Data inici</th>
        <td><input type="text" name="data_inici" size="10"></td>
    </tr>
    <tr>
        <th align="left">Minuts resta inici fitxatge</th>
        <td><input type="text" name="minuts_resta_inici" size="2"> min.</td>
    </tr>

    <tr>
        <th align="left">Temps mínim per tenir dret a descans</th>
        <td><input type="text" name="dret_descans" size="5"> h</td>
    </tr>

    <tr>
        <th align="left">Minuts descans</th>
        <td><input type="text" name="minuts_descans" size="2"> min.</td>
    </tr>
    <tr>
        <th align="left">Hora apareix botó descans</th>
        <td><input type="text" name="boto_inici_descans" size="5"> h.</td>
    </tr>
    <tr>
        <th align="left">Hora desapareix botó descans</th>
        <td><input type="text" name="boto_fi_descans" size="5">h.</td>
    </tr>

</table>
    <input type="submit" name="guardar_config" class="boto_save" value="">
    Avis: Usar amb precaució

    <?php include("comuns3.php");?>
</form>
<br><br>