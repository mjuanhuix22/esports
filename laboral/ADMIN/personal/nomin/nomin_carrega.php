<?php
/*
Pagina de càrrega de nòmines. El formulari carrega l'arxiu .ZIP a la carpeta CARREGUES,
el descomprimeix i analitza els arxius i els trasllada a la carpeta corresponent (any de la nòmina)
renombrant-los amb el dni_any_mes_coditipus 
*/

//rmdir("carregues/nomines/descarregues/");
//mkdir("carregues/nomines/descarregues/",0777);


// obtenemos los datos del archivo
$tamano = $_FILES["archivo"]['size'];
$tipo = $_FILES["archivo"]['type'];
$archivo = $_FILES["archivo"]['name'];
$prefijo = ''; //substr(md5(uniqid(rand())),0,6);
if ($archivo != "") {
	$rutadesti="carregues/nomines/";
	$rutadescarrega = $rutadesti."descarregues/";
	if (!is_dir($rutadescarrega)){
		mkdir($rutadescarrega, 0777);
	}

	// Guardem l'arxiu .ZIP
	$arxiuorigen= $_FILES['archivo']['tmp_name'];
	$destino = $rutadesti.$archivo;

	//if (copy($arxiuorigen,$destino)) {
	if (!move_uploaded_file($arxiuorigen,$destino)){
		msn("Error al pujar arxiu",0);
	} else {
		
	switch($_POST['nomin_tipusfitxer']){
		case 'comprimit_zip':
			//Incluimos la libreria
			require ('includes/pclzip-2-8-2/pclzip.lib.php');

			//forma de llamar la clase
			$archive = new PclZip($destino);
	
			//Ejecutamos la funcion extract
			if ($archive->extract(PCLZIP_OPT_PATH, $rutadescarrega,
				PCLZIP_OPT_REPLACE_NEWER) == 0) {
					unlink($destino); //zip original
					die("Error de descompressió de l'arxiu.");//.$archive->errorInfo(true));
			}else{
				unlink($destino); //zip original
			}
			
			break;
		
		case 'normal':
			rename($destino,$rutadescarrega.$archivo);
			break;
	}
	
	
	//Movem i renombrem arxius
	$rutaarxiudesti = $rutadesti;

	
	function processa_arxius_directori($subdir_rutadescarrega,$archivo){
		require ("conexio.php");
		global $rutaarxiudesti;
		global $array_arxius_error;
		$rutadescarrega = $subdir_rutadescarrega;

		$nomarxiu_origen = $rutadescarrega.$archivo;

		$pos=strpos($archivo,'DNI');

		//echo $archivo."posicio".$pos."pos<br>";
		
		$dni=substr($archivo,$pos+3,8);

		//echo $dni."<br>";


		$sql= "select dni,id_persona from personal where exist=1 and dni = '".$dni."' and dni!='' ";
		//echo $sql;
		$con=mysqli_query($cnx_intranet,$sql);
		if (mysqli_num_rows($con)!=0){
			$fila=mysqli_fetch_array($con);
			$id_persona=$fila["id_persona"];
			
			$con=mysqli_query($cnx_intranet,"select count(*) from nomines where id_persona=".$id_persona." and 
			mes=".$_POST["nomin_mes"]." and any=".$_POST["nomin_any"]." and nomin_tipus=".$_POST["nomin_tipus"]);
			$fila=mysqli_fetch_array($con);
			$versio=$fila["count(*)"];

			
			//Crear codi aleatori
			$caracters=array(1=>'1',2=>'2',3=>'3',4=>'4',5=>'5',6=>'6',7=>'7',8=>'8',9=>'9',10=>'0',11=>'A',12=>'B',13=>'C',14=>'D',15=>'E',16=>'F',17=>'G',18=>'H',19=>'I',20=>'J',21=>'K',22=>'L',23=>'M',24=>'N',25=>'O',26=>'P',27=>'Q',28=>'R',29=>'S',30=>'T',31=>'U',32=>'V',33=>'X',34=>'Y',35=>'Z');
			$rand="";
			for($i=1;$i<=20;$i++){
				$rand.=$caracters[rand(1,35)];
			}
			
			$sql="insert into nomines set 
			id_persona=".$id_persona.",
			mes=".$_POST["nomin_mes"].",
			any=".$_POST["nomin_any"].",
			nomin_tipus=".$_POST["nomin_tipus"].",
			versio=".$versio.",
			rand='".$rand."',
			data_publicacio='".date('d/m/Y H:i')."'";
			//echo $sql;
			$con=mysqli_query($cnx_intranet,$sql);
			mysqli_query($cnx_intranet,$con);
			$id_nomina=mysqli_insert_id($cnx_intranet);
			
			$nomarxiu_desti=$id_nomina."-".$rand.".pdf";
			
			rename($nomarxiu_origen,$rutaarxiudesti.$nomarxiu_desti);
			
			
		}else{
			echo substr($arxiu,0,1);
			if(substr($arxiu,0,1)!="R"){
				$array_arxius_error[] = "<b>".$archivo."</b>"." - DNI no localitzat.";
			}
			unlink($nomarxiu_origen);	
		}
	
	}
	
	function llistar_directoris_ruta($ruta){
		global $num_arxius;
		global $es_subdir;
		
		// abrir un directorio y listarlo recursivo
		if (is_dir($ruta)) {
			if ($dh = opendir($ruta)) {
				while (($file = readdir($dh)) !== false) {
					//esta lnea la utilizaramos si queremos listar todo lo que hay en el directorio
					//mostrara tanto archivos como directorios
					//echo "<br>Nombre de archivo: $file : Es un: " . filetype($ruta . $file);
					if (is_dir($ruta . $file) && $file!="." && $file!=".."){
						//solo si el archivo es un directorio, distinto que "." y ".."
						//echo "<br>Directorio: $ruta$file";
						$es_subdir++;
						llistar_directoris_ruta($ruta . $file . "/");
						
					}elseif($file!="." && $file!=".."){
						$num_arxius++;
						//die("processem fitxers");
						
						processa_arxius_directori($ruta,$file);
					}
				}
				if ($es_subdir > 0){
					//echo "esborrem ".$ruta.$es_subdir."<br>";
					rmdir($ruta);
					$es_subdir=0;
				}else{
					//echo "no esborrem ".$ruta.$es_subdir."<br>";
				}
				closedir($dh);					
			}
		}else
			echo "<br>No és ruta vàlida";
	}
	
	
	$num_arxius = 0;
	$es_subdir = 0;
	$array_arxius_error[]="";
	llistar_directoris_ruta($rutadescarrega);
	
	
	if (isset($array_arxius_error) && count($array_arxius_error)>1){
		//echo "<br>càrrega finalitzada; ".$num_arxius." arxius processats";
		echo "<p style='color:red;'><br>Hi ha hagut ".(count($array_arxius_error)-1)." errors amb els següents arxius que caldràrevisar i tornar a carregar:<br>";
		foreach ($array_arxius_error as $arxiu_erroni){
			echo $arxiu_erroni."<br>";
		}
		echo "</p>";
	}else{
		msn("càrrega finalitzada; ".$num_arxius." arxius processats correctament",1);
	}
	?><br><br><?php 
	
	}
}
?>