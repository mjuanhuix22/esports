<?php if(isset($_POST["pujar_zip"])){
	include("nomin_carrega.php");
}?>
<script>
	function valida_form_nomin(){
		if (form_nomin_zip.nomin_mes.value == "" || form_nomin_zip.nomin_any.value == "" || form_nomin_zip.archivo.value == ""){
			//if (form_nomin_zip.nomin_any.value != ""){
			alert('Cal especificar el mes, l\'any i l\'arxiu de n�mines.');
			return false;
		}

		if(form_nomin_zip.nomin_tipusfitxer.value == "comprimit_zip" && !(/\.(zip)|(aaa)$/.test(form_nomin_zip.archivo.value))){
			alert('L\'arxiu de n�mines ha d\'estar comprimit en format ZIP.');
			form_nomin_zip.archivo.click();
			return false;
		}

		$("#llistat_nomines").html('Carregant fitxer, esperi siusplau...');
		
	}

</script>
<form method="post" id="form_nomines" enctype="multipart/form-data" name="form_nomin_zip"  class="menu_selec" 
onsubmit='return valida_form_nomin()'>
	<div id="contingut_form_nomines">
	<h2>C&agrave;rrega de n&ograve;mines</h2>
<p>Tipus de c&agrave;rrega
	  <select name="nomin_tipusfitxer" class="valor_selec">
	  	<option value="comprimit_zip">En bloc amb fitxer comprimit .zip</option>
	  	<option value="normal">Normal, 1 arxiu independent</option>
	  </select>
</p>
<p>Data de la n&ograve;mina
	  <select name="nomin_mes" class="valor_selec">
	   		<option value=""></option>
		<?php for($i=1;$i<=12;$i++){?>
			<option value="<?php echo   $i;?>" 
            <?php if((isset($_POST["nomin_mes"]))&&($_POST["nomin_mes"]==$i)){?> selected="selected"<?php }?>><?php echo   $nom_mes[$i];?></option>
		<?php }?>
	  </select>
	  <select name="nomin_any"  class="valor_selec">
	   			<option value=""></option>
		<?php for($i=date('Y');$i>=2015;$i--){?>
			<option value="<?php echo   $i;?>" 
            <?php if((isset($_POST["nomin_any"]))&&($_POST["nomin_any"]==$i)){?> selected="selected"<?php }?>><?php echo   $i;?></option>
		<?php }?>
	  </select>
</p>

<p>Tipus	  
	  <select name="nomin_tipus"  class="valor_selec">
		<?php foreach($array_tipusnomina as $tipusnomina => $nomtipus){?>
			<option value="<?php echo   $tipusnomina;?>"><?php echo   $nomtipus;?></option>
		<?php }?>
	  </select>
      <input type="hidden" name="nomin_nomenclator" value="DNI" />
</p>
<p>Arxiu amb n&ograve;mines <input name="archivo" type="file" class="lletra70" size="15"></p>

<p><input type="submit" name="pujar_zip" value="Enviar" class="lletraNegreta70" ></p>

<?php include("comuns3.php");?>
	</div> <!-- contingut_form_nomines -->
</form>

<?php ?>
<br />
<?php 
include("visual.php");
?>
