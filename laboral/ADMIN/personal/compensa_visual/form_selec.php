<div class="quadre_desplegables">
<form  method="post" name="form_horari" >
<span class="lletraNegreta">Treballador:</span> 
    <select name="id_persona" class="lletraNorm">
    <option value=""></option>
    <?php 
    //Surt tot el personal que treballa o treballava i que hagi tingut un contracte
    $con=mysqli_query($cnx_intranet,"select 
	personal.id_persona,personal.cognom1,personal.nom,personal.cognom2
    from personal,t_visual where 
    personal.id_persona=t_visual.treballador and 
    personal.exist=1 and 
    t_visual.visualitzador=".$_SESSION["id_usuari"]." 
    order by personal.cognom1");
    while($fila=mysqli_fetch_array($con)){?>
    <option value="<?php echo   $fila["id_persona"];?>" <?php if((isset($_POST["id_persona"]))&&($fila["id_persona"]==$_POST["id_persona"])){ echo "selected";}?>><?php echo   $fila["cognom1"]." ".$fila["cognom2"]." ,".$fila["nom"];?></option>
    <?php }?>
    </select>
    <br />
    
    <?php if($_SESSION["perfil"]!="CBSS"){?>
        <span class="lletraNegreta">Area</span>
	    <select name="organigrama"  onchange="submit();">
	  <option value=""></option>
	  <?php 
	  $con1=mysqli_query($cnx_intranet,"SELECT * FROM orga1");
	  while($fila1=mysqli_fetch_array($con1)){?>
      	<option value="<?php echo   $fila1["id_orga1"];?>" 
            class="orga1_<?php echo   $fila1["id_orga1"];?>"
            <?php if((isset($_POST["organigrama"]))&&($_POST["organigrama"]==$fila1["id_orga1"])){ echo "selected";}?>
			><?php echo   $fila1["nom_orga1"]?></option>
      
      <?php 
	  
	  $con2=mysqli_query($cnx_intranet,"SELECT * FROM orga2 where id_orga1=".$fila1["id_orga1"]);
	  while($fila2=mysqli_fetch_array($con2)){?>
		<option value="<?php echo   $fila1["id_orga1"]."_".$fila2["id_orga2"];?>" 
		class="orga1_<?php echo   $fila1["id_orga1"];?>"
		<?php if((isset($_POST["organigrama"]))&&($_POST["organigrama"]==($fila1["id_orga1"]."_".$fila2["id_orga2"]))){ echo "selected";}?>
		
		><?php echo   "------>".$fila2["nom_orga2"];?></option>
            
           
            
            <?php }?>
	  <?php }?>
</select>
<br />
<?php }?>
      
    
     <span class="lletraNegreta">Data:</span>	
    <select name="any">
    	<option value=""></option>
    <?php for($i=(date('Y')-1);$i<=(date('Y')+1);$i++){?>
        <option value="<?php echo   $i;?>"
        <?php if(($_POST["any"]==$i)||
		      ( (!isset($_POST["any"]))&&($i==date('Y'))) ){?> selected="selected"<?php }?>><?php echo   $i;?></option>
    <?php }?>
    </select>
    
    <select name="mes">
    <option value=""></option>
    <?php for($m=1;$m<=12;$m++){?>
        <option value="<?php echo   $m;?>"
        <?php if((isset($_POST["mes"]))&&($_POST["mes"]==$m)){?> selected="selected"<?php }?>><?php echo   $nom_mes[$m];?></option>
    <?php }?>
    </select>
    
    <br />
    
    <span class="lletraNegreta">Veure:</span>
    <input type="radio" name="compensa" value="1" <?php 
	if((!isset($_POST["compensa"]))||($_POST["compensa"]==1) ){?> checked="checked"<?php }?>/>Compensació
   	<input type="radio" name="compensa" value="0" 
    <?php 
	if((isset($_POST["compensa"]))&&($_POST["compensa"]==0) ){?> checked="checked"<?php }?>/>Recuperació 
    
    
    
    <input type="checkbox" name="veure_tot" value="1" 
    <?php if((isset($_POST["veure_tot"]))&&($_POST["veure_tot"]==1)){?> checked="checked"<?php }?>/>més i menys
    
    <br />
    <input name="veure" type="submit" id="veure" value="" class="boto_visual"  >
    <?php include("comuns3.php");?>

</form>
</div>