 <?php 
 
 //--------------------------Calcular hores i minuts unless
$hores_unless=0;
$minuts_unless=0;
for($n=1;$n<=$_POST["num_unless"];$n++){
	$dia="dia_unless_".$n;
	$dia=$_POST["$dia"];
	$mes="mes_unless_".$n;
	$mes=$_POST["$mes"];
	$any="any_unless_".$n;
	$any=$_POST["$any"];
	
	$de_h_unless="de_h_unless_".$n;
	$de_h_unless=$_POST["$de_h_unless"];
	$de_m_unless="de_m_unless_".$n;
	$de_m_unless=$_POST["$de_m_unless"];
	$a_h_unless="a_h_unless_".$n;
	$a_h_unless=$_POST["$a_h_unless"];
	$a_m_unless="a_m_unless_".$n;
	$a_m_unless=$_POST["$a_m_unless"];
	list($hores_fa,$minuts_fa)=restar_temps($a_h_unless,$a_m_unless,$de_h_unless,$de_m_unless);
	list($hores_unless,$minuts_unless)=sumar_temps($hores_unless,$minuts_unless,$hores_fa,$minuts_fa);
}


//Si el motiu és hora per 2 hores es recalcula el temps de menys
if(($_POST["id_motiu"]==10) || ($_POST["id_motiu"]==9)){ //Hora per dos hores
		
		$text="Info: En comptes de restar ".$hores_unless.":".$minuts_unless."h ";
		
		//Passar hores a minuts
		$m=$hores_unless*60;
		
		//Tot son minuts
		$minuts_unless=$m+$minuts_unless;
		//echo "<br>1.minuts_inici:".$minuts_unless;
		
	
		if($_POST["id_motiu"]==10){ 
		 	 $minuts_unless=round(($minuts_unless*(1/2)),0);
		}
		elseif($_POST["id_motiu"]==9){ 
			$minuts_unless=round(($minuts_unless*(1/1.75)),0);
		}
		
		//echo "<br>2.minuts_fi:".$minuts_unless;
		
		//Passar els minuts a hores
		$var=$minuts_unless/60;
		
		//Com que el decimal esta en base 100 en compte d'estar en base 60
		$var=explode('.',$var);
		$hores_unless=$var[0];
		$minuts_unless="00";
		
		if(isset($var[1])){
			$m=$var[1];
			if(strlen($m)==1){ $m=$m."0";}
			else{ $m=substr($m,0,2);}
			$minuts_unless=round( (($m*60)/100),0);
		}
		$text.=" es resten ".$hores_unless.":".convert_num2($minuts_unless)."h. del temps que encara pot compensar";
		echo "<p class='lletraNegreta'>".$text."</p>";
}


//-------------------------------------Calcular hores i minuts more
$hores_more=0;
$minuts_more=0;

$con0=mysqli_query($cnx_intranet,"select * from t_compensa where id_persona=".$_POST["id_persona"]." order by any,mes,dia ");
while($fila0=mysqli_fetch_array($con0)){

	$casella="casella_".$fila0["id_compensa"];
	if((isset($_POST["$casella"]))&&($_POST["$casella"]==1)){
		list($h,$m)=restar_temps($fila0["a_h"],$fila0["a_m"],$fila0["de_h"],$fila0["de_m"]);
		list($hores_more,$minuts_more)=sumar_temps($hores_more,$minuts_more,$h,$m);
	}
}
if(($hores_more==0)&&($minuts_more==0)){
	msn("Error. No has seleccionat cap intervat de temps treballat",0);
	$validat=0;
}


//-------------------------Comparar more amb unless
if($validat==1){
	if(($hores_more<$hores_unless)||(($hores_more==$hores_unless)&&($minuts_more<$minuts_unless)) ){
		msn("Error.Heu marcat mes hores compensades(".$hores_unless.":".$minuts_unless."h.) que treballades(".$hores_more.":".$minuts_more."h.)",0);
		$validat=0;
	}
}



?>