<?php
if(!isset($_POST["id_persona"])){
	header("location:http://www.selvaesports.cat/laboral/index.php");
}
include("../../conexio.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>Formulari de sol&middot;licitud</title>
<link href="../../estils.css" rel="stylesheet" type="text/css">
<?php include("../../estils_conceptes.php");?>
<style type="text/css">
<!--
.Estilo1 {
	font-size: 20px;
	font-family: Helvetica, Arial, sans-serif;
}
-->
</style>
</head>

<body>
<table cellpadding="0" cellspacing="10">
  <tr>
    <td><img src="../../imatges/logselva_gris.jpg" alt="logo consell" hspace="10" align="left"></td>
    <td valign="bottom"><p align="center"  class="Estilo1">Sol&middot;licitud de compensaci&oacute; d'hores</p></td>
  </tr>
</table>
<p  class="Estilo1">
  <?php $con=mysqli_query($cnx_intranet,"select * from personal where id_persona=".$_POST["id_persona"]);
 $fila=mysqli_fetch_array($con);
 ?>
</p>
<p  class="lletraNorm">Ompliu el seg&uuml;ent formulari, imprimiu-lo, firmeu-lo, aneu amb el vostre cap perqu&egrave; us el signi i despr&eacute;s entregueu-lo a la cap de Gesti&oacute; personal.</p>

 <form name="form1" method="post" action="">
   <table width="600"  border="1">
     <tr bordercolor="#FFFFFF">
       <td width="40%" class="lletraSubtituls">Nom del treballador:       </td>
       <td><input name="textfield" type="text" class="lletraNorm" size="50" value="<?php echo   $fila["nom"]." ".$fila["cognom1"]." ".$fila["cognom2"];?>"></td>
     </tr>
     <tr bordercolor="#FFFFFF">
       <td class="lletraSubtituls">Data en qu&egrave; es fa la sol&middot;licitud:</td>
       <td><input name="textfield" type="text" class="lletraNorm" value="<?php echo   date('d');?>" size="2" maxlength="2">
/
  <?php $nom_mes=array(1=>'gener',2=>'febrer',3=>'mar&ccedil;',4=>'abril',5=>'maig',6=>'juny',
7=>'juliol',8=>'agost',9=>'setembre',10=>'octubre',11=>'novembre','12'=>"desembre");?>
  <input name="textfield" type="text" size="8" value="<?php echo   $nom_mes[(int)date('m')];?>" class="lletraNorm">
/
<input name="textfield" type="text" class="lletraNorm" value="<?php echo   date('Y');?>" size="4" maxlength="4"></td>
     </tr>
   </table>
   <br>
   <table width="600"  border="1">
     <tr bordercolor="#FFFFFF">
       <td width="40%" class="lletraSubtituls">Tipus de compensaci&oacute;
         : </td>
       <td bordercolor="#FFFFFF" class="lletraNorm"><input name="radiobutton" type="radio" value="radiobutton">       
       Hora normal (hora treballada per hora compensada)<br>
       <input name="radiobutton" type="radio" value="radiobutton">
       Hora <br>
       <input name="radiobutton" type="radio" value="radiobutton">
       Hora extra (caps de setmana, festius o despres de 22h) </td>
     </tr>
     <tr bordercolor="#FFFFFF">
       <td class="lletraSubtituls">Especificaci&oacute; del motiu: </td>
       <td><input name="textfield" type="text" class="lletraNorm" size="50"></td>
     </tr>
   </table>
   <br>
   <table width="600"  border="1">
     <tr>
       <td bordercolor="#FFFFFF" class="lletraSubtituls">
	   Hores treballades(dia,mes,any,hora inici,hora fi):	   </td>
     </tr>
     <tr>
       <td bordercolor="#FFFFFF"><textarea name="textarea" cols="110" rows="10" class="lletraNorm"></textarea></td>
     </tr>
     <tr>
       <td bordercolor="#FFFFFF" class="lletraSubtituls">
	   Hores compensades (dia,mes,any,hora inici,hora fi):
	   </td>
     </tr>
     <tr>
       <td bordercolor="#FFFFFF"><textarea name="textarea" cols="110" rows="10" class="lletraNorm"></textarea></td>
     </tr>
   </table>
   <br>
   <table width="600"  border="1" cellpadding="5">
     <tr>
       <td bordercolor="#FFFFFF" class="lletraSubtituls">Motiu pel qual feu la sol&middot;licitud fora de termini o anul&middot;leu una sol&middot;licitud pr&egrave;via: </td>
     </tr>
     <tr>
       <td bordercolor="#FFFFFF"><textarea name="textarea" cols="110" rows="5" class="lletraNorm"></textarea> </td>
     </tr>
   </table>
   <br>


   <table width="600"  border="1" cellpadding="5" cellspacing="0">
     <tr valign="top" class="lletraNorm">
       <td width="33%"><p>Firma del treballador (<?php echo   $fila["nom"]." ".$fila["cognom1"]." ".$fila["cognom2"];?>)</p>
       <p>&nbsp;</p>
       <p>&nbsp;</p></td>
	   <?php  $con1=mysqli_query($cnx_intranet,"select * from t_validacio where 
		  treballador=".$_SESSION["id_usuari"]." and 
		  validador!=
		  personal.id_persona=arees.id_persona and arees.id_area=".$fila["id_area"]);
		 $fila1=mysqli_fetch_array($con1);
		 if($fila1["id_persona"]!=$_POST["id_persona"]){?>
			   <td width="33%"><p>Firma del Cap d'&agrave;rea (<?php echo   $fila1["nom"]." ".$fila1["cognom1"]." ".$fila1["cognom2"];?>)</p>
			   <p>&nbsp;</p>
			   <p>&nbsp;</p></td>
	   <?php }?>
       <td><p>Firma de Gesti&oacute; de Personal (Elena Samper)
       </p>
       <p>&nbsp;</p>
       <p>&nbsp;         </p></td>
     </tr>
   </table>

   


<p></p>
 </form>
</body>
</html>
