<script language="javascript">
function validar(){
	if( (window.document.form_select.dia.value=="")||
	(window.document.form_select.mes.value=="")||
	(window.document.form_select.any.value=="")||
	(window.document.form_select.id_persona.value=="")){
		alert("Es obligatori omplir tots els camps");
		return (false);
	}else{
		return (true);
	}	
}
</script>
<div class="quadre_desplegables">
<form name="form_select" method="post" action="">

	  <span class="lletraNegreta">Data</span>
         <select name="dia" class="lletraNorm">
	    <option value=""></option>
	    <?php for($i=1;$i<=31;$i++){?>
	  	  <option value="<?php echo   $i;?>" <?php if((isset($_POST["dia"]))&&($i==$_POST["dia"])){ echo "selected";}?>><?php echo   $i;?></option>
		  <?php }?>
         </select>
	   /  
	   <select name="mes" class="lletraNorm">
	    <option value=""></option>
	    <?php for($i=1;$i<=12;$i++){?>
	  	  <option value="<?php echo   $i;?>" <?php if((isset($_POST["mes"]))&&($i==$_POST["mes"])){ echo "selected";}?>><?php echo   $nom_mes[$i];?></option>
		  <?php }?>
       </select>
        /
        <select name="any" class="lletraNorm">
	    <option value=""></option>
	    <?php 
	  for($i=(date('Y')-1);$i<=date('Y');$i++){?>
	  	  <option value="<?php echo   $i;?>" <?php if((isset($_POST["any"]))&&($i==$_POST["any"])){ echo "selected";}?>><?php echo   $i;?></option>
		  <?php }?>
        </select>
        
        <span class="lletraNegreta">Treballador</span>
	    <select name="id_persona" class="lletraNorm" >
          <option value=""></option>
          <?php 
		$con=mysqli_query($cnx_intranet,"SELECT * FROM personal,t_visual where 
		personal.exist=1 and 
		personal.obli_fitxar=1 and 
		personal.id_persona=t_visual.treballador and 
		t_visual.visualitzador=".$_SESSION["id_usuari"]." order by cognom1");
	  while($fila=mysqli_fetch_array($con)){?>
          <option value="<?php echo   $fila["id_persona"];?>" <?php if( (isset($_POST["id_persona"]))&&($_POST["id_persona"]==$fila["id_persona"])){ echo "selected";}?>><?php echo   $fila["cognom1"]." ".$fila["cognom2"].", ".$fila["nom"];?></option>
          <?php }?>
        </select>
      <input type="submit" name="ok" value="ok" onClick="return validar();">
  <?php include("comuns3.php");?>
</form>
</div>
