<?php
$ok=1;
//que el config estigui dins d'aquest id_fitxatge
$con=mysqli_query($cnx_intranet,"select * from t_fitxatge where id_fitxatge=".$_POST["id_fitxatge"]);
$fila=mysqli_fetch_array($con);
if( ($_POST["surt_h"]<$fila["entra_h"])||(($_POST["surt_h"]==$fila["entra_h"])&&($_POST["surt_m"]<$fila["entra_m"])) ){
	msn("ERROR.El període de config ha de ser superior a ".$fila["entra_h"].":".$fila["entra_m"]."h.",0);
	$ok=0;
}
if ($fila["surt_h"]!=""){
	if( ($_POST["surt_h"]>$fila["surt_h"])||(($_POST["surt_h"]==$fila["surt_h"])&&($_POST["surt_m"]>$fila["surt_m"])) ){
		msn("ERROR.El període de config ha d'estar entre 
		".$fila["entra_h"].":".$fila["entra_m"]." i ".$fila["surt_h"].":".$fila["surt_m"]."h.",0);
		$ok=0;
	}
}

if($ok==1){
	//Si en el fitxatge no ha posat quan torna Pot ser que en el config no posi quan torna
	if($_POST["torna_h"]!=""){
		list($dif_h,$dif_m)=restar_temps($_POST["torna_h"],$_POST["torna_m"],$_POST["surt_h"],$_POST["surt_m"]);
	}else{
		$dif_h="";$dif_m="";
	}
	
	if(($_SESSION["perfil"]=="personal")||($_SESSION["perfil"]=="personal_aux")){
		$con=mysqli_query($cnx_intranet,"select * from t_f_descans	where id_descans=".$_POST["id_descans"]);
		$fila=mysqli_fetch_array($con);
		if(($fila["surt_h"]!=$_POST["surt_h"]) || ($fila["surt_m"]!=$_POST["surt_m"]) ){
			$entra=1;
		}else{
			$entra=0;	
		}
		if( (($fila["torna_h"]!=$_POST["torna_h"]) || ($fila["torna_m"]!=$_POST["torna_m"]) ) && 
		   ($_POST["torna_h"]!="")){
			$surt=1;
		}else{
			$surt=0;	
		}
	
		$con=mysqli_query($cnx_intranet,"select * from t_f_arregla where id_descans=".$_POST["id_descans"]);
		if(mysqli_num_rows($con)==0){		
			mysqli_query($cnx_intranet,"insert into t_f_arregla set 
			dia=".$_POST["dia"].",
			mes=".$_POST["mes"].",
			any=".$_POST["any"].",
			id_persona=".$_POST["id_persona"].",
			entra=".$entra.",
			surt=".$surt.",
			rrhh=1,
			id_descans=".$_POST["id_descans"]);
		}else{
			$fila=mysqli_fetch_array($con);
			if($fila["entra"]==1){ $entra=1;}
			if($fila["surt"]==1){ $surt=1;}
			mysqli_query($cnx_intranet,"update t_f_arregla set 
			entra=".$entra.",
			surt=".$surt.", 
			rrhh=1 where 
			id_descans=".$_POST["id_descans"]);	
		}
	}
	$mod=mysqli_query($cnx_intranet,"update t_f_descans set 
	surt_h='".$_POST["surt_h"]."',
	surt_m='".$_POST["surt_m"]."',
	torna_h='".$_POST["torna_h"]."',
	torna_m='".$_POST["torna_m"]."',
	dif_h='".$dif_h."', 
	dif_m='".$dif_m."'  
	where id_descans=".$_POST["id_descans"]);
	
	if(convert_data8($_POST["any"],$_POST["mes"],$_POST["dia"])!=data_actual8()){
		descuadra_fit($_POST["any"],$_POST["mes"],$_POST["dia"],$_POST["id_persona"]);
	}
	
	if($mod){
		msn("Descans modificat correctament",1);
	}
}


?>