function createXMLHttpRequestObject() {
	/*Aquesta condició determina si el navegador té suport
	nadiu per a XMLHttpRequest*/
	if(window.XMLHttpRequest){
		alert("o");return new XMLHttpRequest();
	}
	/*No hi ha suport nadiu, s'intenta obtenir com un objete ActiveX
	per a versions anteriors d'Internet Explorer 7*/
	else if(window.ActiveXObject){
		try{
			return new ActiveXObject('Msxml2.XMLHTTP');
		}
		catch(e){
			try{
				return new ActiveXObject('Microsoft.XMLHTTP');
			}
				catch(E){}
		}
	}
	alert("No s'ha pogut crear una instància d'XMLHttpRequest");
}