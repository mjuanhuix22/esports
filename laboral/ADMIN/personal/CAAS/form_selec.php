<div class="quadre_desplegables">
	<form  method="post" name="form_data" onSubmit="return validar()">
      <span class="lletraNegreta">Jornada laboral dels treballadors durant el</span>      

	    <select name="any"  class="lletraNorm" onchange="submit();">
        <?php 
		for($i=(date('Y')-1);$i<=(date('Y')+1);$i++){?>
        <option value="<?php echo   $i;?>" <?php if((isset($_POST["any"]))&&($i==$_POST["any"])){ echo "selected";}?>><?php echo   $i;?></option>
        <?php }?>
      </select>
      
	  <?php if((isset($_POST["any"]))&&($_POST["any"]!="")){?>
			<select name="setmana" onchange="submit();" class="lletraNorm">
				<option value=""></option>
			<?php for($mes=1;$mes<=12;$mes++){
				for($dia=1;$dia<=total_dies($mes,$_POST["any"]);$dia++){
					if(dia7($_POST["any"],$mes,$dia)==0){?>
						<option value="<?php echo   convert_data8($_POST["any"],$mes,$dia);?>" 
						<?php if($_POST["setmana"]==convert_data8($_POST["any"],$mes,$dia)){ echo "selected";}?>
						><?php echo   $dia." ".$nom_mes[$mes];?></option>
					<?php }?>
				<?php }?>
			
			<?php }?>
		  </select>
	  <?php }?>
      <?php include("comuns3.php");?>
   </form>
</div>