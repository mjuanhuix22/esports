

<h5>Llistat d'incidències</h5><?php 

$SQL="SELECT *  FROM laboral,laboral_tema,personal where 
laboral.sol_tema=laboral_tema.id_tema and 
laboral.id_persona=personal.id_persona ";

if($B_id_persona!=""){ $SQL.=" and personal.id_persona=".$B_id_persona;}
if($B_id_tema!=""){ $SQL.=" and laboral.sol_tema=".$B_id_tema;}
if($B_id_estat!="") {

    if ($B_id_estat == "pendent_validar") {
        $SQL .= " and laboral.validador!=0";
    }
    if ($B_id_estat == "pendent_rrhh") {
        $SQL .= " and laboral.validador=0 and laboral_data=''";
    }
    if ($B_id_estat == "finalitzat") {
        $SQL .= " and laboral_data!=''";
    }
}

if($id_laboral!=0)	{ $SQL.=" and id_laboral=".$id_laboral;}

$SQL.=" order by data_sol DESC";
//echo $SQL;

$con=mysqli_query($cnx_intranet,$SQL);

while($fila=mysqli_fetch_array($con)){?>

<form method="post">
<table border="0" cellpadding="5" cellspacing="0">
<tr><td colspan="2"><strong>Sol&middot;licitud <?php echo $fila["id_laboral"];?></strong></td></tr>

<tr>
	<td width="100px">Treballador</td>
    <td><?php
	echo $fila["nom"]." ".$fila["cognom1"]." ".$fila["cognom2"];
	
	?></td>
</tr>
<tr>
	<td>Data</td>
    <td><?php echo calendari_visual($fila["data_sol"]);?></td>
</tr>

<tr>
	<td>Tema</td>
    <td><?php echo StripSlashes($fila["nom_tema"]);?></td>
</tr>
<tr>
	<td>Assumpte</td>
    <td><?php echo StripSlashes($fila["sol_assumpte"]);?></td>
</tr>
<tr>
	<td>Descripci&oacute;</td>
    <td><?php echo StripSlashes($fila["sol_desc"]);?></td>
</tr>

<?php $con1=mysqli_query($cnx_intranet,"select * from laboral_arxius where id_laboral=".$fila["id_laboral"]);
if(mysqli_num_rows($con1)!=0){?>
	<tr>
	<td>Arxius</td>
    <td><?php  while($fila1=mysqli_fetch_array($con1)){?>
    	<a href="carregues/<?php echo $fila1["ruta"];?>" target="_blank" class="MenuSuperior"><?php echo $fila1["nom_arxiu"]?></a><br />
    <?php }?></td>
</tr>
<?php }?>

<tr><td colspan="2"><strong>Validació</strong></td></tr>
<tr>
    <td>Validat</td>
    <td><?php if($fila["validador"]==0){ echo "si";}else {
            $con1 = mysqli_query($cnx_intranet, "select * from personal where id_persona=" . $fila["validador"]);
            $fila1 = mysqli_fetch_array($con1);

            echo "Pendent " . $fila1["nom"] . " " . $fila1["cognom1"];
        }

        ?></td>
</tr>

<tr><td colspan="2"><strong>RRHH</strong></td></tr>

 <tr>
    <td>Resposta</td>
    <td><?php if($id_laboral!=0){?>
    	<textarea name="laboral_informe" cols="70" rows="10" class="lletraNorm" id="text"><?php echo str_replace('<br />','',StripSlashes($fila["laboral_informe"]));?></textarea>
			
	<?php }else{
		echo StripSlashes($fila["laboral_informe"]);
	}?></td>
</tr>

<?php if($fila["laboral_data"]!=""){?>
<tr>
    <td>Data de resposta</td>
    <td><?php echo calendari_visual($fila["laboral_data"]);?></td>
</tr>
<?php } ?>
</table>

<?php if($fila["validador"]==0){ ?>
    <?php if($id_laboral!=0){?>
        <input type="submit" name="modificar" class="ico_guardar" value="" />

    <?php }else{?>
        <input type="submit" name="editar" class="boto_editar" value="" />
    <?php } ?>
    <input type="hidden" name="id_laboral" value="<?php echo $fila["id_laboral"];?>" />
    
     <input type="hidden" name="B_id_estat" value="<?php echo $B_id_estat;?>" />
     
     
      <input type="hidden" name="B_id_persona" value="<?php echo $B_id_persona;?>" />
      <input type="hidden" name="B_id_tema" value="<?php echo $B_id_tema;?>" />
    <?php include("comuns3.php");?>

<?php }?>

    <hr>
</form>


<?php }?>
