<?php 	
function resum($dia,$mes,$any,$id_persona){
	require_once("conexio.php");
	//-------------------------------------------Sol·licituds aprovades per absencies hores
	
	$con=mysqli_query($cnx_intranet,"select * from t_absencies_hores where 
	dia=".$dia." and mes=".$mes." and any=".$any." 
	and id_persona=".$id_persona);
	if(mysqli_num_rows($con)!=0){
	
		while($fila=mysqli_fetch_array($con)){
			
			$con1=mysqli_query($cnx_intranet,"select * from t_conceptes where id_concepte=".$fila["id_concepte"]);
			$fila1=mysqli_fetch_array($con1);
			
			echo "<span class=\"lletra_concepte_".$fila["id_concepte"]."\">".StripSlashes($fila1["nom_concepte"]);
			echo " de les ".$fila["hora_surt_p"].":".$fila["min_surt_p"]."h";
			if($fila["hora_torna_p"]!=0){
				echo " fins a les ".$fila["hora_torna_p"].":".$fila["min_torna_p"]."h.";
			}
			echo "</span><br>";
		}
	}
	
	
	
	
	//-------------------------------------------Entrades i sortides del treball
	$con0=mysqli_query($cnx_intranet,"select * from t_fitxatge where 
	dia=".$dia." and mes=".$mes." and any=".$any." 
	and id_persona=".$id_persona." order by entra_h");
	while($fila0=mysqli_fetch_array($con0)){
		echo "<span class=\"entrada\">Hora entrada: ".$fila0["entra_h"].":".$fila0["entra_m"]."h. ";
		if($fila0["surt_h"]!=''){
			echo "Hora sortida:".$fila0["surt_h"].":".$fila0["surt_m"]."h. ";
			echo "(";
			if(($fila0["dif_h"]=='')||($fila0["dif_h"]==0)){
				echo $fila0["dif_m"]."min";
			}else{
				echo $fila0["dif_h"].":".$fila0["dif_m"]."h.";
			}
			echo ")";
			echo "</span>";
		}else{
			echo "</span>";
			echo "<span class=\"lletraError\"> No va fitxar al sortir</span>";
		}
		echo "<br>";
		
	
		//-------------------------------------------entrades i sortides per fer el config
		$con=mysqli_query($cnx_intranet,"select * from t_f_descans where id_fitxatge=".$fila0["id_fitxatge"]." order by surt_h");
		while($fila=mysqli_fetch_array($con)){
			echo "<span class=\"descans\">Descans a les ".$fila["surt_h"].":".$fila["surt_m"]."h. ";
			if($fila["torna_h"]!=''){
				echo "fins a les ".$fila["torna_h"].":".$fila["torna_m"]."h. ";
				echo "(";
				if(($fila["dif_h"]=='')||($fila["dif_h"]==0)){
					echo $fila["dif_m"]."min";
				}else{
					echo $fila["dif_h"].":".$fila["dif_m"]."h";
				}
				echo ")";
				echo "</span>";
				
			}else{
				echo "</span>";
				echo "<span class=\"lletraError\"> No s'ha fitxat al tornar</span>";
			}
			echo "<br>";
			
		}
		
				//-------------------------------------------entrades i sortides per fer lactancia
		$con=mysqli_query($cnx_intranet,"select * from t_f_lactancia where id_fitxatge=".$fila0["id_fitxatge"]);
		while($fila=mysqli_fetch_array($con)){
			echo "<span class=\"descans\">lactància a les ".$fila["surt_h"].":".$fila["surt_m"]."h. ";
			if($fila["torna_h"]!=''){
				echo "fins a les ".$fila["torna_h"].":".$fila["torna_m"]."h. ";
				echo "(";
				if(($fila["dif_h"]=='')||($fila["dif_h"]==0)){
					echo $fila["dif_m"]."min";
				}else{
					echo $fila["dif_h"].":".$fila["dif_m"]."h";
				}
				echo ")";
				echo "</span>";
				
			}else{
				echo "</span>";
				echo "<span class=\"lletraError\"> No s'ha fitxat al tornar</span>";
			}
			echo "<br>";
			
		}
		
		//-------------------------------------------Entrades i sortides a l'exterior
		$con=mysqli_query($cnx_intranet,"select * from t_f_exterior where id_fitxatge=".$fila0["id_fitxatge"]." order by surt_h");
		
		while($fila=mysqli_fetch_array($con)){
			echo "<span class=\"exterior\">Sortida per fer un treball exterior (".StripSlashes($fila["lloc"])." - ".StripSlashes($fila["motiu"]).") ";
			echo " a les ".$fila["surt_h"].":".$fila["surt_m"]."h. ";
			if($fila["torna_h"]!=0){
				echo " fins a les ".$fila["torna_h"].":".$fila["torna_m"]."h. ";
				echo "</span>";
			}else{
				echo "</span>";
				echo "<span class=\"lletraError\"> No s'ha fitxat al tornar</span>";
			}
			echo "</span>";
			
			if($fila["automatic"]==1){?>
				<a href="laboral/historial/mensual/auto.html" target="_blank"><img src="imatges/atomatic.jpg" border="0" /></a>
			<?php }
			echo "<br>";
		}
	
	}
		
		
	
		
		
	//-----------------------------Temps de mes o de menys en t_descuadra
	$con=mysqli_query($cnx_intranet,"select * from t_f_descuadra where 
	dia=".$dia." and mes=".$mes." and any=".$any." 
	and id_persona=".$id_persona);
	if(mysqli_num_rows($con)!=0){
		$fila=mysqli_fetch_array($con);
		if($fila["valor"]==1){
					if($fila["dif_h"]==0){?>
						<img src="http://www.selvaesports.cat/laboral/imatges/cara_alegre.gif" width="30" height="30" align="absmiddle">
					<?php }elseif($fila["dif_h"]!=0){?>
							<img src="http://www.selvaesports.cat/laboral/imatges/cara_hores_mes.gif" width="30" height="30"
							align="absmiddle" class="ma" alt="Hores de mes">
					<?php }?>
			<?php }if($fila["valor"]==0){
					if($fila["dif_h"]==0){?>
						<img src="http://www.selvaesports.cat/laboral/imatges/cara_trist.gif" width="30" height="30" align="absmiddle">
					<?php }elseif($fila["dif_h"]!=0){?>
							<img src="http://www.selvaesports.cat/laboral/imatges/cara_terrible.gif" width="30" height="30"
							align="absmiddle"  class="ma" alt="Hores de menys">
					<?php }?>
			<?php }
			
			if($fila["valor"]==0){ echo "<span class=\"incorrecte\"> &#8211;";
			}else{ echo "<span class=\"lletraPeke\">+";}
			
			if($fila["dif_h"]==0){
				echo $fila["dif_m"]." min";
			}else{
				echo $fila["dif_h"].":".$fila["dif_m"]." h";
			}
			echo "</span>";
	}
	
}

?>


