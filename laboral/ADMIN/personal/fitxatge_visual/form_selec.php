<script language="javascript">
function validar_veure(){
	if((window.document.form_seleccio.any.value=='')){
		alert("ERROR.Heu de seleccionar un any");
		return (false);
	}
	if((window.document.form_seleccio.mes.value=="")&&
	(window.document.form_seleccio.dia.value=="")&&
	(window.document.form_seleccio.organigrama.value=="")&&
	(window.document.form_seleccio.any.value=='') ){
		alert("ERROR.Heu de seleccionar algun criteri de visualització ");
		return (false);
	}else{
		window.document.form_seleccio.veure.value=1;
		return (true);
	}
}

</script>
<div class="quadre_desplegables">
  <form  name="form_seleccio" method="post" >

    <span class="lletraNegreta">Data</span>      

	   <select name="any"  class="lletraNorm">
          <?php  for($i=(date('Y')-2);$i<=date('Y');$i++){?>
          <option value="<?php echo   $i;?>" <?php if($_POST["any"]==$i){ echo "selected";}?>><?php echo   $i;?></option>
          <?php }?>
        </select>
        <select name="mes" class="lletraNorm">
          <option value=""></option>
          <?php for($i=1;$i<=12;$i++){?>
          <option value="<?php echo   $i;?>" <?php if( (isset($_POST["mes"]))&&($_POST["mes"]==$i)){ echo "selected";}?>><?php echo   $nom_mes[$i];?></option>
          <?php }?>
        </select>
	  
	  <select name="dia"  class="lletraNorm">
          <option value=""></option>
          <?php 
	  for($i=1;$i<=31;$i++){?>
          <option value="<?php echo   $i;?>" <?php if( (isset($_POST["dia"]))&&($_POST["dia"]==$i)){ echo "selected";}?>><?php echo   $i;?></option>
          <?php }?>
        </select>
		
	 <br />
     <?php if($_SESSION["perfil"]!="CBSS"){?>
    <span class="lletraNegreta">Area</span>
	    <select name="organigrama"  onchange="submit();">
          <option value=""></option>
          <?php  
          $con1=mysqli_query($cnx_intranet,"SELECT * FROM orga1");
          while($fila1=mysqli_fetch_array($con1)){?>
            <option value="<?php echo   $fila1["id_orga1"];?>" 
                class="orga1_<?php echo   $fila1["id_orga1"];?>"
                <?php if((isset($_POST["organigrama"]))&&($_POST["organigrama"]==$fila1["id_orga1"])){ echo "selected";}?>
                ><?php echo   $fila1["nom_orga1"]?></option>
          
          <?php 
              
              $con2=mysqli_query($cnx_intranet,"SELECT * FROM orga2 where id_orga1=".$fila1["id_orga1"]);
              while($fila2=mysqli_fetch_array($con2)){?>
                <option value="<?php echo   $fila1["id_orga1"]."_".$fila2["id_orga2"];?>" 
                class="orga1_<?php echo   $fila1["id_orga1"];?>"
                <?php if((isset($_POST["organigrama"]))&&($_POST["organigrama"]==($fila1["id_orga1"]."_".$fila2["id_orga2"]))){ echo "selected";}?>
                
                ><?php echo   "------>".$fila2["nom_orga2"];?></option>
                
               
                
                
                <?php }?>
          <?php }?>
    </select>
<?php }?>
		
<?php 
$consultaa="";
if((isset($_POST["organigrama"]))&&($_POST["organigrama"]!="")){
	$id_orga1=0;
	$id_orga2=0;

	$con1=mysqli_query($cnx_intranet,"SELECT * FROM orga1");
	while($fila1=mysqli_fetch_array($con1)){
		  $valor=$fila1["id_orga1"];
			
			if($_POST["organigrama"]==$valor){
				$id_orga1=$fila1["id_orga1"];
			}
		
		
		  $con2=mysqli_query($cnx_intranet,"SELECT * FROM orga2 where id_orga1=".$fila1["id_orga1"]);
		  while($fila2=mysqli_fetch_array($con2)){
				  
			$valor=$fila1["id_orga1"]."_".$fila2["id_orga2"];
			
			if($_POST["organigrama"]==$valor){
				$id_orga1=$fila1["id_orga1"];
				$id_orga2=$fila2["id_orga2"];
			}
			
			
		 }
	}
	$consultaa.=" and personal.id_orga1=".$id_orga1;
	if($id_orga2!=0){
		$consultaa.=" and personal.id_orga2=".$id_orga2;
	}

}




	  //Si hem seleccinat una data veiem tots els treballador_firma que aquella data tenien contracte
	  if( (isset($_POST["any"]))&&($_POST["any"]!="")&&(isset($_POST["mes"]))&&($_POST["mes"]!="") ){
	  
			 if( (isset($_POST["dia"]))&&($_POST["dia"]!="") ){
				$data=convert_data8($_POST["any"],$_POST["mes"],$_POST["dia"]);
			}else{
				$data=convert_data8($_POST["any"],$_POST["mes"],date('d'));
			}
			
			$consulta="SELECT * FROM personal,t_visual,t_contracte where 
			personal.id_persona=t_visual.treballador and 
			t_visual.visualitzador=".$_SESSION["id_usuari"]." and 
			personal.id_persona=t_contracte.id_persona and 
			t_contracte.inici<=".$data." and 
			(t_contracte.fi>=".$data." or t_contracte.fi='')and 
			personal.obli_fitxar=1 ".$consultaa."
			group by personal.id_persona
			order by personal.cognom1";
	  }else{
			 $consulta="SELECT * FROM personal,t_visual where 
			personal.id_persona=t_visual.treballador and 
			t_visual.visualitzador=".$_SESSION["id_usuari"]." and 
			personal.exist=1 and 
			personal.obli_fitxar=1 ".$consultaa." 
			group by personal.id_persona
			order by personal.cognom1";
	}
	$con=mysqli_query($cnx_intranet,$consulta);?>
    <br />
    <span class="lletraNegreta">Treballador</span>
	<select name="id_persona" class="lletraNorm" >
	  <option value=""></option>
	 <?php while($fila=mysqli_fetch_array($con)){?>
			  <option value="<?php echo   $fila["id_persona"];?>" 
			  <?php if( (isset($_POST["id_persona"]))&&($_POST["id_persona"]==$fila["id_persona"])){ echo "selected";}?>
			  ><?php echo   $fila["cognom1"]." ".$fila["cognom2"].", ".$fila["nom"];?></option>
	  <?php }?>
	</select>

 
 <br />
      <input type="submit" value="" onClick="return validar_veure();" class="boto_visual">

<input type="hidden" name="veure">
<?php include("comuns3.php");?>
  </form>
</div>
