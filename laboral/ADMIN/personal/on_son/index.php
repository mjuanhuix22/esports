<?php 
if(($_SESSION["perfil"]=="personal")||($_SESSION["perfil"]=="personal_aux")||($_SESSION["perfil"]=="pressupost")||($_SESSION["perfil"]=="gerent")){
	mysqli_query($cnx_intranet,"update elena set on_son='".date('d/m/Y H:i')."';");

}
?>

<style type="text/css">
.t_1{
	color:#393;
	font-size:12px;
}
.t_2{
	color:#900;
	font-size:12px;
}
.t_3{
	color:#000;
	font-size:12px;
}
</style>
<p class="lletraNegreta">A les <?php echo   date('H.i'); ?> els treballadors son a:</p>

<table  border="0" cellspacing="5" cellpadding="0">
<?php 

$con=mysqli_query($cnx_intranet,"SELECT * FROM personal,t_visual where 
personal.id_persona=t_visual.treballador and 
t_visual.visualitzador=".$_SESSION["id_usuari"]." and 
exist=1  
order by personal.cognom1");
while($fila=mysqli_fetch_array($con)){?>
	<tr>
	  <td width="150"  class="lletraNorm"><?php echo   $fila["cognom1"]." ".$fila["cognom2"].", ".$fila["nom"];?> </td>
	  <td>
	   <?php 
	   $entra=0;
	   
		if($entra==0){
		   $con1=mysqli_query($cnx_intranet,"select * from t_f_descans where id_persona=".$fila["id_persona"]." and 
		   dia=".date('d')." and mes=".date('m')." and any=".date('Y')." and torna_h=''");
		   if(mysqli_num_rows($con1)==1){
		   		$fila1=mysqli_fetch_array($con1);
				$on="Està descansant des de les ".$fila1["surt_h"].":".$fila1["surt_m"]."h.";
				$class="t_3";
				$entra=1;
			}
		}
		if($entra==0){
		   $con1=mysqli_query($cnx_intranet,"select * from t_f_exterior where id_persona=".$fila["id_persona"]." and 
		   dia=".date('d')." and mes=".date('m')." and any=".date('Y')." and torna_h=''");
		   if(mysqli_num_rows($con1)==1){
		   		$fila1=mysqli_fetch_array($con1);
				$on="Està treballant a l'exterior (".StripSlashes($fila1["lloc"])." - ".StripSlashes($fila1["motiu"]).") ";
				$on.="des de les ".$fila1["surt_h"].":".$fila1["surt_m"]."h.";
				if($fila1["hora_prevista"]!=""){
					$on.="I té previst tornar a ".$fila1["hora_prevista"];
				}else{
					$on.="I té previst NO tornar i acabar el treball a les ".$fila1["hora_tanca"].":".$fila1["minut_tanca"]."h.";
				}
				$class="t_1";
				$entra=1;
			}
		
		}
		if($entra==0){
		   $con1=mysqli_query($cnx_intranet,"select * from t_fitxatge where id_persona=".$fila["id_persona"]." and 
		   dia=".date('d')." and mes=".date('m')." and any=".date('Y')." and surt_h=''");
		 	if(mysqli_num_rows($con1)==1){
		   		$fila1=mysqli_fetch_array($con1);
				$on="Està treballant des de les ".$fila1["entra_h"].":".$fila1["entra_m"]."h.";
				$class="t_1";
				$entra=1;
			}
		}
		
		
		
		$con1=mysqli_query($cnx_intranet,"select * from t_absencies_jornada where id_persona=".$fila["id_persona"]." and 
	    dia=".date('d')." and mes=".date('m')." and any=".date('Y'));
	    if(mysqli_num_rows($con1)==1){
	   		$on="Te permís retribuït per ";
			$fila1=mysqli_fetch_array($con1);
			$con2=mysqli_query($cnx_intranet,"select * from t_conceptes where id_concepte=".$fila1["id_concepte"]);
			$fila2=mysqli_fetch_array($con2);
			$on.=$fila2["nom_concepte"];
			$class="t_3";
			$entra=1;
			if($fila1["validador"]!=0){ $on.=" (Pendent de validar)";}
		}
		if($entra==0){
			$con1=mysqli_query($cnx_intranet,"select * from t_absencies_hores where id_persona=".$fila["id_persona"]." and 
			dia=".date('d')." and mes=".date('m')." and any=".date('Y')." and 
			(hora_surt_p<".date('H')." or (hora_surt_p=".date('H')." and min_surt_p<".date('i').")) and 
			(hora_torna_p>".date('H')." or (hora_torna_p=".date('H')." and min_torna_p>".date('i').")) "
			);
			if(mysqli_num_rows($con1)==1){
				$on="Te permís retribuït per ";
				$fila1=mysqli_fetch_array($con1);
				$con2=mysqli_query($cnx_intranet,"select * from t_conceptes where id_concepte=".$fila1["id_concepte"]);
				$fila2=mysqli_fetch_array($con2);
				$on.=$fila2["nom_concepte"];
				$class="t_3";
				$entra=1;
				if($fila1["validador"]!=0){ $on.="( Pendent de validar)";}
			}
		}
		if($entra==0){
			if($fila["obli_fitxar"]==0){
				$on="No està obligat a fitxar";
				$class="t_3";
				$entra=1;
			}
		}
		
		
		
		if($entra==0){
			$con1=mysqli_query($cnx_intranet,"SELECT * FROM t_contracte WHERE id_persona=".$fila["id_persona"]." 
			and inici<=".data_actual8()." and (fi>=".data_actual8()." or fi='')");
			if(mysqli_num_rows($con1)==0){
				$class="t_2";
				$on="No té cap contracte vigent";
				$entra=1;
			}else{
				$fila1=mysqli_fetch_array($con1);
				$con1=mysqli_query($cnx_intranet,"select * from t_contracte_hores where id_contracte=".$fila1["id_contracte"]." 
				and diaset=".dia7(date('Y'),date('m'),date('d'))." order by de_h");
				if(mysqli_num_rows($con1)==0){
					$on="Els ".$dies_setmana[date('l')]." no treballa";
					$entra=1;
					$class="t_3";
				}else{
						//Encara que tingui mes de 1 registre nomes m'interessa el primer					
						$fila1=mysqli_fetch_array($con1);
						if( ($fila1["de_h"]>date('H')) || (($fila1["de_h"]==date('H'))&&($fila1["de_m"]>date('i')) ) ){
							$on="Segons el seu contracte ha de començar a treballar a les ".$fila1["de_h"].":".$fila1["de_m"];
							$entra=1;
							$class="t_3";
						}
					
				
				}
			}
		}

		if($entra==0){
			$con1=mysqli_query($cnx_intranet,"select * from t_horari,t_horari_sol where 
			t_horari.id_sol=t_horari_sol.id_sol and 
			t_horari_sol.id_persona=".$fila["id_persona"]." and 
			t_horari.dia=".date('d')." and t_horari.mes=".date('m')." and t_horari.any=".date('Y')." 
			and t_horari.de_h<='".date('H')."' and t_horari.a_h>='".date('H')."' and t_horari.menys=1");
			if(mysqli_num_rows($con1)==1){
				$fila1=mysqli_fetch_array($con1);
				$con2=mysqli_query($cnx_intranet,"select * from t_horari_sol,t_horari_motiu where 
				t_horari_sol.id_sol=".$fila1["id_sol"]." and 
				t_horari_sol.id_motiu=t_horari_motiu.id_motiu");
				$fila2=mysqli_fetch_array($con2);
				if($fila2["menys"]==1){
					$on="Te permis no retribuit per ";
				}
				else{
					$on="Está compensant hores per ";
				}
				$on.=$fila2["nom_motiu"];
				$class="t_3";
				$entra=1;
				if($fila1["validador"]!=0){ $on.=" (Pendent de validar)";}
			}
		}
		if($entra==0){
			 $con1=mysqli_query($cnx_intranet,"select * from t_fitxatge where id_persona=".$fila["id_persona"]." and 
		   dia=".date('d')." and mes=".date('m')." and any=".date('Y'));
		    if(mysqli_num_rows($con1)==0){
				$class="t_2";
				$on="Avui no ha vingut a treballar";
			}else{
				 $con1=mysqli_query($cnx_intranet,"select * from t_fitxatge where id_persona=".$fila["id_persona"]." and 
		  		 dia=".date('d')." and mes=".date('m')." and any=".date('Y'));
				 $fila1=mysqli_fetch_array($con1);
				 $class="t_3";
				 $on="Ha plegat de treballar a les ".$fila1["surt_h"].":".$fila1["surt_m"]."h.";
			}
		}

		
		
	   ?><span class="<?php echo   $class;?>"><?php echo   $on;?></span>
	  </td>
	</tr>
<?php } ?>
</table>