<?php

//Suma del temps treballat

$M_treballats=0;
$H_treballats=0;

$con=mysqli_query($cnx_intranet,"select dif_h,dif_m from t_fitxatge where 
dia=".$dia." and mes=".$mes." and any=".$any." and id_persona=".$id_persona);
while($fila=mysqli_fetch_array($con)){
	list($H_treballats,$M_treballats)=sumar_temps($H_treballats,$M_treballats,$fila["dif_h"],$fila["dif_m"]);
}

$text.="Fitxatge :".$H_treballats.":".$M_treballats."<br>";


//suma temps descansat
$h_descans=0;
$m_descans=0;
$con=mysqli_query($cnx_intranet,"select dif_h,dif_m from t_f_descans where 
dia=".$dia." and mes=".$mes." and any=".$any." and id_persona=".$id_persona);
while($fila=mysqli_fetch_array($con)){
	list($h_descans,$m_descans)=sumar_temps($h_descans,$m_descans,$fila["dif_h"],$fila["dif_m"]);
}

$SQLC="select * from t_config where 
            id_persona=".$id_persona." and  
            data_inici<=".convert_data8($any,$mes,$dia)." and  
            (data_fi>=".convert_data8($any,$mes,$dia)." or data_fi is null)";
$conC=mysqli_query($cnx_intranet,$SQLC);
if(mysqli_num_rows($conC)==0){
    $SQLC="select * from t_config where data_inici<=".convert_data8($any,$mes,$dia)." and 
                (data_fi>=".convert_data8($any,$mes,$dia)." or data_fi is null) 
                and id_persona is null";
}
$conC=mysqli_query($cnx_intranet,$SQLC);
$filaC=mysqli_fetch_array($conC);



//Si ha fet més de 30 minuts de descans restem els minuts de més al la suma del temps treballat
if(($h_descans>0)||($m_descans>$filaC["minuts_descans"])){
	list($h,$m)=restar_temps($h_descans,$m_descans,0,$filaC["minuts_descans"]);
	
	$text.="-Descans: ".$h.":".$m."<br>";
	list($H_treballats,$M_treballats)=restar_temps($H_treballats,$M_treballats,$h,$m);
		
}

$text.="Temps treballat :".$H_treballats.":".$M_treballats."<br><br>";


?>