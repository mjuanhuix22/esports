<?php
if(!isset($_SESSION["id_usuari"])){
	header("location:http://www.selvaesports.cat/laboral/index.php");
}
include("../../conexio.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Formulari de sol&middot;licitud</title>
<link href="../../estils.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body{
	border:0;
}
-->
</style>
</head>

<body>
  <?php $con=mysqli_query($cnx_intranet,"select * from personal where id_persona=".$_SESSION["id_usuari"]);
 $fila=mysqli_fetch_array($con);
 ?>
<table width="600" cellpadding="0" cellspacing="10">
<tr><td>
<img src="../../../imatges/id_empresa_<?php echo   $fila["id_empresa"];?>.gif" alt="logo consell"  hspace="10" >
</td>
<td width="100%" class="lletra20" align="center">Sol&middot;licitud de canvi d'horari</td>
</tr>
</table>


<p  class="lletraNorm">Ompliu el seg&uuml;ent formulari, imprimiu-lo, firmeu-lo i porteu-lo a <?php 
$con1=mysqli_query($cnx_intranet,"select personal.nom,personal.cognom1,personal.sexe from t_validacio,personal where 
t_validacio.validador=personal.id_persona and 
t_validacio.treballador=".$_SESSION["id_usuari"]." and validador!=3 order by esglao");
while($fila1=mysqli_fetch_array($con1)){
	echo article($fila1["nom"],$fila1["sexe"]);
	echo $fila1["nom"]." ".$fila1["cognom1"].", ";
}?>.</p>

 <form name="form1" method="post" action="">
   <table width="600"  border="1">
     <tr bordercolor="#FFFFFF">
       <td width="40%" class="lletraSubtituls">Nom del treballador:       </td>
       <td><input name="textfield" type="text" class="lletraNorm" size="50" value="<?php echo   $fila["nom"]." ".$fila["cognom1"]." ".$fila["cognom2"];?>"></td>
     </tr>
     <tr bordercolor="#FFFFFF">
       <td class="lletraSubtituls">Data en qu&egrave; es fa la sol&middot;licitud:</td>
       <td><input name="textfield" type="text" class="lletraNorm" value="<?php echo   date('d');?>" size="2" maxlength="2">
/
  <?php $nom_mes=array(1=>'gener',2=>'febrer',3=>'mar&ccedil;',4=>'abril',5=>'maig',6=>'juny',
7=>'juliol',8=>'agost',9=>'setembre',10=>'octubre',11=>'novembre','12'=>"desembre");?>
  <input name="textfield" type="text" size="8" value="<?php echo   $nom_mes[(int)date('m')];?>" class="lletraNorm">
/
<input name="textfield" type="text" class="lletraNorm" value="<?php echo   date('Y');?>" size="4" maxlength="4"></td>
     </tr>
   </table>
   <br>
   <table width="600"  border="1">
     <tr bordercolor="#FFFFFF">
       <td width="40%" class="lletraSubtituls">Sol&middot;licitud <?php if($fila2["menys"]==1){?> de perm&iacute;s no retribu&iuml;t per<?php }else{?>d'hores extres<?php }?>:</td>
       <td bordercolor="#FFFFFF"><input name="textfield" type="text" class="lletraNorm" size="50" value="<?php echo   StripSlashes($fila2["nom_motiu"]);?>"></td>
     </tr>
     <tr bordercolor="#FFFFFF">
       <td class="lletraSubtituls">Especificaci&oacute; del motiu: </td>
       <td><input name="textfield" type="text" class="lletraNorm" size="50"></td>
     </tr>
   </table>
   <br>
   <table width="600"  border="1">
     <tr>
       <td bordercolor="#FFFFFF" class="lletraSubtituls">
	   <?php if($fila2["menys"]==1){?>
	   	Hores no treballades (dia,mes,any,hora inici,hora fi):
	   <?php }else{?>
	   Hores treballades(dia,mes,any,hora inici,hora fi):
	   <?php }?></td>
     </tr>
     <tr>
       <td bordercolor="#FFFFFF"><textarea name="textarea" cols="110" rows="10" class="lletraNorm"></textarea></td>
     </tr>
     <tr>
       <td bordercolor="#FFFFFF" class="lletraSubtituls">
	   <?php if($fila2["menys"]==1){?>
	   	Hores de recuperació (dia,mes,any,hora inici,hora fi):
	   <?php }else{?>
	   Hores compensades (dia,mes,any,hora inici,hora fi):
	   <?php }?>
	   </td>
     </tr>
     <tr>
       <td bordercolor="#FFFFFF"><textarea name="textarea" cols="110" rows="10" class="lletraNorm"></textarea></td>
     </tr>

   </table>
   <br>
   <table width="600"  border="1" cellpadding="5">
     <tr>
       <td bordercolor="#FFFFFF" class="lletraSubtituls">Motiu pel qual feu la sol&middot;licitud fora de termini o anul&middot;leu una sol&middot;licitud pr&egrave;via: </td>
     </tr>
     <tr>
       <td bordercolor="#FFFFFF"><textarea name="textarea" cols="110" rows="5" class="lletraNorm"></textarea> </td>
     </tr>
   </table>



<p class="lletraSubtituls">Signatures:</p>
<?php 
$num=1;
$con1=mysqli_query($cnx_intranet,"select count(*) from t_validacio,personal where 
t_validacio.validador=personal.id_persona and 
t_validacio.treballador=".$_SESSION["id_usuari"]." and validador!=3 order by esglao");
while($fila1=mysqli_fetch_array($con1)){
	$num=$num+$fila1["count(*)"];
}
$amplada=100/$num;
?>

 <table width="600"  border="1" cellpadding="5" cellspacing="0" height="100">
     <tr valign="top" class="lletraNorm">
       <td width="<?php echo   $amplada;?>%"><p><?php echo   $fila["nom"]." ".$fila["cognom1"]." ".$fila["cognom2"];?></p>
       </td>

	<?php 
    $con1=mysqli_query($cnx_intranet,"select personal.nom,personal.cognom1,personal.descr,personal.cognom2 
	from t_validacio,personal where 
    t_validacio.validador=personal.id_persona and 
    t_validacio.treballador=".$_SESSION["id_usuari"]." and validador!=3 order by esglao");
    while($fila1=mysqli_fetch_array($con1)){?>
         <td width="<?php echo   $amplada;?>%"><p><?php echo   StripSlashes($fila1["descr"]);?><br> 
         <?php echo   $fila1["nom"]." ".$fila1["cognom1"]." ".$fila1["cognom2"];?></p>
           </td>
    <?php }?>
    </tr>
</table>
 </form>
</body>
</html>
