<?php
function validar_interval($de_h,$de_m,$a_h,$a_m,$id_fitxatge){

	$validat=1;
	require("conexio.php");
	
	$entra_nou=$de_h.$de_m;
	$surt_nou=$a_h.$a_m;
	
	$con=mysqli_query($cnx_intranet,"select * from t_fitxatge_mod where 
	id_fitxatge=".$id_fitxatge);
	$fila=mysqli_fetch_array($con);
	$entra=$fila["entra_h"].$fila["entra_m"];
	
	
	if(($surt_nou<=$entra)||($entra_nou<$entra)) {
		$validat=0;
		msn("Error. NO està dins l'interval de temps de l'entrada/sortida",0);
	}
	if($fila["surt_h"]!=0){
		$surt=$fila["surt_h"].$fila["surt_m"];	
		if(($entra_nou>=$surt)||($surt_nou>$surt)) {
			$validat=0;
			msn("Error. NO està dins l'interval de temps de l'entrada/sortida",0);
		}
	}
	return ($validat);
}

?>