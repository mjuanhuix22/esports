<script language="javascript">

function validar_form(opcio,form){
	
	var formulari=eval('window.document.form_exterior_'+form);
	
	if( (formulari.lloc.value=="")||
		 (formulari.motiu.value=="")){
		alert("Es obligatori omplir el formulari");
		return (false);
	}
	if(opcio==1){i_max=5;}
	else if(opcio==2){i_max=7;}
	else if(opcio==3){i_max=11;}
	
	for(i=3;i<i_max;i++){
		
		if(formulari.elements[i].value==""){
			alert("Es obligatori omplir el formulari");
			return (false);
		}
		if(isNaN(formulari.elements[i].value)) {
			alert("Error.L'horari que heu escrit no existeix");
			return (false);
		}
	}
	
	var i=4;
	while(i<i_max){
		if(formulari.elements[i].value>59){
			alert("Error.L'horari que heu escrit no existeix");
			return (false);
		}
		i=i+2;
	}
	for(i=3;i<=i_max;i++){
		
		if(formulari.elements[i].value.length==1){
			formulari.elements[i].value="0"+formulari.elements[i].value;
		}
	}


			
			
	if(opcio==2){
		if( (formulari.hora_tanca.value<formulari.hora_comen.value)||
		((formulari.hora_tanca.value==formulari.hora_comen.value)&&
		(formulari.minut_tanca.value<formulari.minut_comen.value)) ){
			alert("Error horari");
			return (false);
		}
	
	}
	
	if(opcio==3){
		
		if( (formulari.hora_tanca_mati.value<formulari.hora_comen_mati.value)||
		((formulari.hora_tanca_mati.value==formulari.hora_comen_mati.value)&&
		(formulari.minut_tanca_mati.value<formulari.minut_comen_mati.value)) ){
			alert("Error horari del matí");
			return (false);
		}
		
		if( (formulari.hora_tanca_tarda.value<formulari.hora_comen_tarda.value)||
		((formulari.hora_tanca_tarda.value==formulari.hora_comen_tarda.value)&&
		(formulari.minut_tanca_tarda.value<formulari.minut_comen_tarda.value)) ){
			alert("Error horari de la tarda");
			return (false);
		}
		
		if( (formulari.hora_comen_tarda.value<formulari.hora_tanca_mati.value)||
		((formulari.hora_comen_tarda.value==formulari.hora_tanca_mati.value)&&
		(formulari.minut_comen_tarda.value<formulari.minut_tanca_mati.value)) ){
			alert("Error. L'entrada de la tarda ha de ser superior a la sortida del matí");
			return (false);
		}
	}
	
	return (true);

}
</script>