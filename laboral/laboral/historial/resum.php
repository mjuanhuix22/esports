
<?php 
//-------------------------------------------Entrades i sortides del treball
$con=mysqli_query($cnx_intranet,"select * from t_fitxatge where 
dia=".(int)date('d')." and mes=".(int)date('m')." and any=".date('Y')." 
and id_persona=".$_SESSION["id_usuari"]);
while($fila=mysqli_fetch_array($con)){
	echo "<br><span class=\"entrada\">Hora entrada: ".$fila["entra_h"].":".$fila["entra_m"]."h. ";
	if($fila["surt_h"]!=''){
		echo "Hora sortida:".$fila["surt_h"].":".$fila["surt_m"]."h ";
		echo "(";
		if(($fila["dif_h"]=='')||($fila["dif_h"]==0)){
			echo $fila["dif_m"]."min";
		}else{
			echo $fila["dif_h"].":".$fila["dif_m"]."h";
		}
		echo ")";
		
	}
	echo "</span>";
}

//-------------------------------------------entrades i sortides per fer el config
$con=mysqli_query($cnx_intranet,"select * from t_f_descans where 
dia=".(int)date('d')." and mes=".(int)date('m')." and any=".date('Y')." 
and id_persona=".$_SESSION["id_usuari"]);
while($fila=mysqli_fetch_array($con)){
	echo "<br><span class=\"descans\">Descans de les ".$fila["surt_h"].":".$fila["surt_m"]."h ";
	if($fila["torna_h"]!=''){
		echo "fins a les ".$fila["torna_h"].":".$fila["torna_m"]."h ";
		echo "(";
		if(($fila["dif_h"]=='')||($fila["dif_h"]==0)){
			echo $fila["dif_m"]."min";
		}else{
			echo $fila["dif_h"].":".$fila["dif_m"]."h";
		}
		echo ")";
		
	}
	echo "</span>";
}

//-------------------------------------------Entrades i sortides a l'exterior
$con=mysqli_query($cnx_intranet,"select * from t_f_exterior where 
dia=".(int)date('d')." and mes=".(int)date('m')." and any=".date('Y')." 
and id_persona=".$_SESSION["id_usuari"]);

while($fila=mysqli_fetch_array($con)){
	echo "<br><span class=\"exterior\">Sortida per fer un treball exterior (".StripSlashes($fila["lloc"]).") ";
	echo " de les ".$fila["surt_h"].":".$fila["surt_m"]."h ";
	if($fila["torna_h"]!=0){
		echo " fins a les ".$fila["torna_h"].":".$fila["torna_m"]."h. ";
	}else{
		if($fila["hora_prevista"]!=NULL){
			echo " fins a les ".StripSlashes($fila["hora_prevista"])."h (hora prevista).";
		}
	}
	echo "</span>";
}


//-------------------------------------------Entrades i sortides per absencies hores
$con=mysqli_query($cnx_intranet,"select * from t_absencies_hores where 
dia=".(int)date('d')." and mes=".(int)date('m')." and any=".date('Y')." 
and id_persona=".$_SESSION["id_usuari"]." and hora_surt!=0");
if(mysqli_num_rows($con)!=0){

	while($fila=mysqli_fetch_array($con)){
		
		$con1=mysqli_query($cnx_intranet,"select * from t_conceptes where id_concepte=".$fila["id_concepte"]);
		$fila1=mysqli_fetch_array($con1);
		
		echo "<br><span class=\"concepte_".$fila["id_concepte"]."\">".$fila1["nom_concepte"];
		echo " de les ".$fila["hora_surt_p"].":".$fila["min_surt_p"]."h.";
		echo " fins a les ".$fila["hora_torna_p"].":".$fila["min_torna_p"]."h. (hora prevista).";
		echo "</span>";
	}
	
}
?>

