<?php
//S'ha de tenir en compte els dies que selecciona l'usuari. Mirar quin és el que te la data més petita i sumar 3 mesos a aquesta data. Així sabrem quina és la data màxima de compensació.

//El màxim per compensar és 3 mesos

$mes_min=date('m');
$any_min=date('Y');
$dia_min=date('d');
//Primer busco la data més petita que ha sol·licitat compensar

$con=mysqli_query($cnx_intranet,"select * from t_compensa where id_persona=".$_SESSION["id_usuari"]."  and 
( (any=".$ultim_any." and mes=".$ultim_mes." and dia>".date('d').") or 
(any=".$ultim_any." and mes>".$ultim_mes.") or
(any>".$ultim_any.") ) order by any,mes,dia,de_h,de_m ");
while($fila=mysqli_fetch_array($con)){
	$casella="casella_".$fila["id_compensa"];
	if((isset($_POST["$casella"]))&&($_POST["$casella"]==1)){
		if( ($fila["any"]<$any_min)||
		    (($fila["any"]==$any_min)&&($fila["mes"]<$mes_min))||
			(($fila["any"]==$any_min)&&($fila["mes"]==$mes_min)&&($fila["dia"]<$dia_min)) ){
			$mes_min=$fila["mes"];	
			$dia_min=$fila["dia"];
			$any_min=$fila["any"];
		}
	}
}
$con=mysqli_query($cnx_intranet,"select * from t_horari_motiu");
$fila=mysqli_fetch_array($con); 
$any_max=$any_min;
$mes_max=$mes_min;
for($i=1;$i<=$fila["mesos_sol"];$i++){
	if($mes_max==12){
		$mes_max=1;
		$any_max++;	
	}else{
		$mes_max++;	
	}
}
/*
//Ara hem de sumar 3 mesos
if($mes_min==10){ $mes_max=1;$any_max=$any_min+1;}
elseif($mes_min==11){ $mes_max=2;$any_max=$any_min+1;}
elseif($mes_min==12){ $mes_max=3;$any_max=$any_min+1;}
else{ $mes_max=$mes_min+3;$any_max=$any_min;}*/
$dia_max=$dia_min;
$data_max=convert_data8($any_max,$mes_max,$dia_max);

//echo $data_max;


//Ara comprovem que tots els dies seleccionats que no treballara No son superior a la data maxima
for($n=1;$n<=$_POST["num_unless"];$n++){
	$dia="dia_unless_".$n;
	$dia=$_POST["$dia"];
	$mes="mes_unless_".$n;
	$mes=$_POST["$mes"];
	$any="any_unless_".$n;
	$any=$_POST["$any"];

	$data_unless=convert_data8($any,$mes,$dia);
	if($data_unless>$data_max){
		$validat=0;
		msn("El dia màxim que pots compensar es ".$dia_max."/".$mes_max."/".$any_max,0);
	}
}
?>