<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<h4>Visualitzar incidències</h4><?php


$SQL="SELECT *  FROM laboral,laboral_tema,personal where 
laboral.id_persona=personal.id_persona and 
laboral.sol_tema=laboral_tema.id_tema and  
personal.id_persona=".$_SESSION["id_usuari"];
$SQL.=" group by laboral.id_laboral order by data_sol DESC";

$con=mysqli_query($cnx_intranet,$SQL);

//$con=mysqli_query($cnx_intranet,"SELECT * FROM laboral,laboral_tema where laboral.sol_tema=laboral_tema.id_tema and sol_persona=".$_SESSION["id_usuari"]." group by laboral.id_laboral order by sol_data DESC");
while($fila=mysqli_fetch_array($con)){?>
<table border="0" cellpadding="5" cellspacing="0">
<tr><td colspan="2"><strong>Sol&middot;licitud <?php echo $fila["id_laboral"];?></strong></td></tr>

<tr>
    <td >Estat actual</td>
    <td class="lletraSubtituls" ><?php
        if($fila["validador"]!=0){
            echo "Pendent validar";
        } elseif($fila["laboral_data"]==''){
            echo "Pendent RRHH";
        }else{
            echo "Finalitzat";
        }
        ?>
    </td>
</tr>

<tr>
	<td width="100px">Treballador</td>
    <td ><?php 
	echo $fila["nom"]." ".$fila["cognom1"]." ".$fila["cognom2"];
	
	?></td>
</tr>

<tr>
	<td>Data</td>
    <td><?php echo calendari_visual($fila["data_sol"]);?></td>
</tr>

<tr>
	<td>Tema</td>
    <td><?php echo StripSlashes($fila["nom_tema"]);?></td>
</tr>
<tr>
	<td>Assumpte</td>
    <td><?php echo StripSlashes($fila["sol_assumpte"]);?></td>
</tr>
<tr>
	<td>Descripci&oacute;</td>
    <td><?php echo StripSlashes($fila["sol_desc"]);?></td>
</tr>

<?php $con1=mysqli_query($cnx_intranet,"select * from laboral_arxius where id_laboral=".$fila["id_laboral"]);
if(mysqli_num_rows($con1)!=0){?>
	<tr>
	<td>Arxius</td>
    <td><?php  while($fila1=mysqli_fetch_array($con1)){?>
    	<a href="carregues/<?php echo $fila1["ruta"];?>" target="_blank" class="MenuSuperior"><?php echo $fila1["nom_arxiu"]?></a><br />
    <?php }?></td>
</tr>
<?php }?>



<?php if($fila["laboral_data"]!=""){?>
<tr><td colspan="2"><strong>Resposta de RRHH</strong></td></tr>

	<?php if($fila["laboral_informe"]!=""){?>
     <tr>
        <td>Resposta</td>
        <td><?php echo StripSlashes($fila["laboral_informe"]);?></td>
    </tr>
    <?php }?>

    <?php if($fila["laboral_data"]!=""){?>
    <tr>
        <td>Data de resposta</td>
        <td><?php echo StripSlashes($fila["laboral_data"]);?></td>
    </tr>
    <?php }?>
    
<?php }?>

</table>
<hr />
<?php }?>