<script language="javascript">
function validar_caselles(num,guarda){
	var n;
	for(n=1;n<=num;n++){
		var data=eval('window.document.form_more.data_'+n+'.value');
		var de_h=eval('window.document.form_more.de_h_'+n+'.value');
		var de_m=eval('window.document.form_more.de_m_'+n+'.value');
		var a_h=eval('window.document.form_more.a_h_'+n+'.value');
		var a_m=eval('window.document.form_more.a_m_'+n+'.value');
		
		if( (data=="")||(de_h=="")||(de_m=="")||(a_h=="")||(a_m=="")){
			alert("Error.�s obligatori omplir totes les caselles");
			return (false);
		}else if( (isNaN(de_h))||(isNaN(de_m))||(isNaN(a_h))||(isNaN(a_m))  ){
			alert("Error.�s obligatori omplir l'horari amb dos xifres");
			return (false);
		}
		if((de_h>24)||(de_m>59)||(a_h>24)||(a_m>59)){
			alert("Error.L'horari que heu escrit no existeix");
			return (false);
		}
		
		if(de_h.length==1){
			de_h="0"+de_h;
			eval('window.document.form_more.de_h_'+n+'.value="'+de_h+'"');
		}
		if(de_m.length==1){
			de_m="0"+de_m;
			eval('window.document.form_more.de_m_'+n+'.value="'+de_m+'"');
		}
		if(a_h.length==1){
			a_h="0"+a_h;
			eval('window.document.form_more.a_h_'+n+'.value="'+a_h+'"');
		}
		if(a_m.length==1){
			a_m="0"+a_m;
			eval('window.document.form_more.a_m_'+n+'.value="'+a_m+'"');
		}
		
		if( (de_h>a_h) || 
		((de_h==a_h)&&(de_m>a_m)) || 
		((de_h==a_h)&&(de_m==a_m))
		){
			alert("Error.L'horari escrit �s erroni.");
			return (false);
		}
	}
	
	if(guarda==0){
		num=num+1;
		window.document.form_more.num_more.value=num;
		
	}
	
	return (true);
}
function menys_caselles(num){
	num=num-1;
	window.document.form_more.num_more.value=num;
}

function guarda(num_more){
	var resp=true;
	resp=validar_caselles(num_more,1);
	if(resp==true){
		window.document.form_more.funcio.value="guardar";
		return (true);
	}
	if(resp==false){
		return (false);
	}
}

</script>