<div class="notes">Aquest apartat serveix per comunicar les hores treballades o per treballar en instal&middot;lacions fora del Consell, on no teniu mitjans per fitxar.</div>

<div class="notes">Podeu comunicar aquests treballs sempre que siguin<strong> fora del vostre horari laboral</strong>.</div>

<div class="notes">Es podr&agrave; fer la comunicaci&oacute; 15 dies abans del treball en q&uuml;esti&oacute;, o fins a 5 dies despr&eacute;s d'haver-lo realitzat.</div>

<?php include("laboral/text_validadors.php");?>

<div class="notes">Nota per omplir el formulari: l'horari ha d'estar en format de 24h (ex. a les quatre de la tarda son les 16:00h)</div>