<script language="javascript">
function validar_form(){

	if((window.document.form_exterior.lloc.value=="")||(window.document.form_exterior.motiu.value=="")){
		alert("Es obligatori omplir el formulari");
		return (false);
	}
	

	if(window.document.form_exterior.elements[2].checked==1){
		if(window.document.form_exterior.hora_prevista.value==""){
			alert("Es obligatori omplir el formulari");
			return (false);
		}
	}if(window.document.form_exterior.elements[4].checked==1){
		if((window.document.form_exterior.hora_tanca.value=="")||(window.document.form_exterior.minut_tanca.value=="")){
			alert("Es obligatori omplir el formulari");
			return (false);
		}else{
			if( (isNaN(window.document.form_exterior.hora_tanca.value))|| (isNaN(window.document.form_exterior.minut_tanca.value)) ){
				alert("Error.L'horari que heu escrit no existeix");
				return (false);
			}
			if((window.document.form_exterior.hora_tanca.value>24)||(window.document.form_exterior.minut_tanca.value>59)){
				alert("Error.L'horari que heu escrit no existeix");
				return (false);
			}else{
				var data=new Date();
				var h=data.getHours();
				var m=data.getMinutes();
				if( (window.document.form_exterior.hora_tanca.value<h)||
				((window.document.form_exterior.hora_tanca.value==h)&&(window.document.form_exterior.minut_tanca.value<m)) ){
					alert("Error.L'horari que heu escrit ha de ser futur");
					return (false);
				}else{
						if(window.document.form_exterior.hora_tanca.value.length==1){
							window.document.form_exterior.hora_tanca.value="0"+window.document.form_exterior.hora_tanca.value;
						}
						if(window.document.form_exterior.minut_tanca.value.length==1){
							window.document.form_exterior.minut_tanca.value="0"+window.document.form_exterior.minut_tanca.value;
						}
				}
			
			}
		}
	}

	
}
function visualitzar(valor){
	if(valor==1){
		window.document.form_exterior.hora_prevista.disabled=false;
		window.document.form_exterior.hora_tanca.disabled=true;
		window.document.form_exterior.minut_tanca.disabled=true;
	}else{
		window.document.form_exterior.hora_prevista.disabled=true;
		window.document.form_exterior.hora_tanca.disabled=false;
		window.document.form_exterior.minut_tanca.disabled=false;
	}

}
</script>
<form name="form_exterior" id="form_exterior" method="post" action="<?php echo   $PHP_SELF;?>">
  <table width="100%"  border="0" cellspacing="5" cellpadding="5">
    <tr>
      <td colspan="2" class="fitxa_exterior">Per marxar a l'exterior si us plau ompliu les seg&uuml;ents dades: </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><span class="lletraNorm">Lloc:</span>        </td>
      <td><input name="lloc" type="text" class="lletraNorm" id="lloc" size="50"></td>
    </tr>
    <tr>
      <td class="lletraNorm">Motiu:</td>
      <td><input name="motiu" type="text" class="lletraNorm" id="motiu" size="50" /></td>
    </tr>
    <tr>
      <td><span class="lletraNorm">Tornada:
      </span></td>
      <td><p><span class="lletraNorm">
        <input name="torna" type="radio" value="1" checked onClick="visualitzar('1');">
Hora prevista de tornada:</span>
          <input name="hora_prevista" type="text" class="lletraNorm" id="hora_prevista" size="15" maxlength="50">
          <br>  
          <input name="torna" type="radio" value="0" onClick="visualitzar('0');">
          <span class="lletraNorm">Tinc previst No tornar i sol&middot;licito que se'm tanqui el fitxatge a</span>
          <input name="hora_tanca" type="text" disabled class="lletraNorm" id="hora_tanca" size="2" maxlength="2">
    :
    <input name="minut_tanca" type="text"  disabled class="lletraNorm" id="minut_tanca" size="2" maxlength="2">
    <span class="lletraNorm">    h. </span></p>
      </td>
    </tr>
    <tr>
      <td colspan="2"><input name="guardar" type="submit" class="fitxa_exterior" id="guardar" onClick="return validar_form();" value="Surto a treballar">
      <input type="hidden" name="carpeta" value="<?php echo   $_POST["carpeta"];?>"></td>
    </tr>
    <tr>
      <td colspan="2"  align="center"></td>
    </tr>
  </table>
</form>