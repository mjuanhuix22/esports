<?php
$con=mysqli_query($cnx_intranet,"select max(id_descans) from t_f_descans where dia=".date('d')." and mes=".date('m')." and any=".date('Y')." and id_persona=".$_SESSION["id_usuari"]);
$fila=mysqli_fetch_array($con);
$id_descans=$fila["max(id_descans)"];

$con=mysqli_query($cnx_intranet,"select * from t_f_descans where id_descans=".$id_descans);
$fila=mysqli_fetch_array($con);

list($dif_h,$dif_m)=restar_temps_actual($fila["surt_h"],$fila["surt_m"]);


$mod=mysqli_query($cnx_intranet,"update t_f_descans set 
torna_h='".date('H')."',
torna_m='".date('i')."',
dif_h='".$dif_h."',
dif_m='".$dif_m."' 
where id_descans=".$id_descans);


//Sumo tots els descansos fets per avui i si sumen mes del que tenien dreta a fer surt avis
$hora_fet=0;
$min_fet=0;
$con=mysqli_query($cnx_intranet,"select * from t_f_descans where id_persona=".$_SESSION["id_usuari"]." 
and dia=".date('d')." and mes=".date('m')." and any=".date('Y'));
while($fila=mysqli_fetch_array($con)){
	list($hora_fet,$min_fet)=sumar_temps($fila["dif_h"],$fila["dif_m"],$hora_fet,$min_fet);

}

$min_max=$config_minuts_descans;

$text="";

if(($hora_fet!=0)||($min_fet>$min_max)){
	list($dif_h,$dif_m,$positiu)=restar_temps(0,$min_max,$hora_fet,$min_fet);
	if($hora_fet!=0){
		$text="<br>Heu fet ".$dif_h.":".$dif_m." hores de mes.";
	}else{
		$text="<br>Heu fet ".$dif_m." minuts de mes.";
	}

}

if($mod){
	$con=mysqli_query($cnx_intranet,"select * from t_f_descans
	where id_descans=".$id_descans." and 
	torna_h!='' and 
	torna_m!='' and 
	dif_h!='' and 
	dif_m!='';");
	
	if(mysqli_num_rows($con)==1){
		$mod=1;
	}else{
		$mod=0;
	}
	
}

if($mod){
	msn("Fitxat correctament.<br>Heu tornat del descans a les ".date('H.i').$text,1);
}else{
	msn("ERROR. <br>No s'ha fitxat. Torneu a provar",0);
}
?>
