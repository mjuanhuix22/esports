<table width="100%" cellpadding="0" cellspacing="0">
<tr><td class="lletraNegreta">Resum d'avui</td></tr>
<tr><td bgcolor="#000000"><img src="imatges/img_transparent.gif" ></td></tr>
</table>
<?php
//-------------------------------------------Entrades i sortides del treball
$con0=mysqli_query($cnx_intranet,"select * from t_fitxatge where 
dia=".(int)date('d')." and mes=".(int)date('m')." and any=".date('Y')." 
and id_persona=".$_SESSION["id_usuari"]);
while($fila0=mysqli_fetch_array($con0)){
	 echo "<br>";?>
	<img src="imatges/ico_negra.gif" hspace="0" >
	<?php echo   "<span class=\"entrada\">Hora entrada: ".$fila0["entra_h"].":".$fila0["entra_m"]."h. ";
	if($fila0["surt_h"]!=''){
		echo "Hora sortida:".$fila0["surt_h"].":".$fila0["surt_m"]."h ";
		echo "(";
		if(($fila0["dif_h"]=='')||($fila0["dif_h"]==0)){
			echo $fila0["dif_m"]."min";
		}else{
			echo $fila0["dif_h"].":".$fila0["dif_m"]."h";
		}
		echo ")";
		
	}
	echo "</span>";

	//-------------------------------------------entrades i sortides per fer el config
	$con=mysqli_query($cnx_intranet,"select * from t_f_descans where id_fitxatge=".$fila0["id_fitxatge"]);
	while($fila=mysqli_fetch_array($con)){
		echo "<br>";?>
		<img src="imatges/ico_negra.gif" hspace="0" >
		<?php echo   "<span class=\"descans\">";
		if($fila["torna_h"]==''){
			echo "Descans des de ".$fila["surt_h"].":".$fila["surt_m"]."h.";
		}
		if($fila["torna_h"]!=''){
			echo "Descans de ".$fila["surt_h"].":".$fila["surt_m"]."h ";
			echo " a ".$fila["torna_h"].":".$fila["torna_m"]."h ";
			echo "(";
			if(($fila["dif_h"]=='')||($fila["dif_h"]==0)){
				echo $fila["dif_m"]."min";
			}else{
				echo $fila["dif_h"].":".$fila["dif_m"]."h";
			}
			echo ")";
			
		}
		echo "</span>";
	}
	
	//-------------------------------------------entrades i sortides per fer lactancia
	$con=mysqli_query($cnx_intranet,"select * from t_f_lactancia where id_fitxatge=".$fila0["id_fitxatge"]);
	while($fila=mysqli_fetch_array($con)){
		echo "<br>";?>
		<img src="imatges/ico_negra.gif" hspace="0" >
		<?php echo   "<span class=\"descans\">";
		if($fila["torna_h"]==''){
			echo "lactància des de les ".$fila["surt_h"].":".$fila["surt_m"]."h.";
		}
		if($fila["torna_h"]!=''){
			echo "lactància de les ".$fila["surt_h"].":".$fila["surt_m"]."h ";
			echo "fins a les ".$fila["torna_h"].":".$fila["torna_m"]."h ";
			echo "(";
			if(($fila["dif_h"]=='')||($fila["dif_h"]==0)){
				echo $fila["dif_m"]."min";
			}else{
				echo $fila["dif_h"].":".$fila["dif_m"]."h";
			}
			echo ")";
			
		}
		echo "</span>";
	}
	
	//-------------------------------------------Entrades i sortides a l'exterior
	$con=mysqli_query($cnx_intranet,"select * from t_f_exterior where id_fitxatge=".$fila0["id_fitxatge"]);
	while($fila=mysqli_fetch_array($con)){
		echo "<br>";?>
		<img src="imatges/ico_negra.gif" hspace="0" >
		<?php echo   "<span class=\"exterior\">Sortida per treball exterior";
		if($fila["lloc"]!=""){
			echo "<span class=\"lletra_nota\">";
			echo " (".StripSlashes($fila["lloc"])." - ".StripSlashes($fila["motiu"]).") ";
			echo "</span>";
		}
		
		if($fila["torna_h"]!=0){
			echo " de ".$fila["surt_h"].":".$fila["surt_m"]."h ";
			echo " a ".$fila["torna_h"].":".$fila["torna_m"]."h. ";
		}else{
			if($fila["hora_prevista"]!=NULL){
				echo " a ".$fila["surt_h"].":".$fila["surt_m"]."h. ";
				echo "Hora prevista de tornada: ".StripSlashes($fila["hora_prevista"]).".";
			}
			else{
				echo " a ".$fila["surt_h"].":".$fila["surt_m"]."h. ";

				$con1=mysqli_query($cnx_intranet,"select max(id_exterior) from t_f_exterior_tmp where 
				id_persona=".$_SESSION["id_usuari"]." and dia=".date('d')." and mes=".date('m')." and any=".date('Y'));
				
				$fila1=mysqli_fetch_array($con1);
				$con1=mysqli_query($cnx_intranet,"select * from t_f_exterior_tmp where id_exterior=".$fila1["max(id_exterior)"]);
				$fila1=mysqli_fetch_array($con1);
				echo "Tinc previst no tornar i acabar el treball a les ".$fila1["hora_tanca"].":".$fila1["minut_tanca"]."h.";
			}
			
		  }
		  if($fila["automatic"]==1){
			  echo " <span class=\"lletra_nota\">(Fitxatge automàtic)</span>";		 
		}
		
		echo "</span>";
		
	}
	
	

	
	
}
?>