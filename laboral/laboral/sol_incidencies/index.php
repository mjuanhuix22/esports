<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<h4>Nova incid&egrave;ncia de gestió laboral</h4>

<div class="notes">Aquesta apartat està pensat perquè pogueu sol·licitar aquells arrenjaments de fitxatge que no heu pogut solucionar a través de l'apartat "Modificar fitxatge"</div>
<?php

include("laboral/text_validadors.php");



if(isset($_POST["enviar"])){


	$id_estat=1;

    $con=mysqli_query($cnx_intranet,"select * from t_validacio where treballador=".$_SESSION["id_usuari"]." and esglao=1");
    $fila=mysqli_fetch_array($con);
    $validador=$fila["validador"];
	
	$SQL="insert into laboral set 
	sol_tema=".$_POST["sol_tema"].",
	id_persona=".$_SESSION["id_usuari"].",
	data_sol='".date('Ymd')."',
	sol_assumpte='".AddSlashes($_POST["sol_assumpte"])."',
	sol_desc='".nl2br(AddSlashes(strip_tags(str_replace("\"","'",$_POST["sol_desc"]))))."',
	validador=".$validador;
	
	$ins=mysqli_query($cnx_intranet,$SQL);
	msn('Enviat correctament',$ins);
	
	$id_laboral=mysqli_insert_id($cnx_intranet);

	$caracters=array(1=>'1',2=>'2',3=>'3',4=>'4',5=>'5',6=>'6',7=>'7',8=>'8',9=>'9',10=>'0',11=>'A',12=>'B',13=>'C',14=>'D',15=>'E',16=>'F',17=>'G',18=>'H',19=>'I',20=>'J',21=>'K',22=>'L',23=>'M',24=>'N',25=>'O',26=>'P',27=>'Q',28=>'R',29=>'S',30=>'T',31=>'U',32=>'V',33=>'X',34=>'Y',35=>'Z');

	for($i=1;$i<=3;$i++){
		$nom_arxiu="arxiu".$i;

		$ruta="laboral/".$caracters[rand(1,35)].$caracters[rand(1,35)].$caracters[rand(1,35)].$caracters[rand(1,35)].$caracters[rand(1,35)].$caracters[rand(1,35)].$caracters[rand(1,35)].$caracters[rand(1,35)];;
		
		include("pujar_arxiu.php");
		if($format!=""){

			mysqli_query($cnx_intranet,"insert into laboral_arxius set 
			id_laboral=".$id_laboral.",
			nom_arxiu='".$_FILES["$nom_arxiu"]["name"]."',
			ruta='".$ruta.".".$format."';");	
		}
	}
}else{

    include "comprova_exist.php";
    if($entra==1) {
        include("nova_sol.php");
    }
}



?>