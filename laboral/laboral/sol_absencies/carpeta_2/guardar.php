<?php
//busco el seu validador
$con=mysqli_query($cnx_intranet,"select * from t_validacio where treballador=".$_SESSION["id_usuari"]." and esglao=1");
$fila=mysqli_fetch_array($con);
$validador=$fila["validador"];


$con=mysqli_query($cnx_intranet,"select max(ident_sol) from t_absencies_jornada");
$fila=mysqli_fetch_array($con);
$ident_sol_jorn=$fila["max(ident_sol)"]+1;


$con=mysqli_query($cnx_intranet,"select max(ident_sol) from t_absencies_hores");
$fila=mysqli_fetch_array($con);
$ident_sol_hor=$fila["max(ident_sol)"]+1;

for($any=$any_inici;$any<=$any_fi;$any++){

	if($any==$any_inici){
		$mes_i=(int)$mes_inici;
	}else{
		$mes_i=1;
	}
	if($any==$any_fi){
		$mes_f=(int)$mes_fi;
	}else{
		$mes_f=12;
	}
	for($mes=$mes_i;$mes<=$mes_f;$mes++){
		$num_dies=total_dies($mes,$any);//Num de dies que té el mes
		$dia=1;
		while($dia<=$num_dies){
	
			$data="data_".$any."_".$mes."_".$dia;
			if( (isset($_POST["$data"]))&&($_POST["$data"]==1)){
				$tipus_dia="tipus_dia_".$data;
				if($_POST["$tipus_dia"]==1){
					for($n=1;$n<=$_POST["num_contractes"];$n++){
						//si ha seleccionat hora que surt o torna es una absencia d'hores
						$hora_surt_p="hora_surt_p".$n."_".$data;
						$hora_torna_p="hora_torna_p".$n."_".$data;
						$min_surt_p="min_surt_p".$n."_".$data;
						$min_torna_p="min_torna_p".$n."_".$data;
						if(($_POST["$hora_surt_p"]!="")&&($_POST["$hora_torna_p"]!="")){
							
							if($_POST["id_concepte"]==147){
								$ins=mysqli_query($cnx_intranet,"insert into t_absencies_hores set 
								id_persona=".$_SESSION["id_usuari"].",
								any=".$any.",
								mes=".$mes.",
								dia=".$dia.",
								hora_surt_p='".$_POST["$hora_surt_p"]."',
								min_surt_p='".$_POST["$min_surt_p"]."',
								hora_torna_p='".$_POST["$hora_torna_p"]."',
								min_torna_p='".$_POST["$min_torna_p"]."',
								id_concepte=147,
								ident_sol=".$ident_sol_hor);
							}else{
								$ins=mysqli_query($cnx_intranet,"insert into t_absencies_hores set 
								id_persona=".$_SESSION["id_usuari"].",
								any=".$any.",
								mes=".$mes.",
								dia=".$dia.",
								hora_surt_p='".$_POST["$hora_surt_p"]."',
								min_surt_p='".$_POST["$min_surt_p"]."',
								hora_torna_p='".$_POST["$hora_torna_p"]."',
								min_torna_p='".$_POST["$min_torna_p"]."',
								id_concepte=".$_POST["id_concepte"].",
								validador=".$validador.",
								data_sol=".data_actual8().",
								ident_sol=".$ident_sol_hor);
							}
						}
					
					}

				}
				if($_POST["$tipus_dia"]==2){//si no es una absencia de tot el dia


					if($_POST["id_concepte"]==147){
						$ins=mysqli_query($cnx_intranet,"insert into t_absencies_jornada set 
						id_persona=".$_SESSION["id_usuari"].",
						any=".$any.",
						mes=".$mes.",
						dia=".$dia.",
						id_concepte=147,
						ident_sol=".$ident_sol_jorn);
					}else{
						$ins=mysqli_query($cnx_intranet,"insert into t_absencies_jornada set 
						id_persona=".$_SESSION["id_usuari"].",
						any=".$any.",
						mes=".$mes.",
						dia=".$dia.",
						id_concepte=".$_POST["id_concepte"].",
						validador=".$validador.",
						data_sol=".data_actual8().",
						ident_sol=".$ident_sol_jorn);

					}
				}
				//si el tipus=3 no el guardarem a la taula tmp 
			}//if($_POST["$data"]==1){
			$dia++;
		}//while
	}//for

}//for
if($ins){
	
	if($_POST["id_concepte"]==147){ msn("Permis guardat correctament",1);}
	
	else{
		
		$text="La sol·licitud ha estat enviada correctament.";
		$con=mysqli_query($cnx_intranet,"select * from personal where id_persona=".$_SESSION["id_usuari"]);
		$fila=mysqli_fetch_array($con);
		$mail=$fila["mail"];
		if($mail!=""){
			$text.="<br>Quan s'hagi validat s'enviarà un missatge de confirmació a ".$fila["mail"];
		}
		msn($text,1);
	}
}
?>
