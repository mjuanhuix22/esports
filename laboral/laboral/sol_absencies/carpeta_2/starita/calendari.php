<?php 

$mes=5;
$any=date('Y');


$primer_dia=num_nom_dia($mes,1,$any);

//Num de dies que té el mes
$num_dies=total_dies($mes,$any);

$c=0;
$num_caselles=$num_dies+$primer_dia;

$entrat=false;
$dia=0;?>
<table width="145" border="1"  cellpadding="0" cellspacing="0" align="left" >
<tr><td colspan="7" bgcolor="#666666" class="lletraBlanca75"><?php echo   ucfirst($nom_mes[$mes])." ".$any;?></td>
</tr>

<?php while($c<$num_caselles){
		if($c % 7==0){ 
			if($c==0){ echo "\n<tr >";}
			else{ echo "</tr>\n<tr>";}
		}
	
		if(($entrat==false)&&($c!=$primer_dia)){
			$class="bordeblanc";
		}else{
			$i=$dia+1;
			$activ=1;//-------------------------------Si $activ=1 es veura la casella checkbox
			$class="lletraPeke ";//-------------------------------Estil per la casella
			 
			//que el temps sigui el minim que posa a la taula t_conceptes
			
			$ii=$i;
			
			
			//que sigui superior al dia d'inici
			if($activ==1){
				if(($any==$any_inici)&&($mes==$mes_inici)&&($dia_inici>$i)){
					$activ=0;
					//Si en la data no te cap contracte veiem que es un dia no laboral
					$conc=mysqli_query($cnx_intranet,"select * from t_contracte where id_persona=".$_SESSION["id_usuari"]." 
					and inici<=".convert_data8($any,$mes,$i));
					if(mysqli_num_rows($conc)==0){
						$class="lletraPeke dia_festiu";
						$llegenda[$ll]=$class;
						$textll[$ll]="Dia no laboral";
						$ll++;
					}
				}
			}else{
				$activ=0;
				$class="lletraPeke dia_festiu";
				$llegenda[$ll]=$class;
				$textll[$ll]="Dia no laboral";
				$ll++;
			}
			//Que sigui inferior al dia de fi
			if($activ==1){
				if(($any==$any_fi)&&($mes==$mes_fi)&&($dia_fi<$i)){
					$activ=0;
					//Si en la data no te cap contracte veiem que es un dia no laboral
					$conc=mysqli_query($cnx_intranet,"select * from t_contracte where id_persona=".$_SESSION["id_usuari"]." 
					and (fi>=".convert_data8($any,$mes,$i)." or fi='')");
					if(mysqli_num_rows($conc)==0){
						$class="lletraPeke dia_festiu";
						$llegenda[$ll]=$class;
						$textll[$ll]="Dia no laboral";
						$ll++;
					}
				
				}
			}
			
			 //miro que el treballador li toqui treballar segons la taula t_contracte i t_dies_treball

			$con=mysqli_query($cnx_intranet,"select * from t_contracte where id_persona=".$_SESSION["id_usuari"]." and 
			inici<='".convert_data8($any,$mes,$i)."' and (fi>='".convert_data8($any,$mes,$dia)."' or fi='')");
			
			if(mysqli_num_rows($con)==1){
				$fila=mysqli_fetch_array($con);
				$con=mysqli_query($cnx_intranet,"select * from t_contracte_hores where 
				id_contracte=".$fila["id_contracte"]." and diaset=".dia7($any,$mes,$i));
				if(mysqli_num_rows($con)==0){
					$activ=0;
					$class="lletraPeke dia_festiu";
					$llegenda[$ll]=$class;
					$textll[$ll]="Dia no laboral";
					$ll++;
				}
			}
			
			
			
			$con=mysqli_query($cnx_intranet,"SELECT * FROM t_horari,t_horari_sol WHERE 
			t_horari.id_sol=t_horari_sol.id_sol and 
			t_horari_sol.id_persona=".$_SESSION["id_usuari"]." and 
			t_horari.dia=".$i." and t_horari.mes=".$mes." and t_horari.any=".$any);
			while($fila=mysqli_fetch_array($con)){
				$activ=0;
				$class="lletraPeke dia_motiu ";
				$textll[$ll]="Hores de mes o de menys";
				if($fila["validador"]!=0){
					$class.="temporal";
					$textll[$ll].=" (pendent de validar)";
				}
				
				$llegenda[$ll]=$class;
				$ll++;
			}
			
			
			//miro si es dia absencia 
			$con=mysqli_query($cnx_intranet,"SELECT id_concepte,validador FROM t_absencies_jornada where id_persona=".$_SESSION["id_usuari"]." 
			and any=".$any." and mes=".$mes." and dia=".$i);
			while($fila=mysqli_fetch_array($con)){//Com a max entra 1 vegada
				if($fila["id_concepte"]!=$_POST["id_concepte"]){//Si es un altre concepte no pot seleccionar
					$activ=0;
				}
				$class="lletraPeke concepte_".$fila["id_concepte"]; 		
				$con2=mysqli_query($cnx_intranet,"select nom_concepte from t_conceptes where id_concepte=".$fila["id_concepte"]);
				$fila2=mysqli_fetch_array($con2);
				$textll[$ll]=$fila2["nom_concepte"];	
				if($fila["validador"]!=0){
					$class.=" temporal";
					$textll[$ll].=" (pendent de validar)";	
				}
				$llegenda[$ll]=$class;
				$ll++;
				
			}
			
			
			
			//miro si es dia absencia 
			$con=mysqli_query($cnx_intranet,"SELECT id_concepte,validador FROM t_absencies_hores where id_persona=".$_SESSION["id_usuari"]." 
			and any=".$any." and mes=".$mes." and dia=".$i);
			while($fila=mysqli_fetch_array($con)){
				if($fila["id_concepte"]!=$_POST["id_concepte"]){//Si es un altre concepte no pot seleccionar
					$activ=0;
				}
				$class="lletraPeke concepte_".$fila["id_concepte"]; 		
				$con2=mysqli_query($cnx_intranet,"select nom_concepte from t_conceptes where id_concepte=".$fila["id_concepte"]);
				$fila2=mysqli_fetch_array($con2);
				$textll[$ll]=$fila2["nom_concepte"];	
				if($fila["validador"]!=0){
					$class.=" temporal";
					$textll[$ll].=" (pendent de validar)";	
				}
				$llegenda[$ll]=$class;
				$ll++;
				
			}
			
		}//elseif(($entrat==false)&&($c!=$primer_dia))
		
		
		?>
	<td align="center" class="<?php echo   $class;?>"  >
	<?php 
	
	if( (($entrat==true)||($c==$primer_dia))&&($c<$num_caselles) ){
		$dia++;
		$entrat=true;
		echo $dia;
		
		
		if($activ==1){?>
		<br>
        		<?php $con=mysqli_query($cnx_intranet,"SELECT * from festes where starita=1 and any=".date('Y'));
				$fila=mysqli_fetch_array($con);
				if(($fila["dia"]==$dia)&&($fila["mes"]==$mes)&&($fila["any"]==$any)){?>
                
					<input type="checkbox" name="data_<?php echo   $any."_".$mes."_".$dia;?>" 
                    value="1" checked="checked" />
				<?php }?>   
					
		<?php }else{?>
			<img src="imatges/img_transparent.gif" width="16" height="16" >
		<?php }?>
		
	<?php }else{?>
			<img src="imatges/img_transparent.gif" width="16" height="16" >
	<?php }?></td>
	<?php $c++;
}//while?>

</table>




