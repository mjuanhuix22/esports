<script language="javascript">
function validar_seleccionat(dies_retribuits){
	var total=window.document.form_dies.elements.length;

	total=total-4;
	var selec=0;
	for(i=0;i<total;i++){
		f=window.document.form_dies.elements[i].checked;
		if(f==true){
			selec++;
		}
	}
	if(selec==0){
		alert("ERROR. Heu de seleccionar algun dia");
		return (false);
	}
	if((selec>dies_retribuits)&&(dies_retribuits!=0)){
		alert("ERROR. Heu seleccionat "+selec+" dies i com a maxim en podeu sol·licitar "+dies_retribuits);
		return (false);
	}
	
}
</script>

<form  method="post" name="form_dies">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
  <td  class="lletra_concepte_<?php echo   $_POST["id_concepte"];?>" height="30">Seleccioneu els dies
  <?php 
$llegenda=array();$textll=array();
$ll=0;  
$con=mysqli_query($cnx_intranet,"SELECT * FROM t_conceptes where id_concepte=".$_POST["id_concepte"]);
$fila=mysqli_fetch_array($con);
if($fila["minim"]>=0){?>
   en qu&egrave; voldr&iacute;eu fer 
<?php }else{?>
	que no heu vingut per 
<?php }?>
'<?php echo   $fila["nom_concepte"];?>'
</td>
</tr>
<tr><td>
<?php 

$k=0;
//els valors d'inici venen de dates.php
for($any=$any_inici;$any<=$any_fi;$any++){
	if($any==$any_inici){
		$mes_i=(int)$mes_inici;
	}else{
		$mes_i=1;
	}
	if($any==$any_fi){
		$mes_f=(int)$mes_fi;
	}else{
		$mes_f=12;
	}
	for($mes=$mes_i;$mes<=$mes_f;$mes++){
		$k++;
		$primer_dia=num_nom_dia($mes,1,$any);
		
		//Num de dies que té el mes
		$num_dies=total_dies($mes,$any);
		
		$c=0;
		$num_caselles=$num_dies+$primer_dia;
		
		$entrat=false;
		$dia=0;?>
		<table width="145" border="1" align="left" cellpadding="0" cellspacing="0">
		<tr><td colspan="7" bgcolor="#666666" class="lletraBlanca75"><?php echo   ucfirst($nom_mes[$mes])." ".$any;?></td>
		</tr>
	
		<?php while($c<$num_caselles){
				if($c % 7==0){ 
					if($c==0){ echo "\n<tr >";}
					else{ echo "</tr>\n<tr>";}
				}
			
				if(($entrat==false)&&($c!=$primer_dia)){
					$class="bordeblanc";
				}else{
					$i=$dia+1;
					$activ=1;//-------------------------------Si $activ=1 es veura la casella checkbox
					$class="lletraPeke ";//-------------------------------Estil per la casella
					 
					//que el temps sigui el minim que posa a la taula t_conceptes
					
					$ii=$i;
					
					
					//que sigui superior al dia d'inici
					if($activ==1){
						if(($any==$any_inici)&&($mes==$mes_inici)&&($dia_inici>$i)){
							$activ=0;
							//Si en la data no te cap contracte veiem que es un dia no laboral
							$conc=mysqli_query($cnx_intranet,"select * from t_contracte where id_persona=".$_SESSION["id_usuari"]." 
							and inici<=".convert_data8($any,$mes,$i));
							if(mysqli_num_rows($conc)==0){
								$class="lletraPeke dia_festiu";
								$llegenda[$ll]=$class;
								$textll[$ll]="Dia no laboral";
								$ll++;
							}
						}
					}else{
						$activ=0;
						$class="lletraPeke dia_festiu";
						$llegenda[$ll]=$class;
						$textll[$ll]="Dia no laboral";
						$ll++;
					}
					//Que sigui inferior al dia de fi
					if($activ==1){
						if(($any==$any_fi)&&($mes==$mes_fi)&&($dia_fi<$i)){
							$activ=0;
							//Si en la data no te cap contracte veiem que es un dia no laboral
							$conc=mysqli_query($cnx_intranet,"select * from t_contracte where id_persona=".$_SESSION["id_usuari"]." 
							and (fi>=".convert_data8($any,$mes,$i)." or fi='')");
							if(mysqli_num_rows($conc)==0){
								$class="lletraPeke dia_festiu";
								$llegenda[$ll]=$class;
								$textll[$ll]="Dia no laboral";
								$ll++;
							}
						
						}
					}
					
					 //miro que el treballador li toqui treballar segons la taula t_contracte i t_dies_treball
		
					$con=mysqli_query($cnx_intranet,"select * from t_contracte where id_persona=".$_SESSION["id_usuari"]." and 
					inici<='".convert_data8($any,$mes,$i)."' and (fi>='".convert_data8($any,$mes,$dia)."' or fi='')");
					
					if(mysqli_num_rows($con)==1){
						$fila=mysqli_fetch_array($con);
						$con=mysqli_query($cnx_intranet,"select * from t_contracte_hores where 
						id_contracte=".$fila["id_contracte"]." and diaset=".dia7($any,$mes,$i));
						if(mysqli_num_rows($con)==0){
							$activ=0;
							$class="lletraPeke dia_festiu";
							$llegenda[$ll]=$class;
							$textll[$ll]="Dia no laboral";
							$ll++;
						}
					}
					
					
					
					$con=mysqli_query($cnx_intranet,"SELECT * FROM t_horari,t_horari_sol WHERE 
					t_horari.id_sol=t_horari_sol.id_sol and 
					t_horari_sol.id_persona=".$_SESSION["id_usuari"]." and 
					t_horari.dia=".$i." and t_horari.mes=".$mes." and t_horari.any=".$any);
					while($fila=mysqli_fetch_array($con)){
						$activ=0;
						$class="lletraPeke dia_motiu ";
						$textll[$ll]="Hores de mes o de menys";
						if($fila["validador"]!=0){
							$class.="temporal";
							$textll[$ll].=" (pendent de validar)";
						}
						
						$llegenda[$ll]=$class;
						$ll++;
					}
					
					
					//miro si es dia absencia 
					$con=mysqli_query($cnx_intranet,"SELECT id_concepte,validador FROM t_absencies_jornada where id_persona=".$_SESSION["id_usuari"]." 
					and any=".$any." and mes=".$mes." and dia=".$i);
					while($fila=mysqli_fetch_array($con)){//Com a max entra 1 vegada
						if($fila["id_concepte"]!=$_POST["id_concepte"]){//Si es un altre concepte no pot seleccionar
							$activ=0;
						}
						$class="lletraPeke concepte_".$fila["id_concepte"]; 		
						$con2=mysqli_query($cnx_intranet,"select nom_concepte from t_conceptes where id_concepte=".$fila["id_concepte"]);
						$fila2=mysqli_fetch_array($con2);
						$textll[$ll]=$fila2["nom_concepte"];	
						if($fila["validador"]!=0){
							$class.=" temporal";
							$textll[$ll].=" (pendent de validar)";	
						}
						$llegenda[$ll]=$class;
						$ll++;
						
					}
					
					
					
					//miro si es dia absencia 
					$con=mysqli_query($cnx_intranet,"SELECT id_concepte,validador FROM t_absencies_hores where id_persona=".$_SESSION["id_usuari"]." 
					and any=".$any." and mes=".$mes." and dia=".$i);
					while($fila=mysqli_fetch_array($con)){
						if($fila["id_concepte"]!=$_POST["id_concepte"]){//Si es un altre concepte no pot seleccionar
							$activ=0;
						}
						$class="lletraPeke concepte_".$fila["id_concepte"]; 		
						$con2=mysqli_query($cnx_intranet,"select nom_concepte from t_conceptes where id_concepte=".$fila["id_concepte"]);
						$fila2=mysqli_fetch_array($con2);
						$textll[$ll]=$fila2["nom_concepte"];	
						if($fila["validador"]!=0){
							$class.=" temporal";
							$textll[$ll].=" (pendent de validar)";	
						}
						$llegenda[$ll]=$class;
						$ll++;
						
					}
					
				}//elseif(($entrat==false)&&($c!=$primer_dia))
				
				
				?>
			<td align="center" class="<?php echo   $class;?>"  >
			<?php 
			
			if( (($entrat==true)||($c==$primer_dia))&&($c<$num_caselles) ){
				$dia++;
				$entrat=true;
				echo $dia;
				if($activ==1){?>
				<br>
							<input type="checkbox" name="data_<?php echo   $any."_".$mes."_".$dia;?>" value="1" 
							<?php $con1=mysqli_query($cnx_intranet,"SELECT * FROM t_absencies_jornada where id_persona=".$_SESSION["id_usuari"]." 
							and any=".$any." and mes=".$mes." and dia=".$dia." and id_concepte=".$_POST["id_concepte"]);
							
							if(mysqli_num_rows($con1)!=0){ 
								echo "checked "; 
								$fila=mysqli_fetch_array($con1);
								$data=convert_data8($any,$mes,$dia);
								if($data<=data_actual8()){
									echo "readonly=\"yes\"";
								}
							}
							 $con1=mysqli_query($cnx_intranet,"SELECT * FROM t_absencies_hores where id_persona=".$_SESSION["id_usuari"]." 
							and any=".$any." and mes=".$mes." and dia=".$dia." and id_concepte=".$_POST["id_concepte"]);
							if(mysqli_num_rows($con1)!=0){ 
								echo "checked "; 
								$fila=mysqli_fetch_array($con1);
								$data=convert_data8($any,$mes,$dia);
								if($data<=data_actual8()){
									echo "readonly=\"yes\"";
								}
							}
							?>   />
							
				<?php }else{?>
					<img src="imatges/img_transparent.gif" width="16" height="16" >
				<?php }?>
				
			<?php }else{?>
					<img src="imatges/img_transparent.gif" width="16" height="16" >
			<?php }?></td>
			<?php $c++;
		}//while?>
	
	  </table>
	  <img src="http://www.selvaesports.cat/laboral/imatges/img_transparent.gif" width="10" height="50" align="left">
       <?php if(($k==3)||($k==6)||($k==9)||($k==12)){ echo "</td></tr><tr><td>";}?>
<?php }
}?>
</td></tr>
<tr><td><br />
<?php include("llegenda.php");?>
<br />
<?php $con=mysqli_query($cnx_intranet,"SELECT * FROM t_conceptes where id_concepte=".$_POST["id_concepte"]);
$fila=mysqli_fetch_array($con);?>
	<input name="seguent" type="submit" value="següent" 
	class="boto_guardar concepte_<?php echo   $_POST["id_concepte"];?>" onClick="return validar_seleccionat('<?php echo   $fila["dies_retribuits"];?>');" >
	<input type="hidden" name="id_concepte" value="<?php echo   $_POST["id_concepte"];?>">
	
	<?php include("comuns2.php");?>
</td></tr>
</table>
</form>


