<?php
//----------------Busquem el $mes_inici i $mes_fi per visualitzar el calendari

if(($_POST["id_concepte"]==31)||($_POST["id_concepte"]==32)){ //Assumptes
	$dia_fi=15; $mes_fi=1;$any_fi=date('Y')+1;
	$dia_inici=1;$any_inici=1;$any_inici=date('Y');
}
elseif(($_POST["id_concepte"]==30)||($_POST["id_concepte"]==33)){ 
	$dia_fi=5; $mes_fi=1;$any_fi=date('Y')+1;
	$dia_inici=1;$any_inici=1;$any_inici=date('Y');

}
else{
	
	//La data d'inici sera el minim del que posi en minim
	$con=mysqli_query($cnx_intranet,"select minim from t_conceptes where id_concepte=".$_POST["id_concepte"]);
	$fila=mysqli_fetch_array($con);
	list($any_inici,$mes_inici,$dia_inici)=sum_rest_dies_actual($fila["minim"]);
	
	//Si el minim es negatiu la data de fi sera avui
	if($fila["minim"]<0){
		$any_fi=date('Y');
		$mes_fi=date('m');
		$dia_fi=date('d');
	}else{
		
		//Si és matrimoni,maternitat veu tot l'any
		if(($_POST["id_concepte"]==13)||($_POST["id_concepte"]==102)||($_POST["id_concepte"]==129)||($_POST["id_concepte"]==122)){
			$mes_fi=12;
			$dia_fi=31;
			$any_fi=$any_inici;
		}else{
			
			//La data de fi sera 4 mesos superior a la data d'inici
			$any_fi=$any_inici;
			$mes_fi=$mes_inici;
			for($i=0;$i<11;$i++){
				if($mes_fi==12){
					$any_fi=$any_fi+1;
					$mes_fi=1;
				}else{
					$mes_fi++;
				}
			}
			$dia_fi=total_dies($mes_fi,$any_fi);
		}
	}
	
	//-------------valido que la data de fi tingui un contracte
	
	
	$con=mysqli_query($cnx_intranet,"select * from t_contracte where id_persona=".$_SESSION["id_usuari"]." 
	and (fi>=".convert_data8($any_fi,$mes_fi,$dia_fi)." or fi='')");
	if(mysqli_num_rows($con)==0){
		$con=mysqli_query($cnx_intranet,"select max(fi) from t_contracte where id_persona=".$_SESSION["id_usuari"]);
		$fila=mysqli_fetch_array($con);
		$con=mysqli_query($cnx_intranet,"select * from t_contracte where fi=".$fila["max(fi)"]);
		$fila=mysqli_fetch_array($con);
		$dia_fi=substr($fila["fi"],6,2);
		$mes_fi=substr($fila["fi"],4,2);
		$any_fi=substr($fila["fi"],0,4);
	}
	//-------------valido que la data d'inici tingui un contracte
	
	
	$con=mysqli_query($cnx_intranet,"select * from t_contracte where id_persona=".$_SESSION["id_usuari"]." 
	and inici<=".convert_data8($any_inici,$mes_inici,$dia_inici));
	if(mysqli_num_rows($con)==0){
		$con=mysqli_query($cnx_intranet,"select max(inici) from t_contracte where id_persona=".$_SESSION["id_usuari"]);
		$fila=mysqli_fetch_array($con);
		$con=mysqli_query($cnx_intranet,"select * from t_contracte where inici=".$fila["max(inici)"]);
		$fila=mysqli_fetch_array($con);
		$dia_inici=substr($fila["inici"],6,2);
		$mes_inici=substr($fila["inici"],4,2);
		$any_inici=substr($fila["inici"],0,4);
	}

}

?>