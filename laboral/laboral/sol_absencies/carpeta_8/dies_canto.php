<?php 
//Guardo en en array quins son els dies anterior i posterior de l'absencia
$any_ap=array();
$mes_ap=array();
$dia_ap=array();
$dia_c=0;

//Busco quin és id_concepte dels assumptes d'aquest any
$con=mysqli_query($cnx_intranet,"select * from t_conceptes where carpeta=9 and any=".$any_vac);
$fila=mysqli_fetch_array($con);
$id_concepte=$fila["id_concepte"];


//Guardem els dies de assumptes (aprovats i no aprovats) (futurs i passats)
$con=mysqli_query($cnx_intranet,"select * from t_absencies_jornada where 
id_concepte=".$id_concepte." and id_persona=".$_SESSION["id_usuari"]);
while($fila=mysqli_fetch_array($con)){
	//echo $fila["any"]."/".$fila["mes"]."/".$fila["dia"]."<br>";
	//Quan trobem el dia anterior que li toqui treballar el guardem
	$trobat=0;
	$any=$fila["any"];
	$mes=$fila["mes"];
	$dia=$fila["dia"];
	while($trobat==0){
		list($any,$mes,$dia)=sum_rest_dies('-1',$dia,$mes,$any);
		
		$con1=mysqli_query($cnx_intranet,"select count(*) from t_absencies_jornada where 
		id_persona=".$_SESSION["id_usuari"]." and 
		dia=".$dia." and 
		mes=".$mes." and 
		any=".$any);
		$fila1=mysqli_fetch_array($con1);
		if($fila1["count(*)"]==0){
		
			$con1=mysqli_query($cnx_intranet,"select * from t_contracte where id_persona=".$_SESSION["id_usuari"]." and 
			inici<='".convert_data8($any,$mes,$dia)."' and 
			(fi>='".convert_data8($any,$mes,$dia)."' or fi='')");
			
			if(mysqli_num_rows($con1)==1){
					
					$fila1=mysqli_fetch_array($con1);
					$con1=mysqli_query($cnx_intranet,"select * from t_contracte_hores where 
					id_contracte=".$fila1["id_contracte"]." and diaset=".dia7($any,$mes,$dia));
					if(mysqli_num_rows($con1)!=0){
						$trobat=1;
					}
			}elseif(mysqli_num_rows($con1)==0){//Si no hi ha cap contracte anterior al dia $ap finalitzem busqueda per evitar bucle
			
				$con1=mysqli_query($cnx_intranet,"select * from t_contracte where id_persona=".$_SESSION["id_usuari"]." and 
				inici<='".convert_data8($any_ap[$dia_c],$mes_ap[$dia_c],$dia_ap[$dia_c])."'");
				if(mysqli_num_rows($con1)==0){ $trobat=1;}
			
			}
		}
	}
	$any_ap[$dia_c]=$any;
	$mes_ap[$dia_c]=$mes;
	$dia_ap[$dia_c]=$dia;
	
	$dia_c++;
	
	
	
	
	//Quan trobem el dia posterior que li toca treballar el guardem
	$trobat=0;
	$any=$fila["any"];
	$mes=$fila["mes"];
	$dia=$fila["dia"];
	while($trobat==0){
		list($any,$mes,$dia)=sum_rest_dies('+1',$dia,$mes,$any);
		
		$con1=mysqli_query($cnx_intranet,"select count(*) from t_absencies_jornada where 
		id_persona=".$_SESSION["id_usuari"]." and 
		dia=".$dia." and 
		mes=".$mes." and 
		any=".$any);
		$fila1=mysqli_fetch_array($con1);
		if($fila1["count(*)"]==0){
			
			$con1=mysqli_query($cnx_intranet,"select * from t_contracte where id_persona=".$_SESSION["id_usuari"]." and 
			inici<='".convert_data8($any,$mes,$dia)."' and 
			(fi>='".convert_data8($any,$mes,$dia)."' or fi='')");
					
			if(mysqli_num_rows($con1)==1){
					
					$fila1=mysqli_fetch_array($con1);
					$con1=mysqli_query($cnx_intranet,"select * from t_contracte_hores where 
					id_contracte=".$fila1["id_contracte"]." and diaset=".dia7($any,$mes,$dia));
					if(mysqli_num_rows($con1)!=0){
						$trobat=1;
					}
			}elseif(mysqli_num_rows($con1)==0){//Si no hi ha cap contracte posterior al dia $ap finalitzem busqueda per evitar bucle
				$con1=mysqli_query($cnx_intranet,"select * from t_contracte where id_persona=".$_SESSION["id_usuari"]." and 
				(fi>='".convert_data8($any_ap[$dia_c],$mes_ap[$dia_c],$dia_ap[$dia_c])."' or fi='')");
				if(mysqli_num_rows($con1)==0){ $trobat=1;}
			
		}
		}
	}
	$any_ap[$dia_c]=$any;
	$mes_ap[$dia_c]=$mes;
	$dia_ap[$dia_c]=$dia;
	$dia_c++;
}//while($fila=mysqli_fetch_array($con)){

?>