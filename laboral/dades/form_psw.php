<script language="javascript">
function validar_psw(){
	if((window.document.form_psw.psw_vell.value=="")||(window.document.form_psw.psw_nou.value=="")||
	(window.document.form_psw.psw_nou2.value=="")){
		alert("Es obligatori omplir tots els camps");
		return (false);
	}
	if(window.document.form_psw.psw_nou.value!=window.document.form_psw.psw_nou2.value){
		alert("La confirmació de la nova contrasenya no és correcte");
		return (false);
	}
	var psw_n=window.document.form_psw.psw_nou.value;
	if(psw_n.length<6){
		alert("La contrasenya ha de tenir entre 6 i 10 caracters");
		return (false);
	}
	
	return (true);
}
</script>
<form action="" method="post" name="form_psw" id="form_psw">
  <table   border="0" cellpadding="0" cellspacing="2" bgcolor="#000000">
    <tr>
      <td colspan="2" class="lletraVerda70">Si voleu podeu canviar la contrasenya de la vostra sessi&oacute;: </td>
    </tr>
    <tr>
      <td colspan="2" class="fonsverd"><img src="imatges/img_transparent.gif" width="1" height="1" /></td>
    </tr>
    <tr>
      <td width="50%" class="lletraBlanca65">Contrasenya actual: </td>
      <td><input name="psw_vell" type="password" class="lletraNorm" id="psw_vell" size="11" maxlength="9" /></td>
    </tr>
    <tr>
      <td height="10" colspan="2" align="center" class="lletraGrisa"><img src="imatges/img_transparent.gif" width="10" height="10"></td>
    </tr>
    <tr>
      <td colspan="2" align="center" class="lletraGrisa">La contrasenya ha de tenir entre 6 i 9 caracters </td>
    </tr>
    <tr>
      <td class="lletraBlanca65">Contrasenya nova: </td>
      <td><input name="psw_nou" type="password" class="lletraNorm" id="psw_nou" size="11" maxlength="9" /></td>
    </tr>
    <tr>
      <td class="lletraBlanca65">Confirmar contrasenya: </td>
      <td><input name="psw_nou2" type="password" class="lletraNorm" id="psw_nou2" size="11" maxlength="9" /></td>
    </tr>
    <tr>
      <td colspan="2"><input name="canviar" type="submit" class="boto_negre" id="canviar" value="Canviar" onclick="return validar_psw();" /></td>
    </tr>
  </table>
  <?php include("comuns.php");?>
</form>

