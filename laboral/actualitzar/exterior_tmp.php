<?php
//La taula t_f_exteiror_tmp sempra s'ha d'anar buidant.
$con=mysqli_query($cnx_intranet,"select * from t_f_exterior_tmp");
while($fila=mysqli_fetch_array($con)){
	$data=convert_data8($fila["any"],$fila["mes"],$fila["dia"]);
	$tanca=$fila["hora_tanca"].$fila["minut_tanca"];
	$comen=$fila["hora_comen"].$fila["minut_comen"];
	$hora_act=convert_num2(date('H')).convert_num2(date('i'));
	
	//Entro si la data de comenca és inferior a la data actual
	if( ($data<data_actual8()) || (($data==data_actual8())&&($comen<$hora_act)) ){
		//Si no ha fitxat cap entrada anterior al t_f_exterior_tmp
		$con1=mysqli_query($cnx_intranet,"select * from t_fitxatge where id_persona=".$fila["id_persona"]." 
		and mes=".$fila["mes"]." and dia=".$fila["dia"]." and any=".$fila["any"]." and 
		((entra_h<'".$fila["hora_comen"]."') or 
		 (entra_h='".$fila["hora_comen"]."' and entra_m<='".$fila["minut_comen"]."'))
		");
		if(mysqli_num_rows($con1)==0){
			$con2=mysqli_query($cnx_intranet,"select max(id_fitxatge) from t_fitxatge");
			$fila2=mysqli_fetch_array($con2);
			$id_fitxatge=$fila2["max(id_fitxatge)"]+1;
		
			$ins=mysqli_query($cnx_intranet,"insert into t_fitxatge set 
			id_fitxatge=".$id_fitxatge.",
			id_persona=".$fila["id_persona"].",
			any=".$fila["any"].",
			mes=".$fila["mes"].",
			dia=".$fila["dia"].",
			entra_h='".$fila["hora_comen"]."',
			entra_m='".$fila["minut_comen"]."';");
		
			$ins=mysqli_query($cnx_intranet,"insert into t_f_exterior set 
			id_fitxatge=".$id_fitxatge.",
			id_persona=".$fila["id_persona"].",
			any=".$fila["any"].",
			mes=".$fila["mes"].",
			dia=".$fila["dia"].",
			surt_h='".$fila["hora_comen"]."',
			surt_m='".$fila["minut_comen"]."',
			hora_prevista='".addslashes($fila["hora_prevista"])."',
			hora_tanca='".$fila["hora_tanca"]."',
			minut_tanca='".$fila["minut_tanca"]."',
			lloc='".addslashes($fila["lloc"])."',
			motiu='".addslashes($fila["motiu"])."',
			automatic=1;");
			
			if($ins){
				if($fila["hora_prevista"]!=""){
					$be3=mysqli_query($cnx_intranet,"delete from t_f_exterior_tmp where id_exterior=".$fila["id_exterior"]);
				}
			}
		}
	}
	//Entro si es data de tanca inferior o horari inferior a actual
	if( ($data<data_actual8()) || (($data==data_actual8())&&($tanca<$hora_act)&&($tanca!='')) ){
		//Si no ha fitxat cap entrada anterior al t_f_exterior_tmp
		$con1=mysqli_query($cnx_intranet,"select max(id_fitxatge) from t_fitxatge where id_persona=".$fila["id_persona"]." 
		and mes=".$fila["mes"]." and dia=".$fila["dia"]." and any=".$fila["any"]." and 
		((entra_h<'".$fila["hora_comen"]."') or 
		 (entra_h='".$fila["hora_comen"]."' and entra_m<='".$fila["minut_comen"]."'))
		");
		$fila1=mysqli_fetch_array($con1);
		if($fila1["max(id_fitxatge)"]!=""){
			
			$con1=mysqli_query($cnx_intranet,"select * from t_fitxatge where id_fitxatge=".$fila1["max(id_fitxatge)"]);
			$fila1=mysqli_fetch_array($con1);
			
			list($dif_h,$dif_m)=restar_temps($fila["hora_tanca"],$fila["minut_tanca"],$fila1["entra_h"],$fila1["entra_m"]);
			$ins=mysqli_query($cnx_intranet,"update t_fitxatge set 
			surt_h='".$fila["hora_tanca"]."', surt_m='".$fila["minut_tanca"]."', dif_h='".$dif_h."', dif_m='".$dif_m."' 
			where id_fitxatge=".$fila1["id_fitxatge"]);
			
			$con2=mysqli_query($cnx_intranet,"select * from t_f_exterior where id_fitxatge=".$fila1["id_fitxatge"]." and torna_h=''");
			if(mysqli_num_rows($con2)==1){
				$fila2=mysqli_fetch_array($con2);
				
				mysqli_query($cnx_intranet,"update t_f_exterior set 
				torna_h='".$fila["hora_tanca"]."', torna_m='".$fila["minut_tanca"]."' 
				where id_exterior=".$fila2["id_exterior"]);			
			}
			if($ins){
				$be3=mysqli_query($cnx_intranet,"delete from t_f_exterior_tmp where id_exterior=".$fila["id_exterior"]);
			}	
		}
	}
}
?>