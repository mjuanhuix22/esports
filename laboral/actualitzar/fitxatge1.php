<?php 
//------------------------FITXATGE
/*Cada dia compto les hores que cada persona ha fet des del dia de l'ultima actualització fins ahir.(t_fitxatge)
Comprobare les hores que havia de fer aquell dia de la setmana.(tenin en compte t_contracte, t_absencies_jornada, t_absencies_hores, t_horari)
Si no quadren posare les hores de més o de menys que ha fet a t_f_descuadra
*/

/*Pot ser que hi hagi hagut alguns dies que ningut hagi entrat a la intranet:
per tant calcularem el fitxatge des del dia de l'ultima actualització (ultima_act) fins al dia d'ahir */


//Quin dia va ser l'ultima actualització 
$con=mysqli_query($cnx_intranet,"select * from ultima_act");
$fila=mysqli_fetch_array($con);
$id_act=$fila["id_act"];
$dia=$fila["dia"];
$mes=$fila["mes"];
$any=$fila["any"];

while(($dia!=date('d'))||($mes!=date('m'))||($any!=date('Y'))){

	include("fitxatge2.php");
	if($dia==total_dies($mes,$any)){
		$dia=1;
		if($mes==12){
			$mes=1;
			$any=$any+1;
		}else{
			$mes=$mes+1;
		}
		
	}else{
		$dia++;
	}
}


?>
