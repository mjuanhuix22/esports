<?php 


$exist=0;

$SQL_jornada="SELECT t_absencies_jornada.dia,t_absencies_jornada.mes,t_absencies_jornada.any, t_absencies_jornada.id_persona,
t_conceptes.id_concepte,t_conceptes.nom_concepte  
FROM t_absencies_jornada,t_conceptes where 
t_absencies_jornada.id_persona=".$_SESSION["id_usuari"]." and 
t_conceptes.just!='' and 

t_absencies_jornada.id_concepte=t_conceptes.id_concepte and 
t_absencies_jornada.validador=0 and 
(
(t_absencies_jornada.any<".date('Y').") or 
(t_absencies_jornada.any=".date('Y')." and t_absencies_jornada.mes<".date('m').") or 
(t_absencies_jornada.any=".date('Y')." and t_absencies_jornada.mes=".date('m')." and t_absencies_jornada.dia<=".date('d').")
) and entrega_just=0 
order by t_absencies_jornada.any,t_absencies_jornada.mes,t_absencies_jornada.dia";
$con=mysqli_query($cnx_intranet,$SQL_jornada);

$SQL_hores="SELECT t_absencies_hores.dia,t_absencies_hores.mes,t_absencies_hores.any, 
hora_surt,min_surt,hora_torna,min_torna,hora_surt,min_surt,hora_surt_p,min_surt_p,hora_torna_p,min_torna_p,
t_absencies_hores.id_persona,
t_conceptes.id_concepte,t_conceptes.nom_concepte  
FROM t_absencies_hores,t_conceptes where 
t_absencies_hores.id_persona=".$_SESSION["id_usuari"]." and 
t_conceptes.just!='' and 
t_absencies_hores.id_concepte=t_conceptes.id_concepte and 
t_absencies_hores.validador=0 and 
(
(t_absencies_hores.any<".date('Y').") or 
(t_absencies_hores.any=".date('Y')." and t_absencies_hores.mes<".date('m').") or 
(t_absencies_hores.any=".date('Y')." and t_absencies_hores.mes=".date('m')." and t_absencies_hores.dia<=".date('d').")
) and entrega_just=0 
order by t_absencies_hores.any,t_absencies_hores.mes,t_absencies_hores.dia";


$con1=mysqli_query($cnx_intranet,$SQL_hores);
if((mysqli_num_rows($con)!=0)||(mysqli_num_rows($con1)!=0)){
	
	$exist=1;

	?>
	<table width="100%"  border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td><span class="lletraVerda">Justificants pendents d'entregar a Gesti&oacute; de Personal </span></td>
		  </tr>
		  <tr>
			<td class="fonsverd"><img src="imatges/img_transparent.gif" width="1" height="1"></td>
		  </tr>
	</table>
	<br>
	<table  border="1" cellpadding="4" cellspacing="0">
	<tr>
			<td height="25" bgcolor="#CCCCCC" class="lletraNegreta">Data</td>
			<td height="25" bgcolor="#CCCCCC" class="lletraNegreta">Concepte</td>
			<td height="25" bgcolor="#CCCCCC" class="lletraNegreta">Horari sol&middot;licitat</td>
			<td height="25" bgcolor="#CCCCCC" class="lletraNegreta">Horari fet</td>
	</tr><?php 
	 $con=mysqli_query($cnx_intranet,$SQL_jornada);?>
	<?php while($fila=mysqli_fetch_array($con)){?>
    	
		<tr>
			<td class="lletraNorm"><?php 
			echo " ".$fila["dia"]." ".det_mes($fila["mes"]).$nom_mes[$fila["mes"]];?></td>
			<td class="lletraNorm"><?php echo   $fila["nom_concepte"];?></td>
			<td class="lletraNorm">Tota la jornada laboral</td>
			<td></td>
		</tr>
	<?php }?>
	<?php 

	$con1=mysqli_query($cnx_intranet,$SQL_hores);
	while($fila1=mysqli_fetch_array($con1)){?>
		<tr>
			<td class="lletraNorm"><?php //echo $SQL_just_hores;
			echo " ".$fila1["dia"]." ".det_mes($fila1["mes"]).$nom_mes[$fila1["mes"]];?></td>
			<td class="lletraNorm"><?php echo   $fila1["nom_concepte"];?></td>
			<td class="lletraNorm"><?php echo   "de ".$fila1["hora_surt_p"].":".$fila1["min_surt_p"];
			echo " a ".$fila1["hora_torna_p"].":".$fila1["min_torna_p"];?></td>
			<td class="lletraNorm"><?php 
			if(($fila1["hora_surt"]==$fila1["hora_torna"])&&($fila1["min_surt"]==$fila1["min_torna"])){
				echo "No fitxat correctament";
			}else{
				echo "de ".$fila1["hora_surt"].":".$fila1["min_surt"]." a ".$fila1["hora_torna"].":".$fila1["min_torna"];
			}?></td>
		</tr>
<?php } ?>        
</table>
<?php 
}
if($exist==1){ $entra=1;}?>