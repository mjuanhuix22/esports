<?php

/*------------------Recorro tots els fitxatges. 
Comprovar: I si esta fora de contracte  i no esté fent hores de mes i no està fent descans, o esta fent permis retribuit
Si son 30 minuts seguits i el total es inferior al maxim que es pot fer.
*/




//primer hora d'entrada
$con=mysqli_query($cnx_intranet,"select min(entra_h) from t_fitxatge where dia=".$dia." and mes=".$mes." and any=".$any." and id_persona=".$id_persona);
$fila=mysqli_fetch_array($con);
$h_inici=$fila["min(entra_h)"];


//Ultima hora de sortida
$con=mysqli_query($cnx_intranet,"select max(surt_h) from t_fitxatge where dia=".$dia." and mes=".$mes." and any=".$any." and id_persona=".$id_persona);
$fila=mysqli_fetch_array($con);
$h_fi=$fila["max(surt_h)"]+1;




//Recorro minut per minut
$min_seguits=0; //Comptador per saber si el fitxatge te 30 minuts seguits si no es així es reinicialitza a 0
$e=1;
$total_extra=0; //Comptador total fet no sigui superior al $MAX
$H_extra_inici=array();$M_extra_inici=array();$H_extra_fi=array();$M_extra_fi=array();


for($h=$h_inici;$h<=$h_fi;$h++){
	for($m=0;$m<=59;$m++){

		$entra=0;
		
		//Esta treballant en aquest temps
		$con=mysqli_query($cnx_intranet,"select count(*) from t_fitxatge where 
		dia=".$dia." and mes=".$mes." and any=".$any." and id_persona=".$id_persona." and 
		( ((entra_h<'".convert_num2($h)."')or ((entra_h='".convert_num2($h)."') and (entra_m<='".convert_num2($m)."'))) and 
		((surt_h>'".convert_num2($h)."') or ((surt_h='".convert_num2($h)."') and (surt_m>='".convert_num2($m)."'))))");
		$fila=mysqli_fetch_array($con);
		
		if($fila["count(*)"]==1){
			
			//Si te permis hi esta treballant
			$con=mysqli_query($cnx_intranet,"select count(*) from t_absencies_jornada where 
			validador=0 and 
			id_persona=".$id_persona." and 
			dia=".$dia." and mes=".$mes." and any=".$any."");
			$fila=mysqli_fetch_array($con);
			if($fila["count(*)"]==1){
				$entra=1;
			}else{
	
			
				//No esta treballant dins del contracte
				$con=mysqli_query($cnx_intranet,"select count(*) from t_contracte_hores,t_contracte where 
				id_persona=".$id_persona." and inici<='".convert_data8($any,$mes,$dia)."' and 
				(fi='' or fi>= '".convert_data8($any,$mes,$dia)."') and 
				t_contracte.id_contracte=t_contracte_hores.id_contracte and 
				diaset='".dia7($any,$mes,$dia)."' and 
				( ((de_h<'".convert_num2($h)."')or ((de_h='".convert_num2($h)."') and (de_m<'".convert_num2($m)."'))) and 
				((a_h>'".convert_num2($h)."') or ((a_h='".convert_num2($h)."') and (a_m>'".convert_num2($m)."'))))");
	
				$fila=mysqli_fetch_array($con);
				
				if($fila["count(*)"]==0){
				
					//No esta fent hores extres
					/*$con=mysqli_query($cnx_intranet,"select count(*) from t_horari_sol,t_horari where 
					t_horari_sol.id_sol=t_horari.id_sol and 
					t_horari_sol.id_persona=".$id_persona." and 
					t_horari.dia=".$dia." and t_horari.mes=".$mes." and t_horari.any=".$any." and 
					t_horari.menys=0 and 
					(t_horari.de_h<'".convert_num2($h)."' or (t_horari.de_h='".convert_num2($h)."' 
					and t_horari.de_m<'".convert_num2($m)."')) and 
					(t_horari.a_h>'".convert_num2($h)."' or (t_horari.a_h='".convert_num2($h)."' and t_horari.a_m>'".convert_num2($m)."'))");
					
					
					$fila=mysqli_fetch_array($con);
					if($fila["count(*)"]==0){*/
					
						
						//No està dins de descans
						$con=mysqli_query($cnx_intranet,"select count(*) from t_f_descans where 
						dia=".$dia." and mes=".$mes." and any=".$any." and id_persona=".$id_persona." and 
						( ((surt_h<'".convert_num2($h)."')or ((surt_h='".convert_num2($h)."') and (surt_m<'".convert_num2($m)."'))) and 
						((torna_h>'".convert_num2($h)."') or ((torna_h='".convert_num2($h)."') and (torna_m>'".convert_num2($m)."'))))");
						$fila=mysqli_fetch_array($con);
						if($fila["count(*)"]==0){
							$entra=1;		
						}
					//}
				}
			
			}
		}
		
		
		
		//Si compleix les 4 condicions
		if($entra==1){

			if($MAX>$total_extra){ //No pot fer mes del màxim
				
				//Si ha fet 30 minuts seguits inicia array inici: restant 30 minuts a l'horari actual
				if($min_seguits==30){
					list($H_extra_inici[$e],$M_extra_inici[$e])=restar_temps($h,$m,0,30); 
					$total_extra=$total_extra+30;
				}
				//Si ha fet més de 30 minuts seguits es modifica array fi
				if($min_seguits>=30){
					$H_extra_fi[$e]=$h;
					$M_extra_fi[$e]=$m;
					
					if($min_seguits!=30){
						$total_extra++;	
					}
				}	
				//$text.=$h.":".$m."seg:".$min_seguits."total_extra:".$total_extra."max".$MAX."<br>";
			}
			
			$min_seguits++;	
			
		}else{
			//Si no compleix les 4 condicions es reinicia el comptador a 0
			$min_seguits=0;	
			
			//es crea una nou array extres
			if(isset($H_extra_fi[$e])){
				$e++;	
				$total_extra=$total_extra-1;
			}
			
		}
	}
			
}//for($h)




for($a=1;$a<$e;$a++){
	$text.="3extres<br>";
	$text.="Extra:".$H_extra_inici[$a].":".$M_extra_inici[$a]." a ".$H_extra_fi[$a].":".$M_extra_fi[$a]."";	
	
	
	list($h,$m)=restar_temps($H_extra_fi[$a],$M_extra_fi[$a],$H_extra_inici[$a],$M_extra_inici[$a]); 
	$text.="(".$h.":".$m."h)<br>";
	
	
	
}
$text.="Total extra: ".($total_extra)."<br>";



?>