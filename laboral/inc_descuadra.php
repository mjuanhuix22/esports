<?php
function descuadra_fit($any,$mes,$dia,$id_persona){

    //require("/var/www/laboral_20161103/conexio.php");
    require("conexio.php");

    //echo dirname(__FILE__);

    if($any>=(date('Y')-1)){



        $conQ=mysqli_query($cnx_intranet,"select * from personal where id_persona=".$id_persona);
        $filaQ=mysqli_fetch_array($conQ);
        if($filaQ["obli_fitxar"]==1){
            $mess=$mes;
            $diaa=$dia;
            if(strlen($dia)==1){ $diaa="0".$dia;}
            if(strlen($mes)==1){ $mess="0".$mes;}
            $data8=$any.$mess.$diaa;

            $diaa=date('d');$mess=date('m');
            if(strlen($diaa)==1){ $diaa="0".$diaa;}
            if(strlen($mess)==1){ $mess="0".$mess;}
            $data_actual=date('Y').$mess.$diaa;

            if($data8<$data_actual){


                /*
                Comprobare les hores que havia de fer aquell dia .(tenin en compte t_contracte, t_absencies_jornada, t_absencies_hores, t_horari)
                Si no quadren posare les hores de més o de menys que ha fet a t_f_descuadra


                En aquest arxiu fitxatge3.php entro saben les variables:
                $any,$mes,$dia,$id_persona*/

                //--------------------comprobo que havia de treballar

                mysqli_query($cnx_intranet,"delete from t_f_descuadra where 
			id_persona=".$id_persona." and 
			any=".$any." and 
			mes=".$mes." and
			dia=".$dia);


                $permis_jornada=0;
                $hores_obli=0;$minuts_obli=0;

                //comprobo que no hagues demanat absencia jornada retribuida
                $conD=mysqli_query($cnx_intranet,"select * from t_absencies_jornada where id_persona=".$id_persona." 
			and dia=".$dia." and mes=".$mes." and any=".$any." and validador=0");
                if(mysqli_num_rows($conD)==1){
                    $permis_jornada=1;
                }

                //miro les hores que havia d'haver fet en la taula t_contracte
                $hores_obli=0;$minuts_obli=0;
                $conD=mysqli_query($cnx_intranet,"SELECT * FROM t_contracte WHERE id_persona=".$id_persona." 
			and inici<=".$data8." and (fi>=".$data8." or fi='')");
                if(mysqli_num_rows($conD)==1){
                    $filaD=mysqli_fetch_array($conD);
                    $conD=mysqli_query($cnx_intranet,"SELECT * FROM t_contracte_hores WHERE 
				id_contracte=".$filaD["id_contracte"]." and diaset=".dia7($any,$mes,$dia));
                    while($filaD=mysqli_fetch_array($conD)){
                        list($hores_cont,$minuts_cont)=restar_temps($filaD["a_h"],$filaD["a_m"],$filaD["de_h"],$filaD["de_m"]);
                        list($hores_obli,$minuts_obli)=sumar_temps($hores_obli,$minuts_obli,$hores_cont,$minuts_cont);

                    }

                }
                //echo "1-".$hores_obli.":".$minuts_obli."<br>";

                //Resto les hores de permis retribuit per hores
                $conD=mysqli_query($cnx_intranet,"select * from t_absencies_hores where id_persona=".$id_persona." 
			and dia=".$dia." and mes=".$mes." and any=".$any." and hora_surt!='' 
			and hora_torna!='' and validador=0");
                while($filaD=mysqli_fetch_array($conD)){
                    list($hores_cont,$minuts_cont)=restar_temps($filaD["hora_torna"],$filaD["min_torna"],$filaD["hora_surt"],$filaD["min_surt"]);
                    list($hores_obli,$minuts_obli)=restar_temps($hores_obli,$minuts_obli,$hores_cont,$minuts_cont);
                    //echo $hores_obli." ".$minuts_obli;
                }
                //echo "2-".$hores_obli.":".$minuts_obli."<br>";


                $conD=mysqli_query($cnx_intranet,"select * from t_absencies_jornada where id_persona=".$id_persona." 
			and dia=".$dia." and mes=".$mes." and any=".$any." and validador=0");
                if(mysqli_num_rows($conD)==1){
                    $conD=mysqli_query($cnx_intranet,"SELECT * FROM t_contracte WHERE id_persona=".$id_persona." 
				and inici<=".$data8." and (fi>=".$data8." or fi='')");
                    if(mysqli_num_rows($conD)==1){
                        $filaD=mysqli_fetch_array($conD);
                        $conD=mysqli_query($cnx_intranet,"SELECT * FROM t_contracte_hores WHERE 
					id_contracte=".$filaD["id_contracte"]." and diaset=".dia7($any,$mes,$dia));
                        while($filaD=mysqli_fetch_array($conD)){
                            list($hores_cont,$minuts_cont)=restar_temps($filaD["a_h"],$filaD["a_m"],$filaD["de_h"],$filaD["de_m"]);
                            list($hores_obli,$minuts_obli)=restar_temps($hores_obli,$minuts_obli,$hores_cont,$minuts_cont);

                        }
                    }
                }


                //Els canvis d'horari es sumen o resten a les hores obli
                $conD=mysqli_query($cnx_intranet,"select * from t_horari,t_horari_sol WHERE 
			t_horari.id_sol=t_horari_sol.id_sol and 
			t_horari_sol.id_persona=".$id_persona." 
			and t_horari.dia=".$dia." and t_horari.mes=".$mes." and 
			t_horari_sol.validador=0 and  
			t_horari.any=".$any);
                while($filaD=mysqli_fetch_array($conD)){
                    list($dif_h,$dif_m,$positiu)=restar_temps($filaD["a_h"],$filaD["a_m"],$filaD["de_h"],$filaD["de_m"]);
                    if($filaD["menys"]==1){
                        list($hores_obli,$minuts_obli,$positiu)=restar_temps($hores_obli,$minuts_obli,$dif_h,$dif_m);
                    }elseif($filaD["menys"]==0){
                        list($hores_obli,$minuts_obli)=sumar_temps($hores_obli,$minuts_obli,$dif_h,$dif_m);
                    }
                }
                //echo "obli-".$hores_obli.":".$minuts_obli."<br>";

                //---------------------------------------------FITXATGE--------------------------------------------------------------
                //miro les hores que va fer
                //si la persona no va fitxar al sortir als camps dif_h i dif_m no hi ha res
                $hores_fetes=0;
                $minuts_fets=0;
                $conD=mysqli_query($cnx_intranet,"select * from t_fitxatge where id_persona=".$id_persona." 
			and dia=".$dia." and mes=".$mes." and any=".$any);
                while($filaD=mysqli_fetch_array($conD)){
                    $hores_fetes=$filaD["dif_h"]+$hores_fetes;
                    $minuts_fets=$filaD["dif_m"]+$minuts_fets;
                }
                //echo "4-".$hores_fetes.":".$minuts_fets."<br>";

                //pasar els minuts a hores
                list($hores_fetes,$minuts_fets)=passar_minuts_hores($hores_fetes,$minuts_fets);
                $entra=1;
                if(($hores_obli==0)&&($minuts_obli==0)&&($hores_fetes==0)&&($minuts_fets==0)){
                    $entra=0;
                }

                //miro les diferencies entre temps fet i temps obligatori
                list($dif_h,$dif_m,$positiu)=restar_temps($hores_fetes,$minuts_fets,$hores_obli,$minuts_obli);



                //En la taula t_config diu els minuts de descans i el temps
                $SQLC="select * from t_config where 
                id_persona=".$id_persona." and  
                data_inici<=".convert_data8($any,$mes,$dia)." and  
                (data_fi>=".convert_data8($any,$mes,$dia)." or data_fi is null)";
                $conC=mysqli_query($cnx_intranet,$SQLC);
                if(mysqli_num_rows($conC)==0){
                    $SQLC="select * from t_config where data_inici<=".convert_data8($any,$mes,$dia)." and 
                (data_fi>=".convert_data8($any,$mes,$dia)." or data_fi is null) 
                and id_persona is null";
                }
                $conC=mysqli_query($cnx_intranet,$SQLC);
                $filaC=mysqli_fetch_array($conC);
                $C_minuts_descans=$filaC["minuts_descans"];
                $C_dret_descans=$filaC["dret_descans"];

                $temps_fet=convert_num2($hores_fetes).":".convert_num2($minuts_fets);



                //En la taula t_conceptes diu si te dret a descans
                $conD=mysqli_query($cnx_intranet,"select count(*) from t_absencies_hores,t_conceptes where 
                id_persona=".$id_persona." 
                and dia=".$dia." and mes=".$mes." and t_absencies_hores.any=".$any." and hora_surt!='' 
                and hora_torna!='' and validador=0 and 
                t_absencies_hores.id_concepte=t_conceptes.id_concepte and 
                t_conceptes.resta_descans=0");
                $filaD=mysqli_fetch_array($conD);
                if($filaD["count(*)"]!=0){
                    $max_descans=$C_minuts_descans;
                }
                elseif($temps_fet<$C_dret_descans){
                    $max_descans=0;
                }else{
                    $max_descans=$C_minuts_descans;
                }


                $hores_fetes=0;
                $minuts_fets=0;
                //---------------------------------------------DESCANS-----------------------------------------------------------------
                //miro el temps de descans que va fer i si es superior al temps que tenia dret el resto al temps treballat

                $conD=mysqli_query($cnx_intranet,"select * from t_f_descans where id_persona=".$id_persona." 
			and dia=".$dia." and mes=".$mes." and any=".$any);
                while($filaD=mysqli_fetch_array($conD)){
                    $hores_fetes=$filaD["dif_h"]+$hores_fetes;
                    $minuts_fets=$filaD["dif_m"]+$minuts_fets;
                }
                //echo "5-".$hores_fetes.":".$minuts_fets."<br>";
                //pasar els minuts a hores
                list($hores_fetes,$minuts_fets)=passar_minuts_hores($hores_fetes,$minuts_fets);

                //echo "6-".$hores_fetes.":".$minuts_fets."<br>";



                if(($hores_fetes>0)||($minuts_fets>$max_descans)){
                    //miro les diferencies entre temps descansat i el que podia fer
                    list($h_desc,$m_desc,$p)=restar_temps(0,$max_descans,$hores_fetes,$minuts_fets);
                    //si ha treballat de menys: sumo el fitxatge de menys + el descans de mes
                    if($positiu==0){
                        list($dif_h,$dif_m)=sumar_temps($dif_h,$dif_m,$h_desc,$m_desc);
                    }
                    //si he treballat de mes resto : el fitxatge de mes - els minuts de mes
                    elseif($positiu==1){
                        list($dif_h,$dif_m,$positiu)=restar_temps($dif_h,$dif_m,$h_desc,$m_desc);
                    }
                }

                //-----------------------------------------T_F_DESCUADRA----------------------------------------------
                if(($dif_h!=0)||($dif_m!=0)){
                    mysqli_query($cnx_intranet,"insert into t_f_descuadra set 
				id_persona=".$id_persona.",
				any=".$any.",
				mes=".$mes.",
				dia=".$dia.",
				dif_h=".$dif_h.",
				dif_m='".$dif_m."',
				valor=".$positiu);
                }
            }//if($data8<$data_actual){
        }//if($filaQ["obli_fitxar"]==1){

        compensa_fit($any,$mes,$dia,$id_persona);

    }
}

?>