Mercedes Gomez Poveda

Benestar social
Consell Comarcal de la Selva
Tel.:872 973 003  Fax:972 840 561<br>
A/e:


Aquest correu electr�nic i els documents adjunts que l'integren contenen informaci� confidencial �nicament adre�ada al seu destinatari o destinataris. Queda prohibida la seva divulgaci�, c�pia o distribuci� a tercers sense autoritzaci� escrita del Consell Comarcal de la Selva. En el cas d'haver rebut aquest correu electr�nic per error, us demanem que ens notifiqueu directament aquesta circumst�ncia mitjan�ant el reenviament o resposta a la nostra adre�a electr�nica (que figura en el remitent) i que elimineu el correu dels vostres sistemes inform�tics. Gr�cies.

Abans d'imprimir aquest correu-e penseu b� si �s necessari fer-ho. El medi ambient �s cosa de tothom.