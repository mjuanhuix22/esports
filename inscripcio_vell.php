<?php include("conexio.php");
include("inc.php");


if(!isset($_GET["id_ins"])){ header("location:index.php");}
$id_ins=$_GET["id_ins"];

$conI=mysqli_query($cnx_cesportiu,"select count(*) from i1_inscripcions where id_ins=".$id_ins." and obert=1");
$filaI=mysqli_fetch_array($conI);
if($filaI["count(*)"]==0){ header("location:index.php");}

$conA=mysqli_query($cnx_cesportiu,"select nom_act,a1_activitats.id_act,n_nom_act,nom_ins from a1_activitats,i1_inscripcions where 
a1_activitats.id_act=i1_inscripcions.id_act and 
id_ins=".$id_ins);
$filaA=mysqli_fetch_array($conA);
$titol="Inscripció ".StripSlashes($filaA["nom_act"])." ".StripSlashes($filaJ["nom_ins"]);


$conJ=mysqli_query($cnx_cesportiu,"select nom_jornada,a2_jornades.id_jornada,n_nom_jornada,nom_ins from a2_jornades,i1_inscripcions where 
a2_jornades.id_jornada=i1_inscripcions.id_jornada and 
id_ins=".$id_ins);
$filaJ=mysqli_fetch_array($conJ);
$titol="Inscripció ".StripSlashes($filaJ["nom_jornada"])." ".StripSlashes($filaJ["nom_ins"]);

?>
<html><!-- InstanceBegin template="/Templates/plantilla.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<?php ini_set('default_charset','utf-8');?>
<link rel="icon" type="image/gif" href="favicon.gif" />

<!-- InstanceBeginEditable name="doctitle" -->
<title><?php echo  $titol;?> - Esports del Consell Comarcal de la Selva</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-2354782-1");
pageTracker._trackPageview();
} catch(err) {}</script>


<link href="plantilles/estils_site.css" rel="stylesheet" type="text/css" />
<?php if(preg_match('/MSIE/i',$_SERVER['HTTP_USER_AGENT']))
{?>
	<link href="plantilles/estils_site_explorer.css" rel="stylesheet" type="text/css" />
<?php }?>
<link href="gral/estils.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="scripts.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>

<div id="cont_body">


<div id="toolbar"><a href="ceselva.php" class="menu_dalt">La nostra oficina</a><span style="float:right; color:#FFFFFF; padding-top:5px; padding-left:5px;">|</span>
<a href="entitats/index.php" class="menu_dalt" target="_blank">Intranet de les entitats</a><a href="index.php"><img src="imatges/logoConsellEsportiuSelva_HoritzontalDegradat.png" alt="Consell Esportiu de la Selva" border="0" /></a>
</div>

<div id="menu1" style="margin-bottom:25px;">
	<ul>
	<?php 
	$enllac=array(1=>"index",2=>"index",3=>"parcs_salut",4=>"entitats",5=>"instalacions");
	
	$nom_enllac=array(1=>"Esports",2=>"Municipis",3=>"Parcs urbans de salut",4=>"Entitats",5=>"Instal·lacions esportives");
	
	for($i=1;$i<=count($enllac);$i++){?>
		<li class="nivel1" style="width:<?php echo (100/count($enllac));?>%"><a  href="<?php echo $enllac[$i];?>.php" class="nivel1
		<?php if(strpos($_SERVER['REQUEST_URI'],$enllac[$i])!=false){ echo "actiu";}else{ echo "no_actiu";}?>"><?php echo $nom_enllac[$i];?></a>
		<?php 
		if($i==1){?>
        	<ul>
			<?php $con=mysqli_query($cnx_cesportiu,"select * from Zdeportes order by nom_deporte");
			while($fila=mysqli_fetch_array($con)){?>
			 	 <li><a href="esport.php?nom=<?php echo $fila["n_nom_deporte"]?>" ><img src="imatges/deportes/<?php echo $fila["id_deporte"]?>.gif" width="20" hspace="4" border="0" /><?php echo $fila["nom_deporte"];?></a></li>
			  <?php }?>
       		</ul>
        <?php }elseif($i==2){?>
        	<ul>
            	<?php $con=mysqli_query($cnx_cesportiu,"select * from Zmunicipis order by nom_muni");
				while($fila=mysqli_fetch_array($con)){?>
				  <li style="width:230px;"><a href="municipi.php?nom=<?php echo $fila["normalitza_muni"]?>" ><?php echo $fila["nom_muni"];?></a></li>
                <?php }?>
             </ul>
		 <?php }?>
           
        </li>
	<?php }?>
    </ul>
</div>












<div id="menu_dreta">
<a href="index.php" class="anterior">Inici</a><!-- InstanceBeginEditable name="menu_navegacio" --><?php 


$conA=mysqli_query($cnx_cesportiu,"select nom_act,a1_activitats.id_act,n_nom_act from a1_activitats,i1_inscripcions where 
a1_activitats.id_act=i1_inscripcions.id_act and 
id_ins=".$id_ins);
$filaA=mysqli_fetch_array($conA);
$titol="Inscripció ".StripSlashes($filaA["nom_act"]);
echo "<a href=\"activitats.php?nom_activitat=".$filaA["n_nom_act"]."&id_act=".$filaA["id_act"]."\" class=\"anterior\">".$filaA["nom_act"]."</a>";



$conJ=mysqli_query($cnx_cesportiu,"select nom_jornada,a2_jornades.id_jornada,n_nom_jornada from a2_jornades,i1_inscripcions where 
a2_jornades.id_jornada=i1_inscripcions.id_jornada and 
id_ins=".$id_ins);
$filaJ=mysqli_fetch_array($conJ);
if(mysqli_num_rows($conJ)!=0){
$titol="Inscripció ".StripSlashes($filaJ["nom_jornada"]);
echo "<a href=\"activitats.php?nom_activitat=".$filaA["n_nom_act"]."&id_act=".$filaA["id_act"]."&nom_jornada=".$filaJ["n_nom_jornada"]."&id_jornada=".$filaJ["id_jornada"]."\" class=\"anterior\">".$filaJ["nom_jornada"]."</a>";

}

?><!-- InstanceEndEditable -->
<span class="ante_titol"><!-- InstanceBeginEditable name="titol_nagegacio" --><?php echo  $titol;?><!-- InstanceEndEditable --></span>

<!-- InstanceBeginEditable name="contingut" -->

<h1><?php echo  $titol?></h1>

<?php $conI=mysqli_query($cnx_cesportiu,"select * from i1_inscripcions where id_ins=".$id_ins);
$filaI=mysqli_fetch_array($conI);

echo StripSlashes($filaI["text"]);
?>
<h2>Categories</h2>

<table cellpadding="2" cellspacing="5"  border="0">
  <tr>
    <th >Categoria</th>
    <th >Any de naixament </th>
  </tr>
<?php $con1=mysqli_query($cnx_cesportiu,"select * from i2_cat where id_ins=".$id_ins." order by any_inici");
while($fila1=mysqli_fetch_array($con1)){?>
  <tr>
    <td ><?php echo  $fila1["nom_cat"];?></td>
    <td ><?php echo  "De ".$fila1["any_inici"]." a ".$fila1["any_fi"];?></td>
  </tr>
<?php }?>
</table>


<h2>Inscipcions per entitats selvatanes</h2>

<p>Si sou una entitat esportiva de la comarca de la selva feu la vostra inscripció a traves de la intranet de les entitats

<a href="entitats/" target="_blank">http://www.selvaesports.cat/entitats</a>
</p>


<h2>Inscripcions per esportistes independents o entitats de fora de la comarca</h2>

<script language="javascript">
function validar_pas1(){
	if((window.document.form_cros.id_entidad.value=="")||
	(window.document.form_cros.num.value=="")||
	(isNaN(window.document.form_cros.num.value))||
	(window.document.form_cros.mail_contacte.value=="")||
	(window.document.form_cros.nom_contacte.value=="") ){
		alert("Es obligatori omplir els camps del formulari");
		return (false);
	}
}
function validar_cros2(sol_DNI,sol_federacio){
	
	var resp=validar_pas1();
	if(resp==false){
		return (false);	
	}
	var num=window.document.form_cros.num.value;
	for(var n=1;n<=num;n++){
		
		
		if(sol_DNI==1){
			var DNI="DNI_"+n;
			if(eval('window.document.form_cros.'+DNI+'.value==""')){
				alert("Es obligatori omplir els camps del formulari");
				return (false);
			}
		}else{
			NSS1="NSS1_"+n;NSS2="NSS2_"+n;NSS3="NSS3_"+n;NSS4="NSS4_"+n;NSS5="NSS5_"+n;
			
			if((eval('window.document.form_cros.'+NSS1+'.value==""'))||
			(eval('window.document.form_cros.'+NSS2+'.value==""'))||
			(eval('window.document.form_cros.'+NSS3+'.value==""'))||
			(eval('window.document.form_cros.'+NSS4+'.value==""'))||
			(eval('window.document.form_cros.'+NSS5+'.value==""')) ){
				alert("Es obligatori omplir els camps del formulari");
				return (false);
			}
		}
		
		
		var cognom1="cognom1_"+n;		
		var nom="nom_"+n;
		var any="any_"+n;	
		var id_genere="id_genere_"+n;	
		if((eval('window.document.form_cros.'+cognom1+'.value==""'))||
		(eval('window.document.form_cros.'+nom+'.value==""'))||
		(eval('window.document.form_cros.'+any+'.value==""'))||
		(eval('window.document.form_cros.'+id_genere+'.value==""')) ){
			alert("Es obligatori omplir els camps del formulari");
			return (false);
		}
		
		
		if(sol_federacio!=0){
			var llicencia="llicencia_"+n;	
			var tramit="tramit_"+n;	
			if(eval('window.document.form_cros.'+any+'.value<="'+sol_federacio+'"')){
				if( (eval('window.document.form_cros.'+llicencia+'.value==""'))&&
					(eval('window.document.form_cros.'+tramit+'.checked==false'))
					){ alert("Cal indicar la llicencia dels esportistes nascuts abans del "+sol_federacio);;
						return(false);
					}
			}
		}
		
		
	}
	return (true);

}
function visual_federacio(casella){
	
	eval('window.document.getElementById("'+casella+'").className="visual_si"');
}
function amaga_federacio(casella){
	eval('window.document.getElementById("'+casella+'").className="visual_no"');
}


function veure_cat(casella,valor){
	eval('window.document.getElementById("'+casella+'").value="'+valor+'"');
}

</script>


<form method="post" name="form_cros">

<table border="0">
  <tr>
    <td>Entitat</td>
    <td><select name="id_entidad">
      <option value=""></option>
      <?php $con=mysqli_query($cnx_cesportiu,"select nom_entidad,entidades.id_entidad 
		from entidades,entidades_deportes,i1_inscripcions,a1_activitats where 
		entidades.id_entidad!=1 and 
		id_entitat=0 and 
		entidades.id_entidad=entidades_deportes.id_entidad and 
		entidades_deportes.id_deporte=a1_activitats.id_deporte and 
		a1_activitats.id_act=i1_inscripcions.id_act and 
		i1_inscripcions.id_ins=".$id_ins." 
		order by nom_entidad");
		while($fila=mysqli_fetch_array($con)){?>
      <option value="<?php echo  $fila["id_entidad"]?>" 
            <?php if($_POST["id_entidad"]==$fila["id_entidad"]){?> selected="selected"<?php }?>><?php echo  $fila["nom_entidad"];?></option>
      <?php }?>
    </select></td>
  </tr>
  <tr>
    <td>Mail de contacte</td>
    <td><input type="text" name="mail_contacte" value="<?php if(isset($_POST["mail_contacte"])){echo $_POST["mail_contacte"];}?>" size="40"/></td>
  </tr>
  <tr>
    <td>Nom de contacte </td>
    <td><input type="text" name="nom_contacte" value="<?php if(isset($_POST["nom_contacte"])){echo $_POST["nom_contacte"];}?>"  size="30" /></td>
  </tr>
  <tr>
    <td>N&uacute;m. esportistes</td>
    <td><input name="num" type="text" id="num" size="2" maxlength="2" value="<?php if(isset($_POST["num"])){echo $_POST["num"];}?>"   /></td>
  </tr>
</table>
<br />
    <input name="Seguent" type="submit" class="boto_verd" id="Seguent" onclick="return validar_pas1();" value="seguent" />




<?php if((isset($_POST["id_entidad"]))&&($_POST["id_entidad"]!="")&&($_POST["num"]!="")){?>

    <table border="0" cellpadding="5" cellspacing="5" class="taula" width="100%">
    <tr>
    <th><?php if($filaI["sol_DNI"]==1){ echo "DNI";}else{ echo "CatSalut";}?></th>
    <th>Nom</th>
    <th>Cognom1</th>
    <th>Cognom2</th>
    <th>Genere</th>
    <th>Any de naixament</th>
    <th>Categoria</th>
    <?php if($filaI["sol_federacio"]!=0){?><th>Llicència de la federació</th><?php }?>
    <?php if($filaI["asseguranca"]!=0){?><th>Vols assegurança? </th><?php }?>
    </tr>
    
    <?php for($i=1;$i<=$_POST["num"];$i++){?>
    <tr>	
		<td><?php if($filaI["sol_DNI"]==1){?>
        	<?php $var="DNI_".$i;?>
        	<input  type="text" name="<?php echo  $var;?>" size="9" maxlength="9" 
            value="<?php if(isset($_POST["$var"])){ echo $_POST["$var"];};?>" />
        <?php }else{?>
	        <?php $var="NSS1_".$i;?>
        	<input type="text" name="<?php echo  $var;?>" size="4" maxlength="4" 
             value="<?php if(isset($_POST["$var"])){ echo $_POST["$var"];};?>" />
            <?php $var="NSS2_".$i;?>
            <input type="text" name="<?php echo  $var;?>" size="1" maxlength="1" 
             value="<?php if(isset($_POST["$var"])){ echo $_POST["$var"];};?>" />
            <?php $var="NSS3_".$i;?>
            <input type="text" name="<?php echo  $var;?>" size="6" maxlength="6" 
             value="<?php if(isset($_POST["$var"])){ echo $_POST["$var"];};?>" />
            <?php $var="NSS4_".$i;?>
            <input type="text" name="<?php echo  $var;?>" size="2" maxlength="2" 
             value="<?php if(isset($_POST["$var"])){ echo $_POST["$var"];};?>" />
            <?php $var="NSS5_".$i;?>
            <input type="text" name="<?php echo  $var;?>" size="1" maxlength="1" 
             value="<?php if(isset($_POST["$var"])){ echo $_POST["$var"];};?>" />
        <?php }?>
        </td>
		<td><?php $var="nom_".$i;?>
        <input type="text" name="<?php echo  $var;?>" id="<?php echo  $var;?>" size="8" 
        value="<?php if(isset($_POST["$var"])){ echo $_POST["$var"];};?>" /></td>
		<td><?php $var="cognom1_".$i;?>
        <input type="text" name="<?php echo  $var;?>" id="<?php echo  $var;?>" size="8" 
        value="<?php if(isset($_POST["$var"])){ echo $_POST["$var"];};?>" /></td>
		<td><?php $var="cognom2_".$i;?>
        <input type="text" name="<?php echo  $var;?>" id="<?php echo  $var;?>" size="8" 
        value="<?php if(isset($_POST["$var"])){ echo $_POST["$var"];};?>" /></td>
        
        <td><?php $var="id_genere_".$i;?>
        <select name="<?php echo  $var;?>" id="<?php echo  $var;?>">
        	<option value=""></option>
        	<?php $con2=mysqli_query($cnx_cesportiu,"select * from Zgeneres where id_genere!=3");
			while($fila2=mysqli_fetch_array($con2)){?>
	        	<option value="<?php echo  $fila2["id_genere"];?>" 
                <?php  if((isset($_POST["$var"]))&&($_POST["$var"]==$fila2["id_genere"])){?> selected="selected"<?php }?>
                ><?php echo  $fila2["nom_genere"];?></option>
            <?php }?>
        </select></td>
        
		<td><?php $var="any_".$i;?>
        <select name="<?php echo  $var;?>" id="<?php echo  $var;?>">
        	<option value=""></option>
            <?php $con1=mysqli_query($cnx_cesportiu,"select * from i2_cat where id_ins=".$id_ins." order by any_inici");
            while($fila1=mysqli_fetch_array($con1)){
                for($a=$fila1["any_inici"];$a<=$fila1["any_fi"];$a++){?>
                    <option value="<?php echo  $a;?>"
                    <?php  if((isset($_POST["$var"]))&&($_POST["$var"]==$a)){?> selected="selected"<?php }?>
                     onclick="veure_cat('cat_<?php echo  $i;?>','<?php echo  $fila1["nom_cat"];?>'); <?php 
					 if($a<=$filaI["sol_federacio"]){?>visual_federacio('llicencia_federativa_<?php echo  $i;?>');
                     <?php }else{?>amaga_federacio('llicencia_federativa_<?php echo  $i;?>');
                     <?php }?>"
                    ><?php echo  $a;?></option>
                <?php }
            }?>
            </select>
        </td>
		<td><?php $var="cat_".$i;?>
          <input type="text" name="<?php echo  $var;?>" id="<?php echo  $var;?>" 
          value="<?php if(isset($_POST["$var"])){ echo $_POST["$var"];};?>" size="7" readonly="readonly" />
        </td>
		<?php if($filaI["sol_federacio"]!=0){?>
        	<?php $var="llicencia_federativa_".$i;?>
			<td><input type="text" name="<?php echo  $var;?>" size="10"
    	    id="<?php echo  $var;?>" value="<?php if(isset($_POST["$var"])){ echo $_POST["$var"];};?>" /></td>
		<?php }?>
        <?php if($filaI["asseguranca"]!=0){?>
        	<?php $var="asseguranca_".$i;?>
        	<td><input type="checkbox" name="<?php echo  $var;?>" id="<?php echo  $var;?>" value="1" 
            <?php if( (isset($_POST["$var"])) && ($_POST["$var"]==1) ){ echo "checked=\"checked\"";};?> />Si</td>
        <?php }?>
		
		</tr>

	<?php }?>
    
    
    </table>

<input type="submit" name="guardar" value="Guardar" 
		onClick="return validar_pas2('<?php echo  $filaI["sol_DNI"];?>','<?php echo  $filaI["sol_federacio"];?>');" class="boto_guardar" />    
<?php }?>

</form>
<!-- InstanceEndEditable -->

</div>

<div id="menu_esquerra" >

<fieldset class="fiel_menu_esk"><legend>Properes activitats</legend>

<?php mostrar_act1("portada=1 and data>=".date('Ymd'),'',0);?>

</fieldset>

<fieldset class="fiel_menu_esk"><legend>Activitats finalitzades</legend>
<?php mostrar_act1("portada=1 and data!=0 and data<".date('Ymd'),'',0);?>
</fieldset>




<?php $con=mysqli_query($cnx_cesportiu,"select img_presentacio,id_act,n_nom_act 
from a1_activitats where img_presentacio!='' and id_act IN (select id_act from a2_jornades where data>=".date('Ymd').")");
while($fila=mysqli_fetch_array($con)){?>
	<div class="neteja">
	<a href="activitats.php?nom_activitat=<?php echo $fila["n_nom_act"]?>&id_act=<?php echo $fila["id_act"];?>"  class="banners">
    <img src="carregues/act_presentacio/<?php echo $fila["img_presentacio"]?>" border="0" />
    </a></div>
<?php }?>



<div class="neteja">



<?php $con=mysqli_query($cnx_cesportiu,"select id_deporte,nom_var,id_deporte,id_lliga from ass_1config,ass_2lliga,Zvariables where 
ass_2lliga.portada=1 and 
ass_1config.id_config=ass_2lliga.id_config and 
ass_2lliga.id_nom=Zvariables.id_var");
while($fila=mysqli_fetch_array($con)){?>
	<a href="associacio.php?id_lliga=<?php echo $fila["id_lliga"];?>" style="background-image:url(imatges/banners_associacio/banner_<?php echo $fila["id_deporte"];?>.jpg);" class="banners">
    
    <div class="banner_lliga" ><?php echo StripSlashes($fila["nom_var"]);?></div>
    <?php $con1=mysqli_query($cnx_cesportiu,"select nom_cat,nom_genere from ass_4grup1,Zgeneres,Zcategories where 
	ass_4grup1.id_lliga=".$fila["id_lliga"]." and 
	ass_4grup1.id_cat=Zcategories.id_cat and 
	ass_4grup1.id_genere=Zgeneres.id_genere");
	while($fila1=mysqli_fetch_array($con1)){?>
		<div class="banner_cats" >
		<?php echo $fila1["nom_cat"]." ".$fila1["nom_genere"];?></div>
	<?php }?>
    
    </a>
<?php }?>


<a href="parcs_salut.php" class="banners">
<img src="imatges/banner_parcs.jpg" width="260" height="115" border="0" alt="Parcs urbans de Salut" /></a>
</div>

<div class="esk neteja" style="margin-top:10px;  width:260px">
<a href="https://www.facebook.com/selvaesports" target="_blank" class="picto_xarxes" ><img src="imatges/ico_facebook.gif"
alt="Facebook" width="54" height="54"  border="0" /></a>
<a href="https://twitter.com/selvaesports" target="_blank" class="picto_xarxes"><img src="imatges/ico_twitter.gif" 
alt="Twitter" width="54" height="54"  border="0"   /></a>
<a href="http://instagram.com/selvaesports#" target="_blank" class="picto_xarxes"><img src="imatges/ico_Instagram.gif" 
alt="instagram" width="54" height="54"  border="0" /></a>
<a href="http://www.youtube.com/user/selvaesports" target="_blank" class="esk" ><img src="imatges/ico_youtube.gif" 
alt="You Tube" width="54" height="54"  border="0" /></a>
</div>

</div>

<div class="neteja"></div>

<div id="menu_fixe">
    <a  href="jeec.php" class="link_menu_fixe">Jocs Esportius Escolars de Catalunya per a la temporada <?php 
	$con=mysqli_query($cnx_cesportiu,"select max(any1) from Zcurs");
	$fila=mysqli_fetch_array($con);
	echo $fila["max(any1)"]."/".($fila["max(any1)"]+1);
	
	?></a>

    <a  href="assegurances.php" class="link_menu_fixe">Assegurances esportives</a>
   <a  href="valors_accio.php" class="link_menu_fixe">Valors en acció</a>
   
    <a  href="valors_accio.php" class="link_menu_fixe">Valors en acció</a>
    <a  href="ceselva.php" class="link_menu_fixe">Consell esportiu de la Selva</a>

</div>


<div id="footer">
Passeig de Sant Salvador, 25-27 17430 Santa Coloma de Farners · 972 84 21 61 · 972 84 08 04 · info@selvaesports.cat |
<a id="totop-scroller" href="#page"></a>
</div>

</div>
</body>
<!-- InstanceEnd --></html>
