<?php include("conexio.php");
include("inc.php");
$titol="Assegurances esportives pel curs 2019/2020 (Pòlissa 033710668)";

include "plantilles/sobre.php";


echo "<p style='float:right;'><strong><a href=\"imatges/Resum_AssAccidentsEsportius_CESelva_Allianz_2019_20.pdf\" class=\"descarrega\" target=\"_blank\">Descarregar pdf</a></strong></p>
    <table>

        <tr><td colspan=\"3\" style=\"font-weight: bold;\">
        <h4>
                Grup  1 <br>
                <strong>Esportistes menors de 18 anys |  Participants als JEEC</strong><br />
                        Preu:  10 €</h4>
            </td></tr>
        <tr>
            <td rowspan='5'>&nbsp;&nbsp;</td>
            <td >Mort  per accident</td>
            <td style='width: 300px'>18.000  €</td>
        </tr>
        <tr>
            <td>Incapacitat  permanent</td>
            <td>37.500  €</td>
        </tr>
        <tr>
            <td>Assistència  sanitària</td>
            <td>Il·limitada</td>
        </tr>
        <tr>
            <td>Material  de pròtesis/ortodòncia/ulleres</td>
            <td>1.200  €</td>
        </tr>
        <tr>
            <td>Salvament</td>
            <td>1.200  €</td>
        </tr>

        
        <tr><td colspan=\"3\">&nbsp;</td></tr>


        <tr><td colspan=\"3\" style=\"font-weight: bold;\">
                Grup  2<br />
                <strong>Esportistes majors de 18 anys |  Practiquen esport en equips de lleure, competició puntual amateur</strong><br />
                        Preu:  25 €  **En aquest grup estan <strong>exclosos</strong>: futbol, bàsquet i voleibol
        </td></tr>

        <tr>
            <td rowspan='5'></td>
            <td>Mort  per accident</td>
            <td>6.015  €</td>
        </tr>
        <tr>
            <td>Incapacitat  permanent</td>
            <td>12.025  €</td>
        </tr>
        <tr>
            <td>Assistència  sanitària </td>
            <td>Il·limitada (en centres concertats)</td>
        </tr>
        <tr>
            <td>Material  de pròtesis/ortodòncia/ulleres </td>
            <td>1.200  €</td>
        </tr>
        <tr>
            <td>Salvament</td>
            <td>1.200  €</td>
        </tr>
        
        
        <tr><td colspan=\"3\">&nbsp;</td></tr>



        <tr>
            <td colspan=\"3\" style=\"font-weight: bold;\">
                Grup  3<br>
                <strong>Esportistes menors de 18 anys | Practiquen  esport però no competeixen – Escoles Esportives Municipals</strong><br />
                Preu:  5 €

            </td>
        </tr>
        <tr>
            <td rowspan='5'></td>
            <td>Mort  per accident </td>
            <td>6.015  €</td>
        </tr>
        <tr>
            <td>Incapacitat  permanent</td>
            <td>12.025  €</td>
        </tr>
        <tr>
            <td>Assistència  sanitària</td>
            <td>Il·limitada (en centres concertats)</td>
        </tr>
        <tr>
            <td>Material  de pròtesis/ortodòncia/ulleres</td>
            <td>1.200  €</td>
        </tr>
        <tr>
            <td>Salvament</td>
            <td>1.200  €</td>
        </tr>
        
        <tr><td colspan=\"3\">&nbsp;</td></tr>


        <tr>
            <td colspan=\"3\" style=\"font-weight: bold;\">
                Grup  4<br />
                        <strong>Esportistes majors de 18 anys | Activitat  de baixa intensitat, no competeixen – Activitat de Manteniment, pilates, etc...</strong><br />
                        Preu:  9 €

            </td>
        </tr>

        <tr>
            <td rowspan='5'></td>
            <td>Mort  per accident</td>
            <td>6.015  €</td>
        </tr>
        <tr>
            <td>Incapacitat  permanent </td>
            <td>12.025  €</td>
        </tr>
        <tr>
            <td>Assistència  sanitària</td>
            <td>Il·limitada (en centres concertats)</td>
        </tr>
        <tr>
            <td>Material  de pròtesis/ortodòncia/ulleres</td>
            <td>1.200  €</td>
        </tr>
        <tr>
            <td>Salvament </td>
            <td>1.200  €</td>
        </tr>
        
        <tr><td colspan=\"3\">&nbsp;</td></tr>


        <tr>
            <td colspan=\"3\" style=\"font-weight: bold;\">
                Grup  5<br />
                <strong>Esportistes majors de 18 anys | Participants  en proves de màxima durada 24 hores | Sense risc – 2€ | Curses de Muntanya i  proves a la natura 3€</strong></li>
                    <br>Preu:  2 € | 3 €
            </td>
        </tr>

        <tr>
            <td rowspan='5'></td>
            <td>Mort  per accident</td>
            <td>6.015  €</td>
        </tr>
        <tr>
            <td>Incapacitat  permanent</td>
            <td>12.025  €</td>
        </tr>
        <tr>
            <td>Assistència  sanitària</td>
            <td>Il·limitada (en centres concertats)</td>
        </tr>
        <tr>
            <td>Material  de pròtesis/ortodòncia/ulleres</td>
            <td>1.200  €</td>
        </tr>
        <tr>
            <td>Salvament</td>
            <td>1.200  €</td>
        </tr>
        
        
        
         <tr>
            <td colspan=\"3\" style=\"font-weight: bold;\">
                Grup  6<br />
                <strong>Esportistes menors de 18 anys | Participants  en campus o casals esportius segons el que disposa el decret 267/2016</strong></li>
                    <br>Preu: 5€
            </td>
        </tr>

        <tr>
            <td rowspan='5'></td>
            <td>Mort  per accident</td>
            <td>6.010  €</td>
        </tr>
        <tr>
            <td>Incapacitat  permanent</td>
            <td>6.010  €</td>
        </tr>
        <tr>
            <td>Assistència  sanitària</td>
            <td>Il·limitada (en centres concertats)</td>
        </tr>
        <tr>
            <td>Material  de pròtesis/ortodòncia/ulleres</td>
            <td>1.200  €</td>
        </tr>
        <tr>
            <td>Salvament</td>
            <td>1.200  €</td>
        </tr>

    </table>


    <p>&nbsp;</p>
    <p>** Objecte de l&rsquo;assegurança: Prestació  d&rsquo;assistència sanitària en cas de lesió corporal que derivi d&rsquo;una causa  violenta, sobtada, externa i aliena a la intencionalitat de l&rsquo;assegurat,  sobrevinguda pel fet de l&rsquo;exercici de l&rsquo;activitat esportiva per la que  l&rsquo;assegurat es trobi cobert.</p>
    <p>** En tots els casos no es contempla la cobertura  in itinere. Es a dir, en el desplaçament cap a l&rsquo;activitat o tornant de la mateixa.</p>
    <p>** Queden exclosos els esports de risc, a motor i  l&rsquo;esquí/surf.</p>
    <p>** Cal acreditar la condició d&rsquo;assegurat mitjançant  la presentació del <strong>comunicat d&rsquo;accidents</strong>.  Això no serà necessari en cas d&rsquo;assistència urgent, llavors s&rsquo;aportarà el  comunicat a posteriori, però en tot cas cal que aquest estigui <strong>segellat pel Consell Esportiu de la Selva</strong> per que s&rsquo;autoritzin els tractaments necessaris.</p>
    <p>** Al comunicat d&rsquo;accidents on diu: &ldquo;Datos del  accidente&rdquo; cal posar la màxima informació possible; quin esport s&rsquo;estava  practicant, si ha succeït durant un entrenament o partit, moment de la lesió i  si ha estat per una caiguda o per un cop/xoc.</p>
    <p>** La cobertura va de 01/10/2019 a 01/10/2020</p>
    <p>** Excepte per al grup 6 - Casals i Campus d’Estiu on la cobertura es del 25/06/20 al 31/08/20</p>

    <h2>Protocol d'actuació en cas d'accident</h2>

    <h4>1: Comunicació de l&rsquo;accident</h4>
    <ul>
        <li>Es trucarà immediatament després de l&rsquo;accident al centre  d&rsquo;atenció permanent 24 hores d&rsquo;Allianz al <strong>902.102.687</strong></li>
    </ul>
    <ul>
        <li>Realització del qüestionari telefònic</li>
    </ul>
    <ul>
        <ul>
            <li>Identificació i comprovació de cobertura</li>
            <li>Com s&rsquo;ha produït l&rsquo;accident</li>
            <li>Comunicació del número d&rsquo;expedient</li>
            <li>Derivació al centre mèdic corresponent</li>
        </ul>
    </ul>
    <ul>
        <li><strong>En les 48 hores posteriors  a l&rsquo;accident cal fer arribar el comunicat d&rsquo;accident amb les dades de  l&rsquo;esportista i el número d&rsquo;expedient facilitat per Allianz al Consell Esportiu  de la Selva.</strong></li>
    </ul>
    <ul>
        <li>Per sol·licitar proves complementàries caldrà trucar de  nou al 902.102.687 on es facilitarà la corresponent autorització. El centre pot  ser diferent del que va atendre en primera instància al lesionat.</li>
    </ul>

    <h4>2: Urgència Vital</h4>
    <p>En aquest casos es podrà assistir al centre mèdic  més proper. Un cop rebuda aquesta primera assistència caldrà trucar al  902.102.687 per tal que quan sigui possible i adient el lesionat sigui  traslladat a un centre  concertat.</p>
    <h4>3:Autoritzacions</h4>
    <p>Per a tractaments que requereixen informació  addicional:</p>
    <ul>
        <li>Intervencions quirúrgiques</li>
        <li>Proves de diagnòstic: RMN, TAC, Ecografies, artroscòpies,  ...</li>
        <li>Rehabilitació</li>
    </ul>

    <p>Cal que Allianz disposi del comunicat d&rsquo;accidents  segellat pel Consell Esportiu de la Selva, l&rsquo;informe metge corresponent i prescripció  de la prova específica a realitzar pel metge especialista.</p>
    <p>Els tractament realitzats, no autoritzats, aniran a  càrrec del lesionat.</p>
    <h4>4: Incidències</h4>
    <p>Cal adreçar-se al Consell Esportiu de la Selva al  972 843 522 o bé a <a href=\"mailto:infos@selvaesports.cat\">info@selvaesports.cat</a>                <br />
    </p>";

include "plantilles/sota.php";