<html>
<head>
    <?php ini_set('default_charset','utf-8');?>
    <link rel="icon" type="image/gif" href="favicon.gif" />
    <title><?php echo  $titol;?> - Consell Esportiu de la Selva</title>
    <script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
        try {
            var pageTracker = _gat._getTracker("UA-2354782-1");
            pageTracker._trackPageview();
        } catch(err) {}</script>


    <link href="plantilles/estils_site.css" rel="stylesheet" type="text/css" />
    <?php if(preg_match('/MSIE/i',$_SERVER['HTTP_USER_AGENT']))
    {?>
        <link href="plantilles/estils_site_explorer.css" rel="stylesheet" type="text/css" />
    <?php }?>
    <link href="gral/estils.css?v=1" rel="stylesheet" type="text/css" />
    <script language="javascript" src="scripts.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>


<div id="cont_body">


    <div id="toolbar"><a href="ceselva.php" class="menu_dalt">La nostra oficina</a><span style="float:right; color:#FFFFFF; padding-top:5px; padding-left:5px;">|</span>
        <a href="entitats/index.php" class="menu_dalt" target="_blank">Intranet de les entitats</a><a href="index.php"><img src="imatges/logoConsellEsportiuSelva_HoritzontalDegradat.png" alt="Consell Esportiu de la Selva" border="0" /></a>
    </div>

    <div id="menu1" style="margin-bottom:25px;">
        <ul>
            <?php
            $enllac=array(1=>"index",2=>"index",3=>"parcs_salut",4=>"entitats",5=>"instalacions");

            $nom_enllac=array(1=>"Esports",2=>"Municipis",3=>"Parcs urbans de salut",4=>"Entitats",5=>"Instal·lacions esportives");

            for($i=1;$i<=count($enllac);$i++){?>
                <li class="nivel1" style="width:<?php echo (100/count($enllac));?>%"><a  href="<?php echo $enllac[$i];?>.php" class="nivel1
		<?php if(strpos($_SERVER['REQUEST_URI'],$enllac[$i])!=false){ echo "actiu";}else{ echo "no_actiu";}?>"><?php echo $nom_enllac[$i];?></a>
                    <?php
                    if($i==1){?>
                        <ul>
                            <?php $con=mysqli_query($cnx_cesportiu,"select * from Zdeportes order by nom_deporte");
                            while($fila=mysqli_fetch_array($con)){?>
                                <li><a href="esport.php?nom=<?php echo $fila["n_nom_deporte"]?>" ><img src="imatges/deportes/<?php echo $fila["id_deporte"]?>.gif" width="20" hspace="4" border="0" /><?php echo $fila["nom_deporte"];?></a></li>
                            <?php }?>
                        </ul>
                    <?php }elseif($i==2){?>
                        <ul>
                            <?php $con=mysqli_query($cnx_cesportiu,"select * from Zmunicipis order by nom_muni");
                            while($fila=mysqli_fetch_array($con)){?>
                                <li style="width:230px;"><a href="municipi.php?nom=<?php echo $fila["normalitza_muni"]?>" ><?php echo $fila["nom_muni"];?></a></li>
                            <?php }?>
                        </ul>
                    <?php }elseif($i==4){?>
                        <ul>
                            <li style="width:230px;"><a href="entitats.php" >Les <?php $con=mysqli_query($cnx_cesportiu,"select count(*) from entidades where estat=1");
                                    $fila=mysqli_fetch_array($con); echo $fila["count(*)"];?> entitats de la comarca</a></li>
                            <li style="width:230px;"><a href="alta_entitat.php" >Alta nova entitat</a></li>
                            <li style="width:230px;"><a href="entitats/" target="_blank" >Intranet de les entitats</a></li>
                        </ul>
                    <?php }?>

                </li>
            <?php }?>
        </ul>
    </div>

    <div id="menu_dreta">
        <a href="index.php" class="anterior">Inici</a><?php if(isset($anterior)){echo $anterior;}?>
        <span class="ante_titol"><?php echo  $titol?></span>

        <h1><?php echo  $titol?></h1>