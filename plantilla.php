<?php
/*S'entra des d'una pag web amb
$titol
$contingut
connexio.php
inc.php

*/
?>
<html>
<head>
    <?php ini_set('default_charset','utf-8');?>
    <link rel="icon" type="image/gif" href="favicon.gif" />
    <title><?php echo  $titol;?> - Consell Esportiu de la Selva</title>
    <script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
        try {
            var pageTracker = _gat._getTracker("UA-2354782-1");
            pageTracker._trackPageview();
        } catch(err) {}</script>


    <link href="plantilles/estils_site.css" rel="stylesheet" type="text/css" />
    <?php if(preg_match('/MSIE/i',$_SERVER['HTTP_USER_AGENT']))
    {?>
        <link href="plantilles/estils_site_explorer.css" rel="stylesheet" type="text/css" />
    <?php }?>
    <link href="gral/estils.css?v=1" rel="stylesheet" type="text/css" />
    <script language="javascript" src="scripts.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>


<div id="cont_body">


    <div id="toolbar"><a href="ceselva.php" class="menu_dalt">La nostra oficina</a><span style="float:right; color:#FFFFFF; padding-top:5px; padding-left:5px;">|</span>
        <a href="entitats/index.php" class="menu_dalt" target="_blank">Intranet de les entitats</a><a href="index.php"><img src="imatges/logoConsellEsportiuSelva_HoritzontalDegradat.png" alt="Consell Esportiu de la Selva" border="0" /></a>
    </div>

    <div id="menu1" style="margin-bottom:25px;">
        <ul>
            <?php
            $enllac=array(1=>"index",2=>"index",3=>"parcs_salut",4=>"entitats",5=>"instalacions");

            $nom_enllac=array(1=>"Esports",2=>"Municipis",3=>"Parcs urbans de salut",4=>"Entitats",5=>"Instal·lacions esportives");

            for($i=1;$i<=count($enllac);$i++){?>
                <li class="nivel1" style="width:<?php echo (100/count($enllac));?>%"><a  href="<?php echo $enllac[$i];?>.php" class="nivel1
		<?php if(strpos($_SERVER['REQUEST_URI'],$enllac[$i])!=false){ echo "actiu";}else{ echo "no_actiu";}?>"><?php echo $nom_enllac[$i];?></a>
                    <?php
                    if($i==1){?>
                        <ul>
                            <?php $con=mysqli_query($cnx_cesportiu,"select * from Zdeportes order by nom_deporte");
                            while($fila=mysqli_fetch_array($con)){?>
                                <li><a href="esport.php?nom=<?php echo $fila["n_nom_deporte"]?>" ><img src="imatges/deportes/<?php echo $fila["id_deporte"]?>.gif" width="20" hspace="4" border="0" /><?php echo $fila["nom_deporte"];?></a></li>
                            <?php }?>
                        </ul>
                    <?php }elseif($i==2){?>
                        <ul>
                            <?php $con=mysqli_query($cnx_cesportiu,"select * from Zmunicipis order by nom_muni");
                            while($fila=mysqli_fetch_array($con)){?>
                                <li style="width:230px;"><a href="municipi.php?nom=<?php echo $fila["normalitza_muni"]?>" ><?php echo $fila["nom_muni"];?></a></li>
                            <?php }?>
                        </ul>
                    <?php }elseif($i==4){?>
                        <ul>
                            <li style="width:230px;"><a href="entitats.php" >Les <?php $con=mysqli_query($cnx_cesportiu,"select count(*) from entidades where estat=1");
                                    $fila=mysqli_fetch_array($con); echo $fila["count(*)"];?> entitats de la comarca</a></li>
                            <li style="width:230px;"><a href="alta_entitat.php" >Alta nova entitat</a></li>
                            <li style="width:230px;"><a href="entitats/" target="_blank" >Intranet de les entitats</a></li>
                        </ul>
                    <?php }?>

                </li>
            <?php }?>
        </ul>
    </div>



    <div id="menu_dreta">
        <a href="index.php" class="anterior">Inici</a>
        <span class="ante_titol"><?php echo  $titol?></span>

        <h1><?php echo  $titol?></h1>


        <?php echo $contingut;?>
    </div>

    <div id="menu_esquerra" >

        <fieldset class="fiel_menu_esk"><legend>Properes activitats</legend>

            <?php mostrar_act1("portada=1 and data>=".date('Ymd'),'',0);?>

        </fieldset>

        <fieldset class="fiel_menu_esk"><legend>Activitats finalitzades</legend>
            <?php mostrar_act1("portada=1 and data!=0 and data<".date('Ymd'),'',0);?>
        </fieldset>




        <?php $con=mysqli_query($cnx_cesportiu,"select img_presentacio,id_act,n_nom_act 
from a1_activitats where img_presentacio!='' and id_act IN (select id_act from a2_jornades where data>=".date('Ymd').")");
        while($fila=mysqli_fetch_array($con)){?>
            <div class="neteja">
                <a href="activitats.php?nom_activitat=<?php echo $fila["n_nom_act"]?>&id_act=<?php echo $fila["id_act"];?>"  class="banners">
                    <img src="carregues/act_presentacio/<?php echo $fila["img_presentacio"]?>" border="0" />
                </a></div>
        <?php }?>



        <div class="neteja">



            <?php $con=mysqli_query($cnx_cesportiu,"select id_deporte,nom_var,id_deporte,id_lliga from ass_1config,ass_2lliga,Zvariables where 
ass_2lliga.portada=1 and 
ass_1config.id_config=ass_2lliga.id_config and 
ass_2lliga.id_nom=Zvariables.id_var");
            while($fila=mysqli_fetch_array($con)){?>
                <a href="associacio.php?id_lliga=<?php echo $fila["id_lliga"];?>" style="background-image:url(imatges/banners_associacio/banner_<?php echo $fila["id_deporte"];?>.jpg);" class="banners">

                    <div class="banner_lliga" ><?php echo StripSlashes($fila["nom_var"]);?></div>
                    <?php $con1=mysqli_query($cnx_cesportiu,"select nom_cat,nom_genere from ass_4grup1,Zgeneres,Zcategories where 
	ass_4grup1.id_lliga=".$fila["id_lliga"]." and 
	ass_4grup1.id_cat=Zcategories.id_cat and 
	ass_4grup1.id_genere=Zgeneres.id_genere");
                    while($fila1=mysqli_fetch_array($con1)){?>
                        <div class="banner_cats" >
                            <?php echo $fila1["nom_cat"]." ".$fila1["nom_genere"];?></div>
                    <?php }?>

                </a>
            <?php }?>


            <a href="parcs_salut.php" class="banners">
                <img src="imatges/banner_parcs.jpg" width="260" height="115" border="0" alt="Parcs urbans de Salut" /></a>
        </div>

        <div class="esk neteja" style="margin-top:10px;  width:260px">
            <a href="https://www.facebook.com/selvaesports" target="_blank" class="picto_xarxes" ><img src="imatges/ico_facebook.gif"
                                                                                                       alt="Facebook" width="54" height="54"  border="0" /></a>
            <a href="https://twitter.com/selvaesports" target="_blank" class="picto_xarxes"><img src="imatges/ico_twitter.gif"
                                                                                                 alt="Twitter" width="54" height="54"  border="0"   /></a>
            <a href="http://instagram.com/selvaesports#" target="_blank" class="picto_xarxes"><img src="imatges/ico_Instagram.gif"
                                                                                                   alt="instagram" width="54" height="54"  border="0" /></a>
            <a href="http://www.youtube.com/user/selvaesports" target="_blank" class="esk" ><img src="imatges/ico_youtube.gif"
                                                                                                 alt="You Tube" width="54" height="54"  border="0" /></a>
        </div>

    </div>



    <div class="neteja"></div>

    <div id="menu_fixe">
        <a  href="jeec.php" class="link_menu_fixe">Jocs Esportius Escolars de Catalunya per a la temporada <?php
            $con=mysqli_query($cnx_cesportiu,"select max(any1) from Zcurs");
            $fila=mysqli_fetch_array($con);
            echo $fila["max(any1)"]."/".($fila["max(any1)"]+1);

            ?></a>

        <a  href="assegurances.php" class="link_menu_fixe">Assegurances esportives</a>
        <a  href="valors_accio.php" class="link_menu_fixe">Valors en acció</a>
        <a  href="ceselva.php" class="link_menu_fixe">Consell esportiu de la Selva</a>

    </div>


    <div id="footer">
        Passeig de Sant Salvador, 25-27 17430 Santa Coloma de Farners · 972 84 21 61 · 972 84 08 04 · info@selvaesports.cat |
        <a id="totop-scroller" href="#page"></a>
    </div>

</div>
</body>
