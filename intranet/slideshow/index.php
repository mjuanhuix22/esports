<h2>Slideshow</h2>

<p><strong>Cal penjar imatges jpg de 720*305px</strong></p>
<?php if( (isset($_POST["modificar"])) || (isset($_POST["guardar"])) ){
	
	$SQL="ordre=".$_POST["ordre"].",
	titol='".AddSlashes($_POST["titol"])."',
	subtitol='".AddSlashes($_POST["subtitol"])."',
	enllac='".$_POST["enllac"]."',
	visible=".$_POST["visible"];
	
	
	if($_FILES["imatge"]["name"]!=""){
			$imagedetails = getimagesize($_FILES['imatge']['tmp_name']);
			$amplada = $imagedetails[0];
			$altura = $imagedetails[1];
			if(($amplada!=720)||($altura!=305)){
				msn_error("ERROR. Amplada ".$amplada."px i altura ".$altura."px");
			}else{
				
				if(isset($_POST["modificar"])){
					$con=mysqli_query($cnx_cesportiu,"select imatge from slideshow where id_slide=".$_POST["id_slide"]);
					$fila=mysqli_fetch_array($con);
					unlink("../carregues/slideshow/".$fila["imatge"]);
					unlink("../carregues/slideshow_mini/".$fila["imatge"]);
				}
				
				
				
				$ruta="../carregues/slideshow/".normalitza($_POST["titol"]);
				$arxiu="imatge";
				$obli_imatge=1;
				$amplada_nova=720;
				include("../carregar.php");

				
				if($penjat==1){
					
					//La mini
					$ruta="../carregues/slideshow_mini/".normalitza($_POST["titol"]).".".$extensio;
					copy ("../carregues/slideshow/".normalitza($_POST["titol"]).".".$extensio,$ruta);
					$amplada_nova=113;
					$altura_nova=48;
					
					if($extensio=="jpg"){
						$original=imagecreatefromjpeg($ruta);
					}else{
						$original=imagecreatefromgif($ruta);
					}	
					$nova=imagecreatetruecolor($amplada_nova,$altura_nova);
					imagecopyresampled($nova,$original,0,0,0,0,$amplada_nova,$altura_nova,$amplada_original,$altura_original);
					if($extensio=="jpg"){
						imagejpeg($nova,$ruta,100);
					}else{
						imagegif($nova,$ruta);
					}
					imagedestroy($nova);
					imagedestroy($original);

					
					
					$SQL.=",imatge='".normalitza($_POST["titol"]).".".$extensio."'";
					
				}
			
		}
		
	}
	if((isset($_POST["guardar"])) && ($penjat==1)){
		$ins=mysqli_query($cnx_cesportiu,"insert into slideshow set ".$SQL);
	
		msn($ins);
		
	}
	
	if(isset($_POST["modificar"])){
		$ins=mysqli_query($cnx_cesportiu,"update slideshow set ".$SQL." where id_slide=".$_POST["id_slide"]);	
		//echo "update slideshow set ".$SQL." where id_slide=".$_POST["id_slide"];
		msn($ins);
	}

}
if(isset($_POST["eliminar"])){
	$con=mysqli_query($cnx_cesportiu,"select * from slideshow where id_slide=".$_POST["id_slide"]);
	$fila=mysqli_fetch_array($con);
	unlink("../carregues/slideshow/".$fila["imatge"]);
	unlink("../carregues/slideshow_mini/".$fila["imatge"]);
	
	$ins=mysqli_query($cnx_cesportiu,"delete from slideshow where id_slide=".$_POST["id_slide"]);
	msn($ins);
}
?>



<?php $con=mysqli_query($cnx_cesportiu,"select * from slideshow order by visible DESC,ordre");
while($fila=mysqli_fetch_array($con)){?>

	<form method="post" enctype="multipart/form-data">
    <table>
	<tr>
    	<td>Ordre</td>
        <td><input type="text" name="ordre" size="2" value="<?php echo StripSlashes($fila["ordre"])?>"></td>
    </tr>
    
    <tr>
    	<td>Visible</td>
        <td>Si<input type="radio" name="visible" value="1" <?php if($fila["visible"]==1){?> checked<?php }?>>
        No<input type="radio" name="visible" value="0" <?php if($fila["visible"]==0){?> checked<?php }?>>
        </td>
    </tr>

	<tr>
    	<td>Titol</td>
        <td><input type="text" name="titol" value="<?php echo StripSlashes($fila["titol"])?>" size="70"></td>
    </tr>

	<tr>
    	<td>Subtitol</td>
        <td><input type="text" name="subtitol" value="<?php echo StripSlashes($fila["subtitol"])?>" size="80"></td>
    </tr>	
    
    <tr>
    	<td>Enlla&ccedil;</td>
        <td>http://www.selvaesports.cat/<input type="text" name="enllac" value="<?php echo StripSlashes($fila["enllac"])?>" size="100"></td>
    </tr>
    
    <tr>
    	<td>Imatge</td>
        <td><input type="file" name="imatge"></td>
    </tr>
    
	</table>    
    <img src="../carregues/slideshow/<?php echo $fila["imatge"]?>">
    <br>
	<input type="submit" name="modificar" value="Modificar" class="boto_guardar">
    <input type="submit" name="eliminar" value="Eliminar" class="boto_eliminar">
    <?php include("comuns2.php");?>
    <input type="hidden" name="id_slide" value="<?php echo $fila["id_slide"];?>">
   </form> 
   <hr>
<?php }?>


<form method="post" enctype="multipart/form-data">
    <table>
	<tr>
    	<td>Ordre</td>
        <td><input type="text" name="ordre" size="2" ></td>
    </tr>
    
    <tr>
    	<td>Visible</td>
        <td>Si<input type="radio" name="visible" value="1" >
        No<input type="radio" name="visible" value="0" >
        </td>
    </tr>

	<tr>
    	<td>Titol</td>
        <td><input type="text" name="titol" value="" size="70"></td>
    </tr>

	<tr>
    	<td>Subtitol</td>
        <td><input type="text" name="subtitol" value="" size="80"></td>
    </tr>	
    
    <tr>
    	<td>Enlla&ccedil;</td>
        <td>http://www.selvaesports.cat/<input type="text" name="enllac" size="100" ></td>
    </tr>
    
    <tr>
    	<td>Imatge</td>
        <td><input type="file" name="imatge"></td>
    </tr>
    
	</table>    
	<input type="submit" name="guardar" value="Guardar" class="boto_guardar">
    <?php include("comuns2.php");?>
   </form> 
