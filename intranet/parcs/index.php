
<h2>Els parcs urbans de salut</h2>

<form method="post">
 <select name="id_muni" class="menu_selec" onchange="submit();">
    <option value=""></option>
    <?php $con1=mysqli_query($cnx_cesportiu,"SELECT * FROM Zmunicipis order by nom_muni");
    while($fila1=mysqli_fetch_array($con1)){?>
        <option value="<?php echo $fila1["id_muni"];?>"
        <?php if((isset($_POST["id_muni"]))&&($_POST["id_muni"]==$fila1["id_muni"])){?> selected="selected"<?php }?>><?php echo StripSlashes($fila1["nom_muni"]);?></option>
    <?php }?>
  </select>

<?php include("comuns.php");?>
</form>


<?php 



if((isset($_POST["id_muni"]))&&($_POST["id_muni"]!="")){
	
	$con=mysqli_query($cnx_cesportiu,"SELECT * FROM Zmunicipis where id_muni=".$_POST["id_muni"]);
	$fila=mysqli_fetch_array($con);
	
	if(isset($_POST["guardar_muni"])){
		
		if($_FILES["foto_muni"]["name"]!=""){
			 $ruta="../carregues/parcs_urbans_muni/parc_".normalitza($fila["nom_muni"]).".jpg";
			 if(file_exists($ruta))	{
				unlink($ruta); 
			 }
			 $ruta="../carregues/parcs_urbans_muni/parc_".normalitza($fila["nom_muni"]);
			 $arxiu="foto_muni";$obli_imatge=1; $amplada_nova=147;	 
			 include("../carregar.php");
		}
		
		
		$SQL="enllac='".$_POST["enllac"]."'";
		
		
		if($_FILES["foto"]["name"]!=""){
			$con1=mysqli_query($cnx_cesportiu,"SELECT * FROM parcs_muni where id_muni=".$_POST["id_muni"]);
			$fila1=mysqli_fetch_array($con1);
			if($fila1["foto"]!=""){
				unlink("../carregues/itineraris_salut/".$fila1["foto"]);
			}		
		}
		$arxiu="foto";$obli_imatge=1; $amplada_nova=700;
		$nom="Xarxa_Itineraris_Saludables_".normalitza($fila["nom_muni"]);
		$ruta="../carregues/itineraris_salut/".$nom;
		include("../carregar.php");
		if($penjat==1){
			$SQL.=",foto='".$nom.".".$extensio."'";
		}
		
		$con1=mysqli_query($cnx_cesportiu,"SELECT * FROM parcs_muni where id_muni=".$_POST["id_muni"]);
		$fila1=mysqli_fetch_array($con1);
		if(mysqli_num_rows($con1)==0){
			$SQL="insert into parcs_muni set id_muni=".$_POST["id_muni"].",".$SQL."";
		}else{
			$SQL="update parcs_muni set ".$SQL." where id_muni=".$_POST["id_muni"];	
		}
		
		$accio=mysqli_query($cnx_cesportiu,$SQL);	
		msn($accio);
	}
	elseif((isset($_POST["guardar_parc"])) || (isset($_POST["modificar_parc"])) ){
		
		
		$SQL="nom_parc='".AddSlashes($_POST["nom_parc"])."',
		adreca='".AddSlashes($_POST["adreca"])."',
		color='".$_POST["color"]."'";
		
		
		//primer eliminem la vella foto
		if(isset($_POST["modificar_parc"])){
			if($_FILES["foto"]["name"]!=""){
				$con1=mysqli_query($cnx_cesportiu,"SELECT * FROM parcs where id_parc=".$_POST["id_parc"]);
				$fila1=mysqli_fetch_array($con1);
				if($fila1["foto"]!=""){
					unlink("../carregues/parcs_urbans/".$fila1["foto"]);
				}
			}
		}		
		$arxiu="foto";$obli_imatge=1; $amplada_nova=700;
		$nom="Parc-Urba-Salut_".normalitza($fila["nom_muni"])."_".normalitza($_POST["adreca"]);
		$ruta="../carregues/parcs_urbans/".$nom;
		include("../carregar.php");
		if($penjat==1){
			$SQL.=",foto='".$nom.".".$extensio."'";
		}
		
		if(isset($_POST["guardar_parc"])){
			$SQL="insert into parcs set id_muni=".$_POST["id_muni"].",".$SQL;	
		}else{
			$SQL="update parcs set ".$SQL." where id_parc=".$_POST["id_parc"];	
		}
		
		$accio=mysqli_query($cnx_cesportiu,$SQL);	
		msn($accio);
	}

		
	
	
	?>

    <fieldset id="formulari">
    <legend ><?php echo $fila["nom_muni"];?></legend>
    <form method="post" enctype="multipart/form-data">
   
   <?php  $con1=mysqli_query($cnx_cesportiu,"select * from parcs_muni where id_muni=".$_POST["id_muni"]);
    $fila1=mysqli_fetch_array($con1);?> 
    
    <label>URL</label>
    <input type="text" name="enllac" value="<?php echo $fila1["enllac"];?>" size="90" />
    
    <label>Mapa xarxa d'itineraris saludables</label>
    <?php  if($fila1["foto"]!=""){
        $ruta="../carregues/itineraris_salut/".$fila1["foto"];?>
        <a href="<?php echo $ruta?>" target="_blank"><img src="<?php echo $ruta;?>" width="100" class="esk"></a>
    <?php }?>
    <input type="file" name="foto" />
    
    
    <label>Foto de presentació del muni</label>
    <?php $ruta="../carregues/parcs_urbans_muni/parc_".normalitza($fila["nom_muni"]).".jpg";
	
	if(file_exists($ruta)){?>
    	<img src="<?php echo $ruta;?>"  class="esk">
    <?php }?>
    <input type="file" name="foto_muni" />
    
    <input type="submit" name="guardar_muni" value="Guardar" class="boto_guardar" />
     <input type="hidden" name="id_muni" value="<?php echo $_POST["id_muni"]?>" />
    <?php include("comuns.php");?>
    </form>
   
    
    
    <?php $con=mysqli_query($cnx_cesportiu,"select * from parcs where id_muni=".$_POST["id_muni"]);
    while($fila=mysqli_fetch_array($con)){?>
    
        <fieldset class="fiel">
        <legend>Parc urb&agrave; de salut</legend>
        <form method="post" enctype="multipart/form-data">
    
         <label>Nom parc</label>
        <input type="text" name="nom_parc" value="<?=StripSlashes($fila["nom_parc"]);?>" size="40">
        
        <label>Adre&ccedil;a</label>
        <input type="text" name="adreca" size="40" value="<?php echo StripSlashes($fila["adreca"]);?>" />
        
        <label>Color</label>
        <input name="color" type="text" value="<?=StripSlashes($fila["color"]);?>" size="6" class="parc_<?php echo $fila["id_parc"];?>">

		<label>Foto</label>
        <?php  if($fila["foto"]!=""){
			$ruta="../carregues/parcs_urbans/".$fila["foto"];?>
			<a href="<?php echo $ruta?>" target="_blank"><img src="<?php echo $ruta;?>" width="200" class="esk" ></a>
		<?php }?>
        <input type="file" name="foto" />
        
        <input type="submit" name="modificar_parc" value="Modificar" class="boto_guardar">
    
        <input type="hidden" name="id_parc" value="<?php echo $fila["id_parc"]?>" />
        <input type="hidden" name="id_muni" value="<?php echo $_POST["id_muni"]?>" />
        <?php include("comuns.php");?>
      </form>
      </fieldset>
    <?php }?>

    <fieldset class="fiel">
    <legend>Parc urb&agrave; de salut</legend>
    <form method="post" enctype="multipart/form-data">
    
    <label>Nom parc</label>
    <input type="text" name="nom_parc" size="40">
    
    <label>Adre&ccedil;a</label>
    <input type="text" name="adreca" size="40" />
    
    <label>Color</label>
    <input name="color" type="text" size="6" >
    
    <label>Foto</label>
    <input type="file" name="foto" />
 
    <input type="submit" name="guardar_parc" value="Guardar" class="boto_guardar">

    <input type="hidden" name="id_muni" value="<?php echo $_POST["id_muni"]?>" />
    <?php include("comuns.php");?>
    </form>
    </fieldset>


	 </fieldset>
<?php }?>
