<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Instalacion esportives municipals</title>


<link href="../../plantilles/estils_site.css" rel="stylesheet" type="text/css" />
<body>
<?php 
include("../../conexio.php");
$con=mysqli_query($cnx_cesportiu,"select * from instalacions order by nom_instalacio");
while($fila=mysqli_fetch_array($con)){
 ?>
    
<h1><?php echo StripSlashes($fila["nom_instalacio"]);?></h1>
    <table  border="0" align="center" cellpadding="5" cellspacing="2" >
        

        <tr >
          <td valign="top" class="lletraGrisa">Adre&ccedil;a</td>
          <td >
           <?php 
            if($fila["id_adreca"]!=0){
				$con1=mysqli_query($cnx_adreces,"select * from adreca,tipus where 
				adreca.id_adreca=".$fila["id_adreca"]);
				$fila1=mysqli_fetch_array($con1);
				echo $fila1["nom_tipus"];
				if($fila1["id_article"]!=0){
						$con2=mysqli_query($cnx_adreces,"select * from articles where id_article=".$fila1["id_article"]);
						$fila2=mysqli_fetch_array($con2);
						echo " ".$fila2["nom_article"];
				}
				
				echo " ".StripSlashes($fila1["nom_adreca"]);
            }else{ echo StripSlashes($fila["altre_adreca"]);}
        if($fila["numero"]!=0){echo ", ".$fila["numero"];}?>		 </td>
      </tr>
    
    
      <?php if($fila["telf"]!=0){?>
            
             <tr >
              <td valign="top" class="lletraGrisa">Tel&egrave;fon</td>
              <td ><?php echo substr($fila["telf"],0,3)." ".substr($fila["telf"],3,2)." ".substr($fila["telf"],5,2)." ".substr($fila["telf"],7,3);?></td>
      </tr>
        <?php }?>
    
      <?php 
	  if($fila["id_tipus"]==2){?>
      		<tr >
              <td valign="top" class="lletraGrisa">Pistes</td>
              <td>
                <table>
                    <tr>
                    <th>Nom</th>
                    <th>Dimensions</th>
                    <th>Paviment</th>
                    <th>Aforament</th>
                    </tr>
                
                    
			   <?php $con1=mysqli_query($cnx_cesportiu,"SELECT * FROM instalacions_pistes WHERE id_instalacio=".$fila["id_instalacio"]);
                while($fila1=mysqli_fetch_array($con1)){?>
                     <tr >
                     <td><?php echo StripSlashes($fila1["nom_pista"]);?></td>
                     <td><?php echo StripSlashes($fila1["mides"]);?></td>
                     <td><?php 
                     $con2=mysqli_query($cnx_cesportiu,"SELECT nom_paviment FROM Zinstal_paviment 
                     WHERE id_paviment=".$fila1["id_paviment"]);
                    $fila2=mysqli_fetch_array($con2);
                     echo StripSlashes($fila2["nom_paviment"]);?></td>
                     <td><?php echo StripSlashes($fila1["aforament"]);?> espectadors</td>
                     </tr>
                <?php }?>
                </table>
            </td>
            </tr>
		  
	  <?php }elseif($fila["id_tipus"]==3){?>
      
      		 <tr >
              <td valign="top" class="lletraGrisa">Vasos</td>
              <td>
              		<table cellpadding="1">
                    <tr>
                    <th>Tipus</th>
                    <th>Dimensions</th>
                    <th>Fond&agrave;ria</th>
                    </tr>
				 <?php $con1=mysqli_query($cnx_cesportiu,"SELECT * FROM instalacions_vasos WHERE id_instalacio=".$fila["id_instalacio"]);
                while($fila1=mysqli_fetch_array($con1)){?>
                    <tr>
                        <td>Piscina <?php if($fila1["cobert"]==1){ echo "coberta";}else{ echo "Descoberta";}?></td>
                        <td><?php echo $fila1["mides"];?></td>
                        <td><?php echo $fila1["fondaria"]?></td>
                   </tr>
                <?php }?>
            	</table>
            </td></tr>

	   <?php }else{?>
       		<tr>
            	<td class="lletraGrisa">Paviment</td>
                <td><?php  $con2=mysqli_query($cnx_cesportiu,"SELECT nom_paviment FROM Zinstal_paviment 
				 WHERE id_paviment=".$fila["id_paviment"]);
                $fila2=mysqli_fetch_array($con2);
				 echo StripSlashes($fila2["nom_paviment"]);?></td>
            </tr>
          <?php if($fila["aforament"]!=0){?>
                <tr >
                  <td valign="top" class="lletraGrisa">Aforament</td>
                  <td ><?php echo $fila["aforament"];?> espectadors</td>
                </tr>
            <?php }?>
            <?php if($fila["mides"]!=""){?>
                
                <tr >
                  <td valign="top" class="lletraGrisa">Mides</td>
                  <td ><?php echo StripSlashes($fila["mides"]);?></td>
          </tr>
            <?php }?>
      <?php }?>
      
        
    
        
        <?php if($fila["mapa"]!=""){?>
        	<tr>
            	<td class="lletraGrisa">Localitzaci&oacute;</td>
                <td><?php //echo $fila["mapa"];?></td>
            </tr>
        
        <?php }?>
    </table>
    
    <?php $con1=mysqli_query($cnx_cesportiu,"select * from instalacions_fotos where id_instalacio=".$fila["id_instalacio"]);
	while($fila1=mysqli_fetch_array($con1)){?>
    	<p align="center">
    	<img src="../../carregues/instalacions/<?php echo $fila1["nom_foto"];?>" />
        <?php if($fila1["peu"]!=""){ echo "<br>".StripSlashes($fila1["peu"]);}?>
    	</p>
    <?php }?>
    
    <hr>
 <?php }?>
</body>
</html>