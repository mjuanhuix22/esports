<?php
    $SQL="select * from entidades where id_entidad!=0 ";
    if(isset($_POST["estat"])){ $SQL.=" and estat=".$_POST["estat"];}
    if((isset($_POST["temail"]))&&($_POST["temail"]==1)){ $SQL.=" and email!=''";}
    if((isset($_POST["temail"]))&&($_POST["temail"]==0)){ $SQL.=" and email=''";}
    if(isset($_POST["intern"])){ $SQL.=" and intern=".$_POST["intern"];}

    //---------------Municipis
    $requisit="";
    $con=mysqli_query($cnx_cesportiu,"select * from Zmunicipis  ");
    while($fila=mysqli_fetch_array($con)){
        $var="id_muni_".$fila["id_muni"];
        if((isset($_POST["$var"]))&&($_POST["$var"]==1)){
            if($requisit!=""){ $requisit.=" or ";}
            $requisit.=" id_muni=".$fila["id_muni"];
        }
    }
    if($requisit!=""){
        $SQL.=" and (".$requisit.")";
    }


    //-------------Esports
    $requisit="";
    $con=mysqli_query($cnx_cesportiu,"select * from Zdeportes  ");
    while($fila=mysqli_fetch_array($con)){
        $var="deporte_".$fila["id_deporte"];
        if((isset($_POST["$var"]))&&($_POST["$var"]==1)){
            if($requisit!=""){ $requisit.=" or ";}
            $requisit.=" id_deporte =".$fila["id_deporte"];
        }
    }
    if($requisit!=""){
        $SQL.=" and id_entidad IN (select id_entidad from entidades_deportes where ".$requisit.")";
    }

    //------------------------llicencies
    $requisit="";
    $con=mysqli_query($cnx_cesportiu,"select * from Zcurs where any1>=2012");
    while($fila=mysqli_fetch_array($con)){
        $var="id_curs_".$fila["id_curs"];
        if((isset($_POST["$var"]))&&($_POST["$var"]==1)){
            if($requisit!=""){ $requisit.=" or ";}
            $requisit.=" id_curs =".$fila["id_curs"];
        }
    }
    if($requisit!=""){
        $SQL.=" and id_entidad IN (select id_entidad from llice_sol where ".$requisit.")";
    }


    //Inscripció
    $requisit="";
     $con=mysqli_query($cnx_cesportiu,"select * from i1_inscripcions,a1_activitats where
      i1_inscripcions.id_act=a1_activitats.id_act
      order by id_ins DESC limit 5 ");
      while($fila=mysqli_fetch_array($con)){

            $var="id_ins".$fila["id_ins"];
            if((isset($_POST["$var"]))&&($_POST["$var"]==1)){
                    if($requisit!=""){ $requisit.=" or ";}
                    $requisit.=" id_ins =".$fila["id_ins"];
            }
      }
      if($requisit!=""){
          $SQL.=" and id_entidad IN (select id_entidad from i3_reserva where ".$requisit.")";
      }


    $SQL.=" order by id_entidad";
    echo $SQL;

    $con=mysqli_query($cnx_cesportiu,$SQL);

    $titol=mysqli_num_rows($con)." entitats";

    $text="<table>
    <tr>
    <th>Codi</th>
    <th>Nom</th>
    <th>Interna</th>
    <th>Municipi</th>
    <th>Adreça</th>
    <th>Telèfon</th>
    <th>Fax</th>
    <th>Altres telefons</th>
    <th>Correu electrònic</th>
    <th>Esports</th>
    </tr>";

    while($fila=mysqli_fetch_array($con)){

        $text.="
            <tr>
            <td>".$fila["id_entidad"]."</td>
            <td>".$fila["nom_entidad"]."</td>
            <td>";
            if($fila["intern"]==1){ $text.="Interna";}
            else{ $text.="externa";}
            $text.="</td>
            <td>".$fila["municipi"];
            if($fila["id_muni"]!=0){

                $SQL1="select * from Zmunicipis where id_muni=".$fila["id_muni"];
                $con1=mysqli_query($cnx_cesportiu,$SQL1);
                $fila1=mysqli_fetch_array($con1);
                $text.=$fila1["nom_muni"];
            }

            $text.="</td>
            <td>".visual_adreca($fila["id_entidad"])."</td>
            <td>".$fila["telefon"]."</td>
            <td>".$fila["fax"]."</th>
            <td>".$fila["altres_tel_fax"]."</th>
            <td>".$fila["email"]."</td>
            <td>";


            $primer=1;
             $SQL1="select * from entidades_deportes,Zdeportes where
             id_entidad=".$fila["id_entidad"]."
             and entidades_deportes.id_deporte=Zdeportes.id_deporte";
             $con1=mysqli_query($cnx_cesportiu,$SQL1);
             while($fila1=mysqli_fetch_array($con1)){
                if($primer==0){ $text.=", ";}
                $text.= $fila1["nom_deporte"];
                $primer=0;
             }

            $text.="</td>
            </tr>";

    }
    $text.="</table>";

?>