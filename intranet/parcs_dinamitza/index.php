<h2>Activitats als parcs urbans de salut</h2>
<?php if(isset($_POST["guardar"])){
	$ins=0;
	if(($_POST["id_parc"]!="")&&($_POST["de_data"]!="")&&($_POST["hora"])){
		if($_POST["a_data"]!=""){ $a_data=strtotime($_POST["a_data"]);}else{ $a_data=strtotime($_POST["de_data"]);}
		$data_actual=strtotime($_POST["de_data"]);
		
		while($data_actual<=$a_data){
			if( ($_POST["dia_setmana"]=="") || ($_POST["dia_setmana"]==(date("w",$data_actual)) ) ){
				
				$ins=mysqli_query($cnx_cesportiu,"insert into parcs_dinamitza set 
				id_parc=".$_POST["id_parc"].",
				data=".date('Ymd',$data_actual).",
				hora='".$_POST["hora"]."'");
				echo date('Y-m-d',$data_actual)."<br>";
				
			}
			$data_actual = strtotime ( '+1 day' , $data_actual  ) ;
		}
	}
	msn($ins);
	
}
if(isset($_POST["eliminar"])){
	?>

	<form method="post">
	<?php 
	$num=0;
	$cons="select * from parcs_dinamitza,parcs,Zmunicipis where 
	parcs.id_parc=parcs_dinamitza.id_parc and parcs.id_muni=Zmunicipis.id_muni";
	if($_POST["hora"]!=""){ $cons.=" and hora='".$_POST["hora"]."' ";}
	if($_POST["id_parc"]!=""){ $cons.=" and parcs.id_parc='".$_POST["id_parc"]."' ";}
	if(($_POST["de_data"]!="")&&($_POST["a_data"]!="")){
		$cons.=" and data>=".str_replace('-','',$_POST["de_data"])." and data<=".str_replace('-','',$_POST["a_data"]);
	}
	elseif($_POST["de_data"]!=""){
		$cons.=" and data=".str_replace('-','',$_POST["de_data"]);
	}
	//echo $cons;
	$con=mysqli_query($cnx_cesportiu,$cons);
	while($fila=mysqli_fetch_array($con)){
		$entra=1;
		if($_POST["dia_setmana"]!=""){
			//echo date("w",strtotime($fila["data"]));
			if($_POST["dia_setmana"]!= date("w",strtotime($fila["data"])) ){
				$entra=0;
			}
			
		}
		if($entra==1){
			$num++;
			echo " ".date('Y-m-d',strtotime($fila["data"]))." a ".$fila["hora"]."h. al parc ";
			if($fila["nom_parc"]!="") { echo StripSlashes($fila["nom_parc"]);}
			echo article($fila["nom_muni"]).$fila["nom_muni"]." - ".$fila["adreca"]."<br />";?>
            <input type="hidden" name="<?php echo $fila["id_parc_din"];?>" value="1"/>
		<?php }
	}?>
    
    <?php if($num!=0){?>
    <input type="submit" name="confirmar_eliminar" value="Eliminar <?php echo $num;?> activitats" />
    <?php }?>
    <?php include("comuns.php");?>
    </form>
	
	
<?php }
if(isset($_POST["confirmar_eliminar"])){
	foreach($_POST as $nom => $valor){
		if(is_int($nom)){
			$el=mysqli_query($cnx_cesportiu,"delete from parcs_dinamitza where id_parc_din=".$nom);
		}
	} 
	msn($el);
}
?>

<script type="text/javascript">
	window.onload = function(){
		new JsDatePick({
			useMode:2,
			target:"de_data",
			dateFormat:"%Y-%m-%d"
			
		});
		new JsDatePick({
			useMode:2,
			target:"a_data",
			dateFormat:"%Y-%m-%d"
			
		});

	};
</script>
<form method="post">

<label>Data inici</label>
<input type="text" name="de_data" id="de_data" size="9">

<label>Data fi</label>
<input type="text" name="a_data" id="a_data" size="9">

<label>Parc</label>
<select name="id_parc">
<option value=""></option>
<?php $con=mysqli_query($cnx_cesportiu,"select * from parcs,Zmunicipis where parcs.id_muni=Zmunicipis.id_muni order by nom_muni");
while($fila=mysqli_fetch_array($con)){?>
    <option value="<?php echo $fila["id_parc"];?>"><?php 
	//if($fila["nom_parc"]!="") { echo StripSlashes($fila["nom_parc"]);} 
	//echo article($fila["nom_muni"]);
	echo $fila["nom_muni"];
	echo "-".StripSlashes($fila["adreca"]);
	?></option>
<?php }?>
</select>


<label>Tots els</label>
<select name="dia_setmana">
<option value=""></option>
<?php for($i=0;$i<=6;$i++){?>
    <option value="<?php echo $i;?>"><?php echo $dies7[$i];?></option>
<?php }?>
</select>


<label>Hora</label>
<input type="text" name="hora" id="hora_parc" size="5" onchange="validar_horari('hora_parc','');">


<input type="submit" name="guardar" value="Guardar" class="boto_guardar">
<input type="submit" name="eliminar" value="Eliminar" class="boto_eliminar">
<?php include("comuns.php");?>

</form>
<?php include("llistat.php");?>