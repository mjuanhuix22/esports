<?php 
//-----------------------Modificar
if(isset($_POST["modificar"])){
	$samarreta = 0;
	if(isset($_POST["samarreta"]) && $_POST["samarreta"] == 'on'){
		$samarreta = 1;
	}

    $trans=0;
    if(isset($_POST["trans"]) && $_POST["trans"] == 1){
        $trans = 1;
    }

    $gratis=0;
    if(isset($_POST["gratis"]) && $_POST["gratis"] == 1){
        $gratis = 1;
    }

	$mod=mysqli_query($cnx_cesportiu,"update i1_inscripcions set 
	nom_ins='".AddSlashes($_POST["nom_ins"])."',
	text='".AddSlashes($_POST["text"])."',  
	obert=".$_POST["obert"].",
	gratis='".$gratis."',
	trans='".$trans."',
	asseguranca='".$_POST["asseguranca"]."',
	bus='".$_POST["bus"]."',
	samarreta='".$samarreta."',
	samarreta_infantil='".$_POST["samarreta_infantil"]."',
	sol_federacio='".$_POST["sol_federacio"]."',
	sol_federacio_any='".$_POST["sol_federacio_any"]."',
	sol_DNI='".$_POST["sol_DNI"]."',
	sol_entitat=".$_POST["sol_entitat"]." 
	where id_ins=".$_POST["id_ins"]);
	
	$con=mysqli_query($cnx_cesportiu,"SELECT * from i2_cat where id_ins=".$_POST["id_ins"]."");
	while($fila=mysqli_fetch_array($con)){
		$nom_cat="nom_cat_".$fila["id_cat"];
		$codi="codi_".$fila["id_cat"];
		$any_inici="any_inici_".$fila["id_cat"];
		$any_fi="any_fi_".$fila["id_cat"];
		$edat_inici="edat_inici_".$fila["id_cat"];
		$edat_fi="edat_fi_".$fila["id_cat"];
        $preu="preu_".$fila["id_cat"];
		
		if($_POST["$nom_cat"]!=""){
			mysqli_query($cnx_cesportiu,"update i2_cat set 
			nom_cat='".$_POST["$nom_cat"]."',
			codi='".$_POST["$codi"]."',
			any_inici='".$_POST["$any_inici"]."',
			any_fi='".$_POST["$any_fi"]."',
			edat_inici='".$_POST["$edat_inici"]."',
			edat_fi='".$_POST["$edat_fi"]."',   
			preu='".$_POST["$preu"]."'  
			where id_cat=".$fila["id_cat"]);
		}else{
			mysqli_query($cnx_cesportiu,"delete from i2_cat where id_cat=".$fila["id_cat"]);
		}
	}
		
	if($_POST["nom_cat_nou"]!=""){
		mysqli_query($cnx_cesportiu,"insert into i2_cat set 
		id_ins=".$_POST["id_ins"].",
		nom_cat='".$_POST["nom_cat_nou"]."',
		codi='".$_POST["codi_nou"]."',
		any_inici='".$_POST["any_inici_nou"]."',
		any_fi='".$_POST["any_fi_nou"]."',
		edat_inici='".$_POST["edat_inici_nou"]."',
		edat_fi='".$_POST["edat_fi_nou"]."',
		  preu='".$_POST["preu_nou"]."' ;");
	}
	msn($mod);
	
}

//------------------------------Guardar
elseif(isset($_POST["guardar"])){
	$samarreta = 0;
	if(isset($_POST["samarreta"]) && $_POST["samarreta"] == 'on'){
		$samarreta = 1;
	}

	$trans=0;
    if(isset($_POST["trans"]) && $_POST["trans"] == 1){
        $trans = 1;
    }

    $gratis=0;
    if(isset($_POST["gratis"]) && $_POST["gratis"] == 1){
        $gratis = 1;
    }

	$id=explode("_",$_POST["act_jornada"]);
	$ins=mysqli_query($cnx_cesportiu,"insert into i1_inscripcions set 
	id_act=".$id[0].",
	id_jornada=".$id[1].",
	nom_ins='".AddSlashes($_POST["nom_ins"])."',
	text='".nl2br(AddSlashes($_POST["text"]))."',  
	obert=".$_POST["obert"].",
	gratis='".$gratis."',
	trans='".$trans."',
	asseguranca='".$_POST["asseguranca"]."',
	bus='".$_POST["bus"]."',
	samarreta='".$samarreta."',
	samarreta_infantil='".$_POST["samarreta_infantil"]."',
	sol_federacio='".$_POST["sol_federacio"]."',
	sol_federacio_any='".$_POST["sol_federacio_any"]."',
	sol_DNI='".$_POST["sol_DNI"]."',
	sol_entitat=".$_POST["sol_entitat"].";");
	
	$id_ins=mysqli_insert_id($cnx_cesportiu);

	for($i=1;$i<=10;$i++){
		$nom_cat="nom_cat_".$i;
		if($_POST["$nom_cat"]!=""){
			$codi="codi_".$i;
			$any_inici="any_inici_".$i;
			$any_fi="any_fi_".$i;
			$edat_inici="edat_inici_".$i;
			$edat_fi="edat_fi_".$i;
            $preu="preu_".$i;
			
			if($_POST["$nom_cat"]!=""){
				mysqli_query($cnx_cesportiu,"insert into i2_cat set 
				id_ins=".$id_ins.",
				nom_cat='".$_POST["$nom_cat"]."',
				codi='".$_POST["$codi"]."',
				any_inici='".$_POST["$any_inici"]."',
				any_fi='".$_POST["$any_fi"]."',
				edat_inici='".$_POST["$edat_inici"]."',
				edat_fi='".$_POST["$edat_fi"]."',
				preu='".$_POST["$preu"]."';");
			}
		}
	}
	msn($ins);
}

elseif(isset($_POST["eliminar"])){
	$el=mysqli_query($cnx_cesportiu,"delete from i1_inscripcions where id_ins=".$_POST["id_ins"]);
	mysqli_query($cnx_cesportiu,"delete from i2_cat where id_ins=".$_POST["id_ins"]);
	msn($el);
}

//----------------------
elseif(isset($_POST["eliminar_esportista"])){
	$el=mysqli_query($cnx_cesportiu,"delete from i4_esportista where id_esportista=".$_POST["id_esportista"]);
	
	$SQL="SELECT count(*) from i4_esportista where id_reserva=".$_POST["id_reserva"];
	$con=mysqli_query($cnx_cesportiu,$SQL);
	$fila=mysqli_fetch_array($con);
	if($fila["count(*)"]==0){
		mysqli_query($cnx_cesportiu,"delete from i3_reserva where id_reserva=".$_POST["id_reserva"]);	
	}
	
	msn($el);
}

elseif(isset($_POST["convert_1"])){
	$mod=mysqli_query($cnx_cesportiu,"update i3_reserva set estat=2 where id_reserva=".$_POST["id_reserva"]);


    $conI=mysqli_query($cnx_cesportiu,"select * from i1_inscripcions where id_ins=".$_POST["id_ins"]);
    $fI=mysqli_fetch_array($conI);
    $id_ins=$_POST["id_ins"];

	$id_reserva=$_POST["id_reserva"];
    include("../comuns/inscripcions/veure_sol.php");
    include("../comuns/inscripcions/pas7_mail.php");

	msn($mod);
}
elseif(isset($_POST["eliminar_reserva"])){
	$el=mysqli_query($cnx_cesportiu,"delete from i4_esportista where id_reserva=".$_POST["id_reserva"]);
	mysqli_query($cnx_cesportiu,"delete from i3_reserva where id_reserva=".$_POST["id_reserva"]);	
	msn($el);
}


?>