<script language="javascript">
    function veure_gratis(){
        if(document.getElementById("gratis").checked==true){
            document.getElementById("capa_gratis").className="visual_no";
        }else{
            document.getElementById("capa_gratis").className="";

        }
    }

</script>

<h3>Configuraci&oacute;</h3>
<?php 
if($_POST["id_ins"]!="nou"){
	$con=mysqli_query($cnx_cesportiu,"SELECT * from i1_inscripcions where id_ins=".$_POST["id_ins"]);
	$fila=mysqli_fetch_array($con);
	$obert=$fila["obert"];
	$preu=$fila["preu"];
	$asseguranca=$fila["asseguranca"];
	$text=StripSlashes(str_replace('<br />','',$fila["text"]));

	$nom_ins=StripSlashes($fila["nom_ins"]);
	$sol_DNI=$fila["sol_DNI"];
	$sol_federacio=$fila["sol_federacio"];
	$sol_federacio_any=$fila["sol_federacio_any"];
	$sol_entitat=$fila["sol_entitat"];
	
	$bus=$fila["bus"];
	$samarreta=$fila["samarreta"];
	$samarreta_infantil=$fila["samarreta_infantil"];
	$gratis=$fila["gratis"];
	$trans=$fila["trans"];

}else{
	$obert=0;$preu="";$asseguranca="";$text="";	$nom_ins=""; $sol_federacio=0;$sol_DNI=0;$sol_federacio_any="";$sol_entitat=1;
	$bus=0;
	$samarreta=0;
	$samarreta_infantil=0;
	$gratis=0;
	$trans=0;
}?>

<form method="post">

<table width="95%" cellpadding="5">

<tr>
<td class="lletraGrisa">Inscripcions:</td>
<td>
<input type="radio"  value="1" name="obert"<?php if($obert==1){?> checked="checked"<?php }?>/>Obertes &nbsp;&nbsp;
<input type="radio"  value="0" name="obert" <?php if($obert==0){?> checked="checked"<?php }?>/>Tancades
</td>

<?php if($_POST["id_ins"]=="nou"){?>
	</tr><tr>
	  <td class="lletraGrisa">Jornada:</td>
    <td>
	<select name="act_jornada" >
        <option value=""></option>
    <?php $con=mysqli_query($cnx_cesportiu,"SELECT * from a1_activitats where 
	(data=0 and id_act IN (select id_act from a2_jornades where data>".date('Ymd')."))
	or
	(data>".date('Ymd').")
    order by a1_activitats.data DESC");
    while($fila=mysqli_fetch_array($con)){
		
		if($fila["data"]==0){
			$con1=mysqli_query($cnx_cesportiu,"SELECT *  from a2_jornades where 
			data>".date('Ymd')." and id_act=".$fila["id_act"]." 
		    order by data DESC");
		    while($fila1=mysqli_fetch_array($con1)){
				echo "<option value=\"".$fila["id_act"]."_".$fila1["id_jornada"]."\" >";		
				echo StripSlashes($fila["nom_act"]);
				echo " :".StripSlashes($fila1["nom_jornada"]);
				echo " : ".format_data_intranet($fila1["data"]);
				echo "</option>";
				$entra=1;
			}
			if($entra==1){
				echo "<option value=\"".$fila["id_act"]."_0\" >";		
				echo StripSlashes($fila["nom_act"]);
				echo "</option>";	
				
			}
			
		}else{
			echo "<option value=\"".$fila["id_act"]."_0\" >";		
			echo StripSlashes($fila["nom_act"]);
			echo " : ".format_data_intranet($fila["data"]);
			echo "</option>";
		}
		
     }?>
    
    </select>
    </td></tr>
<?php }?>

<tr>
  	<td class="lletraGrisa">Titol:</td>
	<td><input name="nom_ins" type="text" value="<?php echo $nom_ins;?>" size="50" /></td>
</tr>

    <tr>
        <td width="100" class="lletraGrisa" valign="top">Text de presentaci&oacute;:</td>
        <td>
            <textarea name="text" id="text"><?php echo $text; ?></textarea>
        </td>
    </tr>




<tr>
    <td class="lletraGrisa" valign="top">Gratis
        <input  type="checkbox" name="gratis"
               id="gratis"
               value="1" <?php if($gratis==1){?> checked <?php } ?> onclick="veure_gratis()">

        </td>
    <td>


        <table class="<?php if($gratis==1){ echo "visual_no";} ?>" id="capa_gratis">

            <tr>
                <td class="lletraGrisa">Permet transf.banciaria:</td>
                <td><input  type="checkbox" name="trans" value="1" <?php if($trans==1){?> checked <?php } ?>/></td>
            </tr>

            <tr>
                <td class="lletraGrisa">Asseguran&ccedil;a:</td>
                <td><input type="text" name="asseguranca" value="<?php echo $asseguranca;?>" size="3" /> &#8364;</td>
            </tr>
            <tr>
                <td class="lletraGrisa">Servei de bus:</td>
                <td><input type="text" name="bus" value="<?php echo $bus;?>" size="3" /> &#8364;</td>
            </tr>
            <tr>
                <td class="lletraGrisa">Samarreta:</td>
                <td>
                    <input type="checkbox" name="samarreta" <?php if(isset($samarreta) && $samarreta > 0){ echo 'checked="checked"'; }?>/>
                </td>
            </tr>
            <tr>
                <td class="lletraGrisa">Samarreta infantil:</td>
                <td><input type="text" name="samarreta_infantil" value="<?php echo $samarreta_infantil;?>" size="3" /> &#8364;</td>
            </tr>



        </table>


    </td>

</tr>







<tr>
  <td class="lletraGrisa">Sol·licita llicencia federativa:</td>
<td>

<input type="radio" name="sol_federacio" value="0" <?php if($sol_federacio==0){?> checked="checked"<?php }?> />NO

<input type="radio" name="sol_federacio" value="1" <?php if($sol_federacio==1){?> checked="checked"<?php }?> />Si
  &nbsp;&nbsp;Any inici:<input type="text" name="sol_federacio_any" size="4" maxlength="4" value="<?php echo $sol_federacio_any;?>" />

</td></tr>

<tr>
  <td class="lletraGrisa">Tipus identificacio:</td>
<td>
<input type="radio" name="sol_DNI" value="1" <?php if($sol_DNI==1){?> checked="checked"<?php }?> />DNI
<input type="radio" name="sol_DNI" value="0" <?php if($sol_DNI==0){?> checked="checked"<?php }?> />CatSalut
</td></tr>

<tr>
  <td class="lletraGrisa">Entitat:</td>
<td>
<input type="radio" name="sol_entitat" value="1" <?php if($sol_entitat==1){?> checked="checked"<?php }?> />Base de dades
<input type="radio" name="sol_entitat" value="0" <?php if($sol_entitat==0){?> checked="checked"<?php }?> />Camp de text
</td></tr>


<tr>
  <td class="lletraGrisa" valign="top">Categories:</td>
<td>
<table border="1" cellpadding="5" cellspacing="0">
<tr class="fons_blanc_gris">
	<td class="lletraNegra">Any inici</td>
    <td class="lletraNegra">Any fi</td>
    <td class="lletraNegra">Edat inici</td>
    <td class="lletraNegra">Edat fi</td>
	<td class="lletraNegra">Nom categoria</td>
    <td class="lletraNegra">Codi</td>
    <td class="lletraNegra">Preu</td>
</tr>
<?php 
if($_POST["id_ins"]=="nou"){

	for($i=1;$i<=10;$i++){?>
		<tr>
        <td><input name="any_inici_<?php echo $i;?>" type="text" size="4" maxlength="4" /></td>
        <td><input name="any_fi_<?php echo $i;?>" type="text" size="4" maxlength="4"  /></td>
        <td><input name="edat_inici_<?php echo $i;?>" type="text" size="4" maxlength="4" /></td>
        <td><input name="edat_fi_<?php echo $i;?>" type="text" size="4" maxlength="4"  /></td>
        <td><input type="text" name="nom_cat_<?php echo $i;?>"  /></td>
        <td><input name="codi_<?php echo $i;?>" type="text" size="4" /></td>
            <td><input name="preu_<?php echo $i;?>" type="text" size="4" /></td>
        </tr>
	<?php }

	
}else{
	$con=mysqli_query($cnx_cesportiu,"SELECT * from i2_cat where id_ins='".$_POST["id_ins"]."' order by any_inici,edat_inici DESC");
	while($fila=mysqli_fetch_array($con)){?>
		<tr>
		<td><input name="any_inici_<?php echo $fila["id_cat"];?>" type="text" value="<?php if($fila["any_inici"]!=0){echo $fila["any_inici"];}?>" size="4" maxlength="4" /></td>
		<td><input name="any_fi_<?php echo $fila["id_cat"];?>" type="text" value="<?php if($fila["any_fi"]!=0){echo $fila["any_fi"];}?>" size="4" maxlength="4" /></td>
        
        <td><input name="edat_inici_<?php echo $fila["id_cat"];?>" type="text" value="<?php if($fila["edat_inici"]!=0){echo $fila["edat_inici"];}?>" size="4" maxlength="4" /></td>
		<td><input name="edat_fi_<?php echo $fila["id_cat"];?>" type="text" value="<?php if($fila["edat_fi"]!=0){echo $fila["edat_fi"];}?>" size="4" maxlength="4" /></td>
		<td><input type="text" name="nom_cat_<?php echo $fila["id_cat"];?>" value="<?php echo StripSlashes($fila["nom_cat"]);?>" /></td>
		<td><input name="codi_<?php echo $fila["id_cat"];?>" type="text" value="<?php echo $fila["codi"];?>" size="4" /></td>
        <td><input name="preu_<?php echo $fila["id_cat"];?>" type="text" value="<?php echo $fila["preu"];?>" size="4" /></td>
		</tr>
	<?php }?>
    <tr>
    <td><input name="any_inici_nou" type="text" size="4" maxlength="4" /></td>
    <td><input name="any_fi_nou" type="text" size="4" maxlength="4"  /></td>
    <td><input name="edat_inici_nou" type="text" size="4" maxlength="4" /></td>
    <td><input name="edat_fi_nou" type="text" size="4" maxlength="4"  /></td>
    <td><input type="text" name="nom_cat_nou"  /></td>
    <td><input name="codi_nou" type="text" size="4" /></td>
    <td><input name="preu_nou" type="text" size="4" /></td>
    </tr>
<?php }?>

</table>
</td>
</tr>
</table>

<p><?php if($_POST["id_ins"]=="nou"){?>
	<input type="submit"  name="guardar" value="Guardar" class="boto_guardar" />
<?php }else{?>

    <input type="submit"  name="modificar" value="Modificar" class="boto_guardar" />
    <?php $con=mysqli_query($cnx_cesportiu,"SELECT count(*) from i3_reserva where id_ins=".$_POST["id_ins"]);
	$fila=mysqli_fetch_array($con);
	if($fila["count(*)"]==0){?>
    	<input type="submit"  name="eliminar" value="Eliminar" class="boto_eliminar" onClick="return validar_elim('');" />
    <?php }?>    
<?php }?></p>

<?php include("comuns2.php");?>
<input type="hidden" name="id_ins" value="<?php echo $_POST["id_ins"];?>" />
</form>

<script>
	CKEDITOR.replace('text');
</script>
