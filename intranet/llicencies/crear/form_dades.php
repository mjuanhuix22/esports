<script language="javascript" type="text/javascript">
function validar_llic(){
	if ( (window.document.form_inscripcio.id_deporte.value=="")||(window.document.form_inscripcio.id_tipus.value=="")||
		 (window.document.form_inscripcio.nom.value=="")|| (window.document.form_inscripcio.cognom1.value=="")){
				alert("És obligatori omplir tots els camps");
				return (false);		
	}
	
	if((window.document.form_inscripcio.id_tipus.value==4)&& (document.frm.uploadfile.value == '')){
			alert("És obligatori omplir tots els camps");
			return (false);
	}
	return (true);
}
</script>



<?php if(isset($_POST["seguent"])){
	
	$error=0;
	
	if(!is_string($_POST["NSS1"])){ $error=1; }
	
	if(($_POST["NSS2"]!=1)&&($_POST["NSS2"]!=0)){ $error=1;}
	
	
	if(!is_numeric($_POST["NSS3"])){ $error=1; }
	
	$any=substr($_POST["NSS3"],0,2);
	if($any<=date('y')){ $any="20".$any;}
	else{ $any="19".$any;}

	$mes=substr($_POST["NSS3"],2,2); 
	if(($mes>12)||($mes<1)){ $error=1;}
	
	$dia=substr($_POST["NSS3"],4,2);
	if(($dia>31)||($dia<1)){ $error=1; }
	
	if(!is_numeric($_POST["NSS4"])){ $error=1;}
	if(!is_numeric($_POST["NSS5"])){ $error=1;}
	$identificador=$_POST["NSS1"].$_POST["NSS2"].$_POST["NSS3"].$_POST["NSS4"].$_POST["NSS5"];
	
	if($error==1){ msn_error("CatSalut Incorrecte");}
	
	
	if($error==0){
		$con=mysqli_query($cnx_cesportiu,"select * from esportistes where identificador='".$identificador."'");
		if(mysqli_num_rows($con)==1){
			$fila=mysqli_fetch_array($con);
			$id_esportista=$fila["id_esportista"];

			$con1=mysqli_query($cnx_cesportiu,"select count(*) from llice_sol_esportista,llice_sol where 
			llice_sol_esportista.id_sol=llice_sol.id_sol and 
			llice_sol.id_entidad=".$_POST["id_entidad"]." and 
			id_curs=".curs_actual()." and 
			id_esportista=".$id_esportista);
			
			$fila1=mysqli_fetch_array($con1);
			if($fila1["count(*)"]!=0){
			
				msn_error("Error. Aquest llicencia ja esta entrada");
				$error=1;
			
			}else{
				$nom=$fila["nom"];
				$cognom1=$fila["cognom1"];
				$cognom2=$fila["cognom2"];
				
			}

		}else{
			$nom="";$cognom1="";$cognom2="";	
		}
		
		if($_POST["NSS2"]==1){ $id_genere=1;}
		if($_POST["NSS2"]==0){ $id_genere=2;}
		
		
	}
	
	if($error==0){
	
?>


<form method="post" name="form_inscripcio" id="form_inscripcio" enctype="multipart/form-data">

<table>

	<tr > 
      <td class="lletraGrisa">CatSalut:</td>
      <td><?php echo $identificador;?></td>
    </tr>
    <tr > 
      <td class="lletraGrisa">Nom:</td>
      <td> <input name="nom" type="text" id="nom"  value="<?php echo $nom;?>" /></td>
    </tr>
    <tr > 
      <td class="lletraGrisa">Primer Cognom:</td>
      <td> <input name="cognom1" type="text"  id="cognom1"  value="<?php echo $cognom1;?>" /></td>
    </tr>
    <tr > 
      <td class="lletraGrisa">Segon Cognom:</td>
      <td> <input name="cognom2" type="text" id="cognom2"  value="<?php echo $cognom2;?>"/></td>
    </tr>
	    <tr >
      <td class="lletraGrisa">Sexe:</td>
      <td><?php $con=mysqli_query($cnx_cesportiu,"select * from Zgeneres where id_genere='".$id_genere."'");
		$fila=mysqli_fetch_array($con);
		echo $fila["nom_genere"];?>
        <input type="hidden" name="id_genere" value="<?php echo $id_genere;?>" />
        </td>
    </tr>
    <tr > 
      <td class="lletraGrisa">Data de naixement: </td>
      <td><?php echo $dia."/".$mes."/".$any;?>
      <input type="hidden" name="dia" value="<?php echo $dia;?>" />
      <input type="hidden" name="mes" value="<?php echo $mes;?>" />
      <input type="hidden" name="any" value="<?php echo $any?>" />
      </td>
    </tr>

	<tr > 
      <td class="lletraGrisa">Tipus llic&egrave;ncia:</td>
      <td> 

	  <select name="id_tipus"  id="id_tipus">
          <option value=""></option>
          <?php 
	  	$con=mysqli_query($cnx_cesportiu,"SELECT * FROM Zllice_tipus");
		while($fila=mysqli_fetch_array($con)){?>
          <option value="<?php echo $fila["id_tipus"]?>"><?php echo $fila["nom_tipus"];?></option>
          <?php }?>
        </select>

	   </td>
    </tr>

	<tr > 
      <td class="lletraGrisa">Esport:</td>
      <td>
		<select name="id_deporte" id="id_deporte">
		  <option value=""></option>
		<?php 
		$con=mysqli_query($cnx_cesportiu,"select * from Zdeportes,entidades_deportes where entidades_deportes.id_deporte=Zdeportes.id_deporte 
		and entidades_deportes.id_entidad=".$_POST["id_entidad"]." order by nom_deporte");
		while($fila=mysqli_fetch_array($con)){?>
		  <option value="<?php echo $fila["id_deporte"]?>" ><?php echo $fila["nom_deporte"];?></option>
		  <?php }?>
		</select>
		 </td>
    </tr>

    <tr > 
      <td class="lletraGrisa">Categoria:</td>
      <td > 
 <?php 
 		$id_cat=cat($any); 
	  	$con=mysqli_query($cnx_cesportiu,"SELECT * from Zcategories where id_cat=".$id_cat);
		$fila=mysqli_fetch_array($con);
		echo $fila["nom_cat"];?>
	  <input type="hidden" name="id_cat" value="<?php echo $fila["id_cat"];?>"	 />  </td>
    </tr>
	 
	
	<tr >
      <td class="lletraGrisa">Foto:</td>
      <td><input name="foto" type="file" value="<?php //echo $_POST["foto"];?>" /></td>
    </tr>
	
    <tr > 
      <td align="center"> 
	  <input name="guardar" type="submit" class="boto_guardar" 
	  onclick="return validar_llic();" value="Guardar llic&egrave;ncia" /></td>
      <td align="center">&nbsp;</td>
    </tr>
   </table>

<input type="hidden" name="identificador" value="<?php echo $identificador;?>" />
<input type="hidden" name="id_entidad" value="<?php echo $_POST["id_entidad"]?>" />
<?php if(isset($id_esportista)){?><input type="hidden" name="id_esportista" value="<?php echo $id_esportista;?>" /><?php }?>
<?php include("comuns2.php");?>
</form>
<?php }}


if((!isset($_POST["seguent"]))||($error==1)){?>
	<form method="post">
	Cat Salut<input type="text" name="NSS1" size="4" maxlength="4" />
    <input type="text" name="NSS2" size="1" maxlength="1" />
    
    <input type="text" name="NSS3" size="6" maxlength="6" />
    <input type="text" name="NSS4" size="2" maxlength="2" />
    <input type="text" name="NSS5" size="1" maxlength="1" />
	
    
    <br />Entitat
    <select name="id_entidad"  id="id_entidad" >
      <option value=""></option>
      <?php
    $con=mysqli_query($cnx_cesportiu,"SELECT entidades.id_entidad,nom_entidad FROM entidades where estat=1 and intern=1 order by nom_entidad");
    while($fila=mysqli_fetch_array($con)){?>
      <option value="<?php echo $fila["id_entidad"];?>" ><?php echo StripSlashes($fila["nom_entidad"]);?></option>
      <?php
    }
    ?>
    </select>
    <BR />
    <input type="submit" name="seguent" value="Seguent" />
    <?php include("comuns2.php");?>
</form>

<?php }?>