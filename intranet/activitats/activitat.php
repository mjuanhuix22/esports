<script type="text/javascript">
	window.onload = function(){
		new JsDatePick({
			useMode:2,
			target:"data",
			dateFormat:"%Y-%m-%d"			
		});

	};
	function veure_puntual(){
		if(window.document.form_act.data_unica[0].checked==true){
			window.document.getElementById("capa_data").className="visual_si";
			window.document.getElementById("capa_sensedata").className="visual_no";
		}else{
			window.document.getElementById("capa_data").className="visual_no";
			window.document.getElementById("capa_sensedata").className="visual_si";
		}	
	}

	function valida_ple(){
		if(window.document.getElementById("nom_act").value==""){ return (false);}
		return (true);
	}
</script>
<form  method="post"  name="form_act" enctype="multipart/form-data">
<?php 
if($_POST["id_act"]=="nou"){
	$nom_act="";
	$id_deporte="";	
	$portada=1;
	$data_unica=1;
	$data="";
	$id_muni="";
	$anual=0;
	$img_presentacio="";
	$evdet_id=0;
	
}else{
	$con=mysqli_query($cnx_cesportiu,"SELECT * FROM a1_activitats where id_act=".$_POST["id_act"]);
	$fila=mysqli_fetch_array($con);
	$nom_act=stripslashes($fila["nom_act"]);
	$id_deporte=$fila["id_deporte"];
	$portada=$fila["portada"];
	if($fila["data"]==0){ $data_unica=0;}else{ $data_unica=1;}
	$data=format_data_intranet($fila["data"]);
	$id_muni=$fila["id_muni"];
	$anual=$fila["anual"];
	$img_presentacio=$fila["img_presentacio"];
	$evdet_id=$fila["evdet_id"];
}
?>
<table  border="0" cellspacing="4" cellpadding="1">
<tr>
    <td width="100">Títol</td>
    <td><input name="nom_act" id="nom_act" type="text" value="<?php echo $nom_act;?>" size="70"></td>
</tr>

<tr>
    <td >Esport</td>
    <td>        
    <select name="id_deporte" >
        <option value=""></option>
        <?php $con2=mysqli_query($cnx_cesportiu,"select * from Zdeportes order by nom_deporte");
        while($fila2=mysqli_fetch_array($con2)){?>
        	<option value="<?php echo $fila2["id_deporte"];?>" <?php if($id_deporte==$fila2["id_deporte"]){ echo "selected";}?>><?php echo $fila2["nom_deporte"];?></option>
        <?php }?>
    </select></td>
</tr>


<tr>
	<td>Enllaçar web Consell</td>
    <td><input type="radio" name="evdet_id" value="1" <?php if($evdet_id!=0){?> checked="checked"<?php }?>/>si
   <input type="radio" name="evdet_id" value="0" onclick="" <?php if($evdet_id==0){?> checked="checked"<?php }?> />no</td>
</tr>

<tr>
    <td >Portada</td>
    <td ><input type="radio" name="portada" value="1" <?php if($portada==1){?> checked="checked"<?php }?>>Si
    <input type="radio" name="portada" value="0"  <?php if($portada==0){?> checked="checked"<?php }?>>No</td>
</tr>


<tr><td>Data única</td>
<td>
<?php $con1=mysqli_query($cnx_cesportiu,"SELECT count(*) FROM a2_jornades where id_act='".$_POST["id_act"]."'");
$fila1=mysqli_fetch_array($con1);
if($fila1["count(*)"]==0){?>
	<input type="radio" name="data_unica" value="1" <?php if($data_unica==1){?> checked="checked"<?php }?> onclick=" veure_puntual();"/>Si
    <input type="radio" name="data_unica" value="0" <?php if($data_unica==0){?> checked="checked"<?php }?> onclick=" veure_puntual();"/>No
<?php }else{
	if($data_unica==1){ echo "Si";}else{ echo "No";}?>
	<input type="hidden" name="data_unica" value="<?php echo $data_unica;?>" />
<?php }?>
</td>
</tr>
</table>

<div id="capa_data" class="<?php if($data_unica==0){ echo "visual_no"; }?>">
<table>
<tr>
  <td width="100">Data</td>
  <td><input type="text" name="data" id="data" size="9" value="<?php echo $data;?>" /></td>
</tr>

<tr>
  <td>Programació</td>
  <td><input type="radio" name="anual" value="1" <?php if($anual==1){?> checked="checked"<?php }?>/>anual
   <input type="radio" name="anual" value="0" onclick="" <?php if($anual==0){?> checked="checked"<?php }?> />puntual</td>
</tr>

 <tr>
  <td>Municipi</td>
  <td>
      <select name="id_muni">
    <option value=""></option>
    <?php $con1=mysqli_query($cnx_cesportiu,"SELECT * FROM Zmunicipis");
    while($fila1=mysqli_fetch_array($con1)){?>
        <option value="<?php echo $fila1["id_muni"];?>"
        <?php if($id_muni==$fila1["id_muni"]){?> selected="selected"<?php }?>><?php echo StripSlashes($fila1["nom_muni"]);?></option>
    <?php }?>
  </select>
  </td>
</tr>

</table>
</div>


<div id="capa_sensedata" class="<?php if($data_unica==1){ echo "visual_no"; }?>">
<table>
<tr>
  <td width="100">Banner</td>
  <td><input type="file" name="img_presentacio" />260*115px
  <?php if($img_presentacio!=""){?>
  	<img src="../carregues/act_presentacio/<?php echo $img_presentacio;?>" />
  <?php }?>
  </td>
</tr>
</table>

</div>


<input name="guardar_act" type="submit" class="boto_guardar" value="Guardar" onclick="return valida_ple();">

<?php //si NO existeix cap apartat i jornada amb aquesta activitat es pot eliminar
$con=mysqli_query($cnx_cesportiu,"SELECT count(*) FROM a3_apartats where id_act='".$_POST["id_act"]."'");
$fila=mysqli_fetch_array($con);
$con1=mysqli_query($cnx_cesportiu,"SELECT count(*) FROM a2_jornades where id_act='".$_POST["id_act"]."'");
$fila1=mysqli_fetch_array($con1);
if(($fila["count(*)"]==0)&&($fila1["count(*)"]==0) &&($_POST["id_act"]!="nou") ){?>
  <input name="eliminar_act" type="submit" class="boto_eliminar" onClick="return  validar_elim('activitat')" value="Eliminar Activitat">
<?php }?>


<?php include('comuns.php');
include("comuns3.php");
?>
</form>