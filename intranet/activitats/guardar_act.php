<?php


$SQL=" a1_activitats SET 
nom_act='".AddSlashes(trim($_POST["nom_act"]))."', 
n_nom_act='".normalitza($_POST["nom_act"])."',
id_deporte=".$_POST["id_deporte"].",
portada='".$_POST["portada"]."'";

if($_POST["data_unica"]==1){
	$SQL.=",data=".neteja_data($_POST["data"]).",
	id_muni='".$_POST["id_muni"]."',
	anual='".$_POST["anual"]."'";		
}else{
	$SQL.=",data=0,
	id_muni=0,
	anual=0";		
}

//Imatges i arxius existents
if($_POST["id_act"]!="nou"){
	//Si s'ha modificat el titol hem de reanomerar arxius
	$con=mysqli_query($cnx_cesportiu,"select nom_act,img_presentacio from a1_activitats where id_act=".$_POST["id_act"]);
	$fila=mysqli_fetch_array($con);
	if(($fila["nom_act"]!=$_POST["nom_act"]) ){ 
	
		//Reanomenar arxius que penjen
		$con2=mysqli_query($cnx_cesportiu,"select * from a3_apartats,a4_arxius where 
		a3_apartats.id_act=".$_POST["id_act"]." and
		a3_apartats.id_apartat=a4_arxius.id_apartat");
		while($fila2=mysqli_fetch_array($con2)){
			$extensio=substr($fila2["nom_fitxer"],-3);
			$vell="../carregues/activitats/".$fila2["nom_fitxer"];
			
			$nom_fitxer=normalitza(trim($_POST["nom_act"]))."_".normalitza($fila2["nom_arxiu"]).".".$extensio;
			$nou="../carregues/activitats/".$nom_fitxer;
			
			$r=rename($vell,$nou);
			if($r){
			mysqli_query($cnx_cesportiu,"update a4_arxius set nom_fitxer='".$nom_fitxer."'  where id_arxiu=".$fila2["id_arxiu"]);
			}
		}
		
		//Reanomenar arxius que penjen (activitat i jornada)
		$con2=mysqli_query($cnx_cesportiu,"select * from a2_jornades,a3_apartats,a4_arxius where 
		a2_jornades.id_act=".$_POST["id_act"]." and 
		a2_jornades.id_jornada=a3_apartats.id_jornada and 
		a3_apartats.id_apartat=a4_arxius.id_apartat");
		while($fila2=mysqli_fetch_array($con2)){
			$extensio=substr($fila2["nom_fitxer"],-3);
			$vell="../carregues/activitats/".$fila2["nom_fitxer"];
			
			$nom_fitxer=normalitza(trim($_POST["nom_act"]))."_".normalitza($fila2["nom_jornada"])."_".normalitza($fila2["nom_arxiu"]).".".$extensio;
			$nou="../carregues/activitats/".$nom_fitxer;
			
			$r=rename($vell,$nou);
			if($r){
			mysqli_query($cnx_cesportiu,"update a4_arxius set nom_fitxer='".$nom_fitxer."'  where id_arxiu=".$fila2["id_arxiu"]);
			}
		}
		
		//Reanomenar imatge de presentació de jornada
		$con2=mysqli_query($cnx_cesportiu,"select * from a2_jornades where id_act=".$_POST["id_act"]." and img_presentacio!=''");
		while($fila2=mysqli_fetch_array($con2)){
			$extensio=substr($fila["img_presentacio"],-3);
			$vell="../carregues/act_presentacio/".$fila2["img_presentacio"];
			$nom_fitxer=normalitza(trim($_POST["nom_act"]))."_".normalitza($fila2["nom_jornada"]).".".$extensio;
			$nou="../carregues/act_presentacio/".$nom_fitxer;
			$r=rename($vell,$nou);
			if($r){
				mysqli_query($cnx_cesportiu,"update a2_jornades set img_presentacio='".$nom_fitxer."' where id_jornada=".$fila2["id_jornada"]);
			}
		}
		
		//Reanomenar la imatge de prestentació actual
		if($fila["img_presentacio"]!=""){
	
			$extensio=substr($fila["img_presentacio"],-3);
			$vell="../carregues/act_presentacio/".$fila["img_presentacio"];
			$nou="../carregues/act_presentacio/".normalitza(trim($_POST["nom_act"])).".".$extensio;
			$r=rename($vell,$nou);
			if($r){
			mysqli_query($cnx_cesportiu,"update a1_activitats set img_presentacio='".normalitza(trim($_POST["nom_act"])).".".$extensio."' 
			where id_act=".$_POST["id_act"]);
			}
		
		}
	}
	

	//Si s'ha entrat una nova imatge de presentacio primer s'ha d'eliminar la vella	
	$con=mysqli_query($cnx_cesportiu,"select img_presentacio from a1_activitats where id_act=".$_POST["id_act"]);
	$fila=mysqli_fetch_array($con);
	if(($_FILES["img_presentacio"]["name"]!="") && ($fila["img_presentacio"]!="") ){ 	
		 unlink("../carregues/act_presentacio/".$fila["img_presentacio"]);
	}
	
}


//Penjar la nova imatge de presentació
$arxiu="img_presentacio";
$nom_fitxer=normalitza(trim($_POST["nom_act"]));	
$ruta="../carregues/act_presentacio/".$nom_fitxer;
$obli_imatge=1;
$amplada_nova=260;
include("../carregar.php");
if($penjat==1){
	$SQL.=",img_presentacio='".$nom_fitxer.".".$extensio."'";
}


//Si ja existia una imatge de presentació hem eliminat la vella, la nova que hem intentat penjar no s'ha penjat 
if(($penjat==0)&&($_FILES["img_presentacio"]["name"]!="")){
	$SQL.=",img_presentacio=''";
}

if($_POST["id_act"]=="nou"){
	$SQL="insert into  ".$SQL;
	//echo $SQL;
	$ins=mysqli_query($cnx_cesportiu,$SQL);
	$id_act=mysqli_insert_id($cnx_cesportiu);
}else{
	$SQL="UPDATE ".$SQL." WHERE id_act=".$_POST["id_act"];
	$ins=mysqli_query($cnx_cesportiu,$SQL);
	$id_act=$_POST["id_act"];
}
msn($ins);

?>