<?php 

$SQL="a2_jornades SET 
data=".neteja_data($_POST["data"]).",
nom_jornada='".AddSlashes(trim($_POST["nom_jornada"]))."',
n_nom_jornada='".normalitza($_POST["nom_jornada"])."',
id_muni='".$_POST["id_muni"]."',
portada=".$_POST["portada"].",
id_act=".$_POST["id_act"];

//Imatges i arxius existents
if($_POST["id_jornada"]!="nou"){
	
	//Si s'ha modificat el titol hem de reanomerar arxius
	$con=mysqli_query($cnx_cesportiu,"select nom_jornada,img_presentacio from a2_jornades where id_jornada=".$_POST["id_jornada"]);
	$fila=mysqli_fetch_array($con);
	if($fila["nom_jornada"]!=$_POST["nom_jornada"]) 	{ 
	
		//Reanomenar arxius que penjen
		$con2=mysqli_query($cnx_cesportiu,"select * from a3_apartats,a4_arxius where 
		a3_apartats.id_jornada=".$_POST["id_jornada"]." and
		a3_apartats.id_apartat=a4_arxius.id_apartat");
		while($fila2=mysqli_fetch_array($con2)){
			$extensio=substr($fila2["nom_fitxer"],-3);
			$vell="../carregues/activitats/".$fila2["nom_fitxer"];
			
			$con1=mysqli_query($cnx_cesportiu,"select nom_act from a1_activitats where id_act=".$_POST["id_act"]);
			$fila1=mysqli_fetch_array($con1);
			$nom_fitxer=normalitza($fila1["nom_act"])."-".normalitza(trim($_POST["nom_jornada"]))."_".normalitza($fila2["nom_arxiu"]).".".$extensio;
			$nou="../carregues/activitats/".$nom_fitxer;
			
			rename($vell,$nou);
			mysqli_query($cnx_cesportiu,"update a4_arxius set nom_fitxer='".$nom_fitxer."'  where id_arxiu=".$fila2["id_arxiu"]);
		}
		
	
		//Reanomenar la imatge de presentació existent
		if ($fila["img_presentacio"]!="") 	{ 
			$extensio=substr($fila["img_presentacio"],-3);
			$vell="../carregues/act_presentacio/".$fila["img_presentacio"];
			$con1=mysqli_query($cnx_cesportiu,"select nom_act from a1_activitats where id_act=".$_POST["id_act"]);
			$fila1=mysqli_fetch_array($con1);
			$img_presentacio=normalitza($fila1["nom_act"])."_".normalitza(trim($_POST["nom_jornada"])).".".$extensio;
			$nou="../carregues/act_presentacio/".$img_presentacio;
			rename($vell,$nou);
			mysqli_query($cnx_cesportiu,"update a2_jornades set img_presentacio='".$img_presentacio."'  
			where id_jornada=".$_POST["id_jornada"]);
		}
	}
	//Si s'ha entrat una nova imatge de presentacio primer s'ha d'eliminar la vella	
	$con=mysqli_query($cnx_cesportiu,"select img_presentacio from a2_jornades where id_jornada=".$_POST["id_jornada"]);
	$fila=mysqli_fetch_array($con);
	if(($_FILES["img_presentacio"]["name"]!="") && ($fila["img_presentacio"]!="") ){ 	
		 unlink("../carregues/act_presentacio/".$fila["img_presentacio"]);
	}
}


//La imatge de presentació
$con=mysqli_query($cnx_cesportiu,"select nom_act from a1_activitats where id_act=".$_POST["id_act"]);
$fila=mysqli_fetch_array($con);
$nom_fitxer=normalitza($fila["nom_act"])."_".normalitza(trim($_POST["nom_jornada"]));	
$ruta="../carregues/act_presentacio/".$nom_fitxer;
$arxiu="img_presentacio";
$obli_imatge=1;
include("../carregar.php");
if($penjat==1){
	$SQL.=",img_presentacio='".$nom_fitxer.".".$extensio."'";
}

if($_POST["id_jornada"]=="nou"){
	$SQL="insert into ".$SQL;
	$ins=mysqli_query($cnx_cesportiu,$SQL);	
}else{
	$SQL="update ".$SQL." where id_jornada=".$_POST["id_jornada"];
	$ins=mysqli_query($cnx_cesportiu,$SQL);	
}
msn($ins);



?>