
<?php /*
id_config es refereix a curs i any

Cada id_config nou quan NO hi ha cap lliga creada, les entitats fan preinscripcions amb un formulari diferent. I es guarda en ass_3comu amb id_lliga=0 i id_config actual.
Despres, al crear una nova lliga es busca si existeix una ass_3comu amb id_lliga=0 i id_config actual i es substitueix id_lliga per la recent creada.

Cada vegada que es crea una nova lliga es busquen els equips de l'anterior lliga (mateix id_config)
i es dupliquen les dades de les taules ass_3comu i ass_3equips.
Per tant si un coordinador elimina o modifica dades o equips de la lliga actual no afecta a les anteriors lligues.
*/ ?>


<h2>Lligues dels esports associaci&oacute;</h2>


<table width="100%" cellpadding="2" cellspacing="0">
<tr>
<td valign="top">
<?php $entra=0;
if((isset($_POST["id_config"])) &&(isset($_POST["id_lliga"]))){ $entra=1; }

include("accions.php");

if($entra==1){ 
	
	echo "<h3>";

	if($_POST["id_lliga"]=="nou"){ echo "Nova lliga";}
	else{
		$con=mysqli_query($cnx_cesportiu,"select * from ass_2lliga,Zvariables where 
		ass_2lliga.id_lliga=".$_POST["id_lliga"]." and 
		ass_2lliga.id_nom=Zvariables.id_var");
		$fila=mysqli_fetch_array($con);
		echo stripslashes($fila["nom_var"]);	
	}
	if($_POST["id_grup1"]!=""){
		if($_POST["id_grup1"]=="nou"){ echo " >> Nova categoria/genere";}
		else{
			echo " >> ";
			$con1=mysqli_query($cnx_cesportiu,"select * from ass_4grup1,Zcategories,Zgeneres 
			where id_grup1=".$_POST["id_grup1"]." and 
			ass_4grup1.id_cat=Zcategories.id_cat and 
			ass_4grup1.id_genere=Zgeneres.id_genere");
			$fila1=mysqli_fetch_array($con1);
			echo  $fila1["nom_cat"]." ".$fila1["nom_genere"];
			
		}
	}
	
	if($_POST["id_grup2"]!=""){
		if($_POST["id_grup2"]=="nou"){ echo " >> Nou grup";}
		else{
			$con=mysqli_query($cnx_cesportiu,"select * from ass_4grup2 where id_grup2=".$_POST["id_grup2"]);
			$fila=mysqli_fetch_array($con);
			echo " >> ".stripslashes($fila["nom_grup2"]);
		}
	}
	echo "</h3>";

	if((isset($_POST["id_grup2"]))&&($_POST["id_grup2"]!="")){ 
		include("grup2.php");
		if($_POST["id_grup2"]!="nou"){ include("partits.php");}
	}
	elseif((isset($_POST["id_grup1"]))&&($_POST["id_grup1"]!="") ){ include("grup1.php");}
	else{ include("lliga.php");}

}
?></td>
<td width="200px" valign="top" bgcolor="#E8E8E8" align="right"><?php include('menu_esk.php');?></td>
</tr>
</table>

