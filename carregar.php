<?php
$penjat=0;
//enta am $arxiu,$ruta,$obli_imatge
if($_FILES["$arxiu"]["name"]!=""){
	//------------------Entra amb nomcasella,nomarxiu
	$extensio="";
	$tipuss=$_FILES["$arxiu"]["type"];
	
	if(($tipuss=="application/msword")||($tipuss=="application/vnd.openxmlformats-officedocument.wordprocessingml.document")){ $extensio="doc";}
	elseif($tipuss=="applicaton/octet-stream"){ $extensio="pdf";}
	elseif($tipuss=="application/pdf"){ $extensio="pdf";}
	elseif($tipuss=="image/pjpeg"){ $extensio="jpg";}
	elseif($tipuss=="image/jpeg"){ $extensio="jpg";}
	elseif($tipuss=="image/gif"){ $extensio="gif";}
	elseif($tipuss=="application/vnd.ms-excel"){ $extensio="xls";}
	elseif($extensio=="application/octet-stream"){ $extensio="gpx";}
	elseif($tipuss=="application/pps"){ $extensio="pps";}
		
	$ruta=$ruta.".".$extensio;

	if ($extensio==""){//max 1Gb
		msn_error("La extensi&oacute; del arxiu  no es correcta:<br /> ".$tipuss,0); 
	}
	elseif(file_exists($ruta)){ 
		msn_error("L'arxiu ".$ruta." ja existeix",0); 
	}
	elseif ((isset($obli_imatge)) &&($obli_imatge==1) && ($extensio!="jpg") && ($extensio!="gif") && ($extensio!="png")){
		msn_error("La extensi&oacute; de la imatge no es correcta:<br /> ".$tipuss,0); 
	}
	elseif (!move_uploaded_file($_FILES["$arxiu"]["tmp_name"],$ruta)){
	
		msn_error("Error al pujar l'arxiu. No pot guardar-se"); 
	}else{
		$penjat=1;

		if(($extensio=="jpg")||($extensio=="gif")){
			//------------------------------cambiar l'amplada de la imatge
			$tamany=getimagesize($ruta);
			$amplada_original=$tamany[0];
			$altura_original=$tamany[1];
			if(!isset($amplada_nova)){$amplada_nova=600;}
			
			if( $amplada_original<= $amplada_nova){
				$amplada_nova=$amplada_original;
				$altura_nova=$altura_original;
			}else{
				
				$altura_nova=($altura_original*$amplada_nova)/$amplada_original;
			}
			if($extensio=="jpg"){
				$original=imagecreatefromjpeg($ruta);
			}else{
				$original=imagecreatefromgif($ruta);
			}	
			$nova=imagecreatetruecolor($amplada_nova,$altura_nova);
			imagecopyresampled($nova,$original,0,0,0,0,$amplada_nova,$altura_nova,$amplada_original,$altura_original);
			if($extensio=="jpg"){
				imagejpeg($nova,$ruta,100);
			}else{
				imagegif($nova,$ruta);
			}
			imagedestroy($nova);
			imagedestroy($original);
			$tamany=0;
		}

	} 
}?>