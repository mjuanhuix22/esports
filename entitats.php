<?php include("conexio.php");
include("inc.php");



$con=mysqli_query($cnx_cesportiu,"select count(*) from entidades where estat=1");
$fila=mysqli_fetch_array($con);
$titol="Les ".$fila["count(*)"]." entitats de la comarca";

if(isset($_GET["nom"])){ 
	$nom=neteja($_GET["nom"]);
	$con=mysqli_query($cnx_cesportiu,"select nom_entidad,id_entidad from entidades where n_entidad='".$nom."' and estat=1;");
	$fila=mysqli_fetch_array($con);
	if(mysqli_num_rows($con)==0){
		header("location:entitats.php");
	}else{

		$titol=StripSlashes($fila["nom_entidad"]);
		$id_entidad=$fila["id_entidad"];
	}

}

include "plantilles/sobre.php";


 if(isset($id_entidad)){

	
	$con=mysqli_query($cnx_cesportiu,"select * from entidades where id_entidad=".$id_entidad);
	$fila=mysqli_fetch_array($con);

    if($fila["logo_extensio"]!=''){?>
        <img src="carregues/entitats_logo/<?php echo  $fila["id_entidad"].".".$fila["logo_extensio"];?>" style="float:right;" />
    <?php }?>
    
	<table border="0" cellspacing="2" cellpadding="2">
	
          <tr>
        <td width="100" class="lletraGrisa">Municipi</td>
        <td ><?php $con2=mysqli_query($cnx_cesportiu,"select * from Zmunicipis where id_muni=".$fila["id_muni"]);
              $fila2=mysqli_fetch_array($con2);
              echo $fila2["nom_muni"];?>
              <?php echo  $fila["municipi"];?>
               </td>
      </tr>

       <tr>
        <td class="lletraGrisa">Codi Postal</td>
        <td ><?php  echo $fila2["CP"];?></td>
      </tr>

      <tr>
        <td class="lletraGrisa">Adre&ccedil;a</td>
        <td ><?php
           echo  visual_adreca($id_entidad);
        ?></td>
      </tr>


   
    
      <?php if($fila["telefon"]!="0"){?>
      <tr>
        <td class="lletraGrisa"><img src="imatges/icona_tel_b.gif" hspace="2"  />Tel&egrave;fon</td>
        <td ><?php $tel=$fila["telefon"];
              echo substr($tel,0,3)." ".substr($tel,3,2)." ".substr($tel,5,2)." ".substr($tel,7,2);?></td>
      </tr>

      <?php }?>
      <?php if($fila["fax"]!="0"){?>
      <tr>
        <td class="lletraGrisa"><img src="imatges/icona_fax_b.gif" hspace="2" />Fax</td>
        <td ><?php $fax=$fila["fax"];
              echo substr($fax,0,3)." ".substr($fax,3,2)." ".substr($fax,5,2)." ".substr($fax,7,2);?>    </td>
      </tr>

      <?php }?>
      <?php if(($fila["altres_tel_fax"]!="")&&($fila["altres_tel_fax"]!="NULL")&&($fila["altres_tel_fax"]!=0)){?>
      <tr>
        <td class="lletraGrisa"><img src="imatges/icona_tel_b.gif" hspace="2" />Tel&egrave;fons i/o fax: </td>
        <td ><?php echo  StripSlashes($fila["altres_tel_fax"]);?></td>
      </tr>

      <?php }?>
      <?php if(($fila["email"]!="")&&($fila["email"]!="NULL")){?>
      <tr>
        <td class="lletraGrisa"><img src="imatges/icona_mail_b.gif" hspace="2" />Adre&ccedil;a electr&ograve;nica </td>
        <td >
          <a href="mailto:<?php echo  $fila["email"];?>" class="MenuSuperior"><?php echo  $fila["email"];?></a>&nbsp;
          <?php if(($fila["email2"]!="")&&($fila["email2"]!="NULL")){?>
          <a href="mailto:<?php echo  $fila["email2"];?>" class="MenuSuperior"><?php echo  $fila["email2"];?></a>
         <?php }?>	</td>
      </tr>

      <?php }?>
      <?php if(($fila["web"]!="")&&($fila["web"]!="NULL")){?>
      <tr>
        <td class="lletraGrisa"><img src="imatges/icona_web.gif" width="16" height="16" hspace="2" />Web</td>
        <td><a href="<?php echo  $fila["web"];?>" target="_blank" class="MenuSuperior"><?php echo  $fila["web"];?></a></td>
      </tr>

      <?php }?>
      <?php if($fila["horari"]!=""){?>
      <tr>
        <td class="lletraGrisa"><img src="imatges/icona_horari.gif" width="20" hspace="2" />Horari</td>
        <td colspan="2" ><?php echo  StripSlashes($fila["horari"]);?></td>
      </tr>
 
      <?php }?>
      <?php if($fila["ambit"]!=""){?>
      <tr>
        <td class="lletraGrisa">Ambit</td>
        <td colspan="2" ><?php echo  StripSlashes($fila["ambit"]);?></td>
      </tr>

      <?php }?>
      <?php if($fila["descripcio"]!=""){?>
      <tr>
        <td class="lletraGrisa">Descripci&oacute;</td>
        <td colspan="2" ><?php echo  StripSlashes($fila["descripcio"]);?></td>
      </tr>

      <?php }?>
      
      <?php if($fila["foto_extensio"]!=''){?>
      <tr>
        <td  class="lletraGrisa"></td>
        <td colspan="2" ><img src="carregues/entitats_foto/<?php echo  $fila["id_entidad"].".".$fila["foto_extensio"];?>" /></td>
      </tr>
  
      <?php }?>
    </table>




<?php }else{?>
    <p>Al Consell Esportiu de la Selva treballem amb <strong><?php $con=mysqli_query($cnx_cesportiu,"select count(*) from entidades where estat=1");
    $fila=mysqli_fetch_array($con); echo $fila["count(*)"];?></strong> entitats esportives de la comarca i rodalies.</p>

    <p>Podeu veure les dades relatives a aquestes entitats combinant municipis, esports o ambdos:</p>

    <form method="post" action="#llistat">    
    <table>

    <tr>
      <td width="200" valign="top" class="titol_act">Municipi</td>
      <td width="200" valign="top" class="titol_act">Esport</td>
      <td>&nbsp;</td>
    </tr>
    <tr><td valign="top">
    <?php $con=mysqli_query($cnx_cesportiu,"select * from Zmunicipis order by nom_muni");
    while($fila=mysqli_fetch_array($con)){
        $var="muni_".$fila["id_muni"];?>
        <input type="checkbox" name="<?php echo  $var;?>" value="1" 
        <?php if(isset($_POST["$var"])){?> checked="checked"<?php }?>/><?php echo  $fila["nom_muni"];?><br />
    <?php }?>
            <?php $var="muni_0";?>
            <input type="checkbox" name="<?php echo $var; ?>"
                <?php if(isset($_POST["$var"])){?> checked="checked"<?php }?>>Fora de la comarca de la Selva
    </td>
    
    <td valign="top">
    
    <?php $con=mysqli_query($cnx_cesportiu,"select * from Zdeportes order by nom_deporte");
    while($fila=mysqli_fetch_array($con)){
        $var="deporte_".$fila["id_deporte"];?>
        <input type="checkbox" name="<?php echo  $var;?>" value="1" 
        <?php if(isset($_POST["$var"])){?> checked="checked"<?php }?>/><?php echo  $fila["nom_deporte"];?><br />
    <?php }?>
    </td>
    <td>
   
    </td>
    </tr>
    </table>
    <input name="cercar" type="submit" class="boto_cercar" value="Cercar" />
    </form>

    <p>Si la vostra entitat no està al llistat podeu <a href="alta_entitat.php">donar d'alta una nova entitat</a>.</p>

    
    <?php if(isset($_POST["cercar"])){
		
		$SQL="select * from entidades where estat=1 and intern=1 ";
        
        $primerA=1;
        $con=mysqli_query($cnx_cesportiu,"select * from Zmunicipis order by nom_muni");
        while($fila=mysqli_fetch_array($con)){
            $muni="muni_".$fila["id_muni"];
            if(isset($_POST["$muni"])){
                if($primerA==1){ $SQL.=" and (";}else{ $SQL.=" or ";}
                $primerA=0;
                $SQL.=" id_muni=".$fila["id_muni"];
            }
        }
        $muni="muni_0";
        if(isset($_POST["$muni"])){
            if($primerA==1){ $SQL.=" and (";}else{ $SQL.=" or ";}
            $primerA=0;
            $SQL.=" id_muni=0";
        }

        if($primerA==0){ $SQL.=")";}
        
        $primerB=1;
        $con=mysqli_query($cnx_cesportiu,"select * from Zdeportes");
        while($fila=mysqli_fetch_array($con)){
            $deporte="deporte_".$fila["id_deporte"];
            if(isset($_POST["$deporte"])){
                if($primerB==1){ $SQL.=" and ( id_entidad IN ( select id_entidad from entidades_deportes where ";}else{ $SQL.=" or ";}
                $primerB=0;
                $SQL.=" id_deporte=".$fila["id_deporte"];
            }
        }
        if($primerB==0){ $SQL.=") )";}
        
        
        $SQL.=" order by nom_entidad"; 
      // echo $SQL;
        
        $con=mysqli_query($cnx_cesportiu,$SQL);?>
		<a name="llistat"></a>
		
		
   		<?php echo  " <h2>".mysqli_num_rows($con)." resultats de la cerca</h2>";
    
    
        
        while($fila=mysqli_fetch_array($con)){
        
            echo "<div><a href=\"entitats.php?nom=".$fila["n_entidad"]."\" class=\"llistat_entitats\">";
    
            echo "<div class=\"entitat_logo\">";
            $ruta="../carregues/entitats_logo/".$fila["id_entidad"].".jpg";
            if(!file_exists($ruta)){ $ruta="../carregues/entitats_foto/".$fila["id_entidad"].".gif";}
            if(file_exists($ruta)){
                echo "<img src=\"".$ruta."\" width=\"25\" height=\"19\">";
            }else{  
				$con1=mysqli_query($cnx_cesportiu,"select nom_deporte,Zdeportes.id_deporte from 
				entidades_deportes,Zdeportes where id_entidad=".$fila["id_entidad"]."
				and entidades_deportes.id_deporte=Zdeportes.id_deporte 
				order by nom_deporte");
				$fila1=mysqli_fetch_array($con1);
				echo "<img src=\"imatges/deportes/".$fila1["id_deporte"].".gif\" alt=\"".$fila1["nom_deporte"]."\" width=\"25\" height=\"19\">";}
            echo "</div>";
           
            
            
            echo "<div class=\"entitat_nom\">".StripSlashes($fila["nom_entidad"])."</div>";



            if($fila["id_muni"]==0){
                echo "<div class=\"entitat_muni\">Fora Selva:".$fila["municipi"]."</div>";
            }else{
                $SQL="select * from Zmunicipis where id_muni=".$fila["id_muni"];
                $con1=mysqli_query($cnx_cesportiu,$SQL);
                $fila1=mysqli_fetch_array($con1);
                echo "<div class=\"entitat_muni\">".$fila1["nom_muni"]."</div>";
            }

            
            echo "<div class=\"entitat_esports\">";
            $p=1;
            $con1=mysqli_query($cnx_cesportiu,"select nom_deporte from entidades_deportes,Zdeportes where id_entidad=".$fila["id_entidad"]."
            and entidades_deportes.id_deporte=Zdeportes.id_deporte 
            order by nom_deporte");
            while($fila1=mysqli_fetch_array($con1)){
                if($p==0){ echo ", ";}$p=0;
                echo $fila1["nom_deporte"];
            }
            echo "</div>";
            
            echo "</a></div>";
        
        
        }
    
    }

}

include "plantilles/sota.php";
?>

