<?php 
function neteja($netejar){
  $netejar = preg_replace("/<+\s*\/*\s*([A-Z][A-Z0-9]*)\b[^>]*\/*\s*>+/i","",$netejar);
 
  $netejar = str_replace("'","",$netejar);  
  $netejar = str_replace("\"","",$netejar);
 
  $netejar = str_replace(";","",$netejar);
  $netejar = str_replace("drop","",$netejar);
  $netejar = str_replace("--","",$netejar);
  $netejar = str_replace("=","",$netejar);
  $netejar = str_replace("*","",$netejar);
  $netejar = str_replace("%","",$netejar);
  $netejar = str_replace("[","",$netejar);
  $netejar = str_replace("]","",$netejar);
  $netejar = str_replace("..","",$netejar);
  $netejar = str_replace("/","",$netejar);
  $netejar = str_replace("(","",$netejar);
  $netejar = str_replace(")","",$netejar);
  $netejar = str_replace(">","",$netejar);
  $netejar = str_replace("<","",$netejar);
  $netejar = str_replace("&&","",$netejar);
  $netejar = str_replace("!","",$netejar);
  $netejar = str_replace("||","",$netejar);
   $netejar = str_replace("|","",$netejar);
  $netejar = str_replace(" and ","",$netejar);
  $netejar = str_replace(" or ","",$netejar);
  $netejar = str_replace(" xor ","",$netejar);
  $netejar = str_replace(" union ","",$netejar);
  $netejar = str_replace(" null ","",$netejar);
  $netejar = str_replace(" select ","",$netejar);
  $netejar = str_replace(" from ","",$netejar);
  $netejar = str_replace(" personal ","",$netejar);
  return ($netejar);
 
}
function normalitza ($cadena){

	$cadena=trim($cadena);
	$cadena = str_replace(
			array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
			array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
			$cadena
			);

	$cadena = str_replace(
			array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
			array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
			$cadena
			);

	$cadena = str_replace(
			array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
			array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
			$cadena
			);

	$cadena = str_replace(
			array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
			array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
			$cadena
			);

	$cadena = str_replace(
			array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
			array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
			$cadena
			);

	$cadena = str_replace(
			array('ñ', 'Ñ', 'ç', 'Ç'),
			array('n', 'N', 'c', 'C',),
			$cadena
			);


	//Esta parte se encarga de eliminar cualquier caracter extraño
	$cadena = str_replace(
			array("\\", "¨", "º", "-", "~",
					"#", "@", "|", "!", "\"",
					"·", "$", "%", "&", "/",
					"(", ")", "?", "'", "¡",
					"¿", "[", "^", "`", "]",
					"+", "}", "{", "¨", "´",
					">", "< ", ";", ",", ":",
					".", ""),
			'',
			$cadena
			);



	$cadena = str_replace(" del ","-",$cadena);
	$cadena = str_replace(" de la ","-",$cadena);
	$cadena = str_replace(" els ","-",$cadena);
	$cadena = str_replace(" de ","-",$cadena);
	$cadena = str_replace(" d'","-",$cadena);
	$cadena = str_replace(" la ","-",$cadena);
	$cadena = str_replace(" el ","-",$cadena);
	$cadena = str_replace(" a ","-",$cadena);
	$cadena = str_replace(" l'","-",$cadena);
	$cadena = str_replace("l·l","l",$cadena);

	$cadena=neteja($cadena);


	$cadena = str_replace(" ","-",$cadena);

	//$cadena = strtolower($cadena);
	return utf8_encode($cadena);

}

function crear_psw(){
	$caracters=array(1=>'1',2=>'2',3=>'3',4=>'4',5=>'5',6=>'6',7=>'7',8=>'8',9=>'9',10=>'0',11=>'A',12=>'B',13=>'C',14=>'D',15=>'E',16=>'F',17=>'G',18=>'H',19=>'I',20=>'J',21=>'K',22=>'L',23=>'M',24=>'N',25=>'O',26=>'P',27=>'Q',28=>'R',29=>'S',30=>'T',31=>'U',32=>'V',33=>'X',34=>'Y',35=>'Z');

	$psw=$caracters[rand(1,35)].$caracters[rand(1,35)].$caracters[rand(1,35)].$caracters[rand(1,35)].$caracters[rand(1,35)].$caracters[rand(1,35)].$caracters[rand(1,35)].$caracters[rand(1,35)];
	
	return ($psw);
}

$dies7=array(1=>"Dilluns",2=>"Dimarts",3=>"Dimecres",4=>"Dijous",5=>"Divendres",6=>"Dissabte",0=>"Diumenge");

function nom_data($data){
	
	$data=str_replace('-','',$data);
	$any=substr($data,0,4);
	$mes=substr($data,4,2);
	$dia=substr($data,6,2);
	$mes=intval($mes);


	$dies_setmana=array('Monday'=>"Dilluns",'Tuesday'=>"Dimarts",'Wednesday'=>"Dimecres",'Thursday'=>"Dijous",'Friday'=>"Divendres",'Saturday'=>"Dissabte",'Sunday'=>"Diumenge");
	$dataa= $dies_setmana[ date("l", mktime(0, 0, 0, $mes, $dia, $any)) ];
	$dataa.= " ".(int)$dia." ";
	if(($mes==4)||($mes==8)||($mes==10)){ $data.= "d'";}else{ $data.="de ";}
	$nom_mes=array(1=>'gener',2=>'febrer',3=>'març',4=>'abril',5=>'maig',6=>'juny',
7=>'juliol',8=>'agost',9=>'setembre',10=>'octubre',11=>'novembre',12=>'desembre');


	$dataa.= $nom_mes[$mes];
	$dataa.= " de ".$any;
	return ($dataa);
}

function total_dies($mes,$any){
	$mes=(int)$mes;
	if($mes==2)	{
		if($any%4==0){ return(29);}else{ return(28);}
	}
	elseif(($mes==4)||($mes==6)||($mes==9)||($mes==11)){ return(30);}
	else{ return(31);}
}

function article($nom){
	$primer=substr($nom,0,1);
	if(($primer=='A')||($primer=='E')||($primer=='I')||($primer=='O')||($primer=='U')||($primer=='H')){
		$art=" d'";
	}else{
		$art=" de ";
	}
	return $art;
}


$nom_mes=array(1=>'gener',2=>'febrer',3=>'març',4=>'abril',5=>'maig',6=>'juny',
7=>'juliol',8=>'agost',9=>'setembre',10=>'octubre',11=>'novembre',12=>'desembre');


function msn($accio){
	if($accio){
		echo "<div class=\"ok\"></div>";	
	}else{
		echo "<div class=\"error\"></div>";		
	}
}



function msn_error($error){
	echo "<div class=\"msn_error\">".$error."</div>";	
}
function dia_setmana($mes,$dia,$any){
	return (date("w", mktime(0, 0, 0, $mes, $dia, $any)));
}



function convert_num8($any,$mes,$dia){
	if(strlen($mes)==1){ $mes="0".$mes;}
	if(strlen($dia)==1){ $dia="0".$dia;}
	return($any.$mes.$dia);
}

function edat($dia,$mes,$any){
	if($any!=0)	{
		$edat=date('Y')-$any;
		if($mes!=0){
			if(date('m')<$mes){
				$edat=$edat-1;	
			}
			elseif(date('m')==$mes){
				if(date('d')<$dia){
					$edat=$edat-1;	
				}	
			}
		}
		return ($edat." anys");
	}
	return ("");

}
function veure_telf($telf){
	return (substr($telf,0,3)." ".substr($telf,3,3)." ".substr($telf,06,3));
	
}

function format_data_intranet($data){
	if(strlen($data)==8){
	return ( substr($data,0,4)."-".substr($data,4,2)."-".substr($data,6,2));
	}
}

function format_data($data){
	if(strlen($data)==8){
	return ( substr($data,6,2)."/".substr($data,4,2)."/".substr($data,0,4));
	}
}
function format_data2($data){
	if(strlen($data)==8){
	return ( substr($data,4,2)."/".substr($data,6,2)."/".substr($data,0,4));
	}
}
function format_data3($data){
	$dies=array('Dilluns','Dimarts','Dimecres','Dijous','Divendres','Dissabte','Diumenge');
	return $dies[$data-1];
}
function neteja_data($data){
	return (str_replace('-','',$data));	
}

function veure_a4_arxius($id_arxiu,$peke){
	require("conexio.php");
	$con=mysqli_query($cnx_cesportiu,"select * from a4_arxius where id_arxiu=".$id_arxiu);
	$fila=mysqli_fetch_array($con);
	$extensio=substr($fila["nom_fitxer"],-3);
	
	$ruta="carregues/activitats/".$fila["nom_fitxer"];
	if(!file_exists($ruta)){
		$ruta="../carregues/activitats/".$fila["nom_fitxer"];	
	}
	
	
	//echo $ruta;
	if(($extensio=="gif")||($extensio=="jpg")||($extensio=="png")){
		echo "<img src=\"".$ruta."\" alt=\"".StripSlashes($fila["nom_arxiu"])."\"";
		if($peke==1){ echo " width=\"200px\"";} echo ">";
		//if($fila["nom_arxiu"]!=""){ echo "<br><span class=\"nom_arxiu\">".StripSlashes($fila["nom_arxiu"])."</span>";}
	}else{
		echo "<a href=\"".$ruta."\" target=\"_blank\" class=\"extensio_".$extensio." links_arxius\">".StripSlashes($fila["nom_arxiu"])."</a>";
	}
	
}

function veure_extensio($tipuss){
	
	if($tipuss=="application/msword"){ $tipus="doc";}
	elseif($tipuss=="applicaton/octet-stream"){ $tipus="pdf";}
	elseif($tipuss=="application/pdf"){ $tipus="pdf";}
	elseif($tipuss=="image/pjpeg"){ $tipus="jpg";}
	elseif($tipuss=="image/jpeg"){ $tipus="jpg";}
	elseif($tipuss=="image/gif"){ $tipus="gif";}
	elseif($tipuss=="application/vnd.ms-excel"){ $tipus="xls";}
	elseif($tipus=="application/octet-stream"){ $ext="gpx";}
	elseif($tipuss=="application/pps"){ $tipus="pps";}	
	return ($tipus);
	
}
function nom_lletra($grup){
	$g=array(0=>"",1=>"A",2=>"B",3=>"C",4=>"D",5=>"E");
	return $g[$grup];
	
}
function curs_actual(){
	require("conexio.php");
	$con=mysqli_query($cnx_cesportiu,"select max(id_curs) from Zcurs");
	$fila=mysqli_fetch_array($con);
	return($fila["max(id_curs)"]);
}

function veure_curs_actual(){
	require("conexio.php");
	$con=mysqli_query($cnx_cesportiu,"select max(id_curs) from Zcurs");
	$fila=mysqli_fetch_array($con);
	$con=mysqli_query($cnx_cesportiu,"select * from Zcurs where id_curs=".$fila["max(id_curs)"]);
	$fila=mysqli_fetch_array($con);
	return ($fila["any1"]."/".$fila["any2"]);
}
$dia_joc=array(1=>"dissabte",2=>"diumenge");

function cat($any){
	$edat=date('Y')-$any;
	require("../conexio.php");
	$con=mysqli_query($cnx_cesportiu,"select * from Zcategories where inici<=".$edat." and fi>=".$edat);
	$fila=mysqli_fetch_array($con);
	return ($fila["id_cat"]);
	
}
function convert2($num){
	if(strlen($num)==1){ $num="0".$num;}
	return ($num);
}

function nom_act($id_ins){
	require("../conexio.php");
	$con=mysqli_query($cnx_cesportiu,"select * from i1_inscripcions,a1_activitats where 
	id_ins=".$id_ins." and 
	i1_inscripcions.id_act=a1_activitats.id_act");
	$fila=mysqli_fetch_array($con);
	echo StripSlashes($fila["nom_act"]);
	if($fila["id_jornada"]!=0){ 
		$con1=mysqli_query($cnx_cesportiu,"SELECT * from a2_jornades where id_jornada=".$fila["id_jornada"]);
		$fila1=mysqli_fetch_array($con1);
		echo " : ".StripSlashes($fila1["nom_jornada"])." : ".format_data($fila1["data"]);
	}else{
		echo format_data($fila["data"]);
	}
	if($fila["nom_ins"]!=""){ echo " : ".StripSlashes($fila["nom_ins"]);}
	
	
}



function mostrar_act1($con_gen,$con_uni,$capes){


	echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />";
 	require("conexio.php");
	
	$a=1;$data=array();
	
	
	$SQL_act="select * from a1_activitats where id_act!=0 ";
	if($con_gen!=""){ $SQL_act.=" and ".$con_gen." ";}
	if($con_uni!=""){ $SQL_act.=" and ".$con_uni;}
	$SQL_act.=" and NOT EXISTS(select * from a2_jornades WHERE a1_activitats.id_act = a2_jornades.id_act) ";
	$con=mysqli_query($cnx_cesportiu,$SQL_act);
	//echo $SQL_act;
	while($fila=mysqli_fetch_array($con)){
	   $data[$a]=$fila["data"];
		$a++;
	}
	
	$SQL_jor="select * from a2_jornades";
	if($con_gen!=""){ $SQL_jor.=" where ".$con_gen;}
	if($con_uni!=""){ 
		if($con_gen!=""){ $SQL_jor.=" and ";}
		else{ $SQL_jor.=" where ";}
		$SQL_jor.="  id_act IN(select id_act from a1_activitats where ".$con_uni.")";
	}
	
	//echo $SQL_jor;
	$con=mysqli_query($cnx_cesportiu,$SQL_jor);
	while($fila=mysqli_fetch_array($con)){
		$data[$a]=$fila["data"];
		$a++;
	}
	
	if($con_gen=="portada=1 and data>=".date('Ymd')){sort($data);}else{rsort($data); }
	
	
	if($capes==1){ $capa=1;$item=0;}
	
	$val_ant="";
	foreach ($data as $key => $val) {
		
		$val=str_replace("'","",$val);
		
		if($val_ant!=$val){
			
		$con=mysqli_query($cnx_cesportiu,$SQL_jor." and data=".$val." ");
		while($fila=mysqli_fetch_array($con)){
			
			if($capes==1){
				if($item % 10==0){ 
					if($item!=0){ echo "</div>";}
					echo "<div id=\"capa".$capa."\" ";
					if($capa==1){ echo "class=\"visual_si\"";}else{ echo "class=\"visual_no\"";}
					echo ">";
					$capa++;
				} 
				$item++;	
			}
			
			
			$SQL="select n_nom_act from a1_activitats where id_act=".$fila["id_act"];
			$con1=mysqli_query($cnx_cesportiu,$SQL);
			$fila1=mysqli_fetch_array($con1);
			
 mostrar_act2($fila["id_act"],$fila1["n_nom_act"],$fila["id_jornada"],$fila["n_nom_jornada"],$fila["data"],$fila["nom_jornada"],$fila["id_muni"],$capes);
		}



		$con=mysqli_query($cnx_cesportiu,$SQL_act." and data='".$val.="'");
		while($fila=mysqli_fetch_array($con)){
			if($capes==1){
				if($item % 10==0){ 
					if($item!=0){ echo "</div>";}
					echo "<div id=\"capa".$capa."\" ";
					if($capa==1){ echo "class=\"visual_si\"";}else{ echo "class=\"visual_no\"";}
					echo ">";
					$capa++;
				} 
				$item++;	
			}
		    mostrar_act2($fila["id_act"],$fila["n_nom_act"],0,'',$fila["data"],$fila["nom_act"],$fila["id_muni"],$capes);
		}
		
		}
		
		$val_ant=str_replace("'","",$val);

	}
	if((isset($item))&&($item!=0)){echo "</div>";}
	
	if(($capes==1)&&($capa>2)){
		

		echo "<div class=\"posicio_boto_capes\">";
		for($c=1;$c<$capa;$c++){
			echo "<a href=\"#\" onclick=\"veure_capa('".$c."','".$capa."');\"  id=\"botoll".$c."\"
			class=\"";
			if($c==1){ echo "boto_capes_seleccionat";}else{ echo "boto_capes";}
			echo "\">".$c."</a>";	
		}
		echo "</div>";
		
		?>
		<script language="javascript">
		function veure_capa(c,capes){
			for(var a=1;a<capes;a++){
				
				if(a==c){
					eval('window.document.getElementById("capa'+a+'").className="visual_si"');
					eval('window.document.getElementById("botoll'+a+'").className="boto_capes_seleccionat"');
					
				}else{
					eval('window.document.getElementById("capa'+a+'").className="visual_no"');
					eval('window.document.getElementById("botoll'+a+'").className="boto_capes"');
				}
			}
			
		}
		</script>	
		
	<?php }

}



function mostrar_act2($id_act,$n_nom_act,$id_jornada,$n_nom_jornada,$data,$nom_act,$id_muni,$capes){

	echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />";
 	require("conexio.php");
	echo "<a href=\"activitats.php?nom_activitat=".$n_nom_act."&id_act=".$id_act;
	if($id_jornada!=0){ echo "&nom_jornada=".$n_nom_jornada."&id_jornada=".$id_jornada;}
	echo "\" class=\"activitats_".$capes."\">
	<span class=\"data\">".format_data($data)."</span>";
	if($id_muni!=0){
		$con=mysqli_query($cnx_cesportiu,"select * from Zmunicipis where id_muni=".$id_muni);
		$fila=mysqli_fetch_array($con);
		echo "<span class=\"nom_municipi\">".$fila["nom_muni"]."</span>";
	}
	echo "<br />
	<span class=\"titol_act\">".StripSlashes($nom_act)."</span><br />";
	
	
	$SQL="select count(*) from i1_inscripcions where obert=1 and id_act=".$id_act;
	if($id_jornada!=0){ $SQL.=" and (id_jornada=".$id_jornada." or id_jornada=0)";}
	$con=mysqli_query($cnx_cesportiu,$SQL);
	$fila=mysqli_fetch_array($con);
	if( ($fila["count(*)"]!=0)&&($data>date('Ymd')) ){
		echo "<strong>Inscripcions obertes</strong>";
	}
	else{
	
		if($data<=date('Ymd')){ $abans=0;}else{ $abans=1;} 
	
		$SQL="select text from a3_apartats where abans=".$abans."  and text!=''";
		if($id_jornada!=0){ $SQL.=" and id_jornada=".$id_jornada;}
		else{ $SQL.=" and id_act=".$id_act;}
		$SQL.=" order by ordre";
		//echo $SQL;
		$text="";
		$con1=mysqli_query($cnx_cesportiu,$SQL);
		if(mysqli_num_rows($con1)!=0){
			$fila1=mysqli_fetch_array($con1);
			$text=substr(strip_tags(StripSlashes($fila1["text"])),0,50)."...";
			echo $text;
		}
	
		if($text==""){
			$primer=1;
			
			$con1=mysqli_query($cnx_cesportiu,"select nom_apartat from a3_apartats where id_act=".$id_act." and 
			id_jornada=".$id_jornada." and abans=".$abans." order by ordre");
			while($fila1=mysqli_fetch_array($con1)){
				if($primer==0){ echo ", ";}$primer=0;
				echo StripSlashes($fila1["nom_apartat"]);
			}
		}
	
	}
	
	echo "</a>";

}

function visual_adreca($id_entidad){

    require("conexio.php");
    $con=mysqli_query($cnx_cesportiu,"select * from entidades where 
    id_entidad=".$id_entidad);
    $fila=mysqli_fetch_array($con);

    $text="";

    if($fila["id_adreca"]!=0){
        $id_adreca=$fila["id_adreca"];
        $numero=$fila["numero"];
        $id_pis=$fila["id_pis"];
        $id_porta=$fila["id_porta"];
        $id_escala=$fila["id_escala"];


        $con1=mysqli_query($cnx_adreces,"select * from adreca,tipus where 
            adreca.id_tipus=tipus.id_tipus and adreca.id_adreca=".$id_adreca);
        $fila1=mysqli_fetch_array($con1);
        $text= $fila1["nom_tipus"];

        if($fila1["id_article"]!=0){
            $con2=mysqli_query($cnx_adreces,"select * from articles where id_article=".$fila1["id_article"]);
            $fila2=mysqli_fetch_array($con2);
           $text.= " ".$fila2["nom_article"];
        }
        $text.= " ".StripSlashes($fila1["nom_adreca"]);

        if($numero!=0){
            $text.= ", ".$numero;
        }else{
            $text.= ", s/n";
        }
        if($id_escala!=0){
            $con1=mysqli_query($cnx_adreces,"select * from escala where id_escala=".$id_escala);
            $fila1=mysqli_fetch_array($con1);
            $text.= " ".$fila1["nom_escala"];
        }
        if(($id_pis!=0)||($id_porta!=0)){
            $text.= " ,";
        }
        if($id_pis!=0){
            $con1=mysqli_query($cnx_adreces,"select * from pis where id_pis=".$id_pis);
            $fila1=mysqli_fetch_array($con1);
            $text.= " ".$fila1["nom_pis"];
        }
        if($id_porta!=0){
            $con1=mysqli_query($cnx_adreces,"select * from porta where id_porta=".$id_porta);
            $fila1=mysqli_fetch_array($con1);
            $text.= " ".$fila1["nom_porta"];
        }


    }else{
        $text.= StripSlashes($fila["adreca"]);
    }

    return ($text);


}

$estat_entitat=array(0=>"Pendent de validar",1=>"Alta",2=>"Baixa");

?>