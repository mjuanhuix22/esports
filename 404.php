<?php include("conexio.php");
include("inc.php");?>
<html >
<head>
<link rel="icon" type="image/gif" href="favicon.gif" />


<title>Consell Esportiu de la Selva</title>

<link href="plantilles/estils_site.css" rel="stylesheet" type="text/css" />
<?php if(preg_match('/MSIE/i',$_SERVER['HTTP_USER_AGENT']))
{?>
	<link href="plantilles/estils_site_explorer.css" rel="stylesheet" type="text/css" />
<?php }?>

<link href="gral/estils.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="cont_body">



<div id="toolbar"><a href="ceselva.php" class="menu_dalt">La nostra oficina</a><span style="float:right; color:#FFFFFF; padding-top:5px; padding-left:5px;">|</span>
<a href="entitats/index.php" class="menu_dalt" target="_blank">Intranet de les entitats</a><a href="index.php"><img src="imatges/logoConsellEsportiuSelva_HoritzontalDegradat.png" alt="Consell Esportiu de la Selva" border="0" /></a>
</div>




<div id="menu1" style="margin-bottom:20px;">
	<ul>
	<?php 
	$enllac=array(1=>"index",2=>"index",3=>"parcs_salut",4=>"entitats",5=>"instalacions");
	
	$nom_enllac=array(1=>"Esports",2=>"Municipis",3=>"Parcs urbans de salut",4=>"Entitats",5=>"Instal&middot;lacions esportives");
	
	for($i=1;$i<=count($enllac);$i++){?>
		<li class="nivel1" style="width:<?php echo  (100/count($enllac));?>%"><a  href="<?php echo  $enllac[$i];?>.php" class="nivel1 
		<?php if(strpos($_SERVER['REQUEST_URI'],$enllac[$i])!=false){ echo "actiu";}else{ echo "no_actiu";}?>"><?php echo  $nom_enllac[$i];?></a>
		<?php 
		if($i==1){?>
        	<ul>
			<?php $con=mysqli_query($cnx_cesportiu,"select * from Zdeportes order by nom_deporte");
			while($fila=mysqli_fetch_array($con)){?>
			 	 <li><a href="esport.php?nom=<?php echo  $fila["n_nom_deporte"]?>" ><img src="imatges/deportes/<?php echo  $fila["id_deporte"]?>.gif" width="20" hspace="4" border="0" /><?php echo  $fila["nom_deporte"];?></a></li>
			  <?php }?>
       		</ul>
        <?php }elseif($i==2){?>
        	<ul>
            	<?php $con=mysqli_query($cnx_cesportiu,"select * from Zmunicipis order by nom_muni");
				while($fila=mysqli_fetch_array($con)){?>
				  <li style="width:230px;"><a href="municipi.php?nom=<?php echo  $fila["normalitza_muni"]?>" ><?php echo  $fila["nom_muni"];?></a></li>
                <?php }?>
             </ul>
		 <?php }?>
           
        </li>
	<?php }?>
    </ul>
</div>
<h3>Error 404 - La p&agrave;gina a la que est&agrave;s intentant accedir no existeix.</h3>
<div id="footer">
Passeig de Sant Salvador ,25-27 17430 Santa Coloma de Farners | 972 84 21 61 | 972 84 08 04 | info@selvaesports.cat |
<a id="totop-scroller" href="#page"></a>
</div>

</div>
</body>
</html>