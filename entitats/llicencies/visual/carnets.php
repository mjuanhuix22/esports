<?php session_start();
if ((!isset($_SESSION["id_usuesport"])) && (!isset($_SESSION["id_entidad"]))) {
    header("location:index.php");
}
include "../../../conexio.php"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <?php ini_set('default_charset', 'utf-8'); ?>
    <link rel="stylesheet" type="text/css" media="all" href="../../../estils.css" />
    <title>Carnets esportius</title>
</head>

<body>
    <?php

    $condicio = "";
    if ($_POST["id_curs"] != "") {
        $numero0 = strval($_POST["id_curs"]);
        $numero = (double)$numero0;
        $condicio .= " and llice_sol.id_curs=".'20';
    }
    if ($_POST["id_genere"] != "") {
        $condicio .= " and esportistes.id_genere=" . $_POST["id_genere"];
    }
    if ($_POST["id_entidad"] != "") {
        $condicio .= " and llice_sol.id_entidad=" . $_POST["id_entidad"];
    }
    if ($_POST["id_deporte"] != "") {
        $condicio .= " and llice_sol_esportista.id_deporte=" . $_POST["id_deporte"];
    }
    if ((isset($_POST["id_cat"])) && ($_POST["id_cat"] != "")) {
        $condicio .= " and llice_sol_esportista.id_cat=" . $_POST["id_cat"];
    }
    if ((isset($_POST["id_tipus"])) && ($_POST["id_tipus"] != "")) {
        $condicio .= " and llice_sol_esportista.id_tipus=" . $_POST["id_tipus"];
    }
    if ((isset($_POST["nom"])) && ($_POST["nom"] != "")) {
        $condicio .= " and esportistes.nom='" . $_POST["nom"] . "'";
    }
    if ((isset($_POST["cognom1"])) && ($_POST["cognom1"] != "")) {
        $condicio .= " and esportistes.cognom1='" . $_POST["cognom1"] . "'";
    }
    if ((isset($_POST["cognom2"])) && ($_POST["cognom2"] != "")) {
        $condicio .= " and esportistes.cognom2='" . $_POST["cognom2"] . "'";
    }
    if ((isset($_POST["data_inici"])) && ($_POST["data_inici"] != "")) {
        $condicio .= " and llice_sol.data_valida>='" . neteja_data($_POST["data_inici"]) . "'";
    }
    if ((isset($_POST["data_fi"])) && ($_POST["data_fi"] != "")) {
        $condicio .= " and llice_sol.data_valida<=  '" . neteja_data($_POST["data_fi"]) . "'";
    }
    $SQLa = "SELECT identificador,esportistes.mes,esportistes.dia,esportistes.any,nom,cognom1,cognom2,nom_entidad,nom_genere,nom_deporte,nom_tipus,foto,data_valida,any1,any2,nom_cat
FROM llice_sol_esportista,llice_sol,esportistes,Zcategories,Zllice_tipus,Zgeneres,Zdeportes,Zcurs, entidades where
llice_sol.data_valida is not null and
llice_sol.id_curs=Zcurs.id_curs and
llice_sol.id_entidad=entidades.id_entidad and
llice_sol.id_sol=llice_sol_esportista.id_sol and
llice_sol_esportista.id_deporte=Zdeportes.id_deporte and
llice_sol_esportista.id_cat=Zcategories.id_cat and
llice_sol_esportista.id_tipus=Zllice_tipus.id_tipus and
llice_sol_esportista.id_esportista=esportistes.id_esportista and
esportistes.id_genere=Zgeneres.id_genere " . $condicio . "
group by esportistes.id_esportista,entidades.id_entidad,Zcurs.id_curs order by esportistes.nom,esportistes.cognom1";

    //echo $SQL;
    $con = mysqli_query($cnx_cesportiu, $SQLa);
    while ($fila = mysqli_fetch_array($con)) {

        echo "<div class='carnets'>";
        echo "<div class='fons_carnet1'>";

        echo "<div style='float:left;'>";
        echo "<img src=\"../../../carregues/llicencies/" . $fila["foto"] . "\" width='80' />";

        echo "</div><div style='float: left' >";
        echo "<label>NOM </label><strong><div class='text'> " . $fila["nom"] . " " . $fila["cognom1"] . " " . $fila["cognom2"] . "</strong></div>";
        echo "<label>ENTITAT </label><div class='text'>" . StripSlashes($fila["nom_entidad"]) . "</div>";
        echo "<label>ESPORTS </label><div class='text'>" . StripSlashes($fila["nom_deporte"]) . "</div>";
        echo "<label>CATEGORIA </label><div class='text'>" . $fila["nom_cat"] . " " . $fila["nom_genere"] . "</div>";
        echo "<label>CATSALUT </label><div class='text'>" . $fila["identificador"] . "</div>";
        echo "<label>LLICÈNCIA </label><div class='text'>" . $fila["nom_tipus"] . "</div>";

        echo "</div>";
        echo "</div>";
        echo "<div class='fons_carnet2'></div>";

        echo "</div>";
    }
    ?>
</body>

</html>