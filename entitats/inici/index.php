
<fieldset>
<legend>Què és la intranet de les entitats</legend>

 <p>Els responsables i coordinadors d'entitats podeu accedir a aquesta intranet per:</p>
 <ul>
   <li><strong>Actualitzar les dades de la vostra entitat</strong><br />

   Podeu fer difusió de la vostra entitat entitat a través del web del Consell Esportiu de la Selva.
 <br />
    <br />
   </li>
   <li><strong>Actualitzar els esports que practiqueu</strong><br />
     Per poder fer inscripcions on-line &eacute;s important que tingueu actualitzats els esports.<br />
     <br />
   </li>
   <li><strong>Gestionar les llic&egrave;ncies esportives</strong><br />
     Durant tot el curs escolar podeu donar d'alta on-line les assegurances dels vostres esportistes. <br />
     <br />
   </li>
   <li><strong>Esports d'associaci&oacute;</strong><br />
     Donar d'alta  equips a les diferents lligues i actualitzar els resultats setmanalment. <br>
  </li>
 </ul>
<?php if(!isset($_SESSION["id_entidad"])){?>
<p> Les entitat que estan donades d'alta a aquesta intranet s&oacute;n:</p>
<select name="">
<option value=""></option>
<?php $SQL="select * from entidades where estat=1 and intern=1 order by nom_entidad";
	$con=mysqli_query($cnx_cesportiu,$SQL);
	while($fila=mysqli_fetch_array($con)){?>
    	<option value=""><?php echo StripSlashes($fila["nom_entidad"]);?></option>
    <?php }?>
 </select>   

<p><br>
<a href="../alta_entitat.php">
  Si no estu en el llistat ompliu el següent formulari
</a>
</p>
<?php }?>
</fieldset>
