<?php
session_start();
if(isset($_POST["tancar_sessio"])){
	unset($_SESSION["id_entidad"]);
}
include("../conexio.php"); 
include("../inc.php");


if(isset($_POST["entrar_sessio"])){

	$SQL="select * from arbitres where 
	login='".neteja($_POST["log_entitat"])."' 
	and psw='".md5(neteja($_POST["psw_entitat"]))."';"; 
	
	$con=mysqli_query($cnx_cesportiu,$SQL);
	if(mysqli_num_rows($con)==1){

		$fila=mysqli_fetch_array($con);
		$_SESSION["id_arbitre"]=$fila["id_arbitre"];	
		$_SESSION["nom_arbitre"]=$fila["nom_arbitre"];
		$_SESSION["id_entidad"]=$fila["id_entidad"];
		if($fila["canviar_psw"]==1){$carpeta1="personal_psw";}
		else{ $carpeta1="associacio";}
	}
	else{
		$SQL="select * from entidades where 
		login='".neteja($_POST["log_entitat"])."' 
		and psw='".md5(neteja($_POST["psw_entitat"]))."' 
		and estat=1";

		$con=mysqli_query($cnx_cesportiu,$SQL);
		if(mysqli_num_rows($con)==1){
	
			$fila=mysqli_fetch_array($con);
			$_SESSION["id_entidad"]=$fila["id_entidad"];
			$_SESSION["nom_entidad"]=$fila["nom_entidad"];

			if($fila["canviar_psw"]==1){$carpeta1="personal_psw";}
			else{
				
				$SQL="select count(*) from entidades_deportes,Zdeportes,ass_1config,ass_2lliga,ass_3comu,ass_3equips where 
				entidades_deportes.id_entidad=".$_SESSION["id_entidad"]." and 
				entidades_deportes.id_deporte=Zdeportes.id_deporte and 
				Zdeportes.id_deporte=ass_1config.id_deporte and 
				ass_1config.id_curs=".curs_actual()." and 
				ass_1config.obert=1 and 
				ass_1config.id_config=ass_2lliga.id_config and 
				ass_2lliga.id_lliga=ass_3comu.id_lliga and 
				ass_3comu.id_entidad=".$_SESSION["id_entidad"]." and 
				ass_3comu.id_comu=ass_3equips.id_comu and (
				(ass_3equips.id_equip IN (select id_equip1 from ass_5partits where data_real<=".date('Ymd')."))
				or
				(ass_3equips.id_equip IN (select id_equip2 from ass_5partits where data_real<=".date('Ymd')."))
				)";
				$con1=mysqli_query($cnx_cesportiu,$SQL);
				$fila1=mysqli_fetch_array($con1);
				if($fila1["count(*)"]!=0){ $carpeta1="associacio";}
				else{ $carpeta1="inici";}
			}
		}
	}
}
elseif(isset($_SESSION["id_usuesport"]))
{
    if ((isset($_POST["id_entidad"]))&&(!isset($_SESSION["id_entidad"])))
    {
        $_SESSION["id_entidad"] = $_POST["id_entidad"];
        $_SESSION["nom_entidad"] = $_POST["nom_entidad"];

    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php ini_set('default_charset','utf-8');?>
<title>Intranet de les entitats esportives selvatanes</title>
<link rel="icon" type="image/gif" href="../favicon.gif" />


<link href="../estils.css" rel="stylesheet" type="text/css" />
<style type="text/css">
	body{
	background-color:#E8E8E8;

	}	
</style>
<script language="javascript">
function recarrega(){
	window.document.form_recarrega.submit();
}
</script>

<script language="javascript" src="../scripts.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="../calendar/jsDatePick_ltr.min.css" />
<script type="text/javascript" src="../calendar/jsDatePick.min.1.3.js"></script>
<body <?php if( (isset($_POST["entrar_sessio"])) && (isset($_SESSION["id_entidad"])) ){?> onload="recarrega()"<?php }?>>

<?php if(isset($carpeta1)){?>
    	<form method="post" name="form_recarrega">
        <input type="hidden" name="carpeta1" value="<?php echo $carpeta1;?>" />
        </form>
<?php }?>

<div id="contingut_body">
    <div style="background-color:#1D1E1C"><img src="../imatges/logoConsellEsportiuSelva_HoritzontalDegradat.png"  alt="logotip" /></div>

<?php if(isset($_SESSION["id_entidad"])){
	include("dins.php");
}else{
	include("fora.php");
	include("inici/index.php");
}?>

<a href="http://www.selvaesports.cat" class="peu">www.selvaesports.cat</a>
</div>

</body>
</html>