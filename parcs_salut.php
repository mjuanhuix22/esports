<?php include("conexio.php");
include("inc.php");

$titol="Parcs urbans de salut";
if(isset($_GET["municipi"])){ 
	$municipi=neteja($_GET["municipi"]);

	$conM=mysqli_query($cnx_cesportiu,"select * from Zmunicipis where 
     normalitza_muni='".$municipi."' and (
	(id_muni IN (select id_muni from parcs ))
	or
	(id_muni IN (select id_muni from parcs_muni))
	)");
	if(mysqli_num_rows($conM)==0){ header("location:parcs_salut.php");}
	$filaM=mysqli_fetch_array($conM);
	$anterior=$titol;
	$nom_muni=StripSlashes($filaM["nom_muni"]);
	$id_muni=$filaM["id_muni"];
	$titol="Parcs urbans i itineraris saludables de ".$nom_muni;
}


include "plantilles/sobre.php";
?>


<?php if(isset($id_muni)){?>
	 <?php $con=mysqli_query($cnx_cesportiu,"select * from parcs where id_muni=".$id_muni);
	 if(mysqli_num_rows($con)!=0){?>
		<h2>Dinamització dels parcs urbans de salut</h2>
        
       		<style type="text/css">

			<?php 
			$colors=array(1=>"#0B2161",2=>"#886A08",3=>"#610B21",4=>"#006600");
			
			$i=1;
			$con=mysqli_query($cnx_cesportiu,"select parcs.id_parc,hora from parcs,parcs_dinamitza where 
			id_muni=".$id_muni." and parcs.id_parc=parcs_dinamitza.id_parc
			and data>=".date('Ymd')." 
			group by parcs.id_parc,hora");
			while($fila=mysqli_fetch_array($con)){
				
				$hora=$fila["hora"];$hora=str_replace(":","",$hora);
				$parc[$fila["id_parc"]][$hora]=$i;
				echo ".color_parc_dinamitza_".$fila["id_parc"]."_".$hora."{
					color:".$colors[$i].";
					font-weight:bold;	
				}";
				echo ".color_parc_".$fila["id_parc"]."{
					color:".$colors[$i].";
					font-weight:bold;	
				}";
				$i++;
			}?>
		</style>
	
    	<?php $con=mysqli_query($cnx_cesportiu,"select max(data) from parcs_dinamitza,parcs where 
		parcs_dinamitza.id_parc=parcs.id_parc and 
		parcs.id_muni=".$id_muni." and data>=".date('Ymd'));
		$fila=mysqli_fetch_array($con);
		if($fila["max(data)"]!= NULL ){
			$data_fi=$fila["max(data)"];
			$any_fi=substr($data_fi,0,4);$mes_fi=substr($data_fi,4,2);
			
			$con=mysqli_query($cnx_cesportiu,"select min(data) from parcs_dinamitza,parcs where 
			parcs_dinamitza.id_parc=parcs.id_parc and 
			parcs.id_muni=".$id_muni." and data>=".date('Ymd'));
			$fila=mysqli_fetch_array($con);
			$data_inici=$fila["min(data)"]; 
			$any_inici=substr($data_inici,0,4);$mes_inici=substr($data_inici,4,2);
			
			$total_mesos=0;
			
			for($any=$any_inici;$any<=$any_fi;$any++){
				if($any==$any_fi){ $m_f=$mes_fi;}else{ $m_f=12;}
				if($any==$any_inici){ $m_i=$mes_inici;}else{ $m_i=1;}
				for($mes=$m_i;$mes<=$m_f;$mes++){?>
				
                	<?php $total_mesos++; if($total_mesos==5){ echo "<div class=\"neteja\" style=\"height:0px\"></div>"; $total_mesos=1;}?>
                    
					<div class="mes_calendari">
					<div class="nom_mes_parc">
					<?php echo  $nom_mes[(int)$mes]." ".$any;?></div>
					<?php $dia=1;
					
					while($dia<=total_dies($mes,$any)){?>
						<div class="neteja">
						<?php for($a=1;$a<=7;$a++){
								$p=$a; if($a==7){ $p=0;}
							
								if( ($p==dia_setmana($mes,$dia,$any))
								&&($dia<=total_dies($mes,$any)) ){
									$entra=0;
									echo "<div ";
									if(date('Ymd')<=(convert_num8($any,$mes,$dia))){
										$con1=mysqli_query($cnx_cesportiu,"select parcs.id_parc,hora from parcs_dinamitza,parcs where 
										parcs_dinamitza.id_parc=parcs.id_parc and 
										parcs.id_muni=".$id_muni." and 
										data=".convert_num8($any,$mes,$dia)." order by parcs.id_parc,hora");
										$imatge="";
										while($fila1=mysqli_fetch_array($con1)){
											$entra=1;
											$hora=$fila1["hora"];$hora=str_replace(":","",$hora);
											if($imatge!=""){ $imatge.="_";}
											$imatge.=$parc[$fila1["id_parc"]][$hora];
										}
										if(mysqli_num_rows($con1)!=0){
											echo "style=\"background-image:url(imatges/parcs/".$imatge.".gif);
											 background-repeat:no-repeat;\"";
										}
										
										
										
									} 
									echo " class=\"casella ";
									if($entra==0){
											echo " blanc";
									}else{ echo "lletra_blanca";}
									echo "\">";
									echo $dia;
									echo "</div>";
									$dia++;
								}else{?>
									<div  class="casella"></div>
								<?php }?>
						<?php }?>
						</div>
					<?php }?>
					</div>
				
				<?php }
				
			}
			
			echo "<div class=\"neteja\"></div>";
			$con=mysqli_query($cnx_cesportiu,"select * from parcs_dinamitza,parcs where 
			parcs_dinamitza.id_parc=parcs.id_parc and 
			parcs.id_muni=".$id_muni." and data>=".date('Ymd')." group by parcs.id_parc,hora");
			while($fila=mysqli_fetch_array($con)){
				$hora=$fila["hora"];$hora=str_replace(":","",$hora);
				echo "<p class=\"color_parc_dinamitza_".$fila["id_parc"]."_".$hora."\">".StripSlashes($fila["adreca"])." ".substr($fila["hora"],0,5)."h.</p>";
			}
		}else{
			echo "Actualment no hi ha cap activitat programada en els parcs d'aquest municipi";	
		}?>
        
    	<div class="neteja">
		Els Parcs estan especialment dirigits a adults i gent gran.
        Si voleu proposar una dinamització poseu-vos en contacte amb info@selvaesports.cat
        </div>
        
        <h2>Parcs urbans de salut</h2>
		Cada Parc Urbà de Salut compta amb un plafó general amb informació per utilitzar-lo correctament.      
     
		<?php 
		$con=mysqli_query($cnx_cesportiu,"select * from parcs where id_muni=".$id_muni);
		while($fila=mysqli_fetch_array($con)){?>
	
			<div class="color_parc_<?php echo  $fila["id_parc"];?>" style="margin-top:20px; font-weight:bold; font-size:14px;"><?php echo  $fila["adreca"]?><br />
			<img src="carregues/parcs_urbans/<?php echo  $fila["foto"]?>"   alt="<?php echo  $fila["adreca"];?>" />
			</div>
		<?php }
	 }?>
	
    <?php $con=mysqli_query($cnx_cesportiu,"select * from parcs_muni where id_muni=".$id_muni);
	if(mysqli_num_rows($con)==1){?>
    	<h2>Itineraris saludables</h2>
<p> Els Itineraris Saludables s&oacute;n recorreguts que transcorren tant per dins   el nucli urb&agrave; com pels seus entorns. Estan senyalitzats de manera   permanent per facilitar la pr&agrave;ctica d&rsquo;activitat f&iacute;sica moderada,   principalment, per promoure l&rsquo;acci&oacute; de caminar. <br />
      <br />
      Una Xarxa d&rsquo;Itineraris Saludables &eacute;s un conjunt d&rsquo;Itineraris Saludables connectats entre ells. <br />
      <br />
      El model en xarxa presenta avantatges sobre els itineraris inconnexos   ja que d&oacute;na gran autonomia a l&rsquo;usuari per crear nous recorreguts segons   les seves necessitats. Atenent a la seva capacitat f&iacute;sica, als   despla&ccedil;aments del dia a dia o a altres criteris, l&rsquo;usuari pot triar   l&rsquo;itinerari que m&eacute;s li convingui. Dins la Xarxa, es recomanen 3   Itineraris Saludables: un de curt, un de mitj&agrave; i un de llarg. </p>
<p> Caminar per una Xarxa d&rsquo;Itineraris Saludables permet prendre   consci&egrave;ncia del temps que es triga en rec&oacute;rrer les dist&agrave;ncies   recomanades.<br />
      <br />
    Cada Xarxa d'Itineraris Saludables compta amb un plaf&oacute; d'informaci&oacute;   general on es descriu la Xarxa i les seves caracter&iacute;stiques, s'explica   el funcionament de la senyal&iacute;stica i es donen recomanacions per a la   realitzaci&oacute; d'activitat f&iacute;sica moderada aprofitant els Itineraris   Saludables.</p>
        
		<?php $fila=mysqli_fetch_array($con); 
		
		if($fila["foto"]!=""){?>
			 <img src="carregues/itineraris_salut/<?php echo  $fila["foto"];?>" width="700"  alt="Xarxa d'itineraris saludables a <?php echo  $nom_muni;?>" />
		<?php }
		
		if($fila["enllac"]!=""){?>
        <hr />
		<a href="<?php echo  $fila["enllac"];?>" target="_blank" class="enllac_web">
	 <img src="imatges/itineraris_logo.png" alt="Logo itineraris saludables i parcs urbans de salut" width="225" height="77" hspace="5" align="left"  border="0"/><br />
	 M&eacute;s informaci&oacute;:<br />
	<?php echo  $fila["enllac"];?></a>
        <?php }?>
    <?php }?>
    
<?php }else{?>

<p><img src="imatges/parc_salut_sils.jpg" alt="Innaguraci&oacute; del Parc de Salut de Sils" width="200" height="148" hspace="5" align="right" />Els Parcs   Urbans de Salut s&oacute;n conjunts d&rsquo;aparells que conformen un circuit   dissenyats per realitzar exercici f&iacute;sic moderat. Estan instal&middot;lats a   l&rsquo;aire lliure i a l&rsquo;espai p&uacute;blic, en aquells punts del municipi on hi ha   una important aflu&egrave;ncia de persones i recursos socials i esportius   pr&ograve;xims. </p>
<p> Han estat dissenyats per una comissi&oacute; multidisciplin&agrave;ria d&rsquo;experts per   tal de garantir-ne la seguretat, l&rsquo;accessibilitat i la idone&iuml;tat dels   aparells i dels exercicis que s&rsquo;hi realitzen aix&iacute; com un treball   homogeni de tot el cos.&nbsp; El resultat d'aquest treball de disseny es   troba reflectit a la &ldquo;Guia d&rsquo;Itineraris Saludables i Parcs Urbans de   Salut&nbsp; &ndash;&nbsp; Aproximaci&oacute; metodol&ograve;gica al seu disseny&rdquo; </p>
    <p> Els Parcs Urbans de Salut tenen com a objectiu la salut i no el   rendiment esportiu. Els aparells que formen aquests parcs estan enfocats   a realitzar un treball de prevenci&oacute;, millora i/o manteniment dels   moviments necessaris per a les activitats de la vida di&agrave;ria per aix&iacute;   millorar la qualitat de vida de les persones.<br />
      <br />
      Els aparells que formen els parcs Urbans de Salut s&rsquo;han dissenyat tenint en compte els seg&uuml;ents criteris:<br />
    </p>
<p> <img src="imatges/parc_salut_sils_2.jpg" alt="Innaguraci&oacute; del Parc de Salut de Sils" width="200" height="150" hspace="5" align="right" />&bull; S&rsquo;han escollit aparells que per fer treball anal&iacute;tic ex:(glutis) i general ex:(el&middot;l&iacute;ptica)<br />
      &bull; Estan pensats perqu&egrave; els pugui utilitzar gran part de la poblaci&oacute;. <br />
      &bull; S&oacute;n exercicis amb m&iacute;nim impacte articular.<br />
      &bull; Estan pensats per respectar i millorar, en la mesura que es pugui, la higiene postural. <br />
      &bull; Tenen transfer&egrave;ncia positiva a les activitats de la vida di&agrave;ria.<br />
      &bull; S&rsquo;han seleccionat aparells que ofereixin certa resist&egrave;ncia al moviment per disminuir la velocitat d&rsquo;execuci&oacute; i controlar-la.<br />
      &bull; S&oacute;n moviments guiats i de f&agrave;cil execuci&oacute;.<br />
      &bull; S&oacute;n aparells segurs (baranes de recolzament, material antilliscant, bores dels aparells arrodomides).<br />
      <br />
      Els Parcs estan especialment dirigits a adults i gent gran.<br />
    Cada Parc Urb&agrave; de Salut compta amb un plaf&oacute; general amb informaci&oacute; per utilitzar-lo correctament.</p>
  
  <p class="lletraGrisa">  En la comarca de la Selva hi ha <strong><?php $con=mysqli_query($cnx_cesportiu,"select count(*) from parcs");
   $fila=mysqli_fetch_array($con); echo $fila["count(*)"];?> parcs</strong> distribuits entre els següents municipis:</p>
    
    
   
		<?php $con=mysqli_query($cnx_cesportiu,"select * from Zmunicipis where 
		(id_muni IN (select id_muni from parcs ))
		or
        (id_muni IN (select id_muni from parcs_muni))
		order by nom_muni");
        while($fila=mysqli_fetch_array($con)){ ?>
        	
        	<fieldset class="fiel_parcs">
            <legend class="leg_parcs"><?php echo  $fila["nom_muni"];?></legend>
            <a href="parcs_salut.php?municipi=<?php echo  $fila["normalitza_muni"];?>" class="parcs">
            <img src="carregues/parcs_urbans_muni/parc_<?php echo  normalitza($fila["nom_muni"]);?>.jpg" width="137" hspace="0" vspace="0" height="137" border="0"  /></a>
            
            <?php /*$con1=mysqli_query($cnx_cesportiu,"select foto from parcs where id_muni=".$fila["id_muni"]);
			while($fila1=mysqli_fetch_array($con1)){?>

			}
			$con1=mysqli_query($cnx_cesportiu,"select foto from parcs_muni where id_muni=".$fila["id_muni"]);
			while($fila1=mysqli_fetch_array($con1)){ ?>
            	<img src="carregues/itineraris_salut/<?php echo  $fila1["foto"];?>" height="120" />
			<?php }*/?>
            
            </fieldset>
            

        <?php }?>
   
    
   <a href="http://www.itinerarisiparcsdesalut.cat/" target="_blank" class="enllac_web">
 <img src="imatges/itineraris_logo.png" alt="Logo itineraris saludables i parcs urbans de salut" width="225" height="77" hspace="5" align="left" border="0" /><br />
 M&eacute;s informaci&oacute;:<br />
	http://www.itinerarisiparcsdesalut.cat</a>
    
<?php }?>



<?php include "plantilles/sota.php";?>