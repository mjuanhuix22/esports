<?php include("conexio.php");
include("inc.php");
$titol="Consell Esportiu de la Selva";


include "plantilles/sobre.php";?>

    <p>Els objectius del Consell Esportiu de la Selva són:</p>
    <ul>
        <li>Foment, l'organització i la promoció de l'activitat esportiva en edat escolar dins de la comarca.</li>
        <li>Coordinar i organitzar les activitats esportives en edat escolar de la comarca.</li>
        <li>Assessorar als ajuntaments, consells comarcals, clubs, escoles i altres entitats de la seva demarcació, en l'activitat esportiva a càrrec seu i col·laborar en la seva execució.</li>
        <li>Col·laborar amb l'Escola Catalana de l'Esport i altres organismes competents en l'organització de cursos de formació i perfeccionament, amb l'objecte de cercar la millora qualitativa del personal tècnic esportiu que es dedica a l'esport escolar.</li>
        <li>Col·laborar amb les administracions i les entitats titulars per tal d'impulsar la millor utilització de les instal•lacions esportives de la comarca o àmbit territorial corresponent.</li>
        <li>Promoció i gestió d'instal·lacions esportives i serveis complementaris que faciliti i potenciï la pràctica esportiva.</li>
        <li>Executar o gestionar la política esportiva de la comarca segons convenis establerts amb la Generalitat de Catalunya, ajuntaments, consells comarcals i altres entitats públiques o privades competents.</li>
    </ul>


  <p class="nom_municipi">&nbsp;</p>
  <p class="nom_municipi">Dades de contacte:</p>
  <p>Passeig Sant Salvador,25-27 17430 Santa Coloma de Farners</p>
  <p><img src="imatges/icona_mail_b.gif">Adreça electrònica: info@selvaesports.cat</p>
  <p><img src="imatges/icona_tel_b.gif">Telèfon:972 84 35 22</p>
    <p><img src="imatges/icona_internet.gif">Web: www.selvaesports.cat</p>
  <p><img src="imatges/icona_horari.gif">Horari de dilluns a divendres de 9h a 14h</p>


    <img src="imatges/logo/ConsellEsportiuSelva_HoritzontalDegradat.jpg" width="320px"  alt="Consell Esportiu de la Selva">
    <img src="imatges/logo/ConsellEsportiuSelva_HoritzontalNegre.jpg" width="320px"  alt="Consell Esportiu de la Selva">


    <img src="imatges/logo/ConsellEsportiuSelva_HoritzontalBlau.jpg" width="320px" alt="Consell Esportiu de la Selva">
    <img src="imatges/logo/ConsellEsportiuSelva_HoritzontalVerd.jpg" width="320px"  alt="Consell Esportiu de la Selva">
    <img src="imatges/logo/ConsellEsportiuSelva_HoritzontalGrana.jpg" width="320px"  alt="Consell Esportiu de la Selva">
    <br>


    <img src="imatges/logo/ConsellEsportiuSelva_VerticalDegradat.jpg" width="200px"  alt="Consell Esportiu de la Selva">
    <img src="imatges/logo/ConsellEsportiuSelva_VerticalNegre.jpg" width="200px"  alt="Consell Esportiu de la Selva">
    <img src="imatges/logo/ConsellEsportiuSelva_VerticalBlau.jpg" width="200px"  alt="Consell Esportiu de la Selva">
    <img src="imatges/logo/ConsellEsportiuSelva_VerticalVerd.jpg" width="200px"  alt="Consell Esportiu de la Selva">
    <img src="imatges/logo/ConsellEsportiuSelva_VerticalGrana.jpg" width="200px"  alt="Consell Esportiu de la Selva">


<?php include "plantilles/sota.php";?>