<?php include("conexio.php");
include("inc.php");


if(!isset($_GET["nom"])){ header("location:index.php");}
$nom=neteja($_GET["nom"]);

$conA=mysqli_query($cnx_cesportiu,"select * from Zmunicipis where normalitza_muni='".$nom."';");
if(mysqli_num_rows($conA)==0){ header("location:index.php");}
$filaA=mysqli_fetch_array($conA);
$titol=StripSlashes($filaA["nom_muni"]);
$id_muni=$filaA["id_muni"];
$normalitza_muni=$filaA["normalitza_muni"];

include "plantilles/sobre.php";





$SQL="select count(*) from a1_activitats where id_muni=".$id_muni;
$con1=mysqli_query($cnx_cesportiu,$SQL);
$fila1=mysqli_fetch_array($con1);

$SQL="select count(*) from a2_jornades where id_muni=".$id_muni;
$con2=mysqli_query($cnx_cesportiu,$SQL);
$fila2=mysqli_fetch_array($con2);

$total=$fila1["count(*)"]+$fila2["count(*)"];
if($total!=0){
	$contingut='<h2>Activitats i jornades esportives</h2>
    <p>Des de l\'any 2008  el web del Consell Esportiu de la Selva ha publicat '.$total.' activitats esportives realitzades en  el municipi de '.$titol.'.</p>';
    
    mostrar_act1("id_muni=".$id_muni,"",1);
    

 }


$con=mysqli_query($cnx_cesportiu,"select * from parcs_muni where id_muni=".$id_muni);
if(mysqli_num_rows($con)!=0){
	$fila=mysqli_fetch_array($con);
	$contingut.='<h2>Parcs Urbans de Salut i Xarxa d\'intineraris Saludables</h2>
    
    
     <a href="parcs_salut.php?municipi='.$normalitza_muni.'" class="parcs"><img src="carregues/parcs_urbans_muni/parc_'. normalitza($titol).'.jpg" width="137" border="0"  /></a> 
    
   
    Els Parcs Urbans de Salut són conjunts d\'aparells que conformen un circuit dissenyats per realitzar exercici físic moderat. Estan instal·lats a l\'aire lliure i a l\'espai públic, en aquells punts del municipi on hi ha una important afluència de persones i recursos socials i esportius próxims.';
  
  


$contingut.='<p>';

$con=mysqli_query($cnx_cesportiu,"select * from parcs where id_muni=".$id_muni);
$contingut.= $titol." disposa de <strong>".mysqli_num_rows($con)." parcs de salut</strong> ubicats a:
<ul style=\"padding-left:200px;\">";
while($fila=mysqli_fetch_array($con)){
	$contingut.= "<li >".StripSlashes($fila["adreca"])."</li>";
}
$contingut.= "</ul></p><p>".$titol." també gaudeix <strong>d'Itineraris Saludables</strong> que transcorren tant per dins el nucli urbà com pels seus entorns. Aquests estan senyalitzats de manera permanent per facilitar la pràctica d'activitat física moderada, principalment, per promoure l'acció de caminar. </p>";

}


$contingut.="<h2>Instal·lacions esportives municipals</h2>";
$con=mysqli_query($cnx_cesportiu,"select * from instalacions where id_muni=".$id_muni);
$contingut.="<p>L'ajuntament ".  article($titol).$titol." ha construit ". mysqli_num_rows($con)." instal·lacions esportives publiques en el seu terme municipal.</p>";

while($fila=mysqli_fetch_array($con)){
        $contingut.="<a href=\"instalacions.php?nom=".$fila["n_nom_instalacio"]."\" class=\"instala\" style=\"height:170px;\">";
        $search=article($titol).$titol;
        $contingut.= StripSlashes(str_replace($search,'',StripSlashes($fila["nom_instalacio"])));
        $con1=mysqli_query($cnx_cesportiu,"select nom_foto from instalacions_fotos where id_instalacio=".$fila["id_instalacio"]);
        $fila1=mysqli_fetch_array($con1);
        $ruta="carregues/instalacions/".$fila1["nom_foto"];
        $contingut.= "<img src=\"".$ruta."\" width=\"187\" height=\"120\" border=\"0\" vspace=\"5\">";
    $contingut.="</a>";
 }



$contingut.="<h2>Entitats esportives</h2>";

$SQL="select * from entidades where id_muni=".$id_muni." and estat=1 order by nom_entidad";
$con=mysqli_query($cnx_cesportiu,$SQL);

$contingut.= "<p>Entre les ".mysqli_num_rows($con)." entitats esportives que estan registrades a ".$titol." es practiquen els esports de ";

$SQL1="select nom_deporte from entidades,entidades_deportes,Zdeportes where 
id_muni=".$id_muni." and entidades.id_entidad=entidades_deportes.id_entidad and 
entidades.estat=1 and 
entidades_deportes.id_deporte=Zdeportes.id_deporte group by Zdeportes.id_deporte order by nom_deporte";
$con1=mysqli_query($cnx_cesportiu,$SQL1); $p=1;
while($fila1=mysqli_fetch_array($con1)){
	if($p==0){ $contingut.= ", ";}$p=0;
	$contingut.= $fila1["nom_deporte"];
}
$contingut.= ".</p>";


while($fila=mysqli_fetch_array($con)){

	$contingut.= "<div><a href=\"entitats.php?nom=".$fila["n_entidad"]."\" class=\"llistat_entitats\">";

	$contingut.= "<div class=\"entitat_logo\">";
	$ruta="../carregues/entitats_logo/".$fila["id_entidad"].".jpg";
	if(!file_exists($ruta)){ $ruta="../carregues/entitats_logo/".$fila["id_entidad"].".gif";}
	if(file_exists($ruta)){
		$contingut.= "<img src=\"".$ruta."\" width=\"25\" height=\"19\">";
	}else{  
		$con1=mysqli_query($cnx_cesportiu,"select nom_deporte,Zdeportes.id_deporte 
		from entidades_deportes,Zdeportes where id_entidad=".$fila["id_entidad"]."
		and entidades_deportes.id_deporte=Zdeportes.id_deporte 
		order by nom_deporte");
		$fila1=mysqli_fetch_array($con1);
		$contingut.= "<img src=\"imatges/deportes/".$fila1["id_deporte"].".gif\" alt=\"".$fila1["nom_deporte"]."\" width=\"25\" height=\"19\">";
	}
	$contingut.= "</div>";
   
	
	
	$contingut.= "<div class=\"entitat_nom\">".StripSlashes($fila["nom_entidad"])."</div>";
	
	
	$contingut.= "<div class=\"entitat_esports\">";
	$p=1;
	$con1=mysqli_query($cnx_cesportiu,"select nom_deporte from entidades_deportes,Zdeportes where id_entidad=".$fila["id_entidad"]."
	and entidades_deportes.id_deporte=Zdeportes.id_deporte 
	order by nom_deporte");
	while($fila1=mysqli_fetch_array($con1)){
		if($p==0){ $contingut.= ", ";}$p=0;
		$contingut.= $fila1["nom_deporte"];
	}
	$contingut.= "</div>";
	
	$contingut.= "</a></div>";


}


echo $contingut;

include "plantilles/sota.php";

?>