<?php
include("conexio.php");
include("inc.php");

$titol="Valors en acció";


include "plantilles/sobre.php";

$contingut='
    <img src="imatges/valor_accio.jpg" class="dreta">
    <p>L’Agrupació Territorial de Consells Esportius de Girona ha creat una comissió de valors amb l’objectiu d’impulsar un programa de valors en l’esport que sigui comú a  tota la demarcació gironina a partir  del curs 2016-2017.</p>
    <p>Es tracta d’implementar un nou model d’esport escolar on els valors de l’esport prenguin un protagonisme essencial en les classificacions de les competicions esportives.</p>
    <p>El primer pas, que es desenvolupa al reprendre l’activitat esportiva després de les festes nadalenques del curs 2015-2016 en els esports d’associació, és la TARGETA VERDA, una de les actuacions que s’inclou en el programa “VALORS EN JOC”, dels Consells Esportius de Girona.</p>
    <p>Les targetes verdes són un reconeixement a diverses actuacions positives que poden tenir lloc dins el terreny de joc al llarg del moment esportiu i que senyalitza el Tutor de Joc en les següents situacions:</p>
    <p class="lletraGrisa">a)	Protocol de partit:</p>
    <ul>
        <li>Salutació inicial entre ambdós equips</li>
        <li>Salutació final entre ambdós equips.</li>
        <li>Salutació final al Tutor de Joc.</li>
    </ul>
    <p>*Per assolir una targeta verda, és imprescindible es donin que tots tres punts.</p>

    <p class="lletraGrisa">b)	Desenvolupament del partit:</p>
    <ul>
        <li>Quan s’ajudi a un company de l’altre equip en situació adversa.</li>
        <li>Quan s’animi reiteradament als companys d’equip durant el partit.</li>
        <li>Quan s’ajudi al Tutor de Joc en una situació de conflicte.</li>
        <li>Quan es reconegui que el Tutor de Joc ha pres una decisió errònia a favor del seu equip.</li>
    </ul>

    *En aquest apartat per assolir una targeta verda, calen 3 actuacions positives per part d’un equip. Cada tres actuacions positives, s’aconseguirà una targeta verda.

    <p class="lletraGrisa">c) Targeta verda directa:</p>
    <ul>
        <li>Quan es doni una situació excepcional que destaqui pels valors que aporti individual o col•lectivament.</li>
    </ul>

<p>Per aquest curs 2017-2018 els Consells Esportius de Girona implementen la targeta verda en les seves competicions, amb la intenció de crear una nova classificació que faci un recompte de targetes verdes per categoria i comarca, tancant la temporada amb una gal•la final de reconeixement als equips que assoleixin el major número de targetes verdes, en el marc dels Jocs Emporion.</p>

    <h1>Els valors entren en joc!</h1>

    <img src="imatges/valor_accio_text.jpg" >
</p> ';

echo $contingut;

include "plantilles/sota.php";