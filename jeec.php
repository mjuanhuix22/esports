<?php include("conexio.php");
include("inc.php");
$titol="Jocs Esportius Escolars de Catalunya";
if(date('m')>8){ $any_inici=date('Y');$any_fi=date('Y')+1;}else{ $any_inici=date('Y')-1;$any_fi=date('Y');}	
$con=mysqli_query($cnx_cesportiu,"select max(any1) from Zcurs");
$fila=mysqli_fetch_array($con);
$titol.=" per a la temporada ".$fila["max(any1)"]."/".($fila["max(any1)"]+1);

include "plantilles/sobre.php";

?>


<h3>Objectius</h3>
<p><img src="imatges/JEEC.jpg" alt="jeec" width="234" height="200" align="right" />Els Jocs Esportius Escolars de Catalunya s&oacute;n un programa planificat i   dirigit pel Consell Catal&agrave; de l'Esport de la Generalitat de Catalunya,   que t&eacute; per objectiu fomentar la participaci&oacute; en activitats esportives   del m&agrave;xim nombre de nois i noies escolars, aix&iacute; com facilitar la seva   iniciaci&oacute; en el m&oacute;n de l'esport.</p>
<h3>Fases dels jocs</h3>
    <p><strong>Interna:</strong> organitzada pel mateix centre.</p>
    <p><strong>Local:</strong> organitzada en l&rsquo;&agrave;mbit municipal.</p>
<p><strong>Comarcal:</strong> organitzada pel Consell Esportiu de la comarca.</p>
<p><strong>Territorial:</strong> organitzada per l&rsquo;Agrupaci&oacute; Territorial de Consells Esportius de Girona.</p>
<p><strong>Final:</strong> organitzada per la Uni&oacute; de Consells Esportius de Catalunya.</p>
<h3>Qui pot participar-hi? </h3>
<p>Tots els nens i nenes de la comarca de la Selva que ho desitgin i compleixin els requisits seg&uuml;ents:</p>
<ul>
  <li><strong>Documentaci&oacute;<br />
    </strong>Cada participant ha de tenir la llic&egrave;ncia esportiva vigent per al curs <?php 
	$con=mysqli_query($cnx_cesportiu,"select max(any1) from Zcurs");
	$fila=mysqli_fetch_array($con);
	echo $fila["max(any1)"];
	$con=mysqli_query($cnx_cesportiu,"select max(any2) from Zcurs");
	$fila=mysqli_fetch_array($con);
	echo "/".$fila["max(any2)"];
	?>. <strong>Enguany el cost de la llic&egrave;ncia esportiva són 10 &euro;</strong>.<br />
    <br />
    Cada escola o club ha de   trametre la relaci&oacute; de participants integrants de cada equip en les   diferents modalitats esportives enqu&egrave; competir&agrave; i fer-la arribar al   Consell Esportiu de la Selva.<br />
    <br />
  </li>
  <li><strong>Asseguran&ccedil;a<br />
  </strong>Com a requisit   indispensable per participar als JEEC els esportistes hauran d&rsquo;estar   coberts per la Mutualitat General Esportiva. El cost d&rsquo;aquesta   asseguran&ccedil;a anir&agrave; a c&agrave;rrec del Consell Catal&agrave; de l&rsquo;Esport de la   Generalitat de Catalunya.</li>
</ul>
<h3>Categories i edats </h3>
<table cellspacing="8">
	<tr>
    	<th>Categoria</th>
        <th>Any de naixament</th>
    </tr>
    <?php $con=mysqli_query($cnx_cesportiu,"select * from Zcategories where id_cat!=8 order by inici");
	while($fila=mysqli_fetch_array($con)){?>
		<tr>
            <td><?php echo  $fila["nom_cat"];?></td>
            <td><?php echo  "Entre ".$fila["any_naix_inici"]." i ".$fila["any_naix_fi"]?></td>
        </tr>
	<?php }?>
</table>
<h3>Reglament t&egrave;cnic </h3>
<p>Les competicions es regiran per la   normativa t&egrave;cnica dels JEEC que aprovi el Consell Catal&agrave; de l&rsquo;Esport de   la Generalitat de Catalunya a proposta de la UCEC i per all&ograve; que   s&rsquo;acordi a la reuni&oacute; de delegats pr&egrave;via a l&rsquo;inici de la competici&oacute;. </p>
Cal tenir present que a les   categories prebenjam&iacute;, benjam&iacute; i alev&iacute; s&rsquo;ha de valorar m&eacute;s la vessant   formativa de l&rsquo;esport que no pas la competitiva.<!-- InstanceEndEditable -->

<?php
include "plantilles/sota.php";

?>