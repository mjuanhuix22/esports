<?php include("conexio.php");
include("inc.php");

$titol="Instal·lacions esportives de la comarca";
if(isset($_GET["nom"])){ 
	$n_nom_instalacio=neteja($_GET["nom"]);

	$conA=mysqli_query($cnx_cesportiu,"select id_instalacio,nom_instalacio from instalacions where n_nom_instalacio='".$n_nom_instalacio."'");
	if(mysqli_num_rows($conA)==0){ header("location:index.php");}
	$filaA=mysqli_fetch_array($conA);
	$anterior=$titol;
	$id_instalacio=$filaA["id_instalacio"];
	$titol=StripSlashes($filaA["nom_instalacio"]);
}
if(isset($_GET["id_muni"])){
	$id_muni=neteja($_GET["id_muni"]);
	$conA=mysqli_query($cnx_cesportiu,"select count(*) from Zmunicipis where id_muni='".$id_muni."'");
	$filaA=mysqli_fetch_array($conA);
	if($filaA["count(*)"]==0){ header("location:index.php");}
}
if(isset($_GET["id_tipus"])){
	$id_tipus=neteja($_GET["id_tipus"]);
	$conA=mysqli_query($cnx_cesportiu,"select count(*) from Zinstal_tipus where id_tipus='".$id_tipus."'");
	$filaA=mysqli_fetch_array($conA);
	if($filaA["count(*)"]==0){ header("location:index.php");}
}

include "plantilles/sobre.php";




 if(isset($id_instalacio)){?>
	<?php $con=mysqli_query($cnx_cesportiu,"select * from instalacions where id_instalacio=".$id_instalacio);
    $fila=mysqli_fetch_array($con); ?>
    <table  border="0" align="center" cellpadding="5" cellspacing="2" >
        

        <tr >
          <td valign="top" class="lletraGrisa">Adre&ccedil;a</td>
          <td >
           <?php 
            if($fila["id_adreca"]!=0){
				
				$con1=mysqli_query($cnx_adreces,"select * from adreca,tipus where 
				adreca.id_adreca=".$fila["id_adreca"]);
				$fila1=mysqli_fetch_array($con1);
				echo $fila1["nom_tipus"];
				if($fila1["id_article"]!=0){
						$con2=mysqli_query($cnx_adreces,"select * from articles where id_article=".$fila1["id_article"]);
						$fila2=mysqli_fetch_array($con2);
						echo " ".$fila2["nom_article"];
				}
				
				echo " ".StripSlashes($fila1["nom_adreca"]);
				
            }else{ echo StripSlashes($fila["altre_adreca"]);}
        if($fila["numero"]!=0){echo ", ".$fila["numero"];}?>		 </td>
      </tr>
    
    
      <?php if($fila["telf"]!=0){?>
            
             <tr >
              <td valign="top" class="lletraGrisa">Telèfon</td>
              <td ><?php echo  substr($fila["telf"],0,3)." ".substr($fila["telf"],3,2)." ".substr($fila["telf"],5,2)." ".substr($fila["telf"],7,3);?></td>
      </tr>
        <?php }?>
    
      <?php 
	  if($fila["id_tipus"]==2){?>
      		<tr >
              <td valign="top" class="lletraGrisa">Pistes</td>
              <td>
                <table>
                    <tr>
                    <th>Nom</th>
                    <th>Dimensions</th>
                    <th>Paviment</th>
                    <th>Aforament</th>
                    </tr>
                
                    
			   <?php $con1=mysqli_query($cnx_cesportiu,"SELECT * FROM instalacions_pistes WHERE id_instalacio=".$id_instalacio);
                while($fila1=mysqli_fetch_array($con1)){?>
                     <tr >
                     <td><?php echo  StripSlashes($fila1["nom_pista"]);?></td>
                     <td><?php echo  StripSlashes($fila1["mides"]);?></td>
                     <td><?php 
                     $con2=mysqli_query($cnx_cesportiu,"SELECT nom_paviment FROM Zinstal_paviment 
                     WHERE id_paviment=".$fila1["id_paviment"]);
                    $fila2=mysqli_fetch_array($con2);
                     echo StripSlashes($fila2["nom_paviment"]);?></td>
                     <td><?php echo  StripSlashes($fila1["aforament"]);?> espectadors</td>
                     </tr>
                <?php }?>
                </table>
            </td>
            </tr>
		  
	  <?php }elseif($fila["id_tipus"]==3){?>
      
      		 <tr >
              <td valign="top" class="lletraGrisa">Vasos</td>
              <td>
              		<table cellpadding="1">
                    <tr>
                    <th>Tipus</th>
                    <th>Dimensions</th>
                    <th>Fondària</th>
                    </tr>
				 <?php $con1=mysqli_query($cnx_cesportiu,"SELECT * FROM instalacions_vasos WHERE id_instalacio=".$id_instalacio);
                while($fila1=mysqli_fetch_array($con1)){?>
                    <tr>
                        <td>Piscina <?php if($fila1["cobert"]==1){ echo "coberta";}else{ echo "Descoberta";}?></td>
                        <td><?php echo  $fila1["mides"];?></td>
                        <td><?php echo  $fila1["fondaria"]?></td>
                   </tr>
                <?php }?>
            	</table>
            </td></tr>

	   <?php }else{?>
       		<tr>
            	<td class="lletraGrisa">Paviment</td>
                <td><?php  $con2=mysqli_query($cnx_cesportiu,"SELECT nom_paviment FROM Zinstal_paviment 
				 WHERE id_paviment=".$fila["id_paviment"]);
                $fila2=mysqli_fetch_array($con2);
				 echo StripSlashes($fila2["nom_paviment"]);?></td>
            </tr>
          <?php if($fila["aforament"]!=0){?>
                <tr >
                  <td valign="top" class="lletraGrisa">Aforament</td>
                  <td ><?php echo  $fila["aforament"];?> espectadors</td>
                </tr>
            <?php }?>
            <?php if($fila["mides"]!=""){?>
                
                <tr >
                  <td valign="top" class="lletraGrisa">Mides</td>
                  <td ><?php echo  StripSlashes($fila["mides"]);?></td>
          </tr>
            <?php }?>
      <?php }?>
      
        
    
        
        <?php if($fila["mapa"]!=""){?>
        	<tr>
            	<td valign="top" class="lletraGrisa">Localització</td>
                <td><?php echo  $fila["mapa"];?></td>
            </tr>
        
        <?php }?>
    </table>
    
    <?php $con1=mysqli_query($cnx_cesportiu,"select * from instalacions_fotos where id_instalacio=".$id_instalacio);
	while($fila1=mysqli_fetch_array($con1)){?>
    	<p>
    	<img src="carregues/instalacions/<?php echo  $fila1["nom_foto"];?>" />
        <?php if($fila1["peu"]!=""){ echo "<br>".StripSlashes($fila1["peu"]);}?>
    	</p>
    <?php }?>



<?php }else{?>


<?php /*$con=mysqli_query($cnx_cesportiu,"select * from instalacions where nom_foto!='' order by rand() LIMIT 7");
while($fila=mysqli_fetch_array($con)){?>
<a href="instalacions.php?nom=<?php echo  $fila["n_nom_instalacio"];?>" >
<img src="carregues/instalacions/<?php echo  $fila["nom_foto"]?>" alt="<?php echo  StripSlashes($fila["nom_instalacio"]);?>" height="70" vspace="15" width="95" border="0" /></a>
<?php }*/?>
<p>Entre els 26 municipis de la comarca de la Selva hi ha <strong><?php  
$con=mysqli_query($cnx_cesportiu,"select count(*) from instalacions");
$fila=mysqli_fetch_array($con); echo $fila["count(*)"];?></strong> instal·lacions esportives municipals:

</p>


<table >
	<tr>
    <td valign="top" width="250px">
    <?php $con=mysqli_query($cnx_cesportiu,"select nom_muni,Zmunicipis.id_muni from instalacions, Zmunicipis where
		instalacions.id_muni=Zmunicipis.id_muni group by Zmunicipis.id_muni order by nom_muni");
		while($fila=mysqli_fetch_array($con)){
			echo "<a href=\"instalacions.php?id_muni=".$fila["id_muni"]."#llistat\" class=\"instal_enllac\">".$fila["nom_muni"];
			 $con1=mysqli_query($cnx_cesportiu,"select count(*) from instalacions where id_muni=".$fila["id_muni"]);
			 $fila1=mysqli_fetch_array($con1);
			 echo " (".$fila1["count(*)"].")";
			echo "</a>";
        }?>
    </td>
    
    <td valign="top">
    <?php $con=mysqli_query($cnx_cesportiu,"select * from Zinstal_tipus order by nom_tipus");
		while($fila=mysqli_fetch_array($con)){
			 echo "<a href=\"instalacions.php?id_tipus=".$fila["id_tipus"]."#llistat\" class=\"instal_enllac\">".ucfirst(StripSlashes($fila["nom_tipus_plu"]));
			 $con1=mysqli_query($cnx_cesportiu,"select count(*) from instalacions where id_tipus=".$fila["id_tipus"]);
			$fila1=mysqli_fetch_array($con1);
			 echo " (".$fila1["count(*)"].")";
			 
			 echo "</a>";
    }?>
    <img src="imatges/mapa_selva_instalacions.jpg" alt="Instal&middot;lacions a la comarca" /></td>
    </tr>
</table>


<div class="neteja"></div>

<?php if((isset($id_muni))||(isset($id_tipus)) ){?>

	<a name="llistat"></a>
    <h2>
    <?php if(isset($id_muni)){
		$con=mysqli_query($cnx_cesportiu,"select nom_muni from Zmunicipis where id_muni=".$id_muni);
		$fila=mysqli_fetch_array($con);
		echo $fila["nom_muni"];
	}
	if(isset($id_tipus)){
		$con=mysqli_query($cnx_cesportiu,"select nom_tipus_plu from Zinstal_tipus  where id_tipus=".$id_tipus);
		$fila=mysqli_fetch_array($con);
		echo ucfirst(StripSlashes($fila["nom_tipus_plu"]));
	}
	?>
    </h2>
	<?php 
	$SQL=" select id_instalacio,nom_instalacio,n_nom_instalacio from instalacions where id_instalacio!=0  ";
	if(isset($id_muni)){ $SQL.=" and id_muni=".$id_muni;}
	if(isset($id_tipus)){ $SQL.=" and id_tipus=".$id_tipus;}
	$con=mysqli_query($cnx_cesportiu,$SQL);
	while($fila=mysqli_fetch_array($con)){?>

        <a href="instalacions.php?nom=<?php echo  $fila["n_nom_instalacio"];?>" class="instala"><?php  echo StripSlashes($fila["nom_instalacio"]);?>
         <?php $con1=mysqli_query($cnx_cesportiu,"select nom_foto from instalacions_fotos where id_instalacio=".$fila["id_instalacio"]);
			$fila1=mysqli_fetch_array($con1);
            $ruta="carregues/instalacions/".$fila1["nom_foto"];	
            echo "<img src=\"".$ruta."\" width=\"187\" height=\"120\" border=\"0\" vspace=\"5\">";?>
        </a>
	<?php 
		
	}?>
<?php }
?>

<?php }


 include "plantilles/sota.php";

?>
