<?php

include("/home/selvaesports/www/conexio.php");

include("/home/selvaesports/www/inc.php");


/*
//Les preinscripcions no pagades s'envia mail
$SQL="select i3_reserva.id_reserva,i1_inscripcions.id_ins from i1_inscripcions,i3_reserva where i1_inscripcions.obert=1 and
  i1_inscripcions.id_ins=i3_reserva.id_ins and i3_reserva.estat=0;";
$con=mysqli_query($cnx_cesportiu,$SQL);
while($fila=mysqli_fetch_array($con)){
    $id_reserva=$fila["id_reserva"];
    $id_ins=$fila["id_ins"];

    include("/home/selvaesports/www/comuns/inscripcions/pas7_mail.php");
}
*/



//Tancar les inscripcions que estan obertes però la jornada ha finalitzat
mysqli_query($cnx_cesportiu,"update i1_inscripcions set obert=0 where obert=1 and 
(
(id_jornada!=0 and id_jornada IN (select id_jornada from a2_jornades where data<".date('Ymd').") )
or
(id_jornada=0 and id_act IN (select id_act from a1_activitats where data<".date('Ymd')." and data!=0))
)
");


//Totes les activitats que se celebren anualment recordar a les entitats que la publiquin
//Restem 11 mesos 
$fecha = date('Ymd');
$nuevafecha = strtotime ( '-11 month' , strtotime ( $fecha ) ) ;
$data = date ( 'Ymd' , $nuevafecha );

$con=mysqli_query($cnx_cesportiu,"select * from a1_activitats,entidades where 
a1_activitats.id_entidad=entidades.id_entidad and 
anual=1 and entidades.id_entidad!=1 and data='".$data."';");
while($fila=mysqli_fetch_array($con)){
	$con1=mysqli_query($cnx_cesportiu,"select count(*) from a1_activitats where id_entidad=".$fila["id_entidad"]." and 
	id_deporte=".$fila["id_deporte"]." and 
	data>'".date('Ymd')."';");
	$fila1=mysqli_fetch_array($con1);
	if($fila1["count(*)"]==0){
		$to=$fila["mail_contacte"];
		$assumpte="Recordatori per publicar activitats al web del Consell Esportiu de la Selva";
		$text="A l'atenció de ".$fila["nom_contacte"]." de ".$fila["nom_entidad"].",<br />
		En data ".strtolower(nom_data($fila["data"]))." vareu publicar l'activitat anomenada ".AddSlashes($fila["nom_act"])." especificant que es celebra anualment.
		Des del 
		Podeu entrar a la intranet ...";
		include("enviar_mail.php");
		
	}
}

//Els partits que es varen celebrar ahir i no s'ha entrat puntuacio
//Restem 1 dia  
$fecha = date('Ymd');
$nuevafecha = strtotime ( '-1 day' , strtotime ( $fecha ) ) ;
$data = date ( 'Ymd' , $nuevafecha );

$con=mysqli_query($cnx_cesportiu,"select entidades.email,entidades.nom_entidad,ass_3equips.lletra, ass_5partits.data_real  
from ass_5partits,ass_3equips,ass_3comu,entidades where 
data_real=".$data." and 
p_equip1 is NULL and 
ass_5partits.id_equip1=ass_3equips.id_equip and 
ass_3equips.id_comu=ass_3comu.id_comu and 
ass_3comu.id_entidad=entidades.id_entidad;");
while($fila=mysqli_fetch_array($con)){
	$to=$fila["email"];
	$assumpte="Actualitzar resultats";
	$text.="Cal que entreu a la intranet del Consell per actualitzar els resutats del dia  ".nom_data($fila["data_real"]).$fila["nom_entidad"]." ".nom_lletra($fila["lletra"]);
	include("enviar_mail.php");
}



if(date('dm')=='0109'){

	$con=mysqli_query($cnx_cesportiu,"select max(id_curs) from Zcurs");
	$fila=mysqli_fetch_array($con);
	$id_curs=$fila["max(id_curs)"];
	
	
	//Es crea un nou curs
	$con=mysqli_query($cnx_cesportiu,"insert into Zcurs set 
	id_curs=".($id_curs+1).",
	any1=".date('Y').",
	any2=".(date('Y')+1));


	//Modificar les categories
	$con=mysqli_query($cnx_cesportiu,"select * from Zcategories");
	while($fila=mysqli_fetch_array($con)){	
		mysqli_query($cnx_cesportiu,"update Zcategories set 
		any_naix_inici=".($fila["any_naix_inici"]+1).",
		any_naix_fi=".($fila["any_naix_fi"]+1)." 
		where id_cat=".$fila["id_cat"]);
	}

	
	//Totes les llicències que no han estat eliminades s'eliminen
	$con=mysqli_query($cnx_cesportiu,"select * from llice_sol where id_curs=".$id_curs." and data_envio is null");
	while($fila=mysqli_fetch_array($con)){	
		mysqli_query($cnx_cesportiu,"delete from llice_sol_esportista where id_sol=".$fila["id_sol"]);
	}
	mysqli_query($cnx_cesportiu,"delete from llice_sol where id_curs=".$id_curs." and data_envio is null");


    $to="info@selvaesports.cat";
    $assumpte="Nou curs";
    $text.="S'ha actualitzat el curs a www.selvaesports.cat";
    include("enviar_mail.php");

}

?>